**ALL ARE IN GITLAB CHANGELOG!**



# Version History

**X** = in GitLab board



## v 0.7.0 (XXX 2019)

### TODOs

- -



### New Features

- **QualityControl:AmpQcFlags:** added option to directly apply combined quality flags (Amp QC Flags are an aggregated from multiple single flags added together). For flags to show up here, they need to be created via the Pipeline first. Implemented flags atm: scf, EddyPro SSITC and absolute limits **X**
- **Pipelines:QualityControl**: implemented first, basic version; option to generate separate column for overall flag, needs good concept, add _rejectval cols like for EP default flag; it would make sense to store settings in a separate file that can be shared with others **X**



### Improvements & Changes

- **Pipelines:QC:** added more flags to Pipeline **X**
- QDateEdit: now allows all dates, i.e. also dates that are not found in the data, necessary so that editing works as expected **X**
- **QualityControl:DefaultFlagEddyPro** and **Pipeline**: adjusted code, the center piece is now a @staticmethod that can be accessed directly. Necessary in preparation of the QC pipeline (QC default settings) that will combine multiple methods from multiple classes to one streamlined QC procedure, i.e. the methods are combined so default QC settings can be used with the click of a button. **X**



### Bugs & Issues

- -



## v 0.6.0 (13 Oct 2019)



### New Features

- **Pipelines:** added new category, used to automate and chain-process e.g. combinations of quality flags & checks etc… 
- **Modifications:LimitTimeRange:** limit all variables to selected time period, i.e. constrain time period
- **Modifications:CreateEvent:** Events can now be created within the GUI, and their range can be defined
- INACTIVE ~~**Export:ProfileReport:** added option to output stats to a HTML file using pandas_profiling~~



### Improvements & Changes

- Moved call to markers in respective separate methods in ``main.py``
- Global variables (variable identifiers) are now imported with import * (and without the need of a sub-function).
- added first version of Amp icon
- **Analyses:BeforeAfterEvent:** before / after time periods are now shown as broken horizontal bar plot, which is just a fancy word for colored background
- **Code:** adjusted code structure to make it compatible w/ fbs for creating standalone executable files for app sharing.
- **Code:** added local version control (git)
- **Code:** created new .yml and requirements files for (updated) environment AMP06
- **Code:** changed how data_df is created, now separated into multiple methods
- **Code/GUI:** all icons are now imported in code (.setIcon), i.e. no longer via CSS file, required for ``fbs`` GUI programming to show button icons
- **Code/GUI:** improved Light Theme
- **Plots/GUI:** implementation of central color management to change plot appearance
- **Plots:Overview:** improved the way dates are shown on the x-axis
- **stats:LinearRegression**: changed dependency from to ``import statsmodels.api as sm`` to ``import statsmodels.formula.api as smf``. This is compatible to creating fbs for creating a standalone executable file, e.g. exe on Windows.
- **GapFilling:LinearRegressionSimple:** improved shown results, there is now also a short logging text output to logbox
- importing Events from csv file is deactivated for now
- output folder is now default one folder up from the file (main.py, .exe, …) the app was started from
- INACTIVE: ~~new conda environment AMP06, contains pandas_profiling~~



### Bugs & Issues

- FIXED Events files caused crash on import due to freq

- FIXED **Analyses:BeforeAfterEvent:** problems with text that disappears when plotting

- FIXED **Analyses:BeforeAfterEvent:**  crashes when there is no before or after time period

- FIXED One folder up using ``.parents[1]`` doesnt work in standalone exe. For now I simplified the output folder part: ny default, the folder ``OUTPUT`` is created in the same folder as the folder from which the script was  started. It seems that this way also avoids permission errors when trying to create the subfolder for a a specific run output.

  does not work: ``dir_out_main = dir_start.parents[1] / 'OUTPUT' ``
  does work: ``dir_out_main = dir_start.parent / 'OUTPUT' ``



## v 0.5.1-alpha (26 Sep 2019)

### New Features

- -



### Changes

- Implemented Light Theme as default
- Renamed class ``FluxBay`` to ``Amp``
- Improved code clarity when adding ModBoxes to GUI



### Bugs & Issues

- High-resolution files can now correctly be imported, but are downsampled to 1S if necessary







## v 0.5.0-indev (19 Sep 2019)

### New Features

- **Analyses:Aggregator:** first version that can count occurrences and display additional vars next to it
- **Analyses:Aggregator:** added overall mean as reference period w/ deviations from reference plotted w/ bar plots
- **Analyses:ClassFinder:** now shows number of marked values
- **Analyses:NetGHGBudget:** first version for full CO2-eq budget co2 + n2o + ch4
- **Export**:**SaveFigure**: added option for basic export of overview plots on transparent background
- **Export:VariablesToFiles:** added option to export per year / month
- **Export:VariablesToFiles:** added option for resampling of output file(s)
- **FileTypes**: added template for TOA5 10min time resolution files
- **FileTypes**: added template for custom meteo 10min time resolution files
- **FileTypes:** added support for Amp CSV, e.g. ETH Level-2 output files (timestamp w/o seconds), basically a single uncompressed Amp CSV data file
- **I/O:** Data can now be loaded from multiple files, i.e. multiple files can now be selected in the file dialog. File data are automatically merged.
- **I/O:** Completely changed the way file formats and timestamp formats are loaded. All formats are now in their own respective class.
- **I/O:** implement 13_meteo_nabel (CH-DAV) file format as NABEL_1T
- **I/O:** when reading in files, it now checks for file type instead of timestamp type and reads the file accordingly to the file type
- **Logger:** added logging functionality, log is shown in GUI textbox
- **Modifications:CreateNewColumn:** added option to apply gain to focus column and insert as new column, in addition the units of the new column can be defined
- **Modifications:RemoveVar:** added option to remove selected var
- **Plots:Scatter:** now showing equally-sized bins, also implemented simple linear fit using the bins
- **Plot:Scatter:** implemented fit per dataset, year and month
- **Plot:Hexbins:** added new plot type



### Changes

- Folder *example_data* is now outside scripts folder and its path is now an instance variable
- Target frequency is now set whenever intial data are loaded, based on ‘freq’ in the file settings. Initial data are either data that are loaded automatically when the app starts, or data loaded by the user. If data from another file are later *added* to this initial data, these incoming data are resampled to the target frequency. (kudos SG)
- Renamed ModBox Resampling to Conversions
- Custom variables are now marked with a ```.``` at the beginning instead of the underscore
- Removed focus_df info section that was at the windows bottom
- **Export:VariablesToFiles:** Added suffix for aggregation and freq to file names, also added prefix for Year or Month
- **Export:VariablesToFiles:** export to files now uses shifted timestamps for year / month files, necessary because the file timestamp gives the *start* of the averaging period
- Instead of ``` requirements.txt```, the environment is now saved in the conda-specific ``environment.yml`` file.
- Changed name of ModBox ``Conversions`` to ``Modifications``
- Focus df is now automatically set after data loading by automatically selecting first var in loaded variable list on init
- **Plot:Scatter:** added zero line, added white border around bin points
- implemented fallback option in case the seconds are missing in the timestamp of AmpCSV_30T files
- Options that need the creation of a new tab are now called via their own method ``make_tab``
- Re-structured loading of example files w/ own method
- Re-structured GUI.py for clarity



### Bugs & Issues

- ~~for pandas .read_csv, check for the NotImplementedError when reading in EddyPro data files in which the ‘date’ and ‘time’ columns contain the string ‘not enough data’, I think can be solved w/ try / except date parser function~~ Error occurred b/c there were more data columns than header column names in the EddyPro 7.0.4 full_output file.
- Fixed an error during file merging: merging of EP full_output files generated an error b/c the time resolution of the files was not contained when trying to add data from another file. (kudos SG)
- **Plots:Scatter:** removed bug in scatter plot, occurred when after binning the number of values for x and y data was not equal, therefore raising an exception and showing an empty or not updated plot, .dropna() on the df solved the issue
- Fixed an error that caused a crash whenever the incoming focus_col was already in the df. This can happen when e.g. col1 is modified and added as .col1 to the df, but .col1 was already in the df e.g. from a previous analysis. Now .col1 is automatically renamed to ..col1 (two points)
- **Export:VariablesToFiles:** Fixed crash when trying to export in original time resolution
- BUG: removed error when merging DAV files, occurred during merging of files where the timestamp columns had the same format but differently given units in the unit section, row 2
- BUG: fixed wrong delimiter when loading .amp files
- BUG: fixed when adding n2o_flux filled data col name is .gf_filledmarker







## v 0.4.0-indev (8 August 2019)

### Changes

- **Analyses**:**BeforeAfterEvents**: basic implementation of before / after stats
- **Analyses**:**GapFinder**: implement first version
- **Analyses**:**RunningLinReg**: moving regression in time windows
- **Analyses**:**uStarDetection**: generate ustar flag that can be used for variables df
- **Analyses**:**uStarDetection**: implemented Papale et al. (2006)
- **Corrections**:**StorageCorrection**: implemented storage correction
- **Controls**: add remove/keep marker points instead of the Apply button
- **Controls**: add Keep Marked to control buttons
- **Code Structure**: restructure ModBoxes
- **Export**: export modified df to file
- **Export**: generate to save to file format (*.amp) that can be directly used in the app 
- **Export**:**ToSeparateFiles**: data can now be exported to separate files for each month and year
- **FileTypes**: import events
- **FileTypes**: rename management to events and add .events extension
- **FileTypes**: read FLUXNET files
- **FileTypes**: add support for TOA5 filetypes, template added for 1min time resolution files
- **FileTypes**: resample on import to target frequency (the one from the main file, e.g. 30mins for EC data) when adding file
- **GapFilling**: add more options for rolling gf, e.g. mean
- **GapFilling**:**MDS**: implemented Reichstein et al (2005)
- **GapFilling**:**Linear Interpolation:** reworked logic to fill gaps of varying lengths
- **General:** created requirements.txt file for creation of conda environment to run on other computers
- **GUI**: implement search field to filter shown variables in variable list
- **GUI**: implement tabs
- **GUI**: make tabs closable and better hover effects
- **GUI**: add category button icons
- **GUI**: add icons for add / remove data buttons
- **OutlierDetection**:**Double-Differenced Time Series**: Papale et al. (2006)
- **Plots**: add plot navigation controls in new tab of scatter plot
- **Plots**: remove overview plotting option
- **Plots:Scatter**: in scatter plot z as color
- **Plots:Scatter**: in scatter normalize with percentiles
- **Plots:Scatter**: move scatter plot to tabs w/ controls
- **Plots:Scatter**: format empty scatter plot in new tab
- **Plots:Heatmap**: move heatmap percentile plots to tabs
- **Plots**:**Overview**: daily, weekly and monthly averages now use shifted timestamp index for correct assignment of data to days, weeks and months
- **Plots**:**Threshold Plots**: FLUXNET plots of different NEE etc. versions
- **Plots**:**Threshold Plots**: move threshold plot to tabs w/ controls
- **Plots**:**Threshold Plots**: threshold plot years all in one plot, cumulative data starting per year
- **QualityControl**: get eddypro flags to run
- **QualityControl**: integration (z-d)/L w/ manual limits (-2, 2), see ICOS pubs


### Bugs & Issues

- **General**:*CHECK*: are all dropdown lists updated after adding custom variable
- **Plots**:*ISSUE*: overview plots are sometimes deleted when clicking scatter
- **Plots**:**Scatter**:*BUG*: white background in scatter plot
- **Plots**:**Scatter**:*BUG* display z as colors et al not working
- **Plots**:**Scatter**:*BUG* update dropdowns in scatter tab after new variable was added, checked but seems to work
- **inout**:**DataFunctions**: check merging b/w high-res meteo and 30-min EC data, worked



