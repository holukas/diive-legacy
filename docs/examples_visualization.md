# Visualization Examples

* choose SWC and the use ClassFinder to mark where precipitation is > 0.01, check if they agree
* make gaps in EC air temperature and then fill gaps w/ linear regression using sonic temperature
* in scatter plot, show x=DOY and y=flux and z=wind direction to show where wind comes from 
* in scatter plot, show co2_flux vs footprint in bins, shows uptake higher from specific distances
* in scatter plot, co2_flux vs DOY in bin fits, nicely shows how the co2 flux develops over year, can be done in multi-year datasets, overlay 3rd scalar as colors
* LE and H w/ bowen ratio as z

![image](image.png)

