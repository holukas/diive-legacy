# PLANNED FEATURES

**X** = in GitLab board

### 0.7.0

1. pipeline method for statrawdataflag **X**
2. fully implement material design colors in module **X**
3. make color schemes e.g. 24color list based on material design color methods **X**
4. put Amp logo on exported plots **X**
5. make settings for file formats in txt files **X**
6. **GapFilling: MDS**: general check and refinements **X**
7. **Gapfilling:MDS:** test and check and refine MDS w/ OE2 2019 data for Quirina **X**
8. **GapFilling**:**MDS**: make sure that only the time period where fluxes are available are gapfilled, not e.g. all 20 years for which air temperature is available **X**
9. **GapFilling**:**MDS**: uncertainty estimation, also for other gapfilling methods **X**



### 0.8.0

1. **Analyses: uStarDetection**: general check and refinements **X**
2. **Analyses: uStarDetection**: implement u* filtering Barr et al. 2013 **X**
3. **Analyses: uStarDetection**: sensitivity test? **X**
4. **Analyses**:**uStarDetection**: add u* ignore winter / specific season option **X**



### 0.9.0

1. **HiRes Tests:** plotting of 20Hz data, with auto-resampling for faster plotting? show that resampled in plot w/ text note **X**
2. **HiRes Tests:** flux detection limit from 20Hz data **X**



### 0.10.0

1. pytest unit testing **X**



### Then

1. add site IDs or similar to stats and output files etc **X**
2. **I/O:** solve full timestamp issue with missing or available seconds, auto-check which timestamp format works, starting with the most likely format **X**
3. by default, all vars should be listed in all dropdowns, and the best guess based on global vars should be selected by default, or ix 0 if no global var found **X**
4. generate “infocards” e.g. during Pipelines **X**
5. BUG rejectval_col.sum is off by 2 in EP statistical raw data flags info text in plot **X**
6. Amp QC flags analysis board / tab **X**
7. install miniconda on Windows Subsystem for Linux to generate Linux executables **X**
8. HiRes Tests: stationarity test after Mahrt 1998 **X**
9. Analyses:NetGHGBudget: refine calculations
10. Analyses:NetGHGBudget: better unit conversion, detect from units; GWP60 **X**
11. Analyses:NetGHGBudget: include albedo effect **X**
12. automatic GHG budgets CO2 + N2O + CH4 **X**
13. **Analyses:Aggregator:** expand functionality; analyses for extreme events e.g. high temperatures and check potential impacts, e.g. w/ significances, use results for auto-events generation? compare e.g. same month of other years to current year etc
14. **Analyses:Aggregator:** make it like NBA season finder with more filter options
15. plot multiple variables (below?) each other, maybe in some sort of plot generator (drag&drop? multi-select from list?)
16. add some sort of reference system, add references for methods eg hexbin plot
17. multi-threading
18. autocorrelation: https://www.influxdata.com/blog/autocorrelation-in-time-series-data/
19. **Code/CSS:** check, improve and expand centralized color management
20. **Plot:Scatter:** option to show years etc as X Y or Z
21. **I/O:** when reading files, not all parameters can be set in the GUI e.g. ‘zipped’
22. **I/O**: when importing files, custom setting changes via GUI are currently not working I think
23. data coverage plot for all vars overview
24. **Plots:Heatmap:** strictly show blue colors for uptake, red for emission
25. combine two columny by e.g. multiplication, mean, …
26. general option to analyze and show same month but for different years e.g. in hourly averages plots
27. **Plot:Scatter:** needs readable written-out function based on coefficients and intercept, see: see: https://stackoverflow.com/questions/26951880/scikit-learn-linear-regression-how-to-get-coefficients-respective-features
28. z needs legend in scatter
29. **OutlierDetection**:**Double-Differenced Time Series**: *todo* add missing values at start and end of day blocks for calc of di
30. boxenplot: https://seaborn.pydata.org/examples/large_distributions.html
31. **I/O:** find good solution for importing different timestamp formats
32. automatically create PowerPoint presentation, with selection of what to show on slides
33. when importing data from file, renaming columns possible in GUI, and if name already exists then ask if it should be merged with existing
34. when importing data and data column already exists, define both should be merged or one prioritized over the other
35. output some kind of stats overview page for all vars
36. CHECK: whether other methods also need shifted timestamp column, e.g. I think in u* filtering, or resampling to e.g. daily average (for ustar checked in 0.9.0)
37. **Modifications:** convert *complete* df to e.g. daily average, or also something like running median lines etc…
38. hexbins per year etc option
39. Waffle chart: https://github.com/gyli/PyWaffle
40. CHANGE: **Plot:Scatter:** fit per still needs shifted timestamps, at the moment there is still one HH too much at the beginning of the e.g. month fit, and one HH missing at the end of the month fit (due to the timestamp giving the END of the averaging period)
41. **inout**:**DataFunctions**: refine the small function that shifts the timestamp by 1 unit of freq, needs more general implementation for using the detected freq of the df
42. BUG: not showing gapfilled preview in second running median
43. undo button
44. save as button
45. **Code/CSS:** dark theme
46. make guided data products, comparable to tech tree in civ
47. smaller df sizes, maybe useful: https://old.reddit.com/r/Python/comments/dkbtk9/pandas_datadrame_with_6gb_after_pickleing_and/



### And then

1. https://towardsdatascience.com/a-brief-introduction-to-change-point-detection-using-python-d9bcb5299aa7
2. **Export**:**SaveFigure**: more and better export options
3. BUG: LAE FXN file crashed when trying to produce threshold plots
4. refine resampling option so 1min data can be loaded and then exported as 30min
5. add time series that has different time resolution than focus and is not resampled after import
6. Burba correction
7. save as, some sort for button to define export folder
8. **Analyses:GapFinder:** implement better way to handle df w/ zero gaps, currently simple try / except
9. method to detect consecutive time periods with certain criteria, similar to Gapfinder (MG)
10. adding events should be possible from GUI
11. **Export:VariablesToFiles:** more options for timestamp export
12. **GapFilling: MDS**: using LUT based on data from multiple years
13. ~~**column builder**: add custom column with custom name and units and conversion~~
14. implement footprint calculation code Kljun 2015
15. footprint during different time periods, seasons, b/w management etc.
16. assemble data report (jpg, pdf), similar to the FCT plot output for each variable
17. Plots:Violin Plot: Day/Night
18. table generator, output stats etc.
19. remember last visited directory in load file dialog
20. downsample / upsample option (linear interpolation, forward fill, etc …) for data, also with separate info entry in settings_dict
21. make better implementation of classes so they are called instantiated as objects and then work is done on these objects
22. **GapFilling**: interpolation but based on the heatmap, e.g. w/ values next to each other but also one day before and after (like a cross +, whereby the middle of the cross would be the missing value that is interpolated, or something like that)
23. Plots:Overview: add boxplots instead of some e.g. yearly average
24. GUI: re-arrange buttons that create a new tab?
25. drag&drop from variable list
26. correlation matrix e.g. https://seaborn.pydata.org/examples/many_pairwise_correlations.html **X**
27. test altair for plotting in pyqt5 webview
28. Day Explorer: ähnlich wie in der Microsoft Wetter App, klick auf einen bestimmten Tag und dann zeigts an was an dem Tag Besonderes war mit Stats
29. difference plots, e.g. fingerprint minus fingerprint
30. pie chart: e.g. co2 flux per wind direction
31. seasonal contributions of e.g. precipitation, co2 flux …
32. **Export**:**ToSeparateFiles**: more export options would be nice, e.g. by seasons
33. hide options / refinements when not applicable to current file type
34. https://towardsdatascience.com/an-overview-of-time-series-forecasting-models-a2fa7a358fcb
35. buttons for search filter by group
36. link plots: when zooming one, zoom also the others
37. include albedo effect when calculating climate impact
38. CHANGE: threshold plot label colors in e.g. first plot not correct
39. hide close button on first tab
40. include some bootstrapping stats
41. add event / change point detection for time series in general
42. hover over plot info
43. loading bar
44. add plotting of ICOS storage files
45. add event durations
46. auto-detect events, e.g. rain
47. history: https://realpython.com/how-to-implement-python-stack/#what-is-a-stack
48. give custom name to custom variables
49. option to delete variable(s) from list
50. option to assemble custom variable list w/ add and delete variables, create custom variables w/ formulas
51. include BigLeag, R lib, J Knauer
52. add custom calculations, e.g. multiply with given value, maybe using statsmodels formulas
53. cumulative plots per year/month etc
54. make plot background transparent
55. heightmap in Python, texture in Python, then heightmap with texture in Blender, then export as STL file for use in PowerPoint
56. calculate wet bulb temperature
57. with radiation/albedo, calculate potential snow cover
58. detect spikes in SWC to cross-check logged precipitation
59. detect peaks in variables to check for potential timestamp shifts
60. daily / weekly / monthly average plots and all per year for long files, e.g. FLUXNET
61. gapfilling generally separately for day and night
62. define variable names for FLUXNET files so the filtering dropdowns (like QC etc) can be used
63. assemble overall qc flag
64. **Logger:** implement logging file
65. push button and apply all recommended qc checks
66. Scenario editor
67. Select time range that is shown in plots possible maybe w/ calendar menu
68. Gapfilling w/ other scalar correlation and separate analysis plots x vs y and preview of filled gaps
69. select data points with mouse in plot
70. unit conversions to e.g. kg C m-2 ha-1
71. partitioning
72. calc carbon uptake period (CUP)
73. when opening a file, make option to select subfolder and open all and merge all files in (sub)folder(s)
75. when merging, add file id to variable names, b/c e.g. when merging IRGA72 and QCL results there will be duplicate column names
76. logos for file type selection
77. tachometer wie beim auto das die flussaufnahme zeigt im verlauf eines jahres
78. Reichstein, M., Camps-Valls, G., Stevens, B., Jung, M., Denzler, J., Carvalhais, N., Prabhat, Feb. 2019. Deep learning and process understanding for data-driven Earth system science. Nature 566 (7743), 195. URL [https://www.nature.com/articles/s41](http://www.nature.com/articles/s41586-019-0912-1)586-019-[0912-1](http://www.nature.com/articles/s41586-019-0912-1)
79. Recently, the application of statistical models that translate the response of CO2 fluxes to past climate and vegetation fluctuations (i.e. recurrent neural network (RNN)) has shown great potential for understanding the relevance of learning vegetation and climate’s temporal dynamics in explaining the CO2 flux spatiotemporal variabilities (Besnard et al., 2019; Reichstein et al., 2018)
80. include Phenopix, R lib, ARPA
81. Reichstein, M., Besnard, S., Carvalhais, N., Gans, F., Jung, M., Kraft, B., Mahecha, M., Jul. Modelling Landsurface Time-Series with Recurrent Neural Nets. In: IGARSS 2018 - 2018 IEEE International Geoscience and Remote Sensing Symposium. pp. 7640–7643.
82. Valentini paper w/ multiple imputation(?)
83. some output to HTML, using e.g. Flask or even Django
84. better color definition, more coherent using material design colors
85. ET partitioning: ZHOU et al 2016, PEREZ-PRIEGO et al 2018, NELSON et al 2018
86. ET partitioning: https://github.com/jnelson18/TranspirationEstimationAlgorithm