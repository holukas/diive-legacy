"""
OUTLIER DETECTION: ABSOLUTE LIMITS
==================================

# last update in: v0.22.0

This package is part of DIIVE:
https://gitlab.ethz.ch/holukas/diive

"""

import numpy as np
import pandas as pd

from pkgs.dfun.frames import generate_flag, init_class_df

pd.set_option('display.max_rows', 50)
pd.set_option('display.max_columns', 12)
pd.set_option('display.width', 1000)


class AbsoluteLimits:
    class_tag = "#odAL"

    def __init__(
            self,
            df: pd.DataFrame,
            target_col: tuple,
            upperlim: float or int = None,
            lowerlim: float or int = None
    ):
        # Settings
        self.df = df
        self.target_col = target_col
        self.upperlim = upperlim
        self.lowerlim = lowerlim

        # New cols for calculations
        target_varname = f"{self.target_col[0]}"
        target_units = f"{self.target_col[1]}"
        self.upperlim_col = (f".upper_limit", target_units)
        self.lowerlim_col = (f".lower_limit", target_units)
        self.classvars = [self.upperlim_col, self.lowerlim_col]

        # Prepare
        self.class_df = init_class_df(df=self.df,
                                      subset_cols=[self.target_col],
                                      newcols=self.classvars)
        self._insert_limits()

        # Calcs
        # not needed

        # Flag
        self.class_df, \
        self.flag_col, \
        self.target_nooutliers_col, \
        self.target_outliervals_col = generate_flag(df=self.class_df,
                                                    target_col=self.target_col,
                                                    tag=self.class_tag,
                                                    upperlim_col=self.upperlim_col,
                                                    lowerlim_col=self.lowerlim_col)

    def get_results(self):
        return self.class_df

    def _insert_limits(self):
        self.class_df[self.upperlim_col] = self.upperlim
        self.class_df[self.lowerlim_col] = self.lowerlim


if __name__ == '__main__':
    # For testing purposes
    _random_data = np.random.randint(-10, 10, size=100)
    _random_name = ('_randomname', '[_units]')
    _random_series = pd.Series(name=_random_name, data=_random_data)
    _random_df = pd.DataFrame(_random_series)
    abslim = AbsoluteLimits(df=_random_df,
                            target_col=_random_name,
                            upperlim=8,
                            lowerlim=-4)
    _df = abslim.get_results()
    print(_df)
