"""
OUTLIER DETECTION: ABSOLUTE LIMITS
==================================

    View class for DIIVE GUI integration.

    Code follows the Model-view-controller (MVC) software design pattern.
    dcontroller.py (controller) defines interactions between
    abslim.py (model) and dvgui.py (view).

"""

from PyQt5 import QtGui as qtg

import gui.add_to_layout as add_to_layout
import gui.combine as combine
import gui.make as make
from gui.tabs import TemplateSVP


class BuildTabAbsLim(TemplateSVP):
    """Build tab based on template and add GUI fields"""

    # Controls
    lne_upperlim = None
    lne_lower_lim = None
    btn_calc = None
    btn_keep_marked = None
    btn_remove_marked = None
    btn_add_as_new_var = None

    def __init__(self, app_obj, tab_title, tab_id):
        super().__init__(app_obj, tab_title, tab_id)

        # Icon
        self.TabWidget.setTabIcon(self.tab_ix, qtg.QIcon(app_obj.ctx.tab_icon_outlier_detection))

        # Fields
        self.lne_upperlim = None
        self.lne_lower_lim = None

        self.btn_calc = None
        self.btn_keep_marked = None
        self.btn_remove_marked = None
        self.btn_add_as_new_var = None

        # Tab columns
        self._add_settings_fields()
        self._add_varlist_fields()
        self._add_plotarea_fields()

    def _add_settings_fields(self):
        # Header
        _header = combine.frame_header_subheader(header_txt="Absolute Limits",
                                                 subheader_txt="Outlier Detection")
        add_to_layout.add_to_grid(layout=self.sett_layout, widget=_header,
                                  row=0, col=0, rowspan=1, colspan=1)

        _spaceritem = make.qspaceritem()
        add_to_layout.add_to_grid(layout=self.sett_layout, widget=_spaceritem,
                                  row=1, col=0, rowspan=1, colspan=1)

        # Section: Upper and lower limits
        _section_header = combine.header(header_txt="Set Limits")
        add_to_layout.add_to_grid(layout=self.sett_layout, widget=_section_header,
                                  row=2, col=0, rowspan=1, colspan=1)

        _label, self.lne_upperlim = combine.label_lineedit_horiz(txt='Upper Limit', css_ids=['', 'cyan'],
                                                                 validator='float')
        add_to_layout.add_to_grid(layout=self.sett_layout, widget=_label,
                                  row=3, col=0, rowspan=1, colspan=1)
        add_to_layout.add_to_grid(layout=self.sett_layout, widget=self.lne_upperlim,
                                  row=3, col=1, rowspan=1, colspan=1)

        _label, self.lne_lower_lim = combine.label_lineedit_horiz(txt='Lower Limit', css_ids=['', 'cyan'],
                                                                  validator='float')
        add_to_layout.add_to_grid(layout=self.sett_layout, widget=_label,
                                  row=4, col=0, rowspan=1, colspan=1)
        add_to_layout.add_to_grid(layout=self.sett_layout, widget=self.lne_lower_lim,
                                  row=4, col=1, rowspan=1, colspan=1)

        _spaceritem = make.qspaceritem()
        add_to_layout.add_to_grid(layout=self.sett_layout, widget=_spaceritem,
                                  row=5, col=0, rowspan=1, colspan=1)

        # Buttons
        self.btn_calc = make.qbutton(txt='Calculate Outliers', css_id=None, tooltip=None)
        add_to_layout.add_to_grid(layout=self.sett_layout, widget=self.btn_calc,
                                  row=6, col=0, rowspan=1, colspan=2)

        self.btn_keep_marked = make.qbutton(txt='Keep Marked Values', css_id='btn_keep_marked', tooltip=None)
        add_to_layout.add_to_grid(layout=self.sett_layout, widget=self.btn_keep_marked,
                                  row=7, col=0, rowspan=1, colspan=2)

        self.btn_remove_marked = make.qbutton(txt='Remove Marked Values', css_id='btn_remove_marked', tooltip=None)
        add_to_layout.add_to_grid(layout=self.sett_layout, widget=self.btn_remove_marked,
                                  row=8, col=0, rowspan=1, colspan=2)

        _spaceritem = make.qspaceritem()
        add_to_layout.add_to_grid(layout=self.sett_layout, widget=_spaceritem,
                                  row=9, col=0, rowspan=1, colspan=1)

        self.btn_add_as_new_var = make.qbutton(txt='+ Add As New Var', css_id='btn_add_as_new_var', tooltip=None)
        add_to_layout.add_to_grid(layout=self.sett_layout, widget=self.btn_add_as_new_var,
                                  row=10, col=0, rowspan=1, colspan=2)

        self.sett_layout.setRowStretch(11, 1)

    def _add_varlist_fields(self):
        pass

    def _add_plotarea_fields(self):
        pass
