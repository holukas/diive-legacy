import unittest

import numpy as np
import pandas as pd

from pkgs.outlierdetection.doublediff.doublediff import  DoubleDiff


class Tests(unittest.TestCase):

    def test_doublediff(self):
        #TODO TODO TODO
        # Test data
        from pathlib import Path
        import matplotlib.pyplot as plt

        testfile = Path(
            r"L:\Dropbox\luhk_work\programming\DIIVE_Data_Import_Investigate_Visualize_Export\DIIVE\src\main\resources\base\example_files\TestFile_CH-DAV_2020-07.diive.csv")
        testdata_df = pd.read_csv(testfile, header=[0, 1], index_col=0, parse_dates=True, na_values=[-9999])
        testdata_df = testdata_df[[('co2_flux', '[µmol+1s-1m-2]'), ('SW_IN_CORRECTED_AVG_T1_35_2', '-no-units-')]]
        testdata_df = testdata_df.iloc[0:500]

        doublediff = DoubleDiff(df=testdata_df,
                                target_col=('co2_flux', '[µmol+1s-1m-2]'),
                                daytime_info_col=('SW_IN_CORRECTED_AVG_T1_35_2', '-no-units-'),
                                daytime_threshold=20,
                                set_daytime_if='Smaller Than Threshold',
                                z_val=5.5,
                                blocksize=13)

        _df = doublediff.get_results()

        _df.plot(subplots=True, figsize=(24, 9))
        plt.show()


if __name__ == '__main__':
    unittest.main()
