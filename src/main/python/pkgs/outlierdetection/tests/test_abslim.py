import unittest

import numpy as np
import pandas as pd

from pkgs.outlierdetection.abslim.abslim import AbsoluteLimits


class Tests(unittest.TestCase):

    def test_abslim(self):
        # For testing purposes
        _random_data = np.random.randint(-10, 10, size=1000)
        _random_name = ('randomname', '[_units]')
        _random_series = pd.Series(name=_random_name, data=_random_data)
        _random_df = pd.DataFrame(_random_series)
        abslim = AbsoluteLimits(df=_random_df, target_col=_random_name, upperlim=8, lowerlim=-4)
        _df = abslim.get_results()
        self.assertTrue(isinstance(_df, pd.DataFrame))
        self.assertTrue(_df[('.randomname#odAL', '[_units]')].max() <= 8)
        self.assertTrue(_df[('.randomname#odAL', '[_units]')].min() >= -4)


if __name__ == '__main__':
    unittest.main()
