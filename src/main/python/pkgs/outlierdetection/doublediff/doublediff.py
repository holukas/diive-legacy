"""
OUTLIER DETECTION: DOUBLE-DIFFERENCED TIME SERIES
=================================================

# last update in: v0.22.0

This package is part of DIIVE:
https://gitlab.ethz.ch/holukas/diive

"""

import numpy as np
import pandas as pd
from pandas import DataFrame

from pkgs.dfun.frames import generate_flag, generate_flag_daynight, init_class_df

pd.set_option('display.max_rows', 50)
pd.set_option('display.max_columns', 12)
pd.set_option('display.width', 1000)


class DoubleDiff:
    class_tag = "#odDD"

    def __init__(
            self,
            df: DataFrame,
            target_col: tuple,
            daytime_info_col: tuple,
            daytime_threshold: float,
            set_daytime_if: str,
            z_val: float = 5.5,
            blocksize: int = 13,
            repeat: bool = False,
    ):
        # Settings
        self.df = df
        self.target_col = target_col
        self.daytime_info_col = daytime_info_col
        self.daytime_threshold = daytime_threshold
        self.set_daytime_if = set_daytime_if
        self.z_val = z_val
        self.repeat = repeat

        # New cols for calculations
        target_varname = self.target_col[0]
        target_units = self.target_col[1]
        self.upperlim_col = (f".upper_limit", target_units)
        self.lowerlim_col = (f".lower_limit", target_units)
        self.shifted_down_col = ('.shifted_down', target_units)
        self.shifted_up_col = ('.shifted_up', target_units)
        self.di_col = ('.di', target_units)  # Differences
        self.md_col = ('.Md', target_units)  # Median of differences
        self.mad_col = ('.MAD', target_units)
        self.classvars = [self.shifted_up_col, self.shifted_down_col, self.di_col,
                          self.md_col, self.mad_col, self.upperlim_col, self.lowerlim_col]

        # Prepare
        self.class_df = \
            init_class_df(df=self.df,
                          subset_cols=[self.target_col, self.daytime_info_col],
                          newcols=self.classvars)

        self.class_df, \
        self.flag_daytime_col = \
            generate_flag_daynight(df=self.class_df,
                                   col=self.daytime_info_col,
                                   set_daytime_if='Larger Than Threshold',
                                   daytime_threshold=self.daytime_threshold)

        # Calcs
        self.freq = f"{blocksize}D"
        self.class_df = self._blockloop(df=self.class_df)

        # Flag
        # Value flagged as outlier if di parameter outside limits
        self.class_df, \
        self.flag_col, \
        self.target_nooutliers_col, \
        self.target_outliervals_col = generate_flag(df=self.class_df,
                                                    target_col=self.target_col,
                                                    tag=self.class_tag,
                                                    upperlim_col=self.upperlim_col,
                                                    lowerlim_col=self.lowerlim_col,
                                                    criterion_col=self.di_col)

        self.num_outliers = \
            self.class_df[self.target_outliervals_col].dropna().count()

    def get_results(self):
        return self.class_df

    def _blockloop(self, df: pd.DataFrame):
        """Calculate required variables for each timeblock"""

        # Divide into blocks of x days
        grouper_blocks = df.groupby(pd.Grouper(level=df.index.name, freq=self.freq))

        for block_key, block_df in grouper_blocks:

            # Group by daytime to split into day- and night-data
            grouper_daynight = block_df.groupby(self.flag_daytime_col)

            for daynight_key, daynight_df in grouper_daynight:
                # Following equations in reference
                daynight_df = self._calc_di(df=daynight_df, shift_by=1)
                daynight_df = self._calc_MAD(df=daynight_df)
                daynight_df = self._calc_upper_lower_lim(df=daynight_df, z=self.z_val)

                # Add results for this group to class_df (np.nan in class_df is replaced w/ results)
                df[self.classvars] = \
                    df[self.classvars].combine_first(daynight_df[self.classvars])
        return df

    def _calc_upper_lower_lim(self, df, z):
        # Eq. (2), (3) Upper and lower limits
        df[self.upperlim_col] = df[self.md_col] + ((df[self.mad_col].multiply(z)) / 0.6745)
        df[self.lowerlim_col] = df[self.md_col] - (df[self.mad_col].multiply(z) / 0.6745)
        return df

    def _calc_MAD(self, df):
        """Calculate median of absolute deviation about the median"""
        # Eq. (4) / MAD = median (|di−Md|)
        df[self.mad_col] = df[self.di_col] - df[self.md_col]
        df[self.mad_col] = df[self.mad_col].abs()
        df[self.mad_col] = df[self.mad_col].median()
        return df

    def _calc_di(self, df, shift_by):
        """
        Calculate differences di and median of differences Md

            Eq. (1) / di = (NEEi − NEEi−1) − (NEEi+1 − NEEi )

        """
        # Before potential extension, get original start and end index.
        # Since extension is only needed for the calculation of di,
        # the original start and end can be used to restrict df to
        # it's original range after di was calculated.
        df_start = df.index[0]
        df_end = df.index[-1]
        df = self._extend_df(daynight_df=df, other_df=self.class_df, which='start')
        df = self._extend_df(daynight_df=df, other_df=self.class_df, which='end')

        df.loc[:, self.shifted_down_col] = \
            df[self.target_col].shift(periods=shift_by)

        df.loc[:, self.shifted_up_col] = \
            df[self.target_col].shift(periods=-shift_by)

        df.loc[:, self.di_col] = \
            df[self.target_col].subtract(df[self.shifted_down_col]) - \
            df[self.shifted_up_col].subtract(df[self.target_col])

        df[self.md_col] = \
            df[self.di_col].median()

        df = df.loc[df_start:df_end]  # Restore original range (unextended)
        return df

    def _extend_df(self, daynight_df, other_df, which):
        """
        Extend start or end by one value, for the calc of di

            see Papale et al. (2006), Section 2.2, p573
        """
        extension_df = daynight_df.copy()
        max_possible_ix = len(self.class_df) - 1

        # Search integer index of date in other
        which_ix = 0 if which == 'start' else -1
        date = extension_df.iloc[which_ix].name
        ix_in_other = np.where(other_df.index == date)[0]

        # Calculate index required for extension
        adjacent_ix = -1 if which == 'start' else 1
        extension_ix = ix_in_other + adjacent_ix  # 'start' will get the previous index (-1), 'end' the next (+1)

        if (extension_ix < 0) or (extension_ix > max_possible_ix):  # Not possible, return
            return extension_df

        row = self.class_df.iloc[extension_ix]  # One row of records that will be added
        extension_df = pd.concat([extension_df, row], axis=0)
        extension_df = extension_df.sort_index(axis=0)
        return extension_df


if __name__ == '__main__':
    # For testing purposes

    # Test data
    from pathlib import Path
    import matplotlib.pyplot as plt

    testfile = Path(
        r"L:\Dropbox\luhk_work\programming\DIIVE_Data_Import_Investigate_Visualize_Export\DIIVE\src\main\resources\base\example_files\TestFile_CH-DAV_2020-07.diive.csv")
    testdata_df = pd.read_csv(testfile, header=[0, 1], index_col=0, parse_dates=True, na_values=[-9999])
    testdata_df = testdata_df[[('co2_flux', '[µmol+1s-1m-2]'), ('SW_IN_CORRECTED_AVG_T1_35_2', '-no-units-')]]
    testdata_df = testdata_df.iloc[0:500]

    doublediff = DoubleDiff(df=testdata_df,
                            target_col=('co2_flux', '[µmol+1s-1m-2]'),
                            daytime_info_col=('SW_IN_CORRECTED_AVG_T1_35_2', '-no-units-'),
                            daytime_threshold=20,
                            set_daytime_if='Smaller Than Threshold',
                            z_val=5.5,
                            blocksize=13)

    _df = doublediff.get_results()

    _df.plot(subplots=True, figsize=(24, 9))
    plt.show()
