"""
OUTLIER DETECTION: DOUBLE-DIFFERENCED TIME SERIES
=================================================

    View class for DIIVE GUI integration.

    Code follows the Model-view-controller (MVC) software design pattern.
    dcontroller.py (controller) defines interactions between
    abslim.py (model) and dvgui.py (view).

"""

from PyQt5 import QtGui as qtg

import gui.add_to_layout as add_to_layout
import gui.combine as combine
import gui.make as make
from gui.tabs import TemplateSVP


class BuildTabDoubleDiff(TemplateSVP):
    """Build tab based on template and add GUI fields"""

    def __init__(self, app_obj, tab_title, tab_id):
        super().__init__(app_obj, tab_title, tab_id)

        # Icon
        self.TabWidget.setTabIcon(self.tab_ix, qtg.QIcon(app_obj.ctx.tab_icon_outlier_detection))

        # Fields
        self.drp_daytime_info_col = None
        self.drp_set_daytime_if = None
        self.lne_daytime_threshold = None
        self.lne_outlier_timewin = None
        self.lne_z = None
        self.drp_repeat = None

        self.btn_calc = None
        self.btn_keep_marked = None
        self.btn_remove_marked = None
        self.btn_add_as_new_var = None

        # Tab columns
        self._add_settings_fields()
        self._add_varlist_fields()
        self._add_plotarea_fields()

    def _add_settings_fields(self):
        # Header
        _header = combine.frame_header_subheader(header_txt="Double Difference",
                                                 subheader_txt="Outlier Detection")
        add_to_layout.add_to_grid(layout=self.sett_layout, widget=_header,
                                  row=0, col=0, rowspan=1, colspan=1)

        _spaceritem = make.qspaceritem()
        add_to_layout.add_to_grid(layout=self.sett_layout, widget=_spaceritem,
                                  row=1, col=0, rowspan=1, colspan=1)

        # Section: Day / night
        _section_header = combine.header(header_txt="Day / Night")
        add_to_layout.add_to_grid(layout=self.sett_layout, widget=_section_header,
                                  row=2, col=0, rowspan=1, colspan=1)

        _label, self.drp_daytime_info_col = \
            combine.label_combobox(txt="Define Daytime Based On", css_ids=['', 'cyan'],
                                   max_visible_items=999)
        add_to_layout.add_to_grid(layout=self.sett_layout, widget=_label,
                                  row=3, col=0, rowspan=1, colspan=1)
        add_to_layout.add_to_grid(layout=self.sett_layout, widget=self.drp_daytime_info_col,
                                  row=3, col=1, rowspan=1, colspan=1)

        _label, self.drp_set_daytime_if = \
            combine.label_combobox(txt="Set To Daytime If", css_ids=['', 'cyan'],
                                   max_visible_items=999)
        add_to_layout.add_to_grid(layout=self.sett_layout, widget=_label,
                                  row=4, col=0, rowspan=1, colspan=1)
        add_to_layout.add_to_grid(layout=self.sett_layout, widget=self.drp_set_daytime_if,
                                  row=4, col=1, rowspan=1, colspan=1)
        self.drp_set_daytime_if.addItems(['Larger Than Threshold', 'Smaller Than Threshold'])

        _label, self.lne_daytime_threshold = \
            combine.label_lineedit_horiz(txt='Threshold', css_ids=['', 'cyan'],
                                         validator='float')
        add_to_layout.add_to_grid(layout=self.sett_layout, widget=_label,
                                  row=5, col=0, rowspan=1, colspan=1)
        add_to_layout.add_to_grid(layout=self.sett_layout, widget=self.lne_daytime_threshold,
                                  row=5, col=1, rowspan=1, colspan=1)
        self.lne_daytime_threshold.setText('20')

        _spaceritem = make.qspaceritem()
        add_to_layout.add_to_grid(layout=self.sett_layout, widget=_spaceritem,
                                  row=6, col=0, rowspan=1, colspan=1)

        # Section: Outlier Detection
        _section_header = combine.header(header_txt="Outlier Detection")
        add_to_layout.add_to_grid(layout=self.sett_layout, widget=_section_header,
                                  row=7, col=0, rowspan=1, colspan=1)

        _label, self.lne_outlier_timewin = \
            combine.label_lineedit_horiz(txt='Time Window (Days)', css_ids=['', 'cyan'],
                                         validator='int')
        add_to_layout.add_to_grid(layout=self.sett_layout, widget=_label,
                                  row=8, col=0, rowspan=1, colspan=1)
        add_to_layout.add_to_grid(layout=self.sett_layout, widget=self.lne_outlier_timewin,
                                  row=8, col=1, rowspan=1, colspan=1)
        self.lne_outlier_timewin.setText('13')

        _label, self.lne_z = \
            combine.label_lineedit_horiz(txt='Threshold Value (z)', css_ids=['', 'cyan'],
                                         validator='float')
        add_to_layout.add_to_grid(layout=self.sett_layout, widget=_label,
                                  row=9, col=0, rowspan=1, colspan=1)
        add_to_layout.add_to_grid(layout=self.sett_layout, widget=self.lne_z,
                                  row=9, col=1, rowspan=1, colspan=1)
        self.lne_z.setText('5.5')

        _label, self.drp_repeat = \
            combine.label_combobox(txt="Repeat Until All Outliers Removed", css_ids=['', ''],
                                   max_visible_items=999)
        add_to_layout.add_to_grid(layout=self.sett_layout, widget=_label,
                                  row=10, col=0, rowspan=1, colspan=1)
        add_to_layout.add_to_grid(layout=self.sett_layout, widget=self.drp_repeat,
                                  row=10, col=1, rowspan=1, colspan=1)
        self.drp_repeat.addItems(['No', 'Yes'])

        _spaceritem = make.qspaceritem()
        add_to_layout.add_to_grid(layout=self.sett_layout, widget=_spaceritem,
                                  row=11, col=0, rowspan=1, colspan=1)

        # Buttons
        self.btn_calc = make.qbutton(txt='Calculate Outliers', css_id=None, tooltip=None)
        add_to_layout.add_to_grid(layout=self.sett_layout, widget=self.btn_calc,
                                  row=12, col=0, rowspan=1, colspan=2)

        self.btn_keep_marked = make.qbutton(txt='Keep Marked Values', css_id='btn_keep_marked', tooltip=None)
        add_to_layout.add_to_grid(layout=self.sett_layout, widget=self.btn_keep_marked,
                                  row=13, col=0, rowspan=1, colspan=2)

        self.btn_remove_marked = make.qbutton(txt='Remove Marked Values', css_id='btn_remove_marked', tooltip=None)
        add_to_layout.add_to_grid(layout=self.sett_layout, widget=self.btn_remove_marked,
                                  row=14, col=0, rowspan=1, colspan=2)

        _spaceritem = make.qspaceritem()
        add_to_layout.add_to_grid(layout=self.sett_layout, widget=_spaceritem,
                                  row=15, col=0, rowspan=1, colspan=1)

        self.btn_add_as_new_var = make.qbutton(txt='+ Add As New Var', css_id='btn_add_as_new_var', tooltip=None)
        add_to_layout.add_to_grid(layout=self.sett_layout, widget=self.btn_add_as_new_var,
                                  row=16, col=0, rowspan=1, colspan=2)

        self.sett_layout.setRowStretch(17, 1)

    def _add_varlist_fields(self):
        pass

    def _add_plotarea_fields(self):
        pass
