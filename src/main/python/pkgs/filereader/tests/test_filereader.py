import unittest
from pathlib import Path

from pkgs.filereader.filereader import ConfigFileReader, DataFileReader


class Tests(unittest.TestCase):

    def test_configfilereader(self):
        """Read YAML config file for filetypes"""
        configfilepath = Path('testfile_EDDYPRO_FULL_OUTPUT_30MIN.yml')
        config = ConfigFileReader(configfilepath=configfilepath).read()
        count_values = 0
        for key in config.keys():
            count_values += len(config[key].values())
        self.assertEqual(len(config.keys()), 4)
        self.assertEqual(count_values, 15)
        self.assertEqual(config['DATA']['HEADER_SECTION_ROWS'], [0, 1, 2])

    def test_datafilereader(self):
        configfilepath = Path('testfile_EDDYPRO_FULL_OUTPUT_30MIN.yml')
        datafilepath = Path('testfile_EDDYPRO_FULL_OUTPUT_30MIN.csv')
        filetype_config = ConfigFileReader(configfilepath=configfilepath).read()
        from utils.config import validate_filetype_config
        filetype_config = validate_filetype_config(filetype_config=filetype_config)

        data_df = DataFileReader(
            filepath=datafilepath,
            data_skiprows=filetype_config['DATA']['SKIP_ROWS'],
            data_headerrows=filetype_config['DATA']['HEADER_ROWS'],
            data_headersection_rows=filetype_config['DATA']['HEADER_SECTION_ROWS'],
            data_na_vals=filetype_config['DATA']['NA_VALUES'],
            data_delimiter=filetype_config['DATA']['DELIMITER'],
            data_freq=filetype_config['DATA']['FREQUENCY'],
            timestamp_idx_col=filetype_config['TIMESTAMP']['INDEX_COLUMN'],
            timestamp_datetime_format=filetype_config['TIMESTAMP']['DATETIME_FORMAT'],
            timestamp_start_middle_end=filetype_config['TIMESTAMP']['SHOWS_START_MIDDLE_OR_END_OF_RECORD']
        ).read()

        self.assertEqual(data_df.size, 1160)
        self.assertEqual(data_df.columns.size, 116)


if __name__ == '__main__':
    unittest.main()
