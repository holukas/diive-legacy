import unittest
from pathlib import Path

import pandas as pd
from sklearn.ensemble import RandomForestRegressor
from pkgs.ml.randomforest_ts.randomforest_ts import RandomForestTS
from pandas import DataFrame

class Tests(unittest.TestCase):
    sourcefile = Path('testdata.csv')
    test_df = pd.read_csv(sourcefile, header=[0, 1], parse_dates=True, index_col=0)
    target_col = ('target', '[units]')

    def test_randomforest_ts(self):
        rfts = RandomForestTS(df=self.test_df,
                              target_col=self.target_col,
                              verbose=1,
                              random_state=42,
                              rfecv_step=1,
                              rfecv_min_features_to_select=5,
                              rf_rfecv_n_estimators=1,
                              rf_n_estimators=1,
                              bootstrap=True)

        rfts.lagged_variants(periods=[12],
                             aggtypes=['mean'])
        self.assertEqual(len(rfts.df.columns), 24)

        rfts.include_timestamp_as_features(doy_as_feature=True,
                                           week_as_feature=True,
                                           month_as_feature=True,
                                           hour_as_feature=True)
        self.assertEqual(len(rfts.df.columns), 28)

        rfts.feature_reduction()
        reduced_df, feat_reduction_results = rfts.get_reduced_dataset()
        self.assertTrue(isinstance(feat_reduction_results, dict))
        self.assertEqual(len(feat_reduction_results.keys()), 5)
        self.assertEqual(reduced_df.size, 40176)

        rfts.build_final_model()
        self.assertTrue(isinstance(rfts.model_results, dict))
        self.assertTrue(isinstance(rfts.model, RandomForestRegressor))

        rfts.gapfilling()
        gapfilled_df, gf_results = rfts.get_gapfilled_dataset()
        self.assertTrue(isinstance(gf_results, dict))
        self.assertTrue(isinstance(gapfilled_df, DataFrame))

        self.assertEqual(gapfilled_df[('target#gfRF', '[units]')].sum(), 783.1331000333334)
        self.assertEqual(gapfilled_df[('QCF_target#gfRF', '[0=measured]')].sum(), 1318)


if __name__ == '__main__':
    unittest.main()
