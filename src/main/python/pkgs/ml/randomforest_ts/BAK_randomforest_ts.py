"""
RANDOM FOREST GAP-FILLING FOR TIME SERIES
=========================================

# last update in: v0.22.0

This package is part of DIIVE:
https://gitlab.ethz.ch/holukas/diive


Kudos:
- https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestRegressor.html
- https://towardsdatascience.com/random-forest-in-python-24d0893d51c0
- https://scikit-learn.org/stable/modules/grid_search.html
- Train/test split: https://stackoverflow.com/questions/13610074/is-there-a-rule-of-thumb-for-how-to-divide-a-dataset-into-training-and-validatio

"""
import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestRegressor  # Import the model we are using

from pkgs.dfun.frames import create_lagged_variants

pd.set_option('display.max_rows', 50)
pd.set_option('display.max_columns', 12)
pd.set_option('display.width', 1000)


# from sklearn.model_selection import cross_val_score

class RandomForestTS:
    id = "#gfRF"

    def __init__(
            self,
            df: pd.DataFrame,
            target_col: str or tuple,
            lagged_variants: int = False,
            lagged_variants_ignore_cols: list = None,
            threshold_important_features: float = 0,
            hour_as_feature: bool = True,
            week_as_feature: bool = True,
            month_as_feature: bool = True,
            doy_as_feature: bool = True,
            n_estimators: int = 100,
            criterion: str = 'mse',
            max_depth: int = None,
            min_samples_split: int or float = 2,
            min_samples_leaf: int or float = 1,
            min_weight_fraction_leaf: float = 0.0,
            max_features: int or float = 'auto',
            max_leaf_nodes: int = None,
            min_impurity_decrease: float = 0.0,
            min_impurity_split: float = None,
            bootstrap: bool = True,
            oob_score: bool = False,
            n_jobs: int = -1,  # Custom default
            random_state: int = None,
            verbose: int = 0,
            warm_start: bool = False,
            ccp_alpha: float = 0.0,  # Non-negative
            max_samples: int or float = None
    ):
        """

        Parameters
        ----------
        df: pd.Dataframe
            Contains data and timestamp (in index) for target and features.
        target_col: str or tuple
            Column name of variable that will be gap-filled.
        lagged_variants: int, default=None
            Create lagged variants of non-target columns, shift by value.
        threshold_important_features: float
            Threshold for feature importance. Variables with importances
            below this threshold are removed from the final model.
        week_as_feature
        month_as_feature
        doy_as_feature
        n_estimators
        criterion
        max_depth
        min_samples_split
        min_samples_leaf
        min_weight_fraction_leaf
        max_features
        max_leaf_nodes
        min_impurity_decrease
        min_impurity_split
        bootstrap
        oob_score
        n_jobs
        random_state
        verbose
        warm_start
        ccp_alpha
        max_samples
        """
        self.df = df
        self.target_col = target_col
        self.random_state = random_state
        self.verbose = verbose
        self.lagged_variants = lagged_variants
        self.lagged_variants_ignore_cols = lagged_variants_ignore_cols
        self.threshold_important_features = threshold_important_features

        self.model_params = dict(
            n_estimators=n_estimators,
            random_state=self.random_state,
            criterion=criterion,
            min_samples_split=min_samples_split,
            min_samples_leaf=min_samples_leaf,
            max_depth=max_depth,
            min_weight_fraction_leaf=min_weight_fraction_leaf,
            max_features=max_features,
            max_leaf_nodes=max_leaf_nodes,
            min_impurity_decrease=min_impurity_decrease,
            min_impurity_split=min_impurity_split,
            bootstrap=bootstrap,
            oob_score=oob_score,
            n_jobs=n_jobs,
            warm_start=warm_start,
            ccp_alpha=ccp_alpha,
            max_samples=max_samples
        )

        self.targets = None
        self.features_names = None
        self.features = None
        self.timestamp = None

        self.train_targets = None
        self.test_targets = None
        self.train_features = None
        self.test_features = None

        self.rfmodel = None

        self.predictions = None

        self.importances = None
        self.feature_importances = None

        self.hour_as_feature = hour_as_feature
        self.week_as_feature = week_as_feature
        self.month_as_feature = month_as_feature
        self.doy_as_feature = doy_as_feature

        self.phase = None

        self.gapfilled_df = None

    def gapfilling(self):
        """
        Fill gaps in target

        For gapfilling, the model is trained on all available
        features and targets. Only rows where all records are
        available can be used.

        The model is then used to predict targets for all rows
        where all features are available.

        This means that predictions might not be available for
        some rows, namely those where at least one feature is
        missing.

        """

        # Define column names for output
        self._define_cols()

        # Calculate lagged variants of features
        if self.lagged_variants:

            if not self.lagged_variants_ignore_cols:
                self.lagged_variants_ignore_cols = [self.target_col]
            else:
                self.lagged_variants_ignore_cols.append(self.target_col)

            self.df = create_lagged_variants(
                df=self.df.copy(), num_lags=self.lagged_variants, ignore_cols=self.lagged_variants_ignore_cols)

        # Build model from reduced feature
        model, reduced_features_list, most_important_df, model_r2 = self._feature_reduction_loop()  # List also contains target

        # Use model
        gapfilled_df = self._predict_targets(reduced_features_list=reduced_features_list,
                                             model=model)

        # Fill still existing gaps
        _still_missing_locs = gapfilled_df[self.target_gapfilled_col].isnull()
        _num_still_missing = _still_missing_locs.sum()
        if _num_still_missing > 0:
            fallback_predictions, fallback_timestamp = \
                self._gf_fallback(series=gapfilled_df[self.target_gapfilled_col])
            fallback_series = pd.Series(data=fallback_predictions, index=fallback_timestamp)
            gapfilled_df[self.target_gapfilled_col].fillna(fallback_series, inplace=True)
            gapfilled_df[self.predictions_fallback_col] = fallback_series
            gapfilled_df.loc[_still_missing_locs, self.target_gapfilled_flag_col] = 2  # Adjust flag

        # Cumulative
        gapfilled_df[self.target_gapfilled_cumu_col] = \
            gapfilled_df[self.target_gapfilled_col].cumsum()

        if self.verbose >= 0:
            self._results(gapfilled_df=gapfilled_df,
                          most_important_df=most_important_df,
                          model_r2=model_r2,
                          still_missing_locs=_still_missing_locs)

        return gapfilled_df

    def _gf_fallback(self, series: pd.Series):
        """Fill data gaps using timestamp features only, fallback for still existing gaps"""
        # Check if still gaps in gapfilled

        gf_fallback_df = pd.DataFrame(series)
        gf_fallback_df = self._include_timestamp_as_features(df=gf_fallback_df)

        # Build model for target predictions from timestamp
        # Only model-building, no predictions in this step.
        targets, features, features_names, timestamp = self._convert_to_arrays(
            df=gf_fallback_df, complete_rows=True, target_col=self.target_gapfilled_col)
        model, model_r2 = self._train_model(features=features, targets=targets)  # All features, all targets
        # predictions = self._predict(features=features, model=model)  # Predict targets with features

        # Use model to predict complete time series
        targets, features, features_names, timestamp = self._convert_to_arrays(
            df=gf_fallback_df, complete_rows=False, target_col=self.target_gapfilled_col)
        predictions = self._predict(features=features, model=model)  # Predict targets with features

        return predictions, timestamp

    def _results(self, gapfilled_df, most_important_df, model_r2, still_missing_locs):
        """Summarize gap-filling results"""

        _vals_max = len(gapfilled_df.index)
        _vals_before = len(gapfilled_df[self.target_col].dropna())
        _vals_after = len(gapfilled_df[self.target_gapfilled_col].dropna())
        _vals_fallback_filled = still_missing_locs.sum()
        _perc_fallback_filled = (_vals_fallback_filled / _vals_max) * 100

        print(f"Gap-filling results for {self.target_col}\n"
              f"max possible: {_vals_max} values\n"
              f"before gap-filling: {_vals_before} values\n"
              f"after gap-filling: {_vals_after} values\n"
              f"gap-filled with fallback: {_vals_fallback_filled} values / {_perc_fallback_filled:.1f}%\n"
              f"used features:\n{most_important_df}\n"
              f"predictions vs targets, R2 = {model_r2:.3f}")

    def _predict_targets(self, reduced_features_list, model):

        # Get full data with original timestamp, select most important
        df = self.df.copy()
        df = self._include_timestamp_as_features(df=df)
        df = df[reduced_features_list]

        # Targets w/ full timestamp
        targets_series = pd.Series(data=df[self.target_col], index=df.index)
        df = df.drop(self.target_col, axis=1)  # Remove target, not needed for predictions

        # Apply model
        df = df.dropna()  # Now only contains rows with complete features
        features = np.array(df)  # Features (used to predict target) need to be complete
        timestamp = df.index  # Timestamp for features and therefore predictions
        predictions = self._predict(features=features, model=model)  # Predict targets with features

        gapfilled_df = self._collect(predictions=predictions,
                                     timestamp=timestamp,
                                     targets_series=targets_series)

        return gapfilled_df

    def _collect(self, predictions, timestamp, targets_series):
        # Make series w/ timestamp for predictions
        predicted_series = pd.Series(data=predictions, index=timestamp, name=self.predictions_col)

        # Reindex predictions to same timestamp as targets
        predicted_series = predicted_series.reindex(targets_series.index)

        # Collect predictions and targets in df
        gapfilled_df = \
            pd.concat([targets_series, predicted_series], axis=1)

        # Gap locations
        # Make column that contains predicted values for rows
        # where target is missing.
        _gap_locs = gapfilled_df[self.target_col].isnull()
        gapfilled_df[self.predicted_gaps_col] = \
            gapfilled_df.loc[_gap_locs, self.predictions_col]

        # Flag
        # Make flag column that indicates where predictions for
        # missing targets are available.
        # todo Note that missing predicted gaps = 0. change?
        _gapfilled_locs = gapfilled_df[self.predicted_gaps_col].isnull()  # Non-gapfilled locations
        _gapfilled_locs = ~_gapfilled_locs  # Inverse for gapfilled locations
        gapfilled_df[self.target_gapfilled_flag_col] = _gapfilled_locs
        gapfilled_df[self.target_gapfilled_flag_col] = gapfilled_df[self.target_gapfilled_flag_col].astype(int)

        # Gap-filled time series
        gapfilled_df[self.target_gapfilled_col] = \
            gapfilled_df[self.target_col].fillna(gapfilled_df[self.predictions_col])

        return gapfilled_df

    def _define_cols(self):
        self.predictions_col = (".predictions", "[aux]")
        self.predicted_gaps_col = (".gap_predictions", "[aux]")
        self.target_gapfilled_col = (f"{self.target_col[0]}{self.id}", self.target_col[1])
        self.target_gapfilled_flag_col = (f"QCF_{self.target_gapfilled_col[0]}", "[0=measured]")
        self.predictions_fallback_col = (".predictions_fallback", "[aux]")
        self.target_gapfilled_cumu_col = (".gapfilled_cumulative", "[aux]")

    def _feature_reduction_loop(self):
        """
        Build model with most important features

        The goal is to build a model from a data subset that
        includes the most important features only.

        (1) First, all features are used to predict all targets.
        The most important features (by default those with an
        importance > 0) are retained. If the number of most
        important features < total number of features then
        the next steps are executed.

        (2) The most important features from (1) are used to predict
        all targets. This step is only executed if at least one feature
        in (1) was considered not important (importance = 0).

        (etc) The most important features from the previous step are
        used to predict all targets.

        Iteration ends when no more features were reduced.

        """
        subset_df = None
        model, subset_df, repeat, most_important_df, model_r2 = self._predict_all_targets(subset_df)

        while repeat:
            model, subset_df, repeat, most_important_df, model_r2 = self._predict_all_targets(subset_df)
            continue

        return model, subset_df.columns, most_important_df, model_r2

    def _predict_all_targets(self, subset: None or pd.DataFrame = None):
        """Predict all targets"""
        self.phase = f"[PREDICT ALL TARGETS]"

        # Prepare data
        if not isinstance(subset, pd.DataFrame):
            df = self.df.copy()
            df = self._include_timestamp_as_features(df=df)
        else:
            df = subset.copy()

        # Run forest
        targets, features, features_names, timestamp = self._convert_to_arrays(df=df,
                                                                               complete_rows=True,
                                                                               target_col=self.target_col)
        num_features_in = len(features_names)

        model, model_r2 = self._train_model(features=features, targets=targets)  # All features, all targets
        predictions = self._predict(features=features, model=model)  # Predict targets with features

        most_important_list, most_important_df = \
            self._importances(model=model, features_names=features_names)  # Importances
        num_features_out = len(most_important_list)

        self._stats(predictions=predictions, targets=targets, model_r2=model_r2)  # Stats

        # Subset with most important features
        most_important_list.append(self.target_col)  # Target is needed
        subset = df[most_important_list].copy()

        # Check if features were rejected
        repeat = True if num_features_in != num_features_out else False

        return model, subset, repeat, most_important_df, model_r2

    # def train_test(self, df, phase):
    #     """
    #     Train and test using train_test_split
    #
    #     Model is first trained on training features and training targets,
    #     then the model is used to predict test targets using test features.
    #
    #     """
    #     self.phase = f"PHASE {phase} [TRAIN/TEST]"
    #
    #     # Data as numpy arrays
    #     targets, features, features_names, timestamp = \
    #         self._convert_to_arrays(df=df,
    #                                 complete_rows=True)
    #
    #     # Split dataset into training and testing datasets
    #     tt_train_features, tt_test_features, tt_train_targets, tt_test_targets = \
    #         self._traintest_split(targets=targets,
    #                               features=features)
    #
    #     # Train model on training features and targets
    #     tt_model = \
    #         self._train_model(features=tt_train_features,
    #                           targets=tt_train_targets)
    #
    #     # Predict test targets with test features
    #     tt_test_predictions = \
    #         self._predict(features=tt_test_features,
    #                       targets=tt_test_targets,
    #                       model=tt_model)
    #
    #     # Importances from training / testing
    #     tt_most_important_list = \
    #         self._importances(model=tt_model,
    #                           features_names=features_names)
    #
    #     # Stats
    #     tt_mape, tt_accuracy = \
    #         self._stats_mape(predictions=tt_test_predictions,
    #                          targets=tt_test_targets)
    #
    #     return tt_most_important_list

    def _include_timestamp_as_features(self, df):
        """Include timestamp info as data columns"""
        aux_col = ('_TIMESTAMP', '[aux]')
        df[aux_col] = df.index.copy()
        if self.doy_as_feature:
            df[('.DOY', '[day_of_year]')] = df.index.dayofyear
        if self.week_as_feature:
            df[('.WEEK', '[week_of_year]')] = df[aux_col].dt.isocalendar().week
        if self.month_as_feature:
            df[('.MONTH', '[month]')] = df[aux_col].dt.month
        if self.hour_as_feature:
            df[('.HOUR', '[hour]')] = df[aux_col].dt.hour
        df.drop(aux_col, axis=1, inplace=True)

        if self.verbose > 0:
            print(
                f"{self.phase} Including timestamp as features ...\n"
                f"{self.phase}  ├ Day of year: {self.doy_as_feature}\n"
                f"{self.phase}  ├ Week of year: {self.week_as_feature}\n"
                f"{self.phase}  └ Month: {self.month_as_feature}\n"
            )

        return df

    def _stats(self, predictions, targets, model_r2):
        """Calculate mean absolute percentage error (MAPE) and accuracy"""
        _abs_errors = abs(predictions - targets)  # Calculate the absolute errors
        _mape = 100 * (_abs_errors / targets)
        mape = np.mean(_mape)
        accuracy = 100 - np.mean(mape)

        if self.verbose > 0:
            print(
                f"{self.phase} Stats for predictions vs targets ...\n"
                f"{self.phase}   Mean Absolute Error: {mape:.2f}\n"
                f"{self.phase}   Accuracy: {accuracy:.2f}%\n"
                f"{self.phase}   R2 = {model_r2:.3f}\n"
            )

    def _importances(self, model, features_names):
        feature_importances = self._all_importances(model, features_names)
        most_important_df, most_important_list = self._most_important_features(
            feature_importances=feature_importances, threshold_important_features=self.threshold_important_features)
        return most_important_list, most_important_df

    def _all_importances(self, model, features_names):
        # Get numerical feature importances from current model
        importances = list(model.feature_importances_)

        # List of tuples with variable and importance
        feature_importances = [(
            feature, round(importance, 2)) for feature, importance in zip(features_names, importances)]

        # Sort the feature importances by most important first
        feature_importances = sorted(feature_importances, key=lambda x: x[1], reverse=True)

        # Print out the feature and importances
        if self.verbose > 0:
            print(
                f"{self.phase} Getting model feature importances ..."
            )
            [print(f"{self.phase}  - Variable: {var} Importance: {imp}") for var, imp in feature_importances]
            print("")

        return feature_importances

    def _most_important_features(self, feature_importances, threshold_important_features: float = 0):
        """Return most important features as df and list"""
        _df = pd.DataFrame.from_records(feature_importances, columns=['Var', 'Importance'])
        _df = _df.loc[_df['Importance'] > threshold_important_features, :]
        most_important = _df['Var'].to_list()

        if self.verbose > 0:
            print(
                f"{self.phase} Most important features ...\n"
                f"{self.phase}   ├ Features with importance > {threshold_important_features}: {len(_df.index)}\n"
                f"{self.phase}   └ List of most important features:\n"
                f"{self.phase}       {most_important}\n"
            )
            print(_df)
        return _df, most_important

    def _train_model(self, features, targets):
        rfmodel = RandomForestRegressor(**self.model_params)  # Instantiate model with x decision trees
        rfmodel.fit(features, targets)  # Train the model on data
        model_r2 = rfmodel.score(features, targets)

        if self.verbose > 0:
            print(
                f"{self.phase} Training model ...\n"
                f"{self.phase}   └ Model parameters:\n"
                f"{self.phase}       {rfmodel.get_params()}\n"
            )

        return rfmodel, model_r2

    def _predict(self, features, model):
        """Predict targets with features"""
        predictions = model.predict(features)
        return predictions

    def _convert_to_arrays(self, df, target_col, complete_rows: bool = True):
        """Convert data from df to numpy arrays and prepare targets and features"""
        if complete_rows:
            df = df.dropna()  # Keep complete rows only
        targets = np.array(df[target_col])  # Targets (will be predicted)
        df = df.drop(target_col, axis=1)
        features_names = list(df.columns)  # Column names of features
        features = np.array(df)  # Features (used to predict target)
        timestamp = np.array(df.index)  # Original timestamp, will be merged with data later

        if self.verbose > 0:
            print(
                f"{self.phase} Converting dataframe to arrays ...\n"
                f"{self.phase}   ├ Use only complete rows: {complete_rows}\n"
                f"{self.phase}   ├ Targets:\n"
                f"{self.phase}   |   ├ Values: {targets.size}\n"
                f"{self.phase}   |   └ Name of target: {target_col}\n"
                f"{self.phase}   ├ Features:\n"
                f"{self.phase}   |   ├ Values: {features.size}\n"
                f"{self.phase}   |   ├ Number of features: {len(features_names)}\n"
                f"{self.phase}   |   └ Names of features: {features_names} values\n"
                f"{self.phase}   └ Timestamp:\n"
                f"{self.phase}       ├ Values: {timestamp.size}\n"
                f"{self.phase}       ├ First: {timestamp[0]}\n"
                f"{self.phase}       └ Last: {timestamp[-1]}\n"
            )

        return targets, features, features_names, timestamp

    # def _traintest_split(self, targets, features):
    #     """Split the data into training and testing sets"""
    #     train_features, test_features, \
    #     train_targets, test_targets = \
    #         train_test_split(features,
    #                          targets,
    #                          test_size=0.25,
    #                          random_state=self.random_state)
    #
    #     if self.verbose > 0:
    #         print(
    #             f"{self.phase} Training / testing datasets ...\n"
    #             f"{self.phase}   ├ Training features shape: {train_features.shape}\n"
    #             f"{self.phase}   ├ Training targets shape: {train_targets.shape}\n"
    #             f"{self.phase}   ├ Testing features shape: {test_features.shape}\n"
    #             f"{self.phase}   └ Testing targets shape: {test_targets.shape}\n"
    #         )
    #
    #     return train_features, test_features, train_targets, test_targets


if __name__ == '__main__':
    # For testing purposes
    import matplotlib.pyplot as plt
    from pathlib import Path

    sourcefile = Path('../tests/testdata.csv')
    _test_df = pd.read_csv(sourcefile, header=[0, 1], parse_dates=True, index_col=0)
    target_col = ('target', '[units]')
    rfts = RandomForestTS(df=_test_df, target_col=target_col, n_estimators=10,
                          lagged_variants=48, threshold_important_features=0.01,
                          doy_as_feature=True, week_as_feature=True, verbose=1,
                          lagged_variants_ignore_cols=[('SWC_0.05', '[units]')])
    gapfilled_df = rfts.gapfilling()
    gapfilled_df.plot(subplots=True, figsize=(16, 9))
    plt.show()
    # print(gapfilled_df)

    _scatter_df = gapfilled_df[[('target', '[units]'), ('.predictions', '[aux]')]].dropna()
    _scatter_df.plot.scatter(('target', '[units]'), ('.predictions', '[aux]'))
    plt.show()
