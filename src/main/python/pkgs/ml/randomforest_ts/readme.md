**RANDOM FOREST GAP-FILLING FOR TIME SERIES**
randomforest_ts

This package is part of DIIVE:  
https://gitlab.ethz.ch/holukas/diive

**Kudos:**

- https://mljar.com/blog/feature-importance-in-random-forest/
- https://stackoverflow.com/questions/62537457/right-way-to-use-rfecv-and-permutation-importance-sklearn
- https://www.alpharithms.com/autocorrelation-time-series-python-432909/
- https://machinelearningmastery.com/rfe-feature-selection-in-python/
- https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestRegressor.html
- https://neptune.ai/blog/random-forest-regression-when-does-it-fail-and-why
- https://towardsdatascience.com/random-forest-in-python-24d0893d51c0
- https://scikit-learn.org/stable/modules/grid_search.html
- Train/test
  split: https://stackoverflow.com/questions/13610074/is-there-a-rule-of-thumb-for-how-to-divide-a-dataset-into-training-and-validatio
- https://machinelearningmastery.com/random-forest-for-time-series-forecasting/
- https://scikit-learn.org/stable/modules/cross_validation.html#cross-validation-of-time-series-data
- https://medium.com/keita-starts-data-science/time-series-split-with-scikit-learn-74f5be38489e
- https://www.influxdata.com/blog/autocorrelation-in-time-series-data/
- https://scikit-learn.org/stable/modules/generated/sklearn.feature_selection.RFECV.html
- https://scikit-learn.org/stable/modules/cross_validation.html#time-series-split
- https://towardsdatascience.com/feature-selection-in-python-recursive-feature-elimination-19f1c39b8d15
- https://eli5.readthedocs.io/en/latest/blackbox/permutation_importance.html#eli5-permutation-importance
- https://www.kaggle.com/dansbecker/permutation-importance
- https://alexisperrier.com/datascience/2015/08/27/feature-importance-random-forests-gini-accuracy.html
- https://machinelearningmastery.com/feature-selection-time-series-forecasting-python/
