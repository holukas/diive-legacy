doublediff = """
Source:\n
       Papale et al. (2006) Towards a standardized processing of Net Ecosystem Exchange\n \
           measured with eddy covariance technique: algorithms and uncertainty estimation.\n \
           Biogeosciences, 3(4), 571–583. https://doi.org/10.5194/bg-3-571-2006\n \
       Sachs, L. (1997). Angewandte Statistik. Springer Berlin Heidelberg.\n \
           https://doi.org/10.1007/978-3-662-05746-9
"""

