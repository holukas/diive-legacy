filetype_dialog = """

NOTE

Whenever Amp imports data, it converts the timestamp to the center time of the recorded time period. The entry TIMESTAMP_SHOWS_START_MIDDLE_OR_END_OF_RECORD in the .filetype files defines if the timestamp of the original data marks the start time or the end time of the recorded values. Amp uses this info to convert the timestamp to the center time.

Example
    (a) TIMESTAMP_SHOWS_START_MIDDLE_OR_END_OF_RECORD = end
        The timestamp 2020-01-22 14:30 is converted to 2020-01-22 14:15
        
    (b) TIMESTAMP_SHOWS_START_MIDDLE_OR_END_OF_RECORD = start
        The timestamp 2020-01-22 14:30 is converted to 2020-01-22 14:45

"""