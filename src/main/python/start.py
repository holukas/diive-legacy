import os
from pathlib import Path
import main

abspath = Path(os.path.abspath(__file__)).parent  # directory of start_FCT.py
os.chdir(abspath)
# os.chdir('../..')
wd = os.getcwd()
print(f"Working directory: {wd}")
main.main()
