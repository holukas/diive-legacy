import datetime as dt
import fnmatch
import logging
import os
import pathlib
import sys
import time
import traceback

import matplotlib as mpl
import pandas as pd
from PyQt5 import QtCore as qtc
from PyQt5 import QtGui as qtg
from PyQt5 import QtWidgets as qtw
from fbs_runtime.application_context.PyQt5 import ApplicationContext, cached_property

# from gui.base.buildTab.find_free_tab_id
import gui
import gui.base
import modboxes.analyses as an
import modboxes.createvar as cv
import modboxes.eddycovariance as ec
import modboxes.export as ex
import modboxes.gapfilling as gf
import modboxes.modifications as md
import modboxes.outlierdetection as od_old
import modboxes.plots as pl
import pkgs.dfun as dfun
import utils as utils
import utils.insert
from gui.dialog_windows.select_filetype_settings import SelectFileTypeSettings
from gui.tabs import find_free_tab_id
from logger import log
from pkgs import outlierdetection as od
from pkgs.filereader.filereader import ConfigFileReader

pd.set_option('display.width', 1000)
pd.set_option('display.max_columns', 6)
mpl.rcParams['agg.path.chunksize'] = 10000


class DIIVE(qtw.QMainWindow, gui.gui.Ui_MainWindow, gui.base.SelectFileTypeSettingsFile, ApplicationContext):
    first_file_freq = '30T'  # Default time resolution of data is 30min (for now)
    focus_stats_df = pd.DataFrame()  # Collects stats for the selected variable

    def __init__(self, parent, load_initial_data, ctx):
        super(DIIVE, self).__init__(parent)

        self.setupUi(self, ctx=ctx, version_info=ctx.build_settings['version'])

        self.root_dir = os.path.dirname(os.path.abspath(__file__))
        self.ctx = ctx  # AppContext, needed to access resources, e.g. button icons
        self.dir_select_files = self.ctx.dir_script

        self.limit_plot_xlim = False  # If used, will be in datetime format to filter plot limits for x-axis
        self.limit_timerange_filter_dt = False  # Sometimes needed to filter df, e.g. diel cycles for timerange

        # LOGGER
        # ------
        # Format what is printed to text box
        self.txt_OnScreenOut.setFormatter(logging.Formatter('%(asctime)s - %(message)s', "%Y-%m-%d %H:%M:%S"))
        logging.getLogger().addHandler(self.txt_OnScreenOut)
        logging.getLogger().setLevel(logging.INFO)  # Control the logging level
        # self.txt_OnScreenOut.setFormatter(logging.Formatter('%(asctime)s - %(levelname)s - %(message)s'))

        # Version info
        log(name=DIIVE.__name__, dict={'Version': self.ctx.build_settings['version']}, highlight=False)

        # Run ID
        self.run_id = dfun.times.make_run_id(prefix="DIIVE")

        # Set default output directory
        self.project_rundir, self.project_outdir = utils.dirs.create_default_outdir(ctx=ctx, run_id=self.run_id)
        # self.project_rundir, self.project_outdir = self.create_default_outdir(run_id=self.run_id)

        # EXAMPLE FILES
        # Load example file when script starts
        # available: 'EDDYPRO_FULL_OUTPUT_30MIN'
        # self.initial_data, self.filetype_config = utils.example_files.load(file='EDDYPRO_FULL_OUTPUT_30MIN',
        #                                                                    ctx=self.ctx)
        self.initial_data, self.filetype_config = utils.example_files.load(file='DIIVE_CSV_30MIN',
                                                                           ctx=self.ctx)

        # self.initial_data, self.settings_dict = io.example_files.load(file='SMEAR_II_30MIN', ctx=self.ctx)
        # self.initial_data, self.settings_dict = io.example_files.load(file='DIIVE_CSV_1H', ctx=self.ctx)
        # self.initial_data, self.settings_dict = io.example_files.load(file='DIIVE_CSV_1MIN', ctx=self.ctx)
        # self.initial_data, self.settings_dict = io.example_files.load(file='REDDYPROC_30MIN', ctx=self.ctx)
        # self.initial_data, self.settings_dict = io.example_files.load(file='ETH_METEOSCREENING_30MIN_FORMAT-A', ctx=self.ctx)
        # self.initial_data, self.settings_dict = io.example_files.load(file='ETH_METEOSCREENING_30MIN_FORMAT-B', ctx=self.ctx)
        # self.initial_data, self.settings_dict = io.example_files.load(file='Events', ctx=self.ctx)
        # self.initial_data, self.settings_dict = io.example_files.load(file='FLUXNET_FULLSET_30MIN', ctx=self.ctx)
        # self.initial_data, self.settings_dict = io.example_files.load(file='ICOS_10S', ctx=self.ctx)
        # self.initial_data, self.settings_dict = io.example_files.load(file='TOA5_1MIN', ctx=self.ctx)

        # self.initial_data, self.settings_dict = io.example_files.load(file='Amp_30T')
        # self.initial_data, self.settings_dict = io.example_files.load(file='FluxnetFullSet_30T', ctx=self.ctx)
        # self.initial_data, self.settings_dict = io.example_files.load(file='HiRes_20Hz', ctx=self.ctx)
        # self.initial_data, self.settings_dict = io.example_files.load(file='IfDews_1T', ctx=self.ctx)
        # self.initial_data, self.settings_dict = io.example_files.load(file='NABEL_1T', ctx=self.ctx)

        # ==================================================================
        # CONNECTIONS //////////////////////////////////////////////////////
        # ==================================================================

        # VARIABLE LIST SEARCH FIELD
        # --------------------------
        self.lne_filter_varlist.textChanged.connect(self.filter_var_list)

        # TOP MENU
        # --------
        # File
        self.TopMenu.btn_fileLoadNewFile.clicked.connect(lambda: self.select_source(action='new_file'))
        self.TopMenu.btn_fileAddFile.clicked.connect(lambda: self.select_source(action='add_to_file'))
        self.TopMenu.btn_fileImportSettings.clicked.connect(self.file_settings)
        self.TopMenu.btn_fileOutputDir.clicked.connect(lambda: self.select_outdir())

        # Plots
        self.TopMenu.btn_plotsCorrMatrix.clicked.connect(lambda: self.make_tab_correlation_matrix())
        self.TopMenu.btn_plotsCumulative.clicked.connect(lambda: self.make_tab_cumulative_plots())
        self.TopMenu.btn_plotsDielCycles.clicked.connect(lambda: self.make_tab_diel_cycle_plots())
        self.TopMenu.btn_plotsHeatmap.clicked.connect(lambda: self.make_tab_heatmap_plots())
        self.TopMenu.btn_plotsHexbins.clicked.connect(lambda: self.make_tab_hexbins())
        self.TopMenu.btn_plotsHistogram.clicked.connect(lambda: self.make_tab_histogram())
        self.TopMenu.btn_plotsMultiPanel.clicked.connect(lambda: self.make_tab_multipanel_plots())
        self.TopMenu.btn_plotsQuantiles.clicked.connect(lambda: self.make_tab_quantile_plots())
        self.TopMenu.btn_plotsScatter.clicked.connect(lambda: self.make_tab_scatter_plot())
        self.TopMenu.btn_plotsWindSectors.clicked.connect(lambda: self.make_tab_wind_sectors())

        # Analyses
        self.TopMenu.btn_analysesAggregator.clicked.connect(lambda: self.make_tab_aggregator())
        self.TopMenu.btn_analysesClassFinder.clicked.connect(lambda: self.make_tab_class_finder())
        self.TopMenu.btn_analysesFeatureSelection.clicked.connect(lambda: self.make_tab_feature_selection_an())
        self.TopMenu.btn_analysesGapFinder.clicked.connect(lambda: self.make_tab_gapfinder_an())

        # Gap-filling
        # self.TopMenu.btn_gapfillingMDS.clicked.connect(lambda: self.make_tab(make_tab='GF_LOOKUP_TABLE'))
        self.TopMenu.btn_gapfillingLookupTable.clicked.connect(lambda: self.make_tab_lookup_table_gf())
        self.TopMenu.btn_gapfillingRandomForest.clicked.connect(lambda: self.make_tab_random_forest())
        self.TopMenu.btn_gapfillingSimpleRunning.clicked.connect(lambda: self.make_tab_simple_running_gf())

        # Modifications
        self.TopMenu.btn_modificationsSpanCorrection.clicked.connect(lambda: self.make_tab_span_correction_mf())
        self.TopMenu.btn_modificationsLimitDatasetTimeRange.clicked.connect(lambda: self.limit_dataset_timerange())
        self.TopMenu.btn_modificationsRenameVar.clicked.connect(lambda: self.make_tab_rename_var_mf())
        self.TopMenu.btn_modificationsRemoveTimeRange.clicked.connect(lambda: self.make_tab_remove_time_range())
        self.TopMenu.btn_modificationsSubset.clicked.connect(lambda: self.make_tab_subset())

        # Create Variable
        self.TopMenu.btn_createVariableAddNewEvent.clicked.connect(lambda: self.create_event())
        self.TopMenu.btn_createVariableApplyGain.clicked.connect(lambda: self.make_tab_apply_gain_cv())
        self.TopMenu.btn_createVariableBinning.clicked.connect(lambda: self.make_tab_binning_cv())
        self.TopMenu.btn_createVariableCombineColumns.clicked.connect(lambda: self.make_tab_combine_columns_cv())
        self.TopMenu.btn_createVariableDefineSeasons.clicked.connect(lambda: self.make_tab_define_seasons())
        self.TopMenu.btn_createVariableLagFeatures.clicked.connect(lambda: self.make_tab_lag_features_cv())
        self.TopMenu.btn_createVariableTimeSince.clicked.connect(lambda: self.make_tab_timesince_cv())

        # Outlier Detection
        self.TopMenu.btn_outlierDetAbsLim.clicked.connect(lambda: self._addtab_od_abslim())
        self.TopMenu.btn_outlierDetDouble_Diff.clicked.connect(lambda: self._addtab_od_doublediff())
        self.TopMenu.btn_outlierDetHampelFilter.clicked.connect(lambda: self.make_tab_hampel_od())
        self.TopMenu.btn_outlierDetIQR.clicked.connect(lambda: self.make_tab_iqr_od())
        self.TopMenu.btn_outlierRunning.clicked.connect(lambda: self.make_tab_running_od())
        self.TopMenu.btn_outlierTrim.clicked.connect(lambda: self.make_tab_trim_od())

        # Eddy Covariance
        self.TopMenu.btn_EddyCovarianceQualityControl.clicked.connect(lambda: self.make_tab_ec_quality_control())

        # Export
        self.TopMenu.btn_exportExportDataset.clicked.connect(lambda: self.export_dataset())

        # CONNECTIONS CONTROLS
        # --------------------
        # And connect click on variable in list w/ function
        self.lst_varlist.itemClicked.connect(lambda: self.set_focus(action='load_file_data', marker_col='none'))
        # Tabs
        self.TabWidget.tabCloseRequested.connect(self.TabWidget.closeTab)
        # Time range
        self.obj_time_range_controls.btn_apply.clicked.connect(self._timerange_apply)
        self.obj_time_range_controls.btn_reset.clicked.connect(self._timerange_reset)

        # >> SHORTCUTS
        # --------------------------------
        # For quick testing, data and / or method tabs can be directly loaded upon script start
        self.load_initial_data = load_initial_data
        if self.load_initial_data:
            self.select_source(action='load_initial_data')
            # When data are loaded, select the first var automatically
            self.set_focus(action='load_file_data', marker_col='')
            # self.plot_focus(fig=-9999, plot_type='OVERVIEW')

            # Shortcut: starting tab
            self._addtab_od_abslim()
            # self._addtab_od_doublediff()
            # self.make_tab_random_forest()
            # self.make_tab_binning_cv()
            # self.make_tab_aggregator()
            # self.make_tab_multipanel_plots()
            # self.make_tab_timesince_cv()
            # self.make_tab_ec_quality_control()

    def _timerange_apply(self):
        """
        Shorten data to selected time range.

        The time range *includes* the selected start and end dates.
        """
        start_dt = dt.datetime(year=int(self.obj_time_range_controls.drp_year_start.currentText()),
                               month=int(self.obj_time_range_controls.drp_month_start.currentText()),
                               day=int(self.obj_time_range_controls.drp_day_start.currentText()))
        end_dt = dt.datetime(year=int(self.obj_time_range_controls.drp_year_end.currentText()),
                             month=int(self.obj_time_range_controls.drp_month_end.currentText()),
                             day=int(self.obj_time_range_controls.drp_day_end.currentText()))

        # Limit for plots' x-axes
        # For xlim, one day has to be added to include also the last selected day
        self.limit_plot_xlim = [start_dt, end_dt + dt.timedelta(days=1)]

        # Also generate a filter for the df (not always needed)
        self.limit_timerange_filter_dt = (self.data_df.index >= start_dt) & (self.data_df.index <= end_dt)

        # Plot focus variable time focus_series
        self.make_overview_plots(fig=self.plt_figure)

    def _timerange_reset(self):
        """
        Reset x-axis limit in plots to full data range.
        """
        self.limit_plot_xlim = False
        self.limit_timerange_filter_dt = False

        # Plot focus variable time focus_series
        self.make_overview_plots(fig=self.plt_figure)

    def filter_var_list(self):
        # self.lst_Variables.row.clear()
        filter_txt = self.lne_filter_varlist.text()

        for row in range(self.lst_varlist.count()):
            self.lst_varlist.setRowHidden(row, False)

        if filter_txt != '':
            for row in range(self.lst_varlist.count()):
                # row_txt = str(self.lst_Variables.item(row).text())
                row_tuple = self.col_dict_tuples[row]
                search_term = '*{}*'.format(filter_txt)
                # search_terms = filter_txt.split(' ')

                # Search for term in variable names and units
                if fnmatch.filter(row_tuple, search_term):
                    self.lst_varlist.setRowHidden(row, False)
                else:
                    self.lst_varlist.setRowHidden(row, True)
        log(name=self.filter_var_list.__name__, dict={'filtering for ': filter_txt}, highlight=False)

    # def filter_var_list(self):
    #     """Alternative search with SequenceMatcher"""
    #     # self.lst_Variables.row.clear()
    #     filter_txt = self.lne_FilterVarList.text()
    #
    #     for row in range(self.lst_Variables.count()):
    #         self.lst_Variables.setRowHidden(row, False)
    #
    #     similarities = []
    #     if filter_txt != '':
    #         for row in range(self.lst_Variables.count()):
    #             # row_txt = str(self.lst_Variables.item(row).text())
    #             row_tuple = self.col_dict_tuples[row]
    #             search_term = filter_txt
    #             # search_term = '*{}*'.format(filter_txt)
    #             # search_terms = filter_txt.split(' ')
    #             similarity = SequenceMatcher(None, row_tuple[0], search_term).ratio()
    #
    #             # Search for term in variable names and units
    #             if similarity > 0.5:
    #                 self.lst_Variables.setRowHidden(row, False)
    #             else:
    #                 self.lst_Variables.setRowHidden(row, True)
    #     log(name=self.filter_var_list.__name__, dict={'filtering for ': filter_txt}, highlight=False)

    def _load_initial_data(self):
        source_files_list = [self.initial_data]
        self.load_initial_data = False
        return source_files_list

    def get_source_files_list(self, action):
        # Set path to source file
        source_files_list = []

        # if action == 'load_initial_data':
        #     source_files_list = [self.initial_data]
        #     self.load_initial_data = False

        if (action == 'new_file') | (action == 'add_to_file'):
            # dir with data files
            try:
                source_files_list = dfun.files.select_source_files(start_dir=self.dir_select_files)
                # Convert strings in list to Path
                if source_files_list:  # Check if empty
                    self.dir_select_files = str(pathlib.Path(source_files_list[0]).parent)  # Remember for next time
                    for ix, source_file in enumerate(source_files_list):
                        source_files_list[ix] = pathlib.Path(source_file)
            except OSError as e:
                print(e)
        return source_files_list

    def create_data_df_from_files(self, action, source_files_list):

        all_data_df = pd.DataFrame()
        num_files = len(source_files_list)  # Read incoming data to df

        # Log info
        suffix = 's' if num_files > 1 else ''  # Plural 's'
        log(name=f">>> Starting Data Import From {num_files} File{suffix}", dict={}, highlight=True)
        tic = time.time()  # Timer

        skipped_files_list = []
        generated_missing_header_cols_list = []
        for ix, file in enumerate(source_files_list):

            try:
                if self.filetype_config['GENERAL']['NAME'] != 'Events':
                    self.incoming_data_df, generated_missing_header_cols_list = \
                        dfun.files.read_selected_data_file(filepath=file,
                                                           filetype_config=self.filetype_config)
                else:
                    self.incoming_data_df = dfun.files.read_selected_events_file(filepath=file,
                                                                                 settings_dict=self.filetype_config,
                                                                                 index_data_df=self.data_df.index)

                # NEW OR INITIAL DATA
                # -------------------
                # If incoming data is the first data, i.e. if new file was selected or
                # if the initial data after app start is loaded, set the target frequency
                # for all following files based on frequency of incoming data. This is
                # important b/c later on the user might want to add other data files to
                # the initially loaded data, and then the other data files are *currently*
                # resampled to this target freq.
                if (action == 'load_initial_data') | ((action == 'new_file') & (ix == 0)):
                    # Time resolution of the first/newly loaded file
                    self.first_file_freq = self.filetype_config['DATA']['FREQUENCY']
                    all_data_df = self.incoming_data_df.copy()  # Data of the first/newly loaded file

                # ADDITIONAL DATA OR NEW DATA FROM MULTIPLE FILES
                # -----------------------------------------------
                # When other data are added to the already loaded data, then both must have
                # the same time resolution. Incoming data are therefore resampled to the
                # target frequency. If the incoming data already has the same freq, then no
                # resampling is necessary.
                elif (action == 'add_to_file') | ((action == 'new_file') & (ix > 0)):
                    incoming_freq = self.filetype_config['DATA']['FREQUENCY']
                    if incoming_freq != self.first_file_freq:
                        self.incoming_data_df = self.incoming_data_df.apply(self.first_file_freq).mean()
                    else:
                        pass

                    # Now that incoming data has target freq, it can be merged with already
                    # loaded data, i.e. add incoming data to all data.

                    # Add to already available data, outside method (self.data_df)
                    if (action == 'add_to_file') & (ix == 0):
                        all_data_df = self.data_df  # Only needed for first file that is added
                    # Add to already available data, inside method (all_data_df already available)
                    elif (action == 'new_file'):
                        pass

                    try:
                        all_data_df = all_data_df.combine_first(self.incoming_data_df)
                    except ValueError:
                        # In case there are no overlapping column names, combine_first generates
                        # an error. In this case we use outer join instead.
                        all_data_df = all_data_df.join(self.incoming_data_df, how='outer')

                    # Make sure that the merged df has the correct time resolution
                    if self.filetype_config['DATA']['FREQUENCY'] != '-none-':
                        all_data_df = all_data_df.asfreq(self.first_file_freq)

                toc = time.time() - tic
                size_data_df = self.incoming_data_df.shape
                log(name=f"+ Imported Data From File {ix + 1} / {num_files}",
                    dict={'Source': file,
                          'Action': action,
                          'Rows': size_data_df[0],
                          'Columns': size_data_df[1],
                          'Time Needed': f"{toc:.3f}s",
                          'Missing column headers': f'{len(generated_missing_header_cols_list)}'},
                    highlight=False)

            except ValueError as e:
                print("".format(file))
                traceback.print_exc()
                skipped_files_list.append(file)

                log(name=f"(!)ERROR while working on file {ix + 1} / {num_files} (file was skipped)",
                    dict={'Source': file,
                          'Action': action,
                          'Error': e,
                          'Recommendation': 'Check if File Settings are correct for this file.'}, highlight=False)

        return all_data_df

    def select_source(self, action):
        """
        Create main DataFrame from file(s) or update current DataFrame
        """
        if action == 'update_current':
            pass
        elif action == 'load_initial_data':
            source_files_list = self._load_initial_data()
            self.data_df = self.create_data_df_from_files(action=action, source_files_list=source_files_list)
        elif action == 'add_to_file':
            source_files_list = self.get_source_files_list(action=action)
            if source_files_list:  # Check if there is something in the list
                self.data_df = self.create_data_df_from_files(action=action, source_files_list=source_files_list)
        elif action == 'new_file':
            source_files_list = self.get_source_files_list(action=action)
            if source_files_list:  # Check if there is something in the list
                self.data_df = self.create_data_df_from_files(action=action, source_files_list=source_files_list)

        # else:
        #     source_files_list = self.get_source_files_list(action=action)
        #     if source_files_list:  # Check if there is something in the list
        #         self.data_df = self.create_data_df_from_files(action=action, source_files_list=source_files_list)

        # # Set default file type
        # self.settings_dict = SelectFileTypeSettings.read_settings_from_file(filepath=self.ctx.filetype_EDDYPRO_FULL_OUTPUT_30MIN)

        self.data_df = dfun.frames.sort_multiindex_columns_names_LEGACY(df=self.data_df,
                                                                        priority_vars=utils.vargroups.PRIORITY_VARS)
        self._update_gui_lists()
        self.original_data_df = self.data_df.copy()

    def select_outdir(self):
        """ Get instance variable. """
        self.project_outdir = dfun.files.select_output_dir(start_dir=self.ctx.dir_script,
                                                           rundir_name=self.project_rundir)
        # stats_df needs update b/c output dir was changed
        self.focus_stats_df = utils.insert.AssembleStats(prev_stats_df=self.focus_stats_df,
                                                         run_id=self.run_id,
                                                         data_df=self.data_df,
                                                         series=self.data_df[self.focus_col],
                                                         project_outdir=self.project_outdir,
                                                         filetype_config=self.filetype_config).get()
        # Stats in separate df
        self.focus_stats_df = self.focus_stats_df.round(2)
        utils.insert.statsboxes_contents(self=self)

    def file_settings(self):
        # Displays dialog for file settings_dict
        obj = SelectFileTypeSettings(dir_filetypes=self.ctx.filetype_dir)
        # obj = gui.dialog_windows.SelectFileTypeSettings.SelectFileTypeSettings(dir_filetypes=self.ctx.filetype_dir)

        if obj.exec_():
            configfilepath = obj.get_filepath()

            self.filetype_config = ConfigFileReader(configfilepath=configfilepath).read()
            # self.filetype_config = dfun.files.parse_settingsfile_todict(filepath=filepath)

        # stats_df needs update b/c file type was changed
        self.focus_stats_df = utils.insert.AssembleStats(prev_stats_df=self.focus_stats_df,
                                                         run_id=self.run_id,
                                                         data_df=self.data_df,
                                                         series=self.data_df[self.focus_col],
                                                         project_outdir=self.project_outdir,
                                                         filetype_config=self.filetype_config).get()
        # Stats in separate df
        self.focus_stats_df = self.focus_stats_df.round(2)
        utils.insert.statsboxes_contents(self=self)

    def limit_dataset_timerange(self):
        # Displays dialog for dataset time range
        obj = md.limit_timerange.PopupWindow(df=self.data_df, ctx=self.ctx)
        if obj.exec_():
            self.data_df = md.limit_timerange.Run(obj=obj).apply_limit()
            # Update data_df plots, stats
            self.select_source(action='update_current')
            self.set_focus(action='load_file_data', marker_col='')

    def create_event(self):
        # Displays dialog for dataset time range
        obj = cv.add_new_event.PopupWindow(df=self.data_df, ctx=self.ctx)
        if obj.exec_():
            self.data_df = cv.add_new_event.Run(obj=obj).make_new_event_col()
            # Update data_df plots, stats
            self.select_source(action='update_current')
            self.set_focus(action='load_file_data', marker_col='')

    def export_dataset(self):
        # Displays dialog for dataset time range
        obj = ex.export_dataset.PopupWindow(df=self.data_df, ctx=self.ctx,
                                            first_file_freq=self.first_file_freq,
                                            project_outdir=self.project_outdir,
                                            run_id=self.run_id)
        obj.ref_drp_freq.currentTextChanged.connect(lambda: obj.update_fields())
        obj.ref_drp_agg.currentTextChanged.connect(lambda: obj.update_fields())
        if obj.exec_():
            ex.export_dataset.Run(obj=obj).export()
            # Update data_df plots, stats
            # self.select_source(action='update_current')
            # self.set_focus(action='load_file_data', marker_col='')


    def _update_timerange_controls(self):
        """Update time range controls for visible time range"""

        self.obj_time_range_controls.drp_year_start.clear()  # Time range controls
        self.obj_time_range_controls.drp_month_start.clear()
        self.obj_time_range_controls.drp_day_start.clear()
        self.obj_time_range_controls.drp_year_end.clear()
        self.obj_time_range_controls.drp_month_end.clear()
        self.obj_time_range_controls.drp_day_end.clear()

        # Visible time range
        first_datetime = self.data_df.index[0]
        last_datetime = self.data_df.index[-1]

        # Years
        unique = list(self.data_df.index.year.unique())
        unique = [str(u) for u in unique]  # dropdown needs string items

        self.obj_time_range_controls.drp_year_start.addItems(unique)
        index = self.obj_time_range_controls.drp_year_start.findText(str(first_datetime.year), qtc.Qt.MatchFixedString)
        self.obj_time_range_controls.drp_year_start.setCurrentIndex(index)

        self.obj_time_range_controls.drp_year_end.addItems(unique)
        index = self.obj_time_range_controls.drp_year_end.findText(str(last_datetime.year), qtc.Qt.MatchFixedString)
        self.obj_time_range_controls.drp_year_end.setCurrentIndex(index)

        # Months
        unique = list(self.data_df.index.month.unique())
        unique = [str(u) for u in unique]

        self.obj_time_range_controls.drp_month_start.addItems(unique)
        index = self.obj_time_range_controls.drp_month_start.findText(str(first_datetime.month),
                                                                      qtc.Qt.MatchFixedString)
        self.obj_time_range_controls.drp_month_start.setCurrentIndex(index)

        self.obj_time_range_controls.drp_month_end.addItems(unique)
        index = self.obj_time_range_controls.drp_month_end.findText(str(last_datetime.month), qtc.Qt.MatchFixedString)
        self.obj_time_range_controls.drp_month_end.setCurrentIndex(index)

        # Days
        unique = list(self.data_df.index.day.unique())
        unique = [str(u) for u in unique]

        self.obj_time_range_controls.drp_day_start.addItems(unique)
        index = self.obj_time_range_controls.drp_day_start.findText(str(first_datetime.day), qtc.Qt.MatchFixedString)
        self.obj_time_range_controls.drp_day_start.setCurrentIndex(index)

        self.obj_time_range_controls.drp_day_end.addItems(unique)
        index = self.obj_time_range_controls.drp_day_end.findText(str(last_datetime.day), qtc.Qt.MatchFixedString)
        self.obj_time_range_controls.drp_day_end.setCurrentIndex(index)

    def _update_gui_lists(self, log_output=False):
        """Updates main variables list and time range controls"""
        self.data_df, \
        self.lst_varlist, \
        self.col_dict_tuples, \
        self.col_list_pretty = \
            gui.lists.update_varlist(df=self.data_df, ctx=self.ctx, lst_varlist=self.lst_varlist)

        self._update_timerange_controls()

        if log_output:
            log(name='Variables were updated.', dict={}, highlight=False)  # Log info

    def set_focus(self, action, marker_col):
        """
        Executed after Apply is pressed
        Sets up focus_df
        rejectval_col is used to define which column in focus_df is used to
            - remove values from measured_col (rejectval_col is a boolean filter)
            - add values to measured_col (rejectval_col contains values)
        """

        # Check what is selected in list
        if action == 'load_file_data':
            """
            Whenever a variable is selected from the list, a new df is created that
            contains data from the selected variable. The column name of the selected
            variable name is measured_col. When a variable is selected from the list,
            data for the respective measured_col is copied to the separate df focus_df. 
            The dataframe focus_df is used to work w/ the focus data.
            """

            # Get index of selected variable (index in variable list)
            # The selected var is the 'focus' variable
            self.focus_col_orig = self.lst_varlist.selectedIndexes()
            if marker_col == '':
                selected_var_ix = 0
            else:
                selected_var_ix = self.focus_col_orig[0].row()

            # Get correct column name of selected var
            self.focus_col_orig = self.col_dict_tuples[selected_var_ix]

            # !Currently not used here:
            # Define new name for selected var, simply add a dot to the var name,
            # this way we know from the name that the variable was edited.
            focus_col_renamed = (self.focus_col_orig[0], self.focus_col_orig[1])
            # focus_col_renamed = ('.' + self.focus_col_orig[0], self.focus_col_orig[1])

            # Create new, separate df that is used to edit data: focus_df
            self.focus_df = pd.DataFrame(index=self.data_df.index, columns=[focus_col_renamed])

            # Insert data for the selected var in focus_df
            self.focus_df[focus_col_renamed] = self.data_df[self.focus_col_orig]

            # Set focus to the renamed var name, this is the data we are working with
            self.focus_col = focus_col_renamed

            # Whenever a variable is selected from the sidebar, reset stats_df
            self.focus_stats_df = utils.insert.AssembleStats(prev_stats_df=self.focus_stats_df,
                                                             run_id=self.run_id,
                                                             data_df=self.data_df,
                                                             series=self.data_df[self.focus_col],
                                                             project_outdir=self.project_outdir,
                                                             filetype_config=self.filetype_config).get()

        # stats in separate df
        self.focus_stats_df = self.focus_stats_df.round(2)
        utils.insert.statsboxes_contents(self=self)

        # Info to gui
        # Show in gui the current focus and what is currently in the df (only the focus var)
        label, unit = self.focus_col  # split tuple info into two separate vars
        focus = '{} {}'.format(label, unit)
        self.lbl_focus_varname.setText(focus)

        # Plot focus variable time focus_series
        self.make_overview_plots(fig=self.plt_figure)

    def make_tab_wind_sectors(self):
        """ Make new tab and store current data_df in instance. """
        obj = pl.windsectors.Run(app_obj=self,
                                 title='Wind Sectors',
                                 tab_id=gui.base.buildTab.find_free_tab_id(self.TabWidget, tab_ids=['Wind Sectors']))
        obj.lst_varlist_available.itemClicked.connect(lambda: obj.add_var_to_selected())
        obj.lst_varlist_selected.itemClicked.connect(lambda: obj.remove_var_from_selected())
        obj.btn_export.clicked.connect(lambda: obj.export())
        obj.btn_apply.clicked.connect(lambda: obj.apply())

    def make_tab_histogram(self):
        """ Make new tab and store current data_df in instance. """
        obj = pl.histogram.Run(app_obj=self,
                               title='Histogram',
                               tab_id=gui.base.buildTab.find_free_tab_id(self.TabWidget, tab_ids=['Histogram']))
        obj.lst_varlist_available.itemClicked.connect(lambda: obj.add_var_to_selected())
        obj.lst_varlist_selected.itemClicked.connect(lambda: obj.remove_var_from_selected())
        obj.btn_apply.clicked.connect(lambda: obj.apply())

    def make_tab_hexbins(self):
        """Make new tab and store current data_df in instance"""
        obj = pl.hexbins.Run(app_obj=self,
                             title='Hexbins',
                             tab_id=gui.base.buildTab.find_free_tab_id(self.TabWidget, tab_ids=['Hexbins']))
        obj.btn_show_in_plot.clicked.connect(lambda: obj.plot_hexbins())

    def make_tab_quantile_plots(self):
        """Make new tab and store current data_df in instance"""
        obj = pl.quantiles.Run(app_obj=self,
                               title='Quantiles',
                               tab_id=gui.base.buildTab.find_free_tab_id(self.TabWidget, tab_ids=['Quantiles']))
        obj.lst_varlist_available.itemClicked.connect(lambda: obj.add_var_to_selected())
        obj.lst_varlist_selected.itemClicked.connect(lambda: obj.remove_var_from_selected())
        obj.btn_apply.clicked.connect(lambda: obj.apply())

    def make_tab_cumulative_plots(self):
        """Make new tab and store current data_df in instance"""
        obj = pl.cumulative.Run(app_obj=self,
                                title='Cumulative',
                                tab_id=gui.base.buildTab.find_free_tab_id(self.TabWidget, tab_ids=['Cumulative']))
        obj.lst_varlist_available.itemClicked.connect(lambda: obj.add_var_to_selected())
        obj.lst_varlist_selected.itemClicked.connect(lambda: obj.remove_var_from_selected())
        obj.btn_show_in_plot.clicked.connect(lambda: obj.plot_cumulative_line())

    def make_tab_diel_cycle_plots(self):
        """Make new tab and store current data_df in instance"""
        obj = pl.diel_cycles.Run(app_obj=self,
                                 title='Diel Cycles',
                                 tab_id=gui.base.buildTab.find_free_tab_id(self.TabWidget, tab_ids=['Diel Cycles']))
        obj.lst_varlist_available.itemClicked.connect(lambda: obj.add_var_to_selected())
        obj.lst_varlist_selected.itemClicked.connect(lambda: obj.remove_var_from_selected())
        obj.btn_show_in_plot.clicked.connect(lambda: obj.plot_diel_cycles())

    def make_tab_heatmap_plots(self):
        """Make new tab and store current data_df in instance"""
        obj = pl.heatmap.Run(app_obj=self, title='Heatmap',
                             tab_id=gui.base.buildTab.find_free_tab_id(self.TabWidget, tab_ids=['Heatmap']),
                             version_info=self.ctx.build_settings['version'])
        obj.lst_varlist_available.itemClicked.connect(lambda: obj.make_triplet())
        # obj.lst_varlist_available.itemClicked.connect(lambda: obj.plot_heatmap_triplet())
        obj.btn_save_triplet_png.clicked.connect(lambda: obj.save_heatmap_triplet())
        obj.btn_save_borderless.clicked.connect(lambda: obj.save_borderless_heatmap())

    def make_tab_trim_od(self):
        """Make new tab and store current data_df in instance"""
        obj = od_old.trim.Run(app_obj=self, title='Trim',
                              tab_id=gui.base.buildTab.find_free_tab_id(self.TabWidget, tab_ids=['Trim']))
        obj.lst_varlist_available.itemClicked.connect(lambda: obj.update_target())
        obj.btn_add_timerange.clicked.connect(lambda: obj.add_to_rangelist())
        obj.lst_rangelist_selected.itemClicked.connect(lambda: obj.remove_from_rangelist())
        obj.btn_calc.clicked.connect(lambda: obj.calc())
        obj.btn_add_as_new_var.clicked.connect(lambda: self.get_results_trim_od(obj=obj))

    def get_results_trim_od(self, obj):
        self.data_df = obj.get_selected(main_df=self.data_df.copy())
        self.data_df = dfun.frames.sort_multiindex_columns_names_LEGACY(df=self.data_df,
                                                                        priority_vars=utils.vargroups.PRIORITY_VARS + utils.vargroups.GAPFILLED_GENERAL_SCALARS)
        self._update_gui_lists()

    def make_tab_remove_time_range(self):
        """Make new tab and store current data_df in instance"""
        obj = md.remove_timerange.Run(app_obj=self,
                                      title='Remove Time Range',
                                      tab_id=gui.base.buildTab.find_free_tab_id(self.TabWidget,
                                                                                tab_ids=[
                                                                                    'Remove Time Range']))
        obj.lst_varlist_available.itemClicked.connect(lambda: obj.update_target())
        obj.btn_add_timerange.clicked.connect(lambda: obj.add_to_rangelist())
        obj.lst_rangelist_selected.itemClicked.connect(lambda: obj.remove_from_rangelist())
        obj.btn_preview.clicked.connect(lambda: obj.preview())
        obj.btn_add_as_new_var.clicked.connect(lambda: self.get_results_remove_time_range(obj=obj))

    def get_results_remove_time_range(self, obj):
        self.data_df = obj.get_selected(main_df=self.data_df.copy())
        self.data_df = dfun.frames.sort_multiindex_columns_names_LEGACY(df=self.data_df,
                                                                        priority_vars=utils.vargroups.PRIORITY_VARS + utils.vargroups.GAPFILLED_GENERAL_SCALARS)
        self._update_gui_lists()

    def make_tab_scatter_plot(self):
        """Make new tab and store current data_df in instance"""
        obj = pl.scatter.Run(app_obj=self,
                             title='Scatter',
                             tab_id=gui.base.buildTab.find_free_tab_id(self.TabWidget, tab_ids=['Scatter']))
        obj.btn_show_in_plot.clicked.connect(lambda: obj.plot_scatter())

    def make_tab_multipanel_plots(self):
        """Make new tab and store current data_df in instance"""
        obj = pl.multipanel.Run(app_obj=self,
                                title='Multipanel',
                                tab_id=gui.base.buildTab.find_free_tab_id(self.TabWidget, tab_ids=['Multipanel']))
        obj.lst_varlist_available.itemClicked.connect(lambda: obj.add_var_to_selected())
        obj.lst_varlist_selected.itemClicked.connect(lambda: obj.remove_var_from_selected())
        obj.btn_apply.clicked.connect(lambda: obj.apply())

    def make_tab_correlation_matrix(self):
        """Make new tab and store current data_df in instance"""
        obj = pl.correlation_matrix.Run(app_obj=self,
                                        title='Corr Matrix',
                                        tab_id=gui.base.buildTab.find_free_tab_id(self.TabWidget,
                                                                                  tab_ids=['Corr Matrix']))
        obj.lst_varlist_available.itemClicked.connect(lambda: obj.add_var_to_selected())
        obj.lst_varlist_selected.itemClicked.connect(lambda: obj.remove_var_from_selected())
        obj.btn_apply.clicked.connect(lambda: obj.apply())
        obj.btn_export.clicked.connect(lambda: obj.export())

    def make_tab_aggregator(self):
        """Make new tab and store current data_df in instance"""
        obj = an.aggregator.Run(app_obj=self,
                                title='Aggregator',
                                tab_id=gui.base.buildTab.find_free_tab_id(self.TabWidget, tab_ids=['Aggregator']))
        obj.btn_run.clicked.connect(lambda: obj.run())

    def make_tab_ec_quality_control(self):
        """Make new tab and store current data_df in instance"""
        obj = ec.qualitycontrol.Run(app_obj=self,
                                    title='Quality Control',
                                    tab_id=gui.base.buildTab.find_free_tab_id(self.TabWidget,
                                                                              tab_ids=['Quality Control']))
        obj.lst_varlist_available.itemClicked.connect(lambda: obj.add_target())
        obj.lst_varlist_selected.itemClicked.connect(lambda: obj.remove_target())

        obj.drp_abslim_flag_col.currentIndexChanged.connect(lambda: obj.update_abslim(init=False))
        obj.drp_completeness_flag_col.currentIndexChanged.connect(lambda: obj.update_completeness())
        obj.drp_rawdata_spikes_statflag.currentIndexChanged.connect(lambda: obj.update_rawdata_spikes())
        obj.drp_rawdata_amplres_statflag_col.currentIndexChanged.connect(lambda: obj.update_rawdata_amplres())
        obj.drp_rawdata_dropout_statflag_col.currentIndexChanged.connect(lambda: obj.update_rawdata_dropout())
        obj.drp_rawdata_abslim_statflag_col.currentIndexChanged.connect(lambda: obj.update_rawdata_abslim())
        obj.drp_rawdata_skewkurt_hf_statflag_col.currentIndexChanged.connect(lambda: obj.update_rawdata_skewkurt_hf())
        obj.drp_rawdata_skewkurt_sf_statflag_col.currentIndexChanged.connect(lambda: obj.update_rawdata_skewkurt_sf())
        obj.drp_rawdata_discont_hf_statflag_col.currentIndexChanged.connect(lambda: obj.update_rawdata_discont_hf())
        obj.drp_rawdata_discont_sf_statflag_col.currentIndexChanged.connect(lambda: obj.update_rawdata_discont_sf())
        obj.drp_rawdata_aa_statflag_col.currentIndexChanged.connect(lambda: obj.update_rawdata_attack_angle())
        obj.drp_rawdata_nonsteadyu_statflag_col.currentIndexChanged.connect(lambda: obj.update_rawdata_nonsteadyu())

        obj.btn_calc.clicked.connect(lambda: obj.calc())
        obj.btn_uncheck_all.clicked.connect(lambda: obj.uncheck_all_chk())
        obj.btn_check_all.clicked.connect(lambda: obj.check_all_chk())
        obj.btn_add_as_new_var.clicked.connect(lambda: self.get_results_ec_quality_control(obj=obj))
        obj.btn_save_to_txt_file.clicked.connect(lambda: obj.save_to_txt_file())

    def get_results_ec_quality_control(self, obj):
        self.data_df = obj.get_selected(main_df=self.data_df.copy())
        self.data_df = dfun.frames.sort_multiindex_columns_names_LEGACY(df=self.data_df,
                                                                        priority_vars=utils.vargroups.PRIORITY_VARS + utils.vargroups.GAPFILLED_GENERAL_SCALARS)
        self._update_gui_lists()

    # def make_tab_ec_offseason_uptake_corr(self):
    #     """Make new tab and store current data_df in instance"""
    #     obj = modboxes.eddycovariance._future.selfheating_corr_op_irga.Run(app_obj=self,
    #                                                                        title='Off-season Uptake Correction',
    #                                                                        tab_id=gui.tabs.buildTab.find_free_tab_id(self.TabWidget,
    #                                                                                     tab_ids=[
    #                                                                                         'Off-season Uptake Correction']))
    #     obj.lst_varlist_target.itemClicked.connect(lambda: obj.select_target())
    #     obj.lst_varlist_reference.itemClicked.connect(lambda: obj.select_reference())
    #     obj.btn_optimize.clicked.connect(lambda: obj.optimize())
    #     obj.btn_apply.clicked.connect(lambda: obj.apply_correction_fraction())
    #     obj.btn_load_cf_from_file.clicked.connect(lambda: obj.load_cf_from_file())
    #     obj.btn_save_cf_to_file.clicked.connect(lambda: obj.save_cf_to_file())
    #     obj.btn_add_as_new_var.clicked.connect(lambda: self.get_results_ec_offseason_uptake_corr(obj=obj))

    def get_results_ec_offseason_uptake_corr(self, obj):
        self.data_df = obj.get_selected(main_df=self.data_df.copy())
        self.data_df = dfun.frames.sort_multiindex_columns_names_LEGACY(df=self.data_df,
                                                                        priority_vars=utils.vargroups.PRIORITY_VARS + utils.vargroups.GAPFILLED_GENERAL_SCALARS)
        self._update_gui_lists()

    # def make_tab_stl_od(self):
    #     """Make new tab and store current data_df in instance"""
    #     obj = modboxes.outlierdetection._future.seasonal_trend_loess.Run(app_obj=self,
    #                                                                      title='STL',
    #                                                                      tab_id=gui.tabs.buildTab.find_free_tab_id(self.TabWidget,
    #                                                                                 tab_ids=['STL']))
    #     obj.lst_varlist_available.itemClicked.connect(lambda: obj.add_target())
    #     obj.btn_calc.clicked.connect(lambda: obj.calc())
    #     obj.btn_add_as_new_var.clicked.connect(lambda: self.get_results_stl_od(obj=obj))

    def get_results_stl_od(self, obj):
        self.data_df = obj.get_selected(main_df=self.data_df.copy())
        self.data_df = dfun.frames.sort_multiindex_columns_names_LEGACY(df=self.data_df,
                                                                        priority_vars=utils.vargroups.PRIORITY_VARS + utils.vargroups.GAPFILLED_GENERAL_SCALARS)
        self._update_gui_lists()

    def make_tab_combine_columns_cv(self):
        """Make new tab and store current data_df in instance"""
        obj = cv.combine_columns.Run(app_obj=self,
                                     title='Combine Columns',
                                     tab_id=gui.base.buildTab.find_free_tab_id(self.TabWidget,
                                                                               tab_ids=['Combine Columns']))
        obj.lst_varlist_selected_1.itemClicked.connect(lambda: obj.update_var_1())
        obj.lst_varlist_selected_2.itemClicked.connect(lambda: obj.update_var_2())
        obj.lst_varlist_selected_3.itemClicked.connect(lambda: obj.calc())
        obj.btn_varlist_3.clicked.connect(lambda: obj.continue_combination())
        obj.btn_add_as_new_var.clicked.connect(lambda: self.get_results_combine_columns(obj=obj))

    def get_results_combine_columns(self, obj):
        self.data_df = obj.get_selected(main_df=self.data_df.copy())
        self.data_df = dfun.frames.sort_multiindex_columns_names_LEGACY(df=self.data_df,
                                                                        priority_vars=utils.vargroups.PRIORITY_VARS + utils.vargroups.GAPFILLED_GENERAL_SCALARS)
        self._update_gui_lists()

    def make_tab_class_finder(self):
        """Make new tab and store current data_df in instance"""
        obj = an.classfinder.Run(app_obj=self,
                                 title='Class Finder',
                                 tab_id=gui.base.buildTab.find_free_tab_id(self.TabWidget,
                                                                           tab_ids=['Class Finder']))
        obj.lst_varlist_available.itemClicked.connect(lambda: obj.add_target())
        obj.lst_varlist_selected.itemClicked.connect(lambda: obj.remove_target())
        obj.btn_calc.clicked.connect(lambda: obj.calc())
        obj.btn_keep_marked.clicked.connect(lambda: obj.keep_marked())
        obj.btn_remove_marked.clicked.connect(lambda: obj.remove_marked())
        obj.btn_add_as_new_var.clicked.connect(lambda: self.get_results_class_finder(obj=obj))

    def get_results_class_finder(self, obj):
        self.data_df = obj.get_selected(main_df=self.data_df.copy())
        self.data_df = dfun.frames.sort_multiindex_columns_names_LEGACY(df=self.data_df,
                                                                        priority_vars=utils.vargroups.PRIORITY_VARS + utils.vargroups.GAPFILLED_GENERAL_SCALARS)
        self._update_gui_lists()

    def make_tab_subset(self):
        """Make new tab and store current data_df in instance"""
        obj = md.subset.Run(app_obj=self,
                            title='Subset',
                            tab_id=gui.base.buildTab.find_free_tab_id(self.TabWidget, tab_ids=['Subset']))
        obj.lst_varlist_available.itemClicked.connect(lambda: obj.add_col())
        obj.lst_varlist_selected.itemClicked.connect(lambda: obj.remove_col())
        obj.btn_add_all_vars.clicked.connect(lambda: obj.add_all_to_selected())
        obj.btn_remove_all_vars.clicked.connect(lambda: obj.remove_all_from_selected())
        obj.btn_make_new_subset.clicked.connect(lambda: self.get_results_subset(obj=obj))

    def get_results_subset(self, obj):
        self.data_df = obj.get_selected()
        self.data_df = dfun.frames.sort_multiindex_columns_names_LEGACY(df=self.data_df,
                                                                        priority_vars=utils.vargroups.PRIORITY_VARS + utils.vargroups.GAPFILLED_GENERAL_SCALARS)
        self._update_gui_lists()

    # def make_tab_extreme_events_finder(self):
    #     """Make new tab and store current data_df in instance"""
    #     obj = modboxes.analyses._future._extreme_events_finder.Run(app_obj=self,
    #                                                                title='ExtrEventsFinder',
    #                                                                tab_id=gui.tabs.buildTab.find_free_tab_id(self.TabWidget,
    #                                                                                   tab_ids=[
    #                                                                                       'ExtrEventsFinder']))
    #     obj.lst_varlist_available.itemClicked.connect(lambda: obj.add_target())
    #     obj.lst_varlist_selected.itemClicked.connect(lambda: obj.remove_target())
    #     # obj.btn_calc.clicked.connect(lambda: obj.calc())
    #     # obj.btn_keep_marked.clicked.connect(lambda: obj.keep_marked())
    #     # obj.btn_remove_marked.clicked.connect(lambda: obj.remove_marked())
    #     obj.btn_add_as_new_var.clicked.connect(lambda: self.get_results_class_finder(obj=obj))

    def get_results_extreme_events_finder(self, obj):
        self.data_df = obj.get_selected(main_df=self.data_df.copy())
        self.data_df = dfun.frames.sort_multiindex_columns_names_LEGACY(df=self.data_df,
                                                                        priority_vars=utils.vargroups.PRIORITY_VARS + utils.vargroups.GAPFILLED_GENERAL_SCALARS)
        self._update_gui_lists()

    def make_tab_lookup_table_gf(self):
        """Make new tab and store current data_df in instance."""
        obj = gf.lookup_table.Run(app_obj=self,
                                  title='Look-up Table',
                                  tab_id=gui.base.buildTab.find_free_tab_id(self.TabWidget,
                                                                            tab_ids=['Look-up Table']))
        obj.btn_init.clicked.connect(lambda: obj.init_data())
        obj.btn_start_qual_a.clicked.connect(lambda: obj.gapfilling_quality_A())
        obj.drp_swin.currentIndexChanged.connect(lambda: obj.update_dynamic_fields())
        obj.drp_ta.currentIndexChanged.connect(lambda: obj.update_dynamic_fields())
        obj.drp_vpd.currentIndexChanged.connect(lambda: obj.update_dynamic_fields())
        obj.btn_add_as_new_var.clicked.connect(lambda: self.get_results_lookup_table(obj=obj))

    def get_results_lookup_table(self, obj):
        self.data_df = obj.get_filled(main_df=self.data_df.copy())
        self.data_df = dfun.frames.sort_multiindex_columns_names_LEGACY(df=self.data_df,
                                                                        priority_vars=utils.vargroups.PRIORITY_VARS + utils.vargroups.GAPFILLED_GENERAL_SCALARS)
        self._update_gui_lists()

    def make_tab_simple_running_gf(self):
        """Make new tab and store current data_df in instance."""
        obj = gf.simple_running.Run(app_obj=self,
                                    title='Simple Running',
                                    tab_id=gui.base.buildTab.find_free_tab_id(self.TabWidget,
                                                                              tab_ids=['Simple Running']))
        obj.lst_varlist_available.itemClicked.connect(lambda: obj.select_target())
        obj.btn_calc.clicked.connect(lambda: obj.calc())
        obj.btn_add_as_new_var.clicked.connect(lambda: self.get_results_simple_running_gf(obj=obj))

    def get_results_simple_running_gf(self, obj):
        self.data_df = obj.get_selected(main_df=self.data_df.copy())
        self.data_df = dfun.frames.sort_multiindex_columns_names_LEGACY(df=self.data_df,
                                                                        priority_vars=utils.vargroups.PRIORITY_VARS + utils.vargroups.GAPFILLED_GENERAL_SCALARS)
        self._update_gui_lists()

    def make_tab_random_forest(self):
        """Make new tab and store current data_df in instance"""
        obj = gf.random_forest.Run(app_obj=self,
                                   title='Random Forest',
                                   tab_id=gui.base.buildTab.find_free_tab_id(self.TabWidget,
                                                                             tab_ids=['Random Forest']))
        obj.lst_varlist_available.itemClicked.connect(lambda: obj.add_var_to_selected())
        obj.lst_varlist_selected.itemClicked.connect(lambda: obj.remove_var_from_selected())
        obj.drp_target.currentIndexChanged.connect(lambda: obj.get_settings_from_fields())
        obj.btn_add_vars_auto.clicked.connect(lambda: obj.add_all_vars_to_selected())
        obj.btn_train_model.clicked.connect(lambda: obj.model_training())
        obj.btn_fill_gaps.clicked.connect(lambda: obj.gapfilling())
        obj.btn_add_as_new_var.clicked.connect(lambda: self.get_results_random_forest(obj=obj))

    def get_results_random_forest(self, obj):
        """ Get results from random forest and insert in main df. """
        self.data_df = obj.get_filled(main_df=self.data_df, results_df=obj.rafo_df)
        self.data_df = dfun.frames.sort_multiindex_columns_names_LEGACY(df=self.data_df,
                                                                        priority_vars=utils.vargroups.PRIORITY_VARS + utils.vargroups.GAPFILLED_GENERAL_SCALARS)
        self._update_gui_lists()

    def make_tab_feature_selection_an(self):
        """Make new tab and store current data_df in instance"""
        obj = an.feature_selection.Run(app_obj=self,
                                       title='Feature Selection',
                                       tab_id=gui.base.buildTab.find_free_tab_id(self.TabWidget,
                                                                                 tab_ids=['Feature Selection']))
        obj.lst_varlist_available.itemClicked.connect(lambda: obj.add_var_to_selected())
        obj.lst_varlist_selected.itemClicked.connect(lambda: obj.remove_var_from_selected())
        obj.btn_add_vars_auto.clicked.connect(lambda: obj.add_all_vars_to_selected())
        obj.btn_find_features.clicked.connect(lambda: obj.find_features())
        obj.drp_method.currentIndexChanged.connect(lambda: obj.check_method_selection())
        # obj.btn_fill_gaps.clicked.connect(lambda: obj.fill_gaps())
        # obj.btn_add_as_new_var.clicked.connect(lambda: self.get_results_random_forest(obj=obj))

    # def get_results_feature_selection_an(self, obj):
    #     """ Get results from random forest and insert in main df. """
    #     self.data_df = obj.get_filled(main_df=self.data_df, results_df=obj.rafo_df)
    #     self.data_df = data_fn.sort_multiindex_columns_names(df=self.data_df,
    #                                                          priority_vars=PRIORITY_VARS + GAPFILLED_GENERAL_SCALARS)
    #     self.update_gui_lists()

    def make_tab_define_seasons(self):
        """Make new tab and store current data_df in instance"""
        obj = cv.define_seasons.Run(app_obj=self,
                                    title='Define Seasons',
                                    tab_id=gui.base.buildTab.find_free_tab_id(self.TabWidget,
                                                                              tab_ids=['Define Seasons']))
        obj.btn_apply.clicked.connect(lambda: obj.apply())
        obj.btn_add_as_new_col.clicked.connect(lambda: self.get_results_define_seasons(obj=obj))

    def get_results_define_seasons(self, obj):
        """ Get results from define seasons and insert in main df. """
        self.data_df = obj.get_season_cols(main_df=self.data_df)
        self.data_df = dfun.frames.sort_multiindex_columns_names_LEGACY(df=self.data_df,
                                                                        priority_vars=utils.vargroups.PRIORITY_VARS + utils.vargroups.GAPFILLED_GENERAL_SCALARS)
        self._update_gui_lists()

    def make_tab_iqr_od(self):
        """Make new tab and store current data_df in instance"""
        obj = od_old.iqr.Run(app_obj=self,
                             title='IQR',
                             tab_id=gui.base.buildTab.find_free_tab_id(self.TabWidget, tab_ids=['IQR']))
        obj.lst_varlist_available.itemClicked.connect(lambda: obj.select_target())
        obj.btn_calc.clicked.connect(lambda: obj.calc())
        obj.btn_keep_marked.clicked.connect(lambda: obj.keep_marked())
        obj.btn_remove_marked.clicked.connect(lambda: obj.remove_marked())
        obj.btn_add_as_new_var.clicked.connect(lambda: self.get_results_iqr_od(obj=obj))

    def get_results_iqr_od(self, obj):
        self.data_df = obj.get_selected(main_df=self.data_df.copy())
        self.data_df = dfun.frames.sort_multiindex_columns_names_LEGACY(df=self.data_df,
                                                                        priority_vars=utils.vargroups.PRIORITY_VARS + utils.vargroups.GAPFILLED_GENERAL_SCALARS)
        self._update_gui_lists()

    def make_tab_running_od(self):
        """Make new tab and store current data_df in instance"""
        obj = od_old.running.Run(app_obj=self,
                                 title='Running',
                                 tab_id=gui.base.buildTab.find_free_tab_id(self.TabWidget,
                                                                           tab_ids=['Running']))
        obj.lst_varlist_available.itemClicked.connect(lambda: obj.select_target())
        obj.btn_calc.clicked.connect(lambda: obj.calc())
        obj.btn_keep_marked.clicked.connect(lambda: obj.keep_marked())
        obj.btn_remove_marked.clicked.connect(lambda: obj.remove_marked())
        obj.btn_add_as_new_var.clicked.connect(lambda: self.get_results_runmed_od(obj=obj))

    def get_results_runmed_od(self, obj):
        self.data_df = obj.get_selected(main_df=self.data_df.copy())
        self.data_df = dfun.frames.sort_multiindex_columns_names_LEGACY(df=self.data_df,
                                                                        priority_vars=utils.vargroups.PRIORITY_VARS + utils.vargroups.GAPFILLED_GENERAL_SCALARS)
        self._update_gui_lists()

    def make_tab_hampel_od(self):
        """Make new tab and store current data_df in instance"""
        obj = od_old.hampel.Run(app_obj=self,
                                title='Hampel Filter',
                                tab_id=gui.base.buildTab.find_free_tab_id(self.TabWidget,
                                                                          tab_ids=['Hampel Filter']))
        obj.lst_varlist_available.itemClicked.connect(lambda: obj.select_target())
        obj.btn_calc.clicked.connect(lambda: obj.calc())
        obj.btn_keep_marked.clicked.connect(lambda: obj.keep_marked())
        obj.btn_remove_marked.clicked.connect(lambda: obj.remove_marked())
        obj.btn_add_as_new_var.clicked.connect(lambda: self.get_results_runmed_od(obj=obj))

    def get_results_hampel_od(self, obj):
        self.data_df = obj.get_selected(main_df=self.data_df.copy())
        self.data_df = dfun.frames.sort_multiindex_columns_names_LEGACY(df=self.data_df,
                                                                        priority_vars=utils.vargroups.PRIORITY_VARS + utils.vargroups.GAPFILLED_GENERAL_SCALARS)
        self._update_gui_lists()

    def make_tab_gapfinder_an(self):
        """Make new tab and store current data_df in instance"""
        obj = an.gapfinder.Run(app_obj=self,
                               title='Gapfinder',
                               tab_id=gui.base.buildTab.find_free_tab_id(self.TabWidget, tab_ids=['Gapfinder']))
        obj.lst_varlist_available.itemClicked.connect(lambda: obj.select_target())
        obj.btn_calc.clicked.connect(lambda: obj.calc())

    def make_tab_apply_gain_cv(self):
        """Make new tab and store current data_df in instance"""
        obj = cv.apply_gain.Run(app_obj=self,
                                title='Apply Gain',
                                tab_id=gui.base.buildTab.find_free_tab_id(self.TabWidget,
                                                                          tab_ids=['Apply Gain']))
        obj.lst_varlist_available.itemClicked.connect(lambda: obj.select_target())
        obj.btn_calc.clicked.connect(lambda: obj.calc())
        obj.btn_add_as_new_var.clicked.connect(lambda: self.get_results_apply_gain_cv(obj=obj))

    def get_results_apply_gain_cv(self, obj):
        self.data_df = obj.get_selected(main_df=self.data_df.copy())
        self.data_df = dfun.frames.sort_multiindex_columns_names_LEGACY(df=self.data_df,
                                                                        priority_vars=utils.vargroups.PRIORITY_VARS + utils.vargroups.GAPFILLED_GENERAL_SCALARS)
        self._update_gui_lists()

    def make_tab_binning_cv(self):
        """Make new tab and store current data_df in instance"""
        obj = cv.binning.Run(app_obj=self, title='Binning',
                             tab_id=gui.base.buildTab.find_free_tab_id(self.TabWidget, tab_ids=['Binning']))
        obj.lst_varlist_available.itemClicked.connect(lambda: obj.select_target())
        obj.btn_calc.clicked.connect(lambda: obj.calc())
        obj.btn_add_as_new_var.clicked.connect(lambda: self.get_results_binning_cv(obj=obj))

    def get_results_binning_cv(self, obj):
        self.data_df = obj.get_selected(main_df=self.data_df.copy())
        self.data_df = dfun.frames.sort_multiindex_columns_names_LEGACY(df=self.data_df,
                                                                        priority_vars=utils.vargroups.PRIORITY_VARS + utils.vargroups.GAPFILLED_GENERAL_SCALARS)
        self._update_gui_lists()

    def make_tab_span_correction_mf(self):
        """Make new tab and store current data_df in instance"""
        obj = md.span_correction.Run(app_obj=self,
                                     title='Span Correction',
                                     tab_id=gui.base.buildTab.find_free_tab_id(self.TabWidget,
                                                                               tab_ids=['Span Correction']))
        obj.lst_varlist_target.itemClicked.connect(lambda: obj.select_target())
        obj.lst_varlist_reference.itemClicked.connect(lambda: obj.select_reference())
        obj.btn_calc.clicked.connect(lambda: obj.calc())
        obj.btn_add_as_new_var.clicked.connect(lambda: self.get_results_span_correction_mf(obj=obj))
        obj.lst_rangelist_selected.itemClicked.connect(lambda: obj.remove_from_rangelist())
        obj.btn_add_timerange.clicked.connect(lambda: obj.add_timerange())
        obj.btn_export_to_file.clicked.connect(lambda: obj.export_to_file())

    def get_results_span_correction_mf(self, obj):
        self.data_df = obj.get_selected(main_df=self.data_df.copy())
        self.data_df = dfun.frames.sort_multiindex_columns_names_LEGACY(df=self.data_df,
                                                                        priority_vars=utils.vargroups.PRIORITY_VARS + utils.vargroups.GAPFILLED_GENERAL_SCALARS)
        self._update_gui_lists()

    def make_tab_rename_var_mf(self):
        """Make new tab and store current data_df in instance"""
        obj = md.rename_var.Run(app_obj=self,
                                title='Rename Variables',
                                tab_id=gui.base.buildTab.find_free_tab_id(self.TabWidget,
                                                                          tab_ids=['Rename Variables']))
        obj.btn_apply_prefix_suffix.clicked.connect(lambda: obj.add_prefix_suffix())
        obj.btn_run.clicked.connect(lambda: self.get_results_rename_var_mf(obj=obj))

    def get_results_rename_var_mf(self, obj):
        self.data_df = obj.get_renamed()
        self.data_df = dfun.frames.sort_multiindex_columns_names_LEGACY(df=self.data_df,
                                                                        priority_vars=utils.vargroups.PRIORITY_VARS + utils.vargroups.GAPFILLED_GENERAL_SCALARS)
        self._update_gui_lists()

    def make_tab_lag_features_cv(self):
        """Make new tab and store current data_df in instance"""
        obj = cv.lag_features.Run(app_obj=self,
                                  title='Lag Features',
                                  tab_id=gui.base.buildTab.find_free_tab_id(self.TabWidget,
                                                                            tab_ids=['Lag Features']))
        obj.lst_varlist_available.itemClicked.connect(lambda: obj.add_var_to_selected())
        obj.lst_varlist_selected.itemClicked.connect(lambda: obj.remove_var_from_selected())
        obj.btn_create_lagged_versions.clicked.connect(lambda: obj.create_lagged_versions())
        obj.btn_add_as_new_vars.clicked.connect(lambda: self.get_results_lag_features_cv(obj=obj))

    def get_results_lag_features_cv(self, obj):
        self.data_df = obj.get_variants(main_df=self.data_df.copy())
        self.data_df = dfun.frames.sort_multiindex_columns_names_LEGACY(df=self.data_df,
                                                                        priority_vars=utils.vargroups.PRIORITY_VARS + utils.vargroups.GAPFILLED_GENERAL_SCALARS)
        self._update_gui_lists()

    def make_tab_timesince_cv(self):
        """Make new tab and store current data_df in instance"""
        obj = cv.time_since.Run(app_obj=self,
                                title='Time Since',
                                tab_id=gui.base.buildTab.find_free_tab_id(self.TabWidget,
                                                                          tab_ids=['Time Since']))
        obj.lst_varlist_available.itemClicked.connect(lambda: obj.select_target())
        obj.btn_calc.clicked.connect(lambda: obj.calc())
        obj.btn_add_as_new_var.clicked.connect(lambda: self.get_results_timesince_cv(obj=obj))

    def get_results_timesince_cv(self, obj):
        self.data_df = obj.get_selected(main_df=self.data_df.copy())
        self.data_df = dfun.frames.sort_multiindex_columns_names_LEGACY(df=self.data_df,
                                                                        priority_vars=utils.vargroups.PRIORITY_VARS + utils.vargroups.GAPFILLED_GENERAL_SCALARS)
        self._update_gui_lists()

    def _addtab_od_abslim(self):
        """Add new tab and store current data_df in instance"""
        obj = od.abslim.dvcontroller.ContrAbsLim(app_obj=self,
                                                 tab_title='Absolute Limits',
                                                 tab_id=find_free_tab_id(
                                                     self.TabWidget, tab_ids=['Absolute Limits']))
        obj.btn_add_as_new_var.clicked.connect(lambda: self._get_results_from_tab(obj=obj))

    def _addtab_od_doublediff(self):
        """Add new tab and store current data_df in instance"""
        obj = od.doublediff.dvcontroller.ContrDoubleDiff(app_obj=self,
                                                         tab_title='Double Diff',
                                                         tab_id=find_free_tab_id(
                                                             self.TabWidget, tab_ids=['Double Diff']))
        obj.btn_add_as_new_var.clicked.connect(lambda: self._get_results_from_tab(obj=obj))

    def _get_results_from_tab(self, obj):
        self.data_df = obj.add_as_new_vars(main_df=self.data_df.copy())
        self._update_gui_lists()

    def make_overview_plots(self, fig):
        """
        Make multiple overview plots, showing a range of different plots.
        """
        fig.clear()  # Clear figure to which new axes are added
        canvas = fig.canvas  # Needed to redraw plot

        # By default, the overview is plotted
        self.ax_main, self.gs = pl.overview.Run(fig=self.plt_figure,
                                                focus_df=self.focus_df,
                                                focus_col=self.focus_col,
                                                data_df=self.data_df,
                                                limit_plot_xlim=self.limit_plot_xlim,
                                                limit_timerange_filter_dt=self.limit_timerange_filter_dt).get_ax()
        canvas.draw_idle()  # refresh plot


class AppContext(ApplicationContext):  # 1. Subclass ApplicationContext

    # For more info see here:
    # https://build-system.fman.io/manual/#cached_property

    def run(self):  # 2. Implement run()
        splash = self._splash_screen()
        self.main_window.show()
        splash.close()
        return self.app.exec_()  # 3. End run() with this line

    def _splash_screen(self):
        """Show splash screen w/ logo"""
        splash_pix = qtg.QPixmap(self.logo_diive_256px)
        splash_pix.setDevicePixelRatio(2)
        splash = qtw.QSplashScreen(splash_pix, qtc.Qt.WindowStaysOnTopHint)
        splash.show()
        return splash

    @cached_property
    def main_window(self):
        return DIIVE(parent=None, load_initial_data=True, ctx=self)

    # Detect folder where script was started from
    @cached_property
    def dir_script(self):
        return pathlib.Path(__file__)

    # EXAMPLE FILES
    # -------------
    @cached_property
    def file_DIIVE_CSV_30MIN(self):
        return self.get_resource(
            'example_files/ExampleFile_DIIVE_CSV_30T.diive.csv')  # Returns absolute path

    @cached_property
    def file_DIIVE_CSV_1H(self):
        return self.get_resource(
            'example_files/Example_DIIVE_CSV_1H.csv')  # Returns absolute path

    @cached_property
    def file_DIIVE_CSV_1MIN(self):
        return self.get_resource(
            'example_files/_Dataset_AMP-20201028-085716_Original-1T.amp.csv')  # Returns absolute path

    @cached_property
    def file_EDDYPRO_FULL_OUTPUT_30MIN(self):
        return self.get_resource('example_files/ExampleFile_EDDYPRO_FULL_OUTPUT_30MIN.csv')

    @cached_property
    def file_SMEAR_II_30MIN(self):
        # return self.get_resource('example_files/Example_EddyProFullOutput_CH-AWS-2018_30T.csv')
        return self.get_resource('example_files/SMEAR_II_smeardata_19960101120000.csv')

    @cached_property
    def file_FLUXNET_FULLSET_30MIN(self):
        # return self.get_resource('example_files/__LONG__Example_FLUXNET_FULLSET_30MIN.csv')
        return self.get_resource('example_files/Example_FLUXNET_FULLSET_30MIN.csv')

    @cached_property
    def file_Events(self):
        return self.get_resource('example_files/EventsExamples.events')

    @cached_property
    def file_ETH_METEOSCREENING_30MIN_FORMAT_A(self):
        return self.get_resource('example_files/Example_ETH_METEOSCREENING_30MIN_FORMAT-A.csv')

    @cached_property
    def file_ETH_METEOSCREENING_30MIN_FORMAT_B(self):
        return self.get_resource('example_files/Example_ETH_METEOSCREENING_30MIN_FORMAT-B.dat')

    @cached_property
    def file_TOA5_1MIN(self):
        return self.get_resource('example_files/CH-FRU_iDL_BOX1_0_1_TBL1_20190731-0457.dat')

    @cached_property
    def file_REDDYPROC_30MIN(self):
        return self.get_resource('example_files/REDDYPROC_30MIN_CH-DAV_1997.csv')
        # return self.get_resource('example_files/Example_REDDYPROC_30MIN.csv')

    @cached_property
    def file_ICOS_10S(self):
        return self.get_resource('example_files/Example_ICOS_10S.dat')

    # FILETYPES
    # ---------
    # Files containing the file types are cached so that they can be used when loading example files.
    @cached_property
    def filetype_dir(self):
        return self.get_resource('filetypes/')

    @cached_property
    def filetype_DIIVE_CSV_30MIN(self):
        return self.get_resource('filetypes/DIIVE_CSV_30MIN.yml')

    @cached_property
    def filetype_DIIVE_CSV_1D(self):
        return self.get_resource('filetypes/DIIVE_CSV_1D.filetype')

    @cached_property
    def filetype_DIIVE_CSV_1H(self):
        return self.get_resource('filetypes/DIIVE_CSV_1H.filetype')

    @cached_property
    def filetype_DIIVE_CSV_1H_TS_START(self):
        return self.get_resource('filetypes/DIIVE_CSV_1H_TS-START.filetype')

    @cached_property
    def filetype_DIIVE_CSV_1MIN(self):
        return self.get_resource('filetypes/DIIVE_CSV_1MIN.filetype')

    @cached_property
    def filetype_EDDYPRO_FULL_OUTPUT_30MIN(self):
        return self.get_resource('filetypes/EDDYPRO_FULL_OUTPUT_30MIN.yml')

    @cached_property
    def filetype_SMEAR_II_30MIN(self):
        return self.get_resource('filetypes/SMEAR_II_30MIN.filetype')

    @cached_property
    def filetype_ETH_METEOSCREENING_30MIN_FORMAT_A(self):
        return self.get_resource('filetypes/ETH_METEOSCREENING_30MIN_FORMAT-A.filetype')

    @cached_property
    def filetype_ETH_METEOSCREENING_30MIN_FORMAT_B(self):
        return self.get_resource('filetypes/ETH_METEOSCREENING_30MIN_FORMAT-B.filetype')

    @cached_property
    def filetype_Events(self):
        return self.get_resource('filetypes/Events.filetype')

    @cached_property
    def filetype_FLUXNET_FULLSET_30MIN(self):
        return self.get_resource('filetypes/FLUXNET_FULLSET_30MIN.filetype')

    @cached_property
    def filetype_EFDC_UPLOAD_30MIN(self):
        return self.get_resource('filetypes/EFDC_UPLOAD_30MIN.filetype')

    @cached_property
    def filetype_FLUXNET_FULLSET_1D(self):
        return self.get_resource('filetypes/FLUXNET_FULLSET_1D.filetype')

    @cached_property
    def filetype_ICOS_10S(self):
        return self.get_resource('filetypes/ICOS_10S.filetype')

    @cached_property
    def filetype_SUTRON_METEO_1MIN(self):
        return self.get_resource('filetypes/SUTRON_METEO_1MIN.filetype')

    @cached_property
    def filetype_SUTRON_METEO_EMPTYROWS_1MIN(self):
        return self.get_resource('filetypes/SUTRON_METEO_EMPTYROWS_1MIN.filetype')

    @cached_property
    def filetype_TOA5_10MIN(self):
        return self.get_resource('filetypes/TOA5_10MIN.filetype')

    @cached_property
    def filetype_TOA5_1MIN(self):
        return self.get_resource('filetypes/TOA5_1MIN.filetype')

    @cached_property
    def filetype_TOA5_10S(self):
        return self.get_resource('filetypes/TOA5_10S.filetype')

    @cached_property
    def filetype_TOA5_1S(self):
        return self.get_resource('filetypes/TOA5_1S.filetype')

    @cached_property
    def filetype_REDDYPROC_30MIN(self):
        return self.get_resource('filetypes/REDDYPROC_30MIN.filetype')

    # ICONS
    # -----
    @cached_property
    def icon_diive(self):
        return self.get_resource('icons/logo_diive2.png')  # Returns absolute path

    @cached_property
    def logo_diive(self):
        return self.get_resource('icons/logo_diive1.png')  # Returns absolute path

    @cached_property
    def logo_diive_256px(self):
        return self.get_resource('icons/logo_diive1_256px.png')  # Returns absolute path

    @cached_property
    def icon_open_in_new_tab(self):
        return self.get_resource('icons/tab_grey_192x192.png')  # Returns absolute path
        # return self.get_resource('icons/open_in_new_grey_192x192.png')  # Returns absolute path

    @cached_property
    def icon_cat_pipelines(self):
        return self.get_resource('icons/fast_forward_grey_192x192.png')  # Returns absolute path

    @cached_property
    def icon_cat_plots(self):
        return self.get_resource('icons/equalizer_grey_192x192.png')  # Returns absolute path

    @cached_property
    def icon_cat_hires_tests(self):
        return self.get_resource('icons/blur_on_grey_192x192.png')  # Returns absolute path

    @cached_property
    def icon_cat_quality_control(self):
        return self.get_resource('icons/multitrack_audio_grey_192x192.png')  # Returns absolute path

    @cached_property
    def icon_cat_outlier_detection(self):
        return self.get_resource('icons/flip_to_front_grey_192x192.png')  # Returns absolute path

    @cached_property
    def icon_cat_gapfilling(self):
        return self.get_resource('icons/blur_linear_grey_192x192.png')  # Returns absolute path

    @cached_property
    def icon_cat_analyses(self):
        return self.get_resource('icons/layers_grey_192x192.png')  # Returns absolute path

    @cached_property
    def icon_cat_resampling(self):
        return self.get_resource('icons/dashboard_grey_192x192.png')  # Returns absolute path

    @cached_property
    def icon_cat_modifications(self):
        return self.get_resource('icons/apps_grey_192x192.png')  # Returns absolute path

    @cached_property
    def icon_cat_export(self):
        return self.get_resource('icons/send_grey_192x192.png')  # Returns absolute path

    @cached_property
    def icon_cat_plots(self):
        return self.get_resource('icons/equalizer_grey_192x192.png')  # Returns absolute path

    @cached_property
    def icon_list_newvar(self):
        return self.get_resource('icons/play_arrow_grey_192x192.png')  # Returns absolute path

    @cached_property
    def icon_cat_corrections(self):
        return self.get_resource('icons/line_weight_grey_192x192.png')  # Returns absolute path

    @cached_property
    def icon_btn_controls_add(self):
        return self.get_resource('icons/add_grey_192x192.png')

    @cached_property
    def icon_btn_controls_run(self):
        return self.get_resource('icons/play_arrow_grey_192x192.png')

    @cached_property
    def icon_flux(self):
        return self.get_resource('icons/flux1_192x192.png')

    @cached_property
    def tab_icon_variables(self):
        return self.get_resource('icons/tab_icon_variables.png')

    @cached_property
    def tab_icon_plots(self):
        return self.get_resource('icons/tab_icon_plots.png')

    @cached_property
    def tab_icon_outlier_detection(self):
        return self.get_resource('icons/tab_icon_outlier_detection.png')

    @cached_property
    def tab_icon_analyses(self):
        return self.get_resource('icons/tab_icon_analyses.png')

    @cached_property
    def tab_icon_gapfilling(self):
        return self.get_resource('icons/tab_icon_gapfilling.png')

    @cached_property
    def tab_icon_modifications(self):
        return self.get_resource('icons/tab_icon_modifications.png')

    @cached_property
    def tab_icon_create_variable(self):
        return self.get_resource('icons/tab_icon_create_variable.png')

    @cached_property
    def tab_icon_eddy_covariance(self):
        return self.get_resource('icons/tab_icon_eddy_covariance.png')

    @cached_property
    def tab_icon_export(self):
        return self.get_resource('icons/tab_icon_export.png')

    @cached_property
    def icon_btn_controls_remove(self):
        return self.get_resource('icons/remove_grey_192x192.png')

    @cached_property
    def icon_btn_controls_keep(self):
        return self.get_resource('icons/playlist_add_grey_192x192.png')

    @cached_property
    def icon_btn_menu_add(self):
        return self.get_resource('icons/add_box_grey_192x192.png')

    @cached_property
    def icon_var_default(self):
        return self.get_resource('icons/var1_default_192x192.png')

    @cached_property
    def icon_btn_menu_settings(self):
        return self.get_resource('icons/settings_grey_192x192.png')

    @cached_property
    def icon_btn_new_window(self):
        return self.get_resource('icons/crop_square_grey_192x192.png')


def main():
    appctxt = AppContext()  # 4. Instantiate the subclass

    # Include CSS styles from file, in resources folder
    stylesheet = appctxt.get_resource('css/gui_light.css')
    appctxt.app.setStyleSheet(open(stylesheet).read())

    exit_code = appctxt.run()  # 5. Invoke run()
    sys.exit(exit_code)


if __name__ == '__main__':
    main()
