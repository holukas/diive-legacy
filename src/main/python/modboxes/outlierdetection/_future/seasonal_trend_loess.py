"""

SEASONAL TREND DECOMPOSITION (LOESS)
------------------------------------


"""

import matplotlib.dates as mdates
import matplotlib.gridspec as gridspec
import matplotlib.pyplot as plt
# matplotlib.use('Qt5Agg')
import numpy as np
import pandas as pd
from PyQt5 import QtGui
from statsmodels.tsa.seasonal import STL

import gui.add_to_layout
import gui.base
import gui.elements
import gui.make
import gui.plotfuncs
import logger
from modboxes.gapfilling.random_forest import Run as randomForest
from modboxes.plots.styles.LightTheme import *
from pkgs.dfun.frames import export_to_main


# pd.set_option('display.width', 1000)
# pd.set_option('display.max_columns', 15)
# pd.set_option('display.max_rows', 20)


class addContent(gui.base.buildTab):
    """Build new tab and populate with contents"""

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id, tab_template='SVP')

        # Tab icon
        self.TabWidget.setTabIcon(self.tab_ix, QtGui.QIcon(app_obj.ctx.tab_icon_outlier_detection))

        # Add settings menu contents
        self.btn_calc, self.btn_add_as_new_var, self.lne_trend, self.lne_period, \
        self.lne_seasonal, self.lne_lowpass_filter, \
        self.drp_trend_deg, self.drp_seasonal_deg, self.drp_lowpass_deg = \
            self.add_settings_fields()

        # # Add variables required in settings
        # self.populate_settings_fields()

        # Add available variables to variable lists
        gui.base.buildTab.populate_variable_list(obj=self)

        # # Add plots
        # self.populate_plot_area()

    # def populate_plot_area(self):
    #     # This is done in Run due to dynamic amount of axes
    #     pass

    # def populate_settings_fields(self):
    #     self.drp_define_nighttime_based_on.clear()
    #
    #     # Add all variables to drop-down lists to allow full flexibility yay
    #     for ix, colname_tuple in enumerate(self.tab_data_df.columns):
    #
    #         if any(fnmatch.fnmatch(colname_tuple[0], id) for id in NIGHTTIME_DETECTION):
    #             self.drp_define_nighttime_based_on.addItem(self.col_list_pretty[ix])

    def add_settings_fields(self):
        gui.elements.add_header_subheader_to_grid_top(
            row=0, col=0, rowspan=1, colspan=3, layout=self.sett_layout,
            txt=["Season Trend Decomposition LOESS",
                 "Outlier Removal"])
        gui.add_to_layout.add_spacer_item_to_grid(row=1, col=0, layout=self.sett_layout)

        # Sequence Lengths
        gui.elements.add_header_in_grid_row(
            row=2, layout=self.sett_layout, txt='Sequence Lengths')
        lne_period = gui.elements.add_label_linedit_info_triplet_to_grid(
            layout=self.sett_layout, txt='Periodicity (records)', css_ids=['', 'cyan'],
            row=3, col=0, orientation='horiz', txt_info_hover='XXX')
        lne_period.setText('48')
        lne_trend = gui.elements.add_label_linedit_info_triplet_to_grid(
            layout=self.sett_layout, txt='Trend Smoother (records)', css_ids=['', 'cyan'],
            row=4, col=0, orientation='horiz', txt_info_hover='XXX')
        lne_trend.setText('336')
        lne_seasonal = gui.elements.add_label_linedit_info_triplet_to_grid(
            layout=self.sett_layout, txt='Seasonal Smoother (records)', css_ids=['', 'cyan'],
            row=5, col=0, orientation='horiz', txt_info_hover='XXX')
        lne_seasonal.setText('337')
        lne_lowpass_filter = gui.elements.add_label_linedit_info_triplet_to_grid(
            layout=self.sett_layout, txt='Low-pass Filter (records)', css_ids=['', 'cyan'],
            row=6, col=0, orientation='horiz', txt_info_hover='XXX')
        lne_lowpass_filter.setText('49')
        gui.add_to_layout.add_spacer_item_to_grid(
            row=7, col=0, layout=self.sett_layout)

        # Degrees
        gui.elements.add_header_in_grid_row(
            row=8, layout=self.sett_layout, txt='Degrees')
        drp_trend_deg = gui.elements.add_label_dropdown_info_triplet_to_grid(
            layout=self.sett_layout, txt='Degree Of Trend LOESS', css_ids=['', 'cyan'],
            row=9, col=0, orientation='horiz', txt_info_hover='XXX')
        drp_trend_deg.addItems(['0 (constant)', '1 (constant and trend)'])
        drp_seasonal_deg = gui.elements.add_label_dropdown_info_triplet_to_grid(
            layout=self.sett_layout, txt='Degree Of Seasonal LOESS', css_ids=['', 'cyan'],
            row=10, col=0, orientation='horiz', txt_info_hover='XXX')
        drp_seasonal_deg.addItems(['0 (constant)', '1 (constant and trend)'])
        drp_lowpass_deg = gui.elements.add_label_dropdown_info_triplet_to_grid(
            layout=self.sett_layout, txt='Degree Of Low-pass LOESS', css_ids=['', 'cyan'],
            row=11, col=0, orientation='horiz', txt_info_hover='XXX')
        drp_lowpass_deg.addItems(['0 (constant)', '1 (constant and trend)'])
        gui.add_to_layout.add_spacer_item_to_grid(
            row=12, col=0, layout=self.sett_layout)

        # ref_drp_class_var = \
        #     gui_elements.grd_LabelDropdownPair(txt='Class Variable',
        #                                        css_ids=['', 'cyan'], layout=self.sett_layout,
        #                                        row=1, col=0, orientation='horiz')

        # Buttons
        btn_calc = gui.elements.add_button_to_grid(
            row=13, col=0, rowspan=1, colspan=2,
            grid_layout=self.sett_layout, txt='Calculate', css_id='')
        btn_add_as_new_var = gui.elements.add_button_to_grid(
            row=14, col=0, rowspan=1, colspan=2,
            grid_layout=self.sett_layout, txt='+ Add As New Var', css_id='btn_add_as_new_var')
        self.sett_layout.setRowStretch(15, 1)

        return btn_calc, btn_add_as_new_var, lne_trend, lne_period, lne_seasonal, lne_lowpass_filter, \
               drp_trend_deg, drp_seasonal_deg, drp_lowpass_deg


class Run(addContent):
    grouping_col = ('_group', '[#]')
    doy_col = ('_doy', '[yyyy-mm-dd HH:MM:SS]'),
    doy_fraction_hhmmss_col = ('_fraction_hhmmss_col', '[#]'),
    doy_fraction_col = ('_doy_fraction', '[#]')
    marker_isset = False
    ready_to_export = False
    marker_filter = None  # Filter to mark valid data points
    sub_outdir = "analysis_extreme_events_finder"

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id)
        logger.log(name='>>> Starting Extreme Events Finder', dict={}, highlight=True)  # Log info
        self.sub_outdir = self.project_outdir / self.sub_outdir
        self.selected_vars_lookup_dict = {}
        self.var_selected_df = pd.DataFrame()
        self.vars_available_df = self.tab_data_df.copy()

    def add_target(self):
        # self.remove_aux_cols_from_selected()
        self.var_selected_df = pd.DataFrame()  # Reset
        self.set_target_col_to_add()
        self.add_target_to_selected()
        self.set_colnames()
        self.add_cols()
        # self.calc_outliers()
        self.plot_measured()
        self.btn_add_as_new_var.setDisabled(True)

    def set_colnames(self):
        id = '+odSTL'
        self.target_gf_col = (f"_gf", self.target_col[1])
        self.trend_col = (f"{self.target_col[0]}{id}_gf_trend", self.target_col[1])  # Already named for export
        self.seasonal_col = (f"{self.target_col[0]}{id}_gf_seasonal", self.target_col[1])  # Already named for export
        self.resid_col = (f"{self.target_col[0]}{id}_gf_residuals", self.target_col[1])  # Already named for export

    def add_cols(self):
        self.var_selected_df[self.target_gf_col] = np.nan
        self.var_selected_df[self.trend_col] = np.nan
        self.var_selected_df[self.seasonal_col] = np.nan
        self.var_selected_df[self.resid_col] = np.nan

    def get_settings_from_fields(self):
        self.trend = int(self.lne_trend.text())
        self.season = int(self.lne_seasonal.text())
        self.period = int(self.lne_period.text())
        self.lowpass_filter = int(self.lne_lowpass_filter.text())

        self.trend_deg = 1 if self.drp_trend_deg.currentText() == '1 (constant and trend)' else 0
        self.seasonal_deg = 1 if self.drp_trend_deg.currentText() == '1 (constant and trend)' else 0
        self.lowpass_deg = 1 if self.drp_trend_deg.currentText() == '1 (constant and trend)' else 0

    def calc(self):
        # https://www.statsmodels.org/devel/examples/notebooks/generated/stl_decomposition.html
        # https://www.statsmodels.org/dev/generated/statsmodels.tsa.seasonal.STL.html
        # https://github.com/ServiceNow/stl-decomp-4j
        # https://machinelearningmastery.com/decompose-time-series-data-trend-seasonality/
        # https://towardsdatascience.com/stl-decomposition-how-to-do-it-from-scratch-b686711986ec
        # https://github.com/hafen/stlplus

        self.get_settings_from_fields()
        df = self.var_selected_df.copy()

        # Gapless series needed for STL
        num_gaps = self.var_selected_df[self.target_col].isnull().sum()
        if num_gaps > 0:
            target_series_gf = randomForest.quickfill(df=df.copy(),
                                                      target_col=self.target_col,
                                                      target_gf_col=self.target_gf_col)
            df[self.target_gf_col] = target_series_gf
        else:
            df[self.target_gf_col] = df[self.target_col].copy()

        trend, seasonal, resid = self.decompose(series_gf=df[self.target_gf_col],
                                                loops=0,
                                                period=self.period,
                                                trend=self.trend,
                                                seasonal=self.season,
                                                lowpass_filter=self.lowpass_filter,
                                                trend_deg=self.trend_deg,
                                                seasonal_deg=self.seasonal_deg,
                                                lowpass_deg=self.lowpass_deg)
        df[self.trend_col] = trend
        df[self.seasonal_col] = seasonal
        df[self.resid_col] = resid

        self.var_selected_df = df.copy()

        self.plot_stl_results()

        self.btn_add_as_new_var.setEnabled(True)

    @staticmethod
    def decompose(series_gf, period, trend, lowpass_filter, loops=0, seasonal=7,
                  trend_deg=0, seasonal_deg=0, lowpass_deg=0):
        # series_res_gf = series_gf.resample('H').mean()
        trend += 1 if (trend % 2) == 0 else 0  # Trend needs to be odd number
        res = STL(series_gf, period=period, trend=trend, seasonal=seasonal, low_pass=lowpass_filter,
                  trend_deg=trend_deg, seasonal_deg=seasonal_deg, low_pass_deg=lowpass_deg).fit()
        # res.plot()
        # # res.seasonal[500:1050].plot()
        # plt.show()
        if loops > 0:
            for i in range(0, loops):
                series = res.observed
                series = series.sub(res.trend)
                series = series.sub(res.seasonal)
                res = STL(series, trend=trend, seasonal=seasonal, low_pass=lowpass_filter,
                          trend_deg=trend_deg, seasonal_deg=seasonal_deg, low_pass_deg=lowpass_deg).fit()
                res.plot()
                plt.show()
        return res.trend, res.seasonal, res.resid

    def add_grouping_col(self, df):
        """Add flag to indicate group membership"""
        df[self.grouping_col] = np.nan

        # Check if grouping col was also selected as a feature
        delete_grouping_col = True
        if self.nighttime_col in list(self.selected_vars_lookup_dict.keys()):
            delete_grouping_col = False

        if self.separate_day_night == 'Yes':
            nighttime_filter = None
            df[self.nighttime_col] = self.tab_data_df[self.nighttime_col]  # Add for grouping
            if self.set_nighttime_if == 'Smaller Than Threshold':
                nighttime_filter = df[self.nighttime_col] < self.nighttime_threshold
            elif self.set_nighttime_if == 'Larger Than Threshold':
                nighttime_filter = df[self.nighttime_col] > self.nighttime_threshold
            df.loc[nighttime_filter, [self.grouping_col]] = 0
            df.loc[~nighttime_filter, [self.grouping_col]] = 1
            if delete_grouping_col:
                df.drop(self.nighttime_col, axis=1, inplace=True)  # Only needed for grouping
        else:
            df.loc[:, [self.grouping_col]] = 0  # All in the same group
        return df

    @staticmethod
    def date_as_doy_fraction(df, doy_col, fraction_hhmmss_col, doy_fraction):
        """Insert column with date as doy fraction"""
        # Day of year with time fraction
        df = df.copy()
        df[doy_col] = df.index.dayofyear
        df[fraction_hhmmss_col] = (df.index.hour
                                   + (df.index.minute / 60)
                                   + (df.index.second / 3600)) / 24
        df[doy_fraction] = df[doy_col] \
                           + df[fraction_hhmmss_col]
        return df

    def calc_rolling(self, aux_cols_dict, col):
        # Rolling, based on true date from start to end of complete time series
        _rolling = self.var_selected_df[aux_cols_dict['seasonality_removed']].rolling(window=self.timewin, center=True)
        self.var_selected_df[aux_cols_dict['rollmax_col']] = _rolling.max()
        self.var_selected_df[aux_cols_dict['rollmin_col']] = _rolling.min()

    def remove_trend_linregr(self):
        import matplotlib.pyplot as plt
        subset = pd.DataFrame()
        subset['x'] = self.var_selected_df.index
        subset['y'] = self.var_selected_df[('TA_F_MDS', '-no-units-')].values

        from statsmodels.regression.linear_model import OLS
        least_squares = OLS(subset['y'].values,
                            list(range(subset.shape[0])))
        result = least_squares.fit()
        fit = pd.Series(result.predict(list(range(subset.shape[0]))),
                        index=subset.index)
        detrended = subset['y'] - fit

        detrended.plot()
        plt.show()

        from statsmodels.tsa.seasonal import seasonal_decompose
        period = int(24 * 60 / 30 * 7 * 9)
        result = seasonal_decompose(detrended, model="additive", period=period)
        result.plot()
        plt.show()

        import matplotlib.pyplot as plt
        subset = pd.DataFrame()
        subset['x'] = self.var_selected_df.index
        subset['y'] = self.var_selected_df[('TA_F_MDS', '-no-units-')].values
        subset.set_index('x', inplace=True)
        # subset = self.vars_selected_df[]
        subset.index.name = 'x'
        subset = subset.asfreq('30T')
        period = int(24 * 60 / 30 * 7 * 9)
        result = seasonal_decompose(subset.y, model="additive", period=period)
        trend = result.trend
        seasonal = result.seasonal
        residual = result.resid
        observed = result.observed
        result.plot()
        plt.show()

    def calc_doy_fraction_max_min(self, aux_cols_dict):
        """Get the max for each doy fraction, allows comparison between years"""

        # Small subset for calculations
        _subset_df = self.var_selected_df[[self.doy_fraction_col,
                                           aux_cols_dict['rollmax_col'],
                                           aux_cols_dict['rollmin_col']]]

        # Group by doy fraction and calculate max and min for each
        def q75(x):
            return x.quantile(0.75)

        f = {'mean', 'std', 'count', 'max', 'min', 'sem'}
        _subset_df_grouped = _subset_df \
            .groupby(self.doy_fraction_col) \
            .agg(f)

        # Map results to vars_selected_df, i.e. the doy fraction in vars_selected_df
        # is used to get the corresponding aggregated values from subset, the subset
        # is basically used as a lookup table.
        self.var_selected_df[aux_cols_dict['doy_fraction_max_rollmax_col']] = \
            self.var_selected_df[self.doy_fraction_col] \
                .map(_subset_df_grouped[aux_cols_dict['rollmax_col']]['max'])
        self.var_selected_df[aux_cols_dict['doy_fraction_min_rollmax_col']] = \
            self.var_selected_df[self.doy_fraction_col] \
                .map(_subset_df_grouped[aux_cols_dict['rollmax_col']]['min'])

        self.var_selected_df[aux_cols_dict['doy_fraction_max_rollmin_col']] = \
            self.var_selected_df[self.doy_fraction_col] \
                .map(_subset_df_grouped[aux_cols_dict['rollmin_col']]['max'])
        self.var_selected_df[aux_cols_dict['doy_fraction_min_rollmin_col']] = \
            self.var_selected_df[self.doy_fraction_col] \
                .map(_subset_df_grouped[aux_cols_dict['rollmin_col']]['min'])

    def set_target_col_to_add(self):
        """Get column name of target var from available var list"""
        target_var = self.lst_varlist_available.selectedIndexes()
        target_var_ix = target_var[0].row()
        self.target_col = self.col_dict_tuples[target_var_ix]
        self.target_pretty = self.col_list_pretty[target_var_ix]  # Needs new pretty list (screen names)

    def set_target_col_to_remove(self):
        """Get column name of target var from selected var list"""
        target_var = self.lst_varlist_selected.selectedIndexes()
        target_var_ix = target_var[0].row()
        self.target_col = list(self.selected_vars_lookup_dict.keys())[target_var_ix]  # Get key by index
        # self.target_col = self.col_dict_tuples[target_var_ix]
        # self.target_pretty = self.col_list_pretty[target_var_ix]  # Needs new pretty list (screen names)

    def add_target_to_selected(self):
        """Add var from available to selected df and lookup dict"""
        self.var_selected_df[self.target_col] = self.vars_available_df[self.target_col]

    def get_col_from_drp_pretty(self, drp):
        """ Set target to selected variable. """
        target_pretty = drp.currentText()
        target_pretty_ix = self.col_list_pretty.index(target_pretty)
        target_col = self.col_dict_tuples[target_pretty_ix]
        return target_col

    def prepare_export(self):
        export_df = self.var_selected_df[[self.trend_col, self.seasonal_col, self.resid_col]].copy()
        return export_df

    def get_selected(self, main_df):
        """Return modified class data back to main data"""
        export_df = self.prepare_export()
        main_df = export_to_main(main_df=main_df,
                                 export_df=export_df,
                                 tab_data_df=self.tab_data_df)  # Return results to main
        self.btn_add_as_new_var.setDisabled(True)
        return main_df

    def make_axes_dict(self):
        gs = gridspec.GridSpec(4, 1)  # rows, cols
        gs.update(wspace=0.2, hspace=0.2, left=0.03, right=0.97, top=0.97, bottom=0.03)

        # Make new axes
        ax_measured = self.fig.add_subplot(gs[0, 0])
        ax_trend = self.fig.add_subplot(gs[1, 0], sharex=ax_measured)
        ax_season = self.fig.add_subplot(gs[2, 0], sharex=ax_measured)
        ax_residuals = self.fig.add_subplot(gs[3, 0], sharex=ax_measured)

        axes_dict = dict(ax_measured=ax_measured,
                         ax_trend=ax_trend,
                         ax_season=ax_season,
                         ax_residuals=ax_residuals)

        # Format
        for col, ax in axes_dict.items():
            gui.plotfuncs.default_format(ax=ax, txt_xlabel=False)

        return axes_dict

    def prepare_fig(self):
        """Prepare figure"""
        for ax in self.fig.axes:
            self.fig.delaxes(ax)  # Delete all axes in figure
        axes_dict = self.make_axes_dict()
        return axes_dict

    def plot_measured(self):
        axes_dict = self.prepare_fig()
        ax = axes_dict['ax_measured']
        ax.plot_date(x=self.var_selected_df.index,
                     y=self.var_selected_df[self.target_col],
                     color='#78909C', alpha=.9, ls='-', marker='o', markeredgecolor='none',
                     ms=4, zorder=98, label=f"{self.target_col[0]} {self.target_col[1]}")
        ax.text(0.01, 0.97, f"{self.target_col[0]}",
                size=FONTSIZE_HEADER_AXIS, color=FONTCOLOR_HEADER_AXIS,
                backgroundcolor='none', transform=ax.transAxes, alpha=1,
                horizontalalignment='left', verticalalignment='top', zorder=99)
        gui.plotfuncs.default_grid(ax=ax)
        gui.plotfuncs.default_legend(ax=ax)
        locator = mdates.AutoDateLocator(minticks=3, maxticks=30)
        formatter = mdates.ConciseDateFormatter(locator)
        ax.xaxis.set_major_locator(locator)
        ax.xaxis.set_major_formatter(formatter)
        self.fig.canvas.draw()
        self.fig.canvas.flush_events()

    def plot_stl_results(self):
        axes_dict = self.prepare_fig()

        # Measured
        ax = axes_dict['ax_measured']
        gapfilled_locs = self.var_selected_df[self.target_col].isnull()
        gapfilled_vals = self.var_selected_df[gapfilled_locs][self.target_gf_col]
        num_measured = len(self.var_selected_df[~gapfilled_locs][self.target_gf_col])
        ax.plot_date(x=self.var_selected_df.index,
                     y=self.var_selected_df[self.target_gf_col],
                     color='#78909C', alpha=.7, ls='-', marker='o', markeredgecolor='none',
                     ms=4, zorder=98, label=f"measured ({num_measured} values)")
        ax.plot_date(x=gapfilled_vals.index,
                     y=gapfilled_vals,
                     color='#ef5350', alpha=.9, ls='none', marker='o', markeredgecolor='none',
                     ms=5, zorder=99, label=f"gap-filled values ({len(gapfilled_vals)} values, random forest)")
        ax.text(0.01, 0.97, f"{self.target_col[0]}  {self.target_col[1]}",
                size=FONTSIZE_HEADER_AXIS, color=FONTCOLOR_HEADER_AXIS,
                backgroundcolor='none', transform=ax.transAxes, alpha=1,
                horizontalalignment='left', verticalalignment='top', zorder=99)
        self.fig.canvas.draw()
        self.fig.canvas.flush_events()

        # Trend
        ax = axes_dict['ax_trend']
        ax.plot_date(x=self.var_selected_df.index,
                     y=self.var_selected_df[self.trend_col],
                     color='#78909C', alpha=.9, ls='-', marker='o', markeredgecolor='none',
                     ms=4, zorder=98, label=f"trend over {self.trend} records")
        gapfilled_vals = self.var_selected_df[gapfilled_locs][self.trend_col]
        ax.plot_date(x=gapfilled_vals.index,
                     y=gapfilled_vals,
                     color='#ef5350', alpha=.9, ls='none', marker='o', markeredgecolor='none',
                     ms=5, zorder=99)
        ax.text(0.01, 0.97, f"Trend",
                size=FONTSIZE_HEADER_AXIS, color=FONTCOLOR_HEADER_AXIS,
                backgroundcolor='none', transform=ax.transAxes, alpha=1,
                horizontalalignment='left', verticalalignment='top', zorder=99)
        self.fig.canvas.draw()
        self.fig.canvas.flush_events()

        # Seasonal
        ax = axes_dict['ax_season']
        ax.plot_date(x=self.var_selected_df.index,
                     y=self.var_selected_df[self.seasonal_col],
                     color='#78909C', alpha=.9, ls='-', marker='o', markeredgecolor='none',
                     ms=4, zorder=98, label=f"seasonal over {self.season} records")
        gapfilled_vals = self.var_selected_df[gapfilled_locs][self.seasonal_col]
        ax.plot_date(x=gapfilled_vals.index,
                     y=gapfilled_vals,
                     color='#ef5350', alpha=.9, ls='none', marker='o', markeredgecolor='none',
                     ms=5, zorder=99)
        ax.text(0.01, 0.97, f"Seasonality",
                size=FONTSIZE_HEADER_AXIS, color=FONTCOLOR_HEADER_AXIS,
                backgroundcolor='none', transform=ax.transAxes, alpha=1,
                horizontalalignment='left', verticalalignment='top', zorder=99)
        self.fig.canvas.draw()
        self.fig.canvas.flush_events()

        # Residuals
        ax = axes_dict['ax_residuals']
        ax.plot_date(x=self.var_selected_df.index,
                     y=self.var_selected_df[self.resid_col],
                     color='#78909C', alpha=.9, ls='-', marker='o', markeredgecolor='none',
                     ms=4, zorder=98, label=f"measured (+gapfilled) without trend and seasonal")
        gapfilled_vals = self.var_selected_df[gapfilled_locs][self.resid_col]
        ax.plot_date(x=gapfilled_vals.index,
                     y=gapfilled_vals,
                     color='#ef5350', alpha=.9, ls='none', marker='o', markeredgecolor='none',
                     ms=5, zorder=99)
        ax.text(0.01, 0.97, f"Residuals",
                size=FONTSIZE_HEADER_AXIS, color=FONTCOLOR_HEADER_AXIS,
                backgroundcolor='none', transform=ax.transAxes, alpha=1,
                horizontalalignment='left', verticalalignment='top', zorder=99)
        self.fig.canvas.draw()
        self.fig.canvas.flush_events()

        # Format
        for key, ax in axes_dict.items():
            gui.plotfuncs.default_grid(ax=ax)
            gui.plotfuncs.default_legend(ax=ax)
            locator = mdates.AutoDateLocator(minticks=3, maxticks=30)
            formatter = mdates.ConciseDateFormatter(locator)
            ax.xaxis.set_major_locator(locator)
            ax.xaxis.set_major_formatter(formatter)
        self.fig.canvas.draw()
        self.fig.canvas.flush_events()

    def check_if_selected_empty(self):
        _list = self.lst_varlist_selected
        if not self.selected_vars_lookup_dict:
            _list.addItem('-empty-')
            empty = True
        else:
            empty = False
        return empty
