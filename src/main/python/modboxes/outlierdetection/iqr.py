import matplotlib.dates as mdates
import matplotlib.gridspec as gridspec
import numpy as np
import pandas as pd
from PyQt5 import QtGui

import gui.add_to_layout
import gui.base
import gui.elements
import gui.make
import gui.plotfuncs
import logger
from modboxes.plots.styles.LightTheme import *
from pkgs.dfun.frames import export_to_main


class addContent(gui.base.buildTab):
    """Build new tab and populate with contents"""

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id, tab_template='SVP')

        # Tab icon
        self.TabWidget.setTabIcon(self.tab_ix, QtGui.QIcon(app_obj.ctx.tab_icon_outlier_detection))

        # Add settings menu contents
        self.lne_rng_multiplier, self.btn_calc, self.btn_keep_marked, \
        self.btn_remove_marked, self.btn_add_as_new_var = \
            self.add_settings_fields()

        # Add variables required in settings
        self.populate_settings_fields()

        # Add available variables to variable lists
        gui.base.buildTab.populate_variable_list(obj=self)

        # # Add plots
        # self.populate_plot_area()

    # def populate_plot_area(self):
    #     self.axes_dict = self.add_axes()

    def populate_settings_fields(self):
        pass

    # def add_axes(self):
    #     gs = gridspec.GridSpec(1, 1)  # rows, cols
    #     gs.update(wspace=0.2, hspace=0.2, left=0.03, right=0.96, top=0.96, bottom=0.03)
    #     ax_main = self.fig.add_subplot(gs[0, 0])
    #     axes_dict = {'ax_main': ax_main}
    #     for key, ax in axes_dict.items():
    #         gui.plotfuncs.default_format(ax=ax, txt_xlabel=False)
    #     return axes_dict

    def add_settings_fields(self):
        gui.elements.add_header_subheader_to_grid_top(layout=self.sett_layout,
                                                      txt=["Interquartile Range",
                                                           "Outlier Detection"])

        gui.add_to_layout.add_spacer_item_to_grid(layout=self.sett_layout, row=1, col=0)
        gui.elements.add_header_in_grid_row(layout=self.sett_layout, txt="Range", row=2)

        lne_rng_multiplier = gui.elements.add_label_linedit_pair_to_grid(
            txt='Range Multiplier', css_ids=['', 'cyan'], layout=self.sett_layout,
            row=3, col=0, orientation='horiz')
        lne_rng_multiplier.setText('1.5')

        gui.add_to_layout.add_spacer_item_to_grid(layout=self.sett_layout, row=4, col=0)
        btn_calc = gui.elements.add_button_to_grid(grid_layout=self.sett_layout,
                                                   txt='Calculate Outliers', css_id='',
                                                   row=5, col=0, rowspan=1, colspan=2)
        btn_keep_marked = gui.elements.add_button_to_grid(grid_layout=self.sett_layout,
                                                          txt='Keep Marked Values', css_id='',
                                                          row=6, col=0, rowspan=1, colspan=2)
        btn_remove_marked = gui.elements.add_button_to_grid(grid_layout=self.sett_layout,
                                                            txt='Remove Marked Values', css_id='',
                                                            row=7, col=0, rowspan=1, colspan=2)

        gui.add_to_layout.add_spacer_item_to_grid(layout=self.sett_layout, row=8, col=0)
        btn_add_as_new_var = gui.elements.add_button_to_grid(grid_layout=self.sett_layout,
                                                             txt='+ Add As New Var', css_id='btn_add_as_new_var',
                                                             row=9, col=0, rowspan=1, colspan=2)

        self.sett_layout.setRowStretch(10, 1)
        return lne_rng_multiplier, btn_calc, btn_keep_marked, btn_remove_marked, \
               btn_add_as_new_var


class Run(addContent):
    target_loaded = False
    marker_isset = False
    ready_to_export = False
    marker_filter = None  # Filter to mark detected outliers
    sub_outdir = "outlier_removal_iqr"

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id)
        logger.log(name='>>> Starting Outlier Removal: IQR', dict={}, highlight=True)  # Log info
        self.sub_outdir = self.project_outdir / self.sub_outdir
        self.class_df = pd.DataFrame()
        self.target_col = None
        self.set_colnames()
        gui.base.buildTab.update_btn_status(obj=self)
        self.axes_dict = self.make_axes_dict()

    def select_target(self):
        """Select target var from list"""
        self.set_target_col()
        self.class_df = self.init_class_df()
        self.update_fields()
        self.init_new_cols()
        self.plot_data()
        self.target_loaded = True
        gui.base.buildTab.update_btn_status(obj=self)

    def init_class_df(self):
        return self.tab_data_df[[self.target_col]].copy()

    def update_fields(self):
        pass

    def set_target_col(self):
        """Get column name of target var from var list"""
        target_var = self.lst_varlist_available.selectedIndexes()
        target_var_ix = target_var[0].row()
        self.target_col = self.col_dict_tuples[target_var_ix]

    def get_settings_from_fields(self):
        rng_multiplier = float(self.lne_rng_multiplier.text())
        return rng_multiplier

    def calc(self):
        self.class_df = self.class_df[
            [self.target_col]]  # Needed in case executed several times in a row, reset aux cols
        rng_multiplier = self.get_settings_from_fields()
        self.init_new_cols()

        # Calculates the 25th and 75th percentiles to determine the interquartile range.
        # Additional columns are added to the focus_df that contain True/False depending
        # on whether the respective data point falls into the respective range.
        # kudos: https://stackoverflow.com/questions/34782063/how-to-use-pandas-filter-with-iqr
        # more info about IQR: https://www.back2code.me/2017/08/outliers/
        # https://en.wikipedia.org/wiki/Outlier#Tukey's_fences
        self.calc_percentiles()
        self.calc_upper_lower_lim(rng_multiplier=rng_multiplier)
        self.generate_flag()
        self.mark_in_plot()

    def calc_percentiles(self):
        self.class_df[self.p25_col] = self.class_df[self.target_col].quantile(0.25)
        self.class_df[self.p75_col] = self.class_df[self.target_col].quantile(0.75)
        self.class_df[self.range_col] = self.class_df[self.p75_col] - self.class_df[self.p25_col]

    def calc_upper_lower_lim(self, rng_multiplier):
        self.class_df[self.upper_lim_col] = \
            self.class_df[self.p75_col] + rng_multiplier * self.class_df[self.range_col]
        self.class_df[self.lower_lim_col] = \
            self.class_df[self.p25_col] - rng_multiplier * self.class_df[self.range_col]
        # self.outlier_limit_upper = self.q3 + self.multiplier_value * self.range
        # self.outlier_limit_lower = self.q1 - self.multiplier_value * self.range
        # self.focus_df[self.marker_col_outlier_limit_upper] = self.outlier_limit_upper
        # self.focus_df[self.marker_col_outlier_limit_lower] = self.outlier_limit_lower

    def generate_flag(self):
        """Flag values that are outside upper or lower limit"""
        self.class_df[self.qcflag_col] = \
            (self.class_df[self.target_col] < self.class_df[self.lower_lim_col]) | \
            (self.class_df[self.target_col] > self.class_df[self.upper_lim_col])

    def mark_in_plot(self):
        self.marker_filter = self.class_df[self.qcflag_col] == True
        self.marker_isset = True
        self.ready_to_export = False
        gui.base.buildTab.update_btn_status(obj=self)
        self.plot_data()

    def keep_marked(self):
        if self.marker_isset:
            self.class_df[~self.marker_filter] = np.nan
            self.marker_isset = False
            self.ready_to_export = True
            gui.base.buildTab.update_btn_status(obj=self)
            self.plot_data()

    def remove_marked(self):
        if self.marker_isset:
            self.class_df[self.marker_filter] = np.nan
            self.marker_isset = False
            self.ready_to_export = True
            gui.base.buildTab.update_btn_status(obj=self)
            self.plot_data()

    def prepare_export(self):
        export_df = self.class_df[[self.target_col]].copy()  # Outliers here already removed
        outlier_removed_col = (self.target_col[0] + "+odIQR", self.target_col[1])
        export_df[outlier_removed_col] = export_df[self.target_col]
        export_df = export_df[[outlier_removed_col]]
        return export_df

    def get_selected(self, main_df):
        """Return modified class data back to main data"""
        export_df = self.prepare_export()
        main_df = export_to_main(main_df=main_df,
                                 export_df=export_df,
                                 tab_data_df=self.tab_data_df)  # Return results to main
        self.btn_add_as_new_var.setDisabled(True)
        return main_df

    def plot_data(self):
        # Delete all axes in figure
        for ax in self.fig.axes:
            self.fig.delaxes(ax)

        self.axes_dict = self.make_axes_dict()
        ax = self.axes_dict['ax_main']

        df = self.class_df
        col = self.target_col
        legend_lns = []

        lns = ax.plot_date(x=df.index, y=df[col],
                           color='#546E7A', alpha=1, ls='-',
                           marker='o', markeredgecolor='none', ms=4, zorder=98, label=f"{col[0]} {col[1]}")
        legend_lns.append(lns[0])
        ax.text(0.01, 0.97, f"{col[0]}",
                size=FONTSIZE_HEADER_AXIS, color=FONTCOLOR_HEADER_AXIS,
                backgroundcolor='none', transform=ax.transAxes, alpha=1,
                horizontalalignment='left', verticalalignment='top', zorder=99)

        if self.marker_isset:
            # marker_S = df[col].copy()[self.marker_filter]
            marker_S = df[self.target_col].copy()[self.marker_filter]

            num_outliers = len(marker_S)
            ax.plot_date(x=marker_S.index, y=marker_S,
                         color='#ef5350', alpha=1, ls='none', mew=1, mec='#e53935',
                         marker='o', ms=5, zorder=98, label=f'outliers ({num_outliers} found)')

            ax.plot_date(x=self.class_df.index, y=self.class_df[self.upper_lim_col],
                         color='#FFA726', alpha=.5, ls='--',
                         marker='None', ms=0, zorder=98, label=f'upper outlier limit')
            ax.plot_date(x=self.class_df.index, y=self.class_df[self.lower_lim_col],
                         color='#29B6F6', alpha=.5, ls='--',
                         marker='None', ms=0, zorder=98, label=f'lower outlier limit')

        gui.plotfuncs.default_grid(ax=ax)
        gui.plotfuncs.default_legend(ax=ax, from_line_collection=True, line_collection=legend_lns)

        locator = mdates.AutoDateLocator(minticks=3, maxticks=30)
        formatter = mdates.ConciseDateFormatter(locator)
        ax.xaxis.set_major_locator(locator)
        ax.xaxis.set_major_formatter(formatter)

        self.fig.canvas.draw()
        self.fig.canvas.flush_events()

    def set_colnames(self):
        # Marker for values in the outlier range
        self.qcflag_col = ('QCF_od_iqr_outlier', '[True=outlier]')
        self.upper_lim_col = ('_upper_limit', '[aux]')
        self.lower_lim_col = ('_lower_limit', '[aux]')
        self.p25_col = ('_P25', '[aux]')
        self.p75_col = ('_P75', '[aux]')
        self.range_col = ('_range_iqr', '[aux]')

    def init_new_cols(self):
        self.class_df[self.qcflag_col] = np.nan
        self.class_df[self.upper_lim_col] = np.nan
        self.class_df[self.lower_lim_col] = np.nan
        self.class_df[self.p25_col] = np.nan
        self.class_df[self.p75_col] = np.nan

    def make_axes_dict(self):
        gs = gridspec.GridSpec(1, 1)  # rows, cols
        gs.update(wspace=0.2, hspace=0.2, left=0.03, right=0.96, top=0.96, bottom=0.03)
        ax_main = self.fig.add_subplot(gs[0, 0])
        axes_dict = {'ax_main': ax_main}
        for key, ax in axes_dict.items():
            gui.plotfuncs.default_format(ax=ax, txt_xlabel=False)
        return axes_dict
