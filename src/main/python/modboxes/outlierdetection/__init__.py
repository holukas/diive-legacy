# from . import abs_limit  is now a package
from . import doublediff
from . import hampel
from . import iqr
from . import running
from ._future import seasonal_trend_loess
from . import trim
