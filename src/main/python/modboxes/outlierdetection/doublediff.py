import fnmatch

import matplotlib.dates as mdates
import matplotlib.gridspec as gridspec
import numpy as np
import pandas as pd
from PyQt5 import QtGui

import gui
import gui.add_to_layout
import gui.base
import gui.make
import utils as io
import logger
import modboxes.plots.styles.LightTheme as theme
from pkgs.dfun.frames import export_to_main
from help.tooltips import od_doublediff as tooltips

# pd.set_option('display.width', 1000)
# pd.set_option('display.max_columns', 15)
# pd.set_option('display.max_rows', 20)


class addContent(gui.base.buildTab):
    """Build new tab and populate with contents"""

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id, tab_template='SVP')

        # Tab icon
        self.TabWidget.setTabIcon(self.tab_ix, QtGui.QIcon(app_obj.ctx.tab_icon_outlier_detection))

        # Add settings menu contents
        self.drp_define_nighttime_based_on, self.drp_set_nighttime_if, self.lne_nighttime_threshold, \
        self.lne_z, self.btn_keep_marked, self.btn_remove_marked, self.btn_add_as_new_var, self.btn_calc, \
        self.lne_outlier_timewin, self.drp_repeat = \
            self.add_settings_fields()

        # Add variables required in settings
        self.populate_settings_fields()

        # Add available variables to variable lists
        gui.base.buildTab.populate_variable_list(obj=self)

        # # Add plots
        # self.populate_plot_area()

    # def populate_plot_area(self):
    #     self.axes_dict = self.add_axes()

    def populate_settings_fields(self):
        for ix, colname_tuple in enumerate(self.tab_data_df.columns):
            if any(fnmatch.fnmatch(colname_tuple[0], id) for id in io.vargroups.NIGHTTIME_DETECTION):
                self.drp_define_nighttime_based_on.addItem(self.col_list_pretty[ix])

    # def add_axes(self):
    #     gs = gridspec.GridSpec(1, 1)  # rows, cols
    #     gs.update(wspace=0.2, hspace=0.2, left=0.03, right=0.96, top=0.96, bottom=0.03)
    #     ax_main = self.fig.add_subplot(gs[0, 0])
    #     axes_dict = {'ax_main': ax_main}
    #     for key, ax in axes_dict.items():
    #         gui.plotfuncs.default_format(ax=ax, txt_xlabel=False)
    #     return axes_dict

    def add_settings_fields(self):
        gui.elements.add_header_subheader_to_grid_top(layout=self.sett_layout,
                                                      txt=["Double Difference",
                                                           "Outlier Detection"])
        gui.elements.add_info_hover_to_grid(layout=self.sett_layout, row=0, col=1,
                                            txt_info_hover=tooltips.doublediff)
        gui.add_to_layout.add_spacer_item_to_grid(layout=self.sett_layout, row=1, col=0)

        # Day / night
        gui.elements.add_header_in_grid_row(
            row=2, layout=self.sett_layout, txt='Day / Night')
        drp_define_nighttime_based_on = gui.elements.grd_LabelDropdownPair(
            txt='Define Nighttime Based On', css_ids=['', 'cyan'], layout=self.sett_layout,
            row=3, col=0, orientation='horiz')
        drp_set_nighttime_if = gui.elements.grd_LabelDropdownPair(
            txt='Set To Nighttime If', css_ids=['', 'cyan'], layout=self.sett_layout,
            row=4, col=0, orientation='horiz')
        drp_set_nighttime_if.addItems(['Smaller Than Threshold', 'Larger Than Threshold'])
        lne_nighttime_threshold = gui.elements.add_label_linedit_pair_to_grid(
            txt='Threshold', css_ids=['', 'cyan'], layout=self.sett_layout,
            row=5, col=0, orientation='horiz')
        lne_nighttime_threshold.setText('20')
        gui.add_to_layout.add_spacer_item_to_grid(self.sett_layout, 6, 0)

        # Outlier settings
        gui.elements.add_header_in_grid_row(
            row=7, layout=self.sett_layout, txt="Outlier Threshold")
        lne_outlier_timewin = gui.elements.add_label_linedit_pair_to_grid(
            txt='Time Window (Days)', css_ids=['', 'cyan'], layout=self.sett_layout,
            row=8, col=0, orientation='horiz')
        lne_outlier_timewin.setText('13')
        lne_z = gui.elements.add_label_linedit_pair_to_grid(
            txt='Threshold Value (z)', css_ids=['', 'cyan'], layout=self.sett_layout,
            row=9, col=0, orientation='horiz')
        lne_z.setText('5.5')
        drp_repeat = gui.elements.grd_LabelDropdownPair(
            txt='Repeat Until All Outliers Removed', css_ids=['', ''], layout=self.sett_layout,
            row=10, col=0, orientation='horiz')
        drp_repeat.addItems(['No', 'Yes'])
        gui.add_to_layout.add_spacer_item_to_grid(
            row=11, col=0, layout=self.sett_layout)

        # Buttons
        btn_calc = gui.elements.add_button_to_grid(
            grid_layout=self.sett_layout, txt='Calculate Outliers', css_id='',
            row=12, col=0, rowspan=1, colspan=2)
        btn_keep_marked = gui.elements.add_button_to_grid(
            grid_layout=self.sett_layout, txt='Keep Marked Values', css_id='',
            row=13, col=0, rowspan=1, colspan=2)
        btn_remove_marked = gui.elements.add_button_to_grid(
            grid_layout=self.sett_layout, txt='Remove Marked Values', css_id='',
            row=14, col=0, rowspan=1, colspan=2)
        gui.add_to_layout.add_spacer_item_to_grid(
            row=15, col=0, layout=self.sett_layout)
        btn_add_as_new_var = gui.elements.add_button_to_grid(
            grid_layout=self.sett_layout, txt='+ Add As New Var', css_id='btn_add_as_new_var',
            row=16, col=0, rowspan=1, colspan=2)

        self.sett_layout.setRowStretch(17, 1)
        return drp_define_nighttime_based_on, drp_set_nighttime_if, lne_nighttime_threshold, \
               lne_z, btn_keep_marked, btn_remove_marked, btn_add_as_new_var, btn_calc, \
               lne_outlier_timewin, drp_repeat


class Run(addContent):
    target_loaded = False
    marker_isset = False
    ready_to_export = False
    marker_filter = None  # Filter to mark detected outliers
    grouping_col = ('_group', '[#]')
    sub_outdir = "outlier_removal_double_diff"

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id)
        logger.log(name='>>> Starting Class Finder', dict={}, highlight=True)  # Log info
        self.sub_outdir = self.project_outdir / self.sub_outdir
        self.class_df = pd.DataFrame()
        self.target_col = None
        self.set_colnames()
        gui.base.buildTab.update_btn_status(obj=self)
        self.axes_dict = self.make_axes_dict()
        self.get_settings_from_fields()

    def select_target(self):
        """Select target var from list"""
        self.set_target_col()
        self.class_df = self.init_class_df()
        self.update_fields()
        self.init_new_cols()
        self.plot_data()
        self.target_loaded = True
        gui.base.buildTab.update_btn_status(obj=self)

    def init_class_df(self):
        return self.tab_data_df[[self.target_col]].copy()

    def set_target_col(self):
        """Get column name of target var from var list"""
        target_var = self.lst_varlist_available.selectedIndexes()
        target_var_ix = target_var[0].row()
        self.target_col = self.col_dict_tuples[target_var_ix]

    def update_fields(self):
        pass

    def get_settings_from_fields(self):
        self.z_val = float(self.lne_z.text())
        self.nighttime_col = self.get_col_from_drp_pretty(drp=self.drp_define_nighttime_based_on)
        self.set_nighttime_if = self.drp_set_nighttime_if.currentText()
        self.nighttime_threshold = int(self.lne_nighttime_threshold.text())
        self.outlier_timewin = float(self.lne_outlier_timewin.text())
        self.repeat = self.drp_repeat.currentText()

    def get_col_from_drp_pretty(self, drp):
        """ Set target to selected variable. """
        target_pretty = drp.currentText()
        target_pretty_ix = self.col_list_pretty.index(target_pretty)
        target_col = self.col_dict_tuples[target_pretty_ix]
        return target_col

    def calc(self):
        self.get_settings_from_fields()
        self.class_df = self.class_df[[self.target_col]]
        self.add_daynight_data()
        self.init_new_cols()

        # Group into e.g. 13-day blocks
        freq = f"{self.outlier_timewin}D"

        num_outliers = 1
        while num_outliers > 0:
            grouped_freq = self.class_df.groupby(pd.Grouper(level=self.class_df.index.name, freq=freq))
            for key_freq, group_freq_df in grouped_freq:
                print(f"FROM {group_freq_df.index[0]}"
                      f"    TO {group_freq_df.index[-1]}"
                      f"    VALUES {len(group_freq_df)}")

                # Group by daytime to split into day- and night-data
                grouped_daytime = group_freq_df.groupby(self.grouping_col)
                for key_daytime, group_daynight_df in grouped_daytime:
                    # Following equations in reference
                    _group_df = group_daynight_df.copy()
                    _group_df = self.calc_di(df=_group_df, shift_by=1)
                    _group_df = self.calc_MAD(df=_group_df)
                    _group_df = self.calc_upper_lower_lim(df=_group_df, z=self.z_val)
                    _group_df = self.generate_flag(df=_group_df)
                    # Add results for this group to focus_df (np.nan in focus_df is replaced w/ results)
                    self.class_df[self.priority_cols] = \
                        self.class_df[self.priority_cols].combine_first(_group_df[self.priority_cols])

                num_outliers = self.class_df[self.qcflag_col].sum()
            self.mark_in_plot()
            if self.repeat == 'Yes':
                self.remove_marked()
            if self.repeat == 'No':
                break

    def extend_df(self, daynight_df, other_df, which):
        """
        Extend start or end by one value, for the calc of di

            see Papale et al. (2006), Section 2.2, p573
        """
        extension_df = daynight_df.copy()
        max_possible_ix = len(self.class_df) - 1

        # Search integer index of date in other
        which_ix = 0 if which == 'start' else -1
        date = extension_df.iloc[which_ix].name
        ix_in_other = np.where(other_df.index == date)[0]

        # Calculate index required for extension
        adjacent_ix = -1 if which == 'start' else 1
        extension_ix = ix_in_other + adjacent_ix  # 'start' will get the previous index (-1), 'end' the next (+1)

        if (extension_ix < 0) or (extension_ix > max_possible_ix):  # Not possible, return
            return extension_df

        row = self.class_df.iloc[extension_ix]  # One row of records that will be added
        extension_df = pd.concat([extension_df, row], axis=0)
        extension_df = extension_df.sort_index(axis=0)
        return extension_df

    def set_colnames(self):
        self.shifted_down_col = ('_shifted_down', '[aux]')
        self.shifted_up_col = ('_shifted_up', '[aux]')
        self.di_col = ('_di', '[aux]')  # Differences
        self.Md_col = ('_Md', '[aux]')  # Median of differences
        self.MAD_col = ('_MAD', '[aux]')
        self.qcflag_col = ('QCF_od_doublediff_outlier', '[True=outlier]')
        self.upper_lim_col = ('_upper_limit', '[aux]')
        self.lower_lim_col = ('_lower_limit', '[aux]')
        self.date_group_from_col = ('_date_group_from', '[aux]')
        self.date_group_to_col = ('_date_group_to', '[aux]')
        self.outlier_col = ('_filtered_ts', '[aux]')

        self.priority_cols = [self.di_col, self.qcflag_col,
                              self.upper_lim_col,
                              self.lower_lim_col]

    def add_daynight_data(self):
        """Add flag to indicate group membership, in this case day/night data"""
        self.class_df[self.grouping_col] = np.nan

        # Check if grouping col is part of the class df
        delete_grouping_col = True
        if self.nighttime_col in list(self.class_df.columns):
            delete_grouping_col = False

        # Generate flag
        nighttime_filter = None
        self.class_df[self.nighttime_col] = self.tab_data_df[self.nighttime_col]  # Add for grouping
        if self.set_nighttime_if == 'Smaller Than Threshold':
            nighttime_filter = self.class_df[self.nighttime_col] < self.nighttime_threshold
        elif self.set_nighttime_if == 'Larger Than Threshold':
            nighttime_filter = self.class_df[self.nighttime_col] > self.nighttime_threshold
        self.class_df.loc[nighttime_filter, [self.grouping_col]] = 0
        self.class_df.loc[~nighttime_filter, [self.grouping_col]] = 1
        if delete_grouping_col:
            self.class_df.drop(self.nighttime_col, axis=1, inplace=True)  # Only needed for grouping

    def init_new_cols(self):
        self.class_df[self.shifted_down_col] = np.nan
        self.class_df[self.shifted_up_col] = np.nan
        self.class_df[self.di_col] = np.nan
        self.class_df[self.Md_col] = np.nan
        self.class_df[self.MAD_col] = np.nan
        self.class_df[self.qcflag_col] = np.nan
        self.class_df[self.upper_lim_col] = np.nan
        self.class_df[self.lower_lim_col] = np.nan
        self.class_df[self.date_group_from_col] = np.nan
        self.class_df[self.date_group_to_col] = np.nan

    def calc_di(self, df, shift_by):
        """
        Calculate differences di and median of differences Md

            Eq. (1) / di = (NEEi − NEEi−1) − (NEEi+1 − NEEi )

        """
        # Before potential extension, get original start and end index.
        # Since extension is only needed for the calculation of di,
        # the original start and end can be used to restrict df to
        # it's original range after di was calcualted.
        df_start = df.index[0]
        df_end = df.index[-1]
        df = self.extend_df(daynight_df=df, other_df=self.class_df, which='start')
        df = self.extend_df(daynight_df=df, other_df=self.class_df, which='end')

        df.loc[:, self.shifted_down_col] = df[self.target_col].shift(periods=shift_by)
        df.loc[:, self.shifted_up_col] = df[self.target_col].shift(periods=-shift_by)
        df.loc[:, self.di_col] = \
            df[self.target_col].subtract(df[self.shifted_down_col]) - \
            df[self.shifted_up_col].subtract(df[self.target_col])
        df[self.Md_col] = df[self.di_col].median()
        df = df.loc[df_start:df_end]  # Restore original range (unextended)
        return df

    def calc_MAD(self, df):
        """Calculate median of absolute deviation about the median"""
        # Eq. (4) / MAD = median (|di−Md|)
        df[self.MAD_col] = df[self.di_col] - df[self.Md_col]
        df[self.MAD_col] = df[self.MAD_col].abs()
        df[self.MAD_col] = df[self.MAD_col].median()
        return df

    def calc_upper_lower_lim(self, df, z):
        # Eq. (2), (3) Upper and lower limits
        df[self.upper_lim_col] = df[self.Md_col] + ((df[self.MAD_col].multiply(z)) / 0.6745)
        df[self.lower_lim_col] = df[self.Md_col] - (df[self.MAD_col].multiply(z) / 0.6745)
        return df

    def generate_flag(self, df):
        """Flag values that are outside upper or lower limit"""
        df[self.qcflag_col] = (df[self.di_col] > df[self.upper_lim_col]) | \
                              (df[self.di_col] < df[self.lower_lim_col])
        return df

    def mark_in_plot(self):
        self.marker_filter = self.class_df[self.qcflag_col] == True
        self.marker_isset = True
        self.ready_to_export = False
        gui.base.buildTab.update_btn_status(obj=self)
        self.plot_data()

    def keep_marked(self):
        if self.marker_isset:
            self.class_df[~self.marker_filter] = np.nan
            self.marker_isset = False
            self.ready_to_export = True
            gui.base.buildTab.update_btn_status(obj=self)
            self.plot_data()

    def remove_marked(self):
        if self.marker_isset:
            self.class_df[self.marker_filter] = np.nan
            self.marker_isset = False
            self.ready_to_export = True
            gui.base.buildTab.update_btn_status(obj=self)
            self.plot_data()

    def prepare_export(self):
        export_df = self.class_df[[self.target_col]].copy()  # Outliers here already removed
        outlier_removed_col = (self.target_col[0] + "+odDD", self.target_col[1])
        export_df[outlier_removed_col] = export_df[self.target_col]
        export_df = export_df[[outlier_removed_col]]
        return export_df

    def get_selected(self, main_df):
        """Return modified class data back to main data"""
        export_df = self.prepare_export()
        main_df = export_to_main(main_df=main_df,
                                 export_df=export_df,
                                 tab_data_df=self.tab_data_df)  # Return results to main
        self.btn_add_as_new_var.setDisabled(True)
        return main_df

    def plot_data(self):
        # Delete all axes in figure
        for ax in self.fig.axes:
            self.fig.delaxes(ax)

        self.axes_dict = self.make_axes_dict()
        ax = self.axes_dict['ax_main']

        df = self.class_df
        col = self.target_col
        legend_lns = []

        lns = ax.plot_date(x=df.index, y=df[col],
                           color='#546E7A', alpha=1, ls='-',
                           marker='o', markeredgecolor='none', ms=4, zorder=98, label=f"{col[0]} {col[1]}")
        legend_lns.append(lns[0])
        ax.text(0.01, 0.97, f"{col[0]}",
                size=theme.FONTSIZE_HEADER_AXIS, color=theme.FONTCOLOR_HEADER_AXIS,
                backgroundcolor='none', transform=ax.transAxes, alpha=1,
                horizontalalignment='left', verticalalignment='top', zorder=99)

        if self.marker_isset:
            # marker_S = df[col].copy()[self.marker_filter]
            marker_S = df[self.target_col].copy()[self.marker_filter]

            num_outliers = len(marker_S)
            ax.plot_date(x=marker_S.index, y=marker_S,
                         color='#ef5350', alpha=1, ls='none', mew=1, mec='#e53935',
                         marker='o', ms=5, zorder=98, label=f'outliers ({num_outliers} found)')

            twin_x = gui.plotfuncs.make_secondary_yaxis(ax=ax)
            gui.plotfuncs.default_format(ax=twin_x, txt_ylabel='di value')
            lns = twin_x.plot_date(x=self.class_df.index, y=self.class_df[self.di_col],
                                   color='#e57373', alpha=.5, ls='--',
                                   marker='None', ms=0, zorder=98, label=f'di')
            legend_lns.append(lns[0])
            lns = twin_x.plot_date(x=self.class_df.index, y=self.class_df[self.upper_lim_col],
                                   color='#FFA726', alpha=.5, ls='--',
                                   marker='None', ms=0, zorder=98, label=f'upper di limit')
            legend_lns.append(lns[0])
            lns = twin_x.plot_date(x=self.class_df.index, y=self.class_df[self.lower_lim_col],
                                   color='#29B6F6', alpha=.5, ls='--',
                                   marker='None', ms=0, zorder=98, label=f'lower di limit')
            legend_lns.append(lns[0])

        gui.plotfuncs.default_grid(ax=ax)
        gui.plotfuncs.default_legend(ax=ax, from_line_collection=True, line_collection=legend_lns)

        locator = mdates.AutoDateLocator(minticks=3, maxticks=30)
        formatter = mdates.ConciseDateFormatter(locator)
        ax.xaxis.set_major_locator(locator)
        ax.xaxis.set_major_formatter(formatter)

        self.fig.canvas.draw()
        self.fig.canvas.flush_events()

    def make_axes_dict(self):
        gs = gridspec.GridSpec(1, 1)  # rows, cols
        gs.update(wspace=0.2, hspace=0.2, left=0.03, right=0.96, top=0.96, bottom=0.03)
        ax_main = self.fig.add_subplot(gs[0, 0])
        axes_dict = {'ax_main': ax_main}
        for key, ax in axes_dict.items():
            gui.plotfuncs.default_format(ax=ax, txt_xlabel=False)
        return axes_dict

    #                                                            y=plot_df[self.marker_col_outlier_limit_upper], linestyle='-',
    #                                                            y=plot_df[self.marker_col_outlier_limit_lower], linestyle='-',
    #     modboxes.default.plots._shared.add_to_existing_ax(self.fig, add_to_ax=self.ax, x=plot_df.index, y=plot_df[self.di_col],
