import matplotlib.dates as mdates
import matplotlib.gridspec as gridspec
import numpy as np
import pandas as pd
from PyQt5 import QtGui

import gui.base
import gui.elements
import gui.elements
import gui.plotfuncs
import logger
import modboxes.plots._shared
from gui import elements
from modboxes.plots.styles.LightTheme import *


class addContent(gui.base.buildTab):
    """Build new tab and populate with contents"""

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id, tab_template='SVP')

        # Tab icon
        self.TabWidget.setTabIcon(self.tab_ix, QtGui.QIcon(app_obj.ctx.tab_icon_analyses))

        # Add settings menu contents
        self.btn_calc = \
            self.add_settings_fields()

        # # Add variables required in settings
        # self.populate_settings_fields()

        # Add available variables to variable lists
        gui.base.buildTab.populate_variable_list(obj=self)

        # # Add plots
        # self.populate_plot_area()

    # def populate_plot_area(self):
    #     self.axes_dict = self.add_axes()

    # def populate_settings_fields(self):
    #     pass

    # def add_axes(self):
    #     gs = gridspec.GridSpec(1, 1)  # rows, cols
    #     gs.update(wspace=0.2, hspace=0.2, left=0.03, right=0.96, top=0.96, bottom=0.03)
    #     ax_main = self.fig.add_subplot(gs[0, 0])
    #     axes_dict = {'ax_main': ax_main}
    #     for key, ax in axes_dict.items():
    #         gui.plotfuncs.default_format(ax=ax, txt_xlabel=False)
    #     return axes_dict

    def add_settings_fields(self):
        gui.elements.add_header_to_grid_top(layout=self.sett_layout, txt='Settings')

        btn_calc = elements.add_button_to_grid(grid_layout=self.sett_layout,
                                               txt='Find Gaps', css_id='',
                                               row=1, col=0, rowspan=1, colspan=2)

        self.sett_layout.setRowStretch(2, 1)
        return btn_calc


class Run(addContent):
    target_loaded = False
    marker_isset = False
    ready_to_export = False
    marker_filter = None  # Filter to mark detected outliers
    sub_outdir = "analysis_gapfinder"

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id)
        logger.log(name='>>> Starting Analysis: GapFinder', dict={}, highlight=True)  # Log info
        self.sub_outdir = self.project_outdir / self.sub_outdir
        self.class_df = pd.DataFrame()
        self.gapfinder_df = pd.DataFrame()
        self.target_col = None
        self.set_colnames()
        self.init_new_cols()
        gui.base.buildTab.update_btn_status(obj=self, target=True, marker=False, export=False)
        self.axes_dict = self.make_axes_dict()

    def select_target(self):
        """Select target var from list"""
        self.marker_isset = False
        self.set_target_col()
        self.class_df = self.init_class_df()
        self.init_new_cols()
        # self.update_fields()
        self.plot_data()
        self.target_loaded = True
        self.ready_to_export = False

        gui.base.buildTab.update_btn_status(obj=self, target=True, marker=False, export=False)

    def calc(self):
        # Needed in case executed several times in a row, reset aux cols:
        self.class_df = self.class_df[[self.target_col]]
        # upper_lim, lower_lim = self.get_settings_from_fields()
        # self.init_new_cols()
        self.find_gaps()
        # self.generate_flag()
        # self.mark_in_plot()
        self.mark_in_plot()

    def find_gaps(self):
        self.class_df[self.gap_marker_col] = self.class_df[self.target_col].isnull()
        self.gapfinder_df = GapFinder(df=self.class_df, col=self.target_col, limit=False).get_results()
        self.info_txt = self.count_occurrences()

    def count_occurrences(self):
        info_txt = ""
        counts_df = self.gapfinder_df.groupby('count').aggregate({'count': ['count']})
        counts_df.columns = counts_df.columns.droplevel([0])
        counts_df['gap_length'] = counts_df.index
        info_txt += ('Longest gap: {} values'.format(counts_df['gap_length'].max()))
        # info_txt += ('Shortest gap: {} values\n'.format(test_df['gap_length'].min()))
        return info_txt

    def init_class_df(self):
        return self.tab_data_df[[self.target_col]].copy()

    def init_new_cols(self):
        self.class_df[self.gap_marker_col] = np.nan

    def set_target_col(self):
        """Get column name of target var from var list"""
        target_var = self.lst_varlist_available.selectedIndexes()
        target_var_ix = target_var[0].row()
        self.target_col = self.col_dict_tuples[target_var_ix]

    def get_settings_from_fields(self):
        pass

    def generate_flag(self):
        pass

    def mark_in_plot(self):
        self.marker_filter = self.class_df[self.gap_marker_col] == True
        self.marker_isset = True
        # self.ready_to_export = False
        gui.base.buildTab.update_btn_status(obj=self, target=True, marker=False, export=False)
        self.plot_data()

    def plot_data(self):
        # Delete all axes in figure
        for ax in self.fig.axes:
            self.fig.delaxes(ax)

        self.axes_dict = self.make_axes_dict()
        ax = self.axes_dict['ax_main']

        plot_df = self.class_df.copy()

        ax.plot_date(x=plot_df.index, y=plot_df[self.target_col],
                     color='#546E7A', alpha=1, ls='-',
                     marker='o', markeredgecolor='none', ms=4, zorder=98,
                     label=f"{self.target_col[0]} {self.target_col[1]}")

        if self.marker_isset:
            gapval = plot_df[self.target_col].quantile(0.999)  # For showing in plot only
            plot_df[self.gap_marker_col] = gapval

            ax.plot_date(x=plot_df[self.marker_filter].index, y=plot_df.loc[self.marker_filter, self.gap_marker_col],
                         color='#FFCA28', alpha=.9, ls='none',
                         marker='o', mew=1, mec='#FF8F00', ms=8, zorder=98,
                         label=f"Gap in {self.target_col[0]} ({self.marker_filter.sum()} found)")
            ax.text(.03, .03, self.info_txt, weight='bold',
                    size=FONTSIZE_HEADER_AXIS, color=FONTCOLOR_HEADER_AXIS,
                    backgroundcolor='none', transform=ax.transAxes, alpha=1,
                    horizontalalignment='left', verticalalignment='bottom', zorder=99)

        ax.text(0.01, 0.97, f"{self.target_col[0]}", weight='bold',
                size=FONTSIZE_HEADER_AXIS, color=FONTCOLOR_HEADER_AXIS,
                backgroundcolor='#B0BEC5', transform=ax.transAxes, alpha=1,
                horizontalalignment='left', verticalalignment='top', zorder=99)

        gui.plotfuncs.default_grid(ax=ax)
        gui.plotfuncs.default_legend(ax=ax, from_line_collection=False)

        locator = mdates.AutoDateLocator(minticks=3, maxticks=30)
        formatter = mdates.ConciseDateFormatter(locator)
        ax.xaxis.set_major_locator(locator)
        ax.xaxis.set_major_formatter(formatter)

        self.fig.canvas.draw()
        self.fig.canvas.flush_events()

    def set_colnames(self):
        self.gap_marker_col = ('_gapmarker', '[aux]')

    def update_fields(self):
        pass

    def make_axes_dict(self):
        gs = gridspec.GridSpec(1, 1)  # rows, cols
        gs.update(wspace=0.2, hspace=0.2, left=0.03, right=0.96, top=0.96, bottom=0.03)
        ax_main = self.fig.add_subplot(gs[0, 0])
        axes_dict = {'ax_main': ax_main}
        for key, ax in axes_dict.items():
            gui.plotfuncs.default_format(ax=ax, txt_xlabel=False)
        return axes_dict


class _Run(addContent):
    """ Uses GapFinder to check for gaps, then shows results and info
        in the GUI.
    """

    def __init__(self, focus_df, focus_col, fig, ax, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id)
        self.ax = ax
        self.fig = fig
        self.focus_df = focus_df
        self.focus_col = focus_col

        try:
            self.gapfinder_agg_df = self.find_gaps()
            self.info_txt = self.count_occurrences()
            self.show_in_plot()
        except:
            # TODO better way to handle df with zero gaps
            pass

    # def find_gaps(self):
    #     gapfinder_agg_df = GapFinder(df=self.focus_df, col=self.focus_col, limit=False).get_results()
    #     return gapfinder_agg_df

    # def count_occurrences(self):
    #
    #     info_txt = ''
    #
    #     test_df = \
    #         self.gapfinder_agg_df.groupby('count').aggregate({
    #             'count': ['count'],
    #         })
    #     test_df.columns = test_df.columns.droplevel([0])
    #     test_df['gap_length'] = test_df.index
    #
    #     info_txt += ('Longest gap: {} values)'.format(test_df['gap_length'].max()))
    #     # info_txt += ('Shortest gap: {} values\n'.format(test_df['gap_length'].min()))
    #
    #     return info_txt

    def show_in_plot(self):
        color = '#F48FB1'  # yellow
        # Auxiliary lines

        gapmarker_col = ('gap', '[marker]')
        plot_df = self.focus_df[[self.focus_col]]
        plot_df[gapmarker_col] = np.nan
        filter_nanonly = self.focus_df[self.focus_col].isnull()
        plot_df.loc[filter_nanonly, gapmarker_col] = self.focus_df[self.focus_col].quantile(0.999)

        modboxes.Plots._shared.add_to_existing_ax(self.fig, add_to_ax=self.ax, x=plot_df.index,
                                                  y=plot_df[gapmarker_col],
                                                  linestyle='',
                                                  linewidth=0)

        self.ax.text(0.05, 0.05, self.info_txt,
                     horizontalalignment='left', verticalalignment='top', transform=self.ax.transAxes,
                     size='large', color=color, backgroundcolor='#FCE4EC')

        self.fig.canvas.draw()  # update plot


class GapFinder:
    """
    Find gaps in DataFrame.

    The gapfinder_agg_df contains info about the location of the gaps
    within the limit.
    """
    # Define MultiIndex column names
    gap_values = ('gf_lininterp_values', 'marker')
    focus_col_gf = ('gf_LinInterp', 'marker')
    isgap_col = ('isgap', '[1=True]')  # flag that shows where the gaps are
    isval_col = ('isvalue', '[1=True]')  # flag that shows where the numeric values are
    isval_cumsum_col = ('isvalue_cumsum', '[cumulative]')  # for detecting consecutive gaps

    gaplen_below_limit_col = 'gaplen_below_lim'

    # gap_datetime_col = 'gap_datetime'
    # consec_gaps_col = 'consec_gaps'

    def __init__(self, df, col, limit):
        self.col = col
        self.limit = limit  # number of allowed consecutive gaps
        self.gapfinder_fullres_df = df.copy()
        self.make_required_cols()
        self.gapfinder_agg_df = self.check_gaps()

    def make_required_cols(self):
        # Init new columns
        self.gapfinder_fullres_df[
            self.gapfinder_fullres_df.index.name] = self.gapfinder_fullres_df.index  # needed for filling gap values
        self.gapfinder_fullres_df[self.gap_values] = np.nan
        self.gapfinder_fullres_df[self.focus_col_gf] = np.nan
        self.gapfinder_fullres_df[self.isgap_col] = np.nan
        self.gapfinder_fullres_df[self.isval_col] = np.nan
        self.gapfinder_fullres_df[self.gaplen_below_limit_col] = np.nan

        # The cumulative sum is central to the detection of consecutive gaps.
        # Since values are flagged with 1 and gaps with 0, the cumsum does not
        # change after the last value before the gap. Consecutive gaps will
        # therefore have the same cumsum. This can be used later by grouping
        # data on basis of cumsum.
        self.gapfinder_fullres_df[self.isval_cumsum_col] = np.nan

    def check_gaps(self):
        # self.filter_suffix = '_GF-LIN'

        # Detect consecutive gaps in measured
        # kudos: https://stackoverflow.com/questions/29007830/identifying-consecutive-nans-with-pandas

        # Fill flags into new columns
        self.gapfinder_fullres_df[self.isgap_col] = self.gapfinder_fullres_df[self.col].isnull().astype(
            int)  # NaNs, 1 = True = NaN
        self.gapfinder_fullres_df[self.isval_col] = self.gapfinder_fullres_df[self.col].notnull().astype(
            int)  # 1 = True = number
        self.gapfinder_fullres_df[self.isval_cumsum_col] = self.gapfinder_fullres_df[self.isval_col].cumsum()

        # # Narrow down the df to contain only the gap data
        filter_isgap = self.gapfinder_fullres_df[self.isgap_col] == 1
        self.gapfinder_fullres_df = self.gapfinder_fullres_df[filter_isgap]
        datetime_col = self.gapfinder_fullres_df.index.name
        self.gapfinder_fullres_df[datetime_col] = self.gapfinder_fullres_df.index  # needed for aggregate

        # Aggregate the df, gives stats about number of consecutive gaps
        # and their start and end datetime.
        gapfinder_agg_df = \
            self.gapfinder_fullres_df.groupby(self.isval_cumsum_col).aggregate({
                self.gapfinder_fullres_df.index.name: ['min', 'max'],
                self.isval_cumsum_col: ['count']
            })
        # TODO Fehler wenn keine gaps mehr gibt

        # Remove the original column names (tuples w/ two elements)
        gapfinder_agg_df.columns = gapfinder_agg_df.columns.droplevel([0, 1])

        # # Put first_spotted column as index, needed for filtering
        # gap_groups_overview_df.index = gap_groups_overview_df[self.gap_datetime_col]

        if self.limit:
            # Make flag that show if gap is above or below limit
            gapfinder_agg_df[self.gaplen_below_limit_col] = np.nan
            filter_limit = gapfinder_agg_df['count'] <= self.limit
            gapfinder_agg_df.loc[filter_limit, self.gaplen_below_limit_col] = 1  # True, below or equal limit
            filter_limit = gapfinder_agg_df['count'] > self.limit
            gapfinder_agg_df.loc[filter_limit, self.gaplen_below_limit_col] = 0  # False, above limit

            # Get all gap positions below the limit
            filter_above_lim = gapfinder_agg_df[self.gaplen_below_limit_col] == 1
            gapfinder_agg_df = gapfinder_agg_df[filter_above_lim]

        return gapfinder_agg_df

    def get_results(self):
        return self.gapfinder_agg_df
