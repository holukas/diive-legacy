import pandas as pd

import gui.elements
import gui.plotfuncs
from gui import elements


class AddControls():
    """
        Creates the gui control elements and their handles for usage.

    """

    def __init__(self, opt_stack, opt_frame, opt_layout, ref_stack):
        self.opt_stack = opt_stack
        self.opt_frame = opt_frame
        self.opt_layout = opt_layout
        self.ref_stack = ref_stack

        self.add_option()
        self.add_refinements()
        # self.get_handles()

    def get_handles(self):
        return

    def add_option(self):
        # Option Button: [1] Class Finder
        self.opt_btn = elements.add_button_to_grid(grid_layout=self.opt_layout,
                                                   txt='Before / After Event', css_id='',
                                                   row=5, col=0, rowspan=1, colspan=1)

        self.opt_stack.addWidget(self.opt_frame)

    def add_refinements(self):
        ref_frame, ref_layout = elements.add_frame_grid()
        self.ref_stack.addWidget(ref_frame)

        gui.elements.add_header_to_grid_top(layout=ref_layout, txt='AN: Before / After Event')

        self.ref_drp_var = \
            elements.grd_LabelDropdownPair(txt='Variable',
                                           css_ids=['', 'cyan'], layout=ref_layout,
                                           row=1, col=0, orientation='horiz')

        self.ref_lne_win_size = elements.add_label_linedit_pair_to_grid(layout=ref_layout,
                                                                        txt='Window Size (Days)',
                                                                        css_ids=['', 'cyan'],
                                                                        orientation='horiz',
                                                                        row=2, col=0)

        self.ref_lne_win_min_vals = elements.add_label_linedit_pair_to_grid(layout=ref_layout,
                                                                            txt='Minimum Values In Window',
                                                                            css_ids=['', 'cyan'],
                                                                            orientation='horiz',
                                                                            row=3, col=0)

        self.ref_btn_showInPlot = elements.add_button_to_grid(grid_layout=ref_layout,
                                                              txt='Show in Plot', css_id='',
                                                              row=4, col=0, rowspan=1, colspan=2)

        ref_layout.setRowStretch(5, 1)


class Call:

    def __init__(self, data_df, fig, ax, focus_df, focus_col, win_size, win_min_vals,
                 col_dict_tuples, drp_var):
        self.data_df = data_df
        self.window_size = win_size
        self.window_min_vals = win_min_vals
        self.col_dict_tuples = col_dict_tuples
        self.drp_var = drp_var

        self.fig = fig  # needed for redraw
        self.ax = ax  # adds to existing axis
        self.focus_df = focus_df.copy()
        self.focus_df = self.focus_df[
            [focus_col]]  ## make sure to only have the measured_col in focus_df before analysis
        self.var_col = focus_col

        self.event_cols_list = []  # list of found events

        # Define MultiIndex column names
        self.marker_col = ('an_runLinRegr', 'marker')
        self.datetime_col = self.focus_df.index.name  # needed in separate column for aggregation

        self.ax = gui.plotfuncs.remove_prev_lines(ax=self.ax)

        self.prepare_data()
        self.make_before_after_stats()

    def prepare_data(self):
        var_ix = self.drp_var.currentIndex()
        self.var_col = self.col_dict_tuples[var_ix]

        # expand focus_df
        self.focus_df[self.datetime_col] = self.focus_df.index
        self.focus_df[self.var_col] = self.data_df[self.var_col]

        for col in self.data_df.columns:
            if col[1] == '[event]':
                self.focus_df[col] = self.data_df[col]
                self.event_cols_list.append(col)

    def make_before_after_stats(self):

        for ix, event_col in enumerate(self.event_cols_list):
            # Group by event to get its respective start and end datetime
            # Makes a df with row indices 0 and 1, whereby 1 marks time periods
            # during the event and 0 time periods outside the event.
            _agg_df = self.focus_df.groupby(event_col).agg({
                self.datetime_col: ['min', 'max', 'count'],
                self.var_col: ['mean', 'std']
            })

            # Get start and end datetimes of event
            # Access agg results for event with .loc[1] (1=Event yes)
            event_start = _agg_df[self.datetime_col]['min'].loc[1]
            event_end = _agg_df[self.datetime_col]['max'].loc[1]

            # Define before and after datetime ranges
            event_before = event_start - pd.Timedelta(days=self.window_size)
            event_after = event_end + pd.Timedelta(days=self.window_size)

            # Flag before and after ranges in event column
            filter_before = (self.focus_df.index >= event_before) & (self.focus_df.index < event_start)
            self.focus_df.loc[filter_before, [event_col]] = -1  # -1 means before event (+1 means event)
            filter_after = (self.focus_df.index > event_end) & (self.focus_df.index <= event_after)
            self.focus_df.loc[filter_after, [event_col]] = 2  # 2 means after event

            # Use flag to group data and get respective stats
            plot_df = self.focus_df.groupby(event_col).agg({
                self.datetime_col: ['min', 'max', 'count'],
                self.var_col: ['min', 'max', 'mean', 'std', 'count']
            })

            # Check if time periods before and after event exist
            # 0=all data, 1=during event, -1=before event, 2=after event
            event_before_after_ids = [1]
            if min(plot_df.index) == -1:
                # Before event exists
                event_before_after_ids = [-1] + event_before_after_ids
            if max(plot_df.index) == 2:
                # After event exists
                event_before_after_ids.append(2)

            # self.focus_df.loc[self.focus_df[self.datetime_col].between(event_start, event_end, inclusive=True)]

            self.show_results_in_plot(plot_df=plot_df, event_name=event_col[0],
                                      event_before_after_ids=event_before_after_ids)
        return None

    def show_results_in_plot(self, plot_df, event_name, event_before_after_ids):
        for ix, id in enumerate(event_before_after_ids):
            self.plot_ranges(plot_df=plot_df, id=id, event_name=event_name)
        self.fig.canvas.draw()  # update plot

    def plot_ranges(self, plot_df, id, event_name):

        # Check if number of values in time window fulfills requirements
        if plot_df.loc[id, (self.var_col[0], self.var_col[1], 'count')] < self.window_min_vals:
            return None

        if id == -1:
            prefix = r'$\bf{BEFORE}$'
            color = '#00BCD4'  # cyan500
        elif id == 1:
            prefix = r'$\bf{DURING}$'
            color = '#FFEB3B'  # yellow500
        elif id == 2:
            prefix = r'$\bf{AFTER}$'
            color = '#f44336'  # red500
        else:
            prefix = '-not-found-'
            color = 'red'

        show_event_name = True if id == 1 else False

        # Plot before
        start = plot_df.loc[id, (self.datetime_col[0], self.datetime_col[1], 'min')]
        end = plot_df.loc[id, (self.datetime_col[0], self.datetime_col[1], 'max')]
        mean = plot_df.loc[id, (self.var_col[0], self.var_col[1], 'mean')]
        min = plot_df.loc[id, (self.var_col[0], self.var_col[1], 'min')]
        max = plot_df.loc[id, (self.var_col[0], self.var_col[1], 'max')]
        count = plot_df.loc[id, (self.var_col[0], self.var_col[1], 'count')]

        x_start = start
        x_end = end
        x_width = x_end - x_start
        x_range = [(x_start, x_width)]
        x_range_middle = x_width / 2  # Position of Event text
        x_range_middle = x_start + x_range_middle
        y_range = (0, 1)

        # Colored background
        self.ax.broken_barh(x_range, y_range, facecolor=color, edgecolor='', hatch='', lw=0,
                            transform=self.ax.get_xaxis_transform(), alpha=0.1)

        # Name of event
        if show_event_name:
            self.ax.text(x_range_middle, 1.05, '{}'.format(event_name),
                         horizontalalignment='center', verticalalignment='top',
                         transform=self.ax.get_xaxis_transform(),
                         size='large', color='#424242', backgroundcolor='none', zorder=100)

        # Stats for this event
        sym_mean = r'$\phi$'
        sym_min = r'$\downarrow$'
        sym_max = r'$\uparrow$'
        sym_count = r'$\Sigma$'
        self.ax.text(x_range_middle, 0.95, '{}:\n'
                                           '{} {:.2f}\n'
                                           '{} {:.2f}\n'
                                           '{} {:.2f}\n'
                                           '{} {:.2f}'.format(prefix, sym_mean, mean, sym_min, min, sym_max, max,
                                                              sym_count, count),
                     horizontalalignment='center', verticalalignment='top',
                     transform=self.ax.get_xaxis_transform(),
                     size=12, color='#424242', backgroundcolor='none', zorder=100)

    def get_results(self):
        return self.focus_df, self.marker_col
