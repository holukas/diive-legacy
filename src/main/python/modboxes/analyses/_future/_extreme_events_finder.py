"""

EXTREME EVENTS FINDER
---------------------


"""
import fnmatch

import matplotlib.dates as mdates
import matplotlib.gridspec as gridspec
# matplotlib.use('Qt5Agg')
import numpy as np
import pandas as pd
from PyQt5 import QtWidgets as qw
from numpy import polyfit

import gui.add_to_layout
import gui.base
import gui.elements
import gui.make
import gui.plotfuncs
import logger
from modboxes.plots.styles.LightTheme import *
from utils.vargroups import *


# pd.set_option('display.width', 1000)
# pd.set_option('display.max_columns', 15)
# pd.set_option('display.max_rows', 20)


class addContent(gui.base.buildTab):
    """Build new tab and populate with contents"""

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id, tab_template='SVonVP')

        # Add settings menu contents
        self.lne_timewin, self.lne_upper_nonoutlier_lim, self.lne_lower_nonoutlier_lim, self.btn_add_as_new_var, \
        self.drp_separate_day_night, self.drp_define_nighttime_based_on, self.drp_set_nighttime_if, \
        self.lne_nighttime_threshold, self.lne_seasonality_fit_degree = \
            self.add_settings_fields()

        # Add variables required in settings
        self.populate_settings_fields()

        # Add available variables to variable lists
        gui.base.buildTab.populate_variable_list(obj=self)

        # # Add plots
        # self.populate_plot_area()

    def populate_plot_area(self):
        # This is done in Run due to dynamic amount of axes
        pass

    def populate_settings_fields(self):
        self.drp_define_nighttime_based_on.clear()

        # Add all variables to drop-down lists to allow full flexibility yay
        for ix, colname_tuple in enumerate(self.tab_data_df.columns):

            if any(fnmatch.fnmatch(colname_tuple[0], id) for id in NIGHTTIME_DETECTION):
                self.drp_define_nighttime_based_on.addItem(self.col_list_pretty[ix])

    def add_settings_fields(self):
        # Settings
        gui.elements.add_header_in_grid_row(layout=self.sett_layout, txt='Settings', row=0)
        # self.sett_layout.setRowStretch(1, 0)  # empty row

        # ref_drp_class_var = \
        #     gui_elements.grd_LabelDropdownPair(txt='Class Variable',
        #                                        css_ids=['', 'cyan'], layout=self.sett_layout,
        #                                        row=1, col=0, orientation='horiz')

        lne_timewin = gui.elements.add_label_linedit_pair_to_grid(
            layout=self.sett_layout, txt='Time Window (records)', css_ids=['', 'cyan'],
            row=1, col=0, orientation='horiz')
        lne_timewin.setText('480')
        lne_upper_nonextr_lim = gui.elements.add_label_linedit_pair_to_grid(layout=self.sett_layout,
                                                                        txt='Upper Non-Outlier Limit (percentile)',
                                                                        css_ids=['', 'cyan'],
                                                                        orientation='horiz',
                                                                        row=2, col=0)
        lne_upper_nonextr_lim.setText('0.9')
        lne_lower_nonextr_lim = gui.elements.add_label_linedit_pair_to_grid(layout=self.sett_layout,
                                                                        txt='Lower Non-Outlier Limit (percentile)',
                                                                        css_ids=['', 'cyan'],
                                                                        orientation='horiz',
                                                                        row=3, col=0)
        lne_lower_nonextr_lim.setText('0.1')
        gui.add_to_layout.add_spacer_item_to_grid(self.sett_layout, 4, 0)

        # Seasonality
        gui.elements.add_header_in_grid_row(layout=self.sett_layout, txt='Seasonality', row=5)
        lne_seasonality_fit_degree = gui.elements.add_label_linedit_pair_to_grid(
            layout=self.sett_layout, txt='Fit Degree', css_ids=['', 'cyan'],
            row=6, col=0, orientation='horiz')
        lne_seasonality_fit_degree.setText('4')
        gui.add_to_layout.add_spacer_item_to_grid(self.sett_layout, 7, 0)

        # Day / night
        gui.elements.add_header_in_grid_row(
            row=8, layout=self.sett_layout, txt='Day / Night')
        drp_separate_day_night = gui.elements.grd_LabelDropdownPair(
            txt='Separate For Day / Night', css_ids=['', 'cyan'], layout=self.sett_layout,
            row=9, col=0, orientation='horiz')
        drp_separate_day_night.addItems(['Yes', 'No'])
        drp_define_nighttime_based_on = gui.elements.grd_LabelDropdownPair(
            txt='Define Nighttime Based On', css_ids=['', 'cyan'], layout=self.sett_layout,
            row=10, col=0, orientation='horiz')
        drp_set_nighttime_if = gui.elements.grd_LabelDropdownPair(
            txt='Set To Nighttime If', css_ids=['', 'cyan'], layout=self.sett_layout,
            row=11, col=0, orientation='horiz')
        drp_set_nighttime_if.addItems(['Smaller Than Threshold', 'Larger Than Threshold'])
        lne_nighttime_threshold = gui.elements.add_label_linedit_pair_to_grid(
            txt='Threshold', css_ids=['', 'cyan'], layout=self.sett_layout,
            row=12, col=0, orientation='horiz')
        lne_nighttime_threshold.setText('20')
        gui.add_to_layout.add_spacer_item_to_grid(self.sett_layout, 13, 0)

        # Buttons
        # ref_btn_calc = gui_elements.add_button_to_grid(grid_layout=self.sett_layout,
        #                                                txt='Show in Plot', css_id='',
        #                                                row=4, col=0, rowspan=1, colspan=2)
        btn_add_as_new_var = gui.elements.add_button_to_grid(grid_layout=self.sett_layout,
                                                         txt='+ Add As New Var', css_id='btn_add_as_new_var',
                                                         row=14, col=0, rowspan=1, colspan=2)
        self.sett_layout.setRowStretch(15, 1)

        return lne_timewin, lne_upper_nonextr_lim, lne_lower_nonextr_lim, btn_add_as_new_var, \
               drp_separate_day_night, drp_define_nighttime_based_on, drp_set_nighttime_if, \
               lne_nighttime_threshold, lne_seasonality_fit_degree


class Run(addContent):
    grouping_col = ('_group', '[#]')
    doy_col = ('_doy', '[yyyy-mm-dd HH:MM:SS]'),
    doy_fraction_hhmmss_col = ('_fraction_hhmmss_col', '[#]'),
    doy_fraction_col = ('_doy_fraction', '[#]')
    marker_isset = False
    ready_to_export = False
    marker_filter = None  # Filter to mark valid data points
    sub_outdir = "analysis_extreme_events_finder"

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id)
        logger.log(name='>>> Starting Extreme Events Finder', dict={}, highlight=True)  # Log info
        self.sub_outdir = self.project_outdir / self.sub_outdir
        self.selected_vars_lookup_dict = {}
        self.vars_selected_df = pd.DataFrame()
        self.vars_available_df = self.tab_data_df.copy()

    def add_grouping_col(self, df):
        """Add flag to indicate group membership"""
        df[self.grouping_col] = np.nan

        # Check if grouping col was also selected as a feature
        delete_grouping_col = True
        if self.nighttime_col in list(self.selected_vars_lookup_dict.keys()):
            delete_grouping_col = False

        if self.separate_day_night == 'Yes':
            nighttime_filter = None
            df[self.nighttime_col] = self.tab_data_df[self.nighttime_col]  # Add for grouping
            if self.set_nighttime_if == 'Smaller Than Threshold':
                nighttime_filter = df[self.nighttime_col] < self.nighttime_threshold
            elif self.set_nighttime_if == 'Larger Than Threshold':
                nighttime_filter = df[self.nighttime_col] > self.nighttime_threshold
            df.loc[nighttime_filter, [self.grouping_col]] = 0
            df.loc[~nighttime_filter, [self.grouping_col]] = 1
            if delete_grouping_col:
                df.drop(self.nighttime_col, axis=1, inplace=True)  # Only needed for grouping
        else:
            df.loc[:, [self.grouping_col]] = 0  # All in the same group
        return df

    @staticmethod
    def date_as_doy_fraction(df, doy_col, fraction_hhmmss_col, doy_fraction):
        """Insert column with date as doy fraction"""
        # Day of year with time fraction
        df = df.copy()
        df[doy_col] = df.index.dayofyear
        df[fraction_hhmmss_col] = (df.index.hour
                                   + (df.index.minute / 60)
                                   + (df.index.second / 3600)) / 24
        df[doy_fraction] = df[doy_col] \
                           + df[fraction_hhmmss_col]
        return df

    def calc_rolling(self, aux_cols_dict, col):
        # Rolling, based on true date from start to end of complete time series
        _rolling = self.vars_selected_df[aux_cols_dict['seasonality_removed']].rolling(window=self.timewin, center=True)
        self.vars_selected_df[aux_cols_dict['rollmax_col']] = _rolling.max()
        self.vars_selected_df[aux_cols_dict['rollmin_col']] = _rolling.min()

    def remove_trend_linregr(self):
        import matplotlib.pyplot as plt
        subset = pd.DataFrame()
        subset['x'] = self.vars_selected_df.index
        subset['y'] = self.vars_selected_df[('TA_F_MDS', '-no-units-')].values

        from statsmodels.regression.linear_model import OLS
        least_squares = OLS(subset['y'].values,
                            list(range(subset.shape[0])))
        result = least_squares.fit()
        fit = pd.Series(result.predict(list(range(subset.shape[0]))),
                        index=subset.index)
        detrended = subset['y'] - fit

        detrended.plot()
        plt.show()

        from statsmodels.tsa.seasonal import seasonal_decompose
        period = int(24 * 60 / 30 * 7 * 9)
        result = seasonal_decompose(detrended, model="additive", period=period)
        result.plot()
        plt.show()

        import matplotlib.pyplot as plt
        subset = pd.DataFrame()
        subset['x'] = self.vars_selected_df.index
        subset['y'] = self.vars_selected_df[('TA_F_MDS', '-no-units-')].values
        subset.set_index('x', inplace=True)
        # subset = self.vars_selected_df[]
        subset.index.name = 'x'
        subset = subset.asfreq('30T')
        period = int(24 * 60 / 30 * 7 * 9)
        result = seasonal_decompose(subset.y, model="additive", period=period)
        trend = result.trend
        seasonal = result.seasonal
        residual = result.resid
        observed = result.observed
        result.plot()
        plt.show()

    def calc_doy_fraction_max_min(self, aux_cols_dict):
        """Get the max for each doy fraction, allows comparison between years"""

        # Small subset for calculations
        _subset_df = self.vars_selected_df[[self.doy_fraction_col,
                                            aux_cols_dict['rollmax_col'],
                                            aux_cols_dict['rollmin_col']]]

        # Group by doy fraction and calculate max and min for each
        def q75(x):
            return x.quantile(0.75)

        f = {'mean', 'std', 'count', 'max', 'min', 'sem'}
        _subset_df_grouped = _subset_df \
            .groupby(self.doy_fraction_col) \
            .agg(f)

        # Map results to vars_selected_df, i.e. the doy fraction in vars_selected_df
        # is used to get the corresponding aggregated values from subset, the subset
        # is basically used as a lookup table.
        self.vars_selected_df[aux_cols_dict['doy_fraction_max_rollmax_col']] = \
            self.vars_selected_df[self.doy_fraction_col] \
                .map(_subset_df_grouped[aux_cols_dict['rollmax_col']]['max'])
        self.vars_selected_df[aux_cols_dict['doy_fraction_min_rollmax_col']] = \
            self.vars_selected_df[self.doy_fraction_col] \
                .map(_subset_df_grouped[aux_cols_dict['rollmax_col']]['min'])

        self.vars_selected_df[aux_cols_dict['doy_fraction_max_rollmin_col']] = \
            self.vars_selected_df[self.doy_fraction_col] \
                .map(_subset_df_grouped[aux_cols_dict['rollmin_col']]['max'])
        self.vars_selected_df[aux_cols_dict['doy_fraction_min_rollmin_col']] = \
            self.vars_selected_df[self.doy_fraction_col] \
                .map(_subset_df_grouped[aux_cols_dict['rollmin_col']]['min'])

    def calc_outliers(self):
        self.remove_prev_qcf_method_columns()
        self.get_settings_from_fields()
        self.vars_selected_df = self.add_grouping_col(df=self.vars_selected_df)

        # DOY fraction
        self.vars_selected_df = self.date_as_doy_fraction(df=self.vars_selected_df,
                                                          doy_col=self.doy_col,
                                                          fraction_hhmmss_col=self.doy_fraction_hhmmss_col,
                                                          doy_fraction=self.doy_fraction_col)

        for col in self.selected_vars_lookup_dict.keys():
            aux_cols_dict = self.set_aux_cols(col=col)
            self.add_aux_cols(aux_cols_dict=aux_cols_dict)  # Aux cols for this variable
            self.calc_daily_min_max(aux_cols_dict=aux_cols_dict, col=col)
            self.add_seasonality_min_max_fits(aux_cols_dict=aux_cols_dict, col=col)
            self.add_seasonality_min_max_removed(aux_cols_dict=aux_cols_dict)
            # self.calc_rolling(aux_cols_dict=aux_cols_dict, col=col)
            # TODO hier weiter
            # self.remove_seasonality(aux_cols_dict=aux_cols_dict, col=col)
            # self.calc_doy_fraction_max_min(aux_cols_dict=aux_cols_dict)
            # self.insert_flags(aux_cols_dict=aux_cols_dict, col=col)

    def calc_daily_min_max(self, aux_cols_dict, col):
        # Make subset
        _subset_hires_cols = [aux_cols_dict['daily_max_col'],
                              aux_cols_dict['daily_min_col']]
        _subset_hires_df = self.vars_selected_df[_subset_hires_cols].copy()
        _subset_hires_df['_date'] = _subset_hires_df.index.date

        # Calcs
        daily_max = self.vars_selected_df[col].resample('1D').max()  # Max for each *day*
        daily_min = self.vars_selected_df[col].resample('1D').min()

        # Mapping due to different time resolutions:
        #   Map results to vars_selected_df, i.e. the date in subset
        #   is used to get the corresponding aggregated values from
        #   daily_max, which is basically used as a lookup table.
        _subset_hires_df[aux_cols_dict['daily_max_col']] = _subset_hires_df['_date'].map(
            daily_max)  # Map to hi-res, for each *day*
        _subset_hires_df[aux_cols_dict['daily_min_col']] = _subset_hires_df['_date'].map(daily_min)

        # Insert results
        self.vars_selected_df[aux_cols_dict['daily_max_col']] = _subset_hires_df[aux_cols_dict['daily_max_col']]
        self.vars_selected_df[aux_cols_dict['daily_min_col']] = _subset_hires_df[aux_cols_dict['daily_min_col']]

    def add_seasonality_min_max_fits(self, aux_cols_dict, col):
        # https://machinelearningmastery.com/time-series-seasonality-with-python/

        # Make subset
        _subset_cols = [col, self.doy_fraction_col,
                        aux_cols_dict['daily_max_col'], aux_cols_dict['daily_min_col'],
                        aux_cols_dict['ssn_fit_daily_max_col'], aux_cols_dict['ssn_fit_daily_min_col']]
        _subset = self.vars_selected_df[_subset_cols].copy()
        _subset = _subset.set_index(self.doy_fraction_col)  # Based on DOY fraction

        # Calcs
        ssn_fit_daily_max = self.calc_fit(series=_subset[aux_cols_dict['daily_max_col']])
        ssn_fit_daily_min = self.calc_fit(series=_subset[aux_cols_dict['daily_min_col']])

        # Insert results
        self.vars_selected_df[aux_cols_dict['ssn_fit_daily_max_col']] = ssn_fit_daily_max
        self.vars_selected_df[aux_cols_dict['ssn_fit_daily_min_col']] = ssn_fit_daily_min

    def calc_fit(self, series):
        """Calculate fit curve, based on DOY fraction"""
        X = series.index
        y = series.values
        degree = self.seasonality_fit_degree
        coef = polyfit(X, y, degree)
        curve = list()
        for i in range(len(X)):
            value = coef[-1]
            for d in range(degree):
                value += X[i] ** (degree - d) * coef[d]
            curve.append(value)
        return curve

    def add_seasonality_min_max_removed(self, aux_cols_dict):
        """
        Adjust measured data points to seasonality, separately for daily min and max

            ssn ... seasonality
            ssnr ... seasonality removed
        """
        ssnr_daily_max = self.remove_seasonality(series=self.vars_selected_df[aux_cols_dict['daily_max_col']],
                                                 fit_curve=self.vars_selected_df[
                                                     aux_cols_dict['ssn_fit_daily_max_col']])
        ssnr_daily_min = self.remove_seasonality(series=self.vars_selected_df[aux_cols_dict['daily_min_col']],
                                                 fit_curve=self.vars_selected_df[
                                                     aux_cols_dict['ssn_fit_daily_min_col']])

        self.vars_selected_df[aux_cols_dict['ssnr_daily_max_col']] = ssnr_daily_max
        self.vars_selected_df[aux_cols_dict['ssnr_daily_min_col']] = ssnr_daily_min

    def remove_seasonality(self, series, fit_curve):

        measured = series.values
        # fit_curve = self.vars_selected_df[aux_cols_dict['seasonality_fit']]
        diff = list()
        for i in range(len(measured)):
            value = measured[i] - fit_curve[i]
            diff.append(value)
        return diff

        # # Analyze max diff
        # _subset['diff'] = diff
        # _subset_agg = _subset['diff'].copy()
        # # _subset_agg = _subset_agg.resample('1D').agg(['max'])  # Daily max diff
        # _subset_agg = _subset_agg.rolling('1D').agg(['max'])  # Daily max diff
        # _subset_agg.rolling('1D').agg(['max']).plot()
        # _subset_agg.rolling('10D').agg(['max']).plot()
        # plt.show()
        # # _subset_agg = _subset_agg.resample('1D').agg(['mean', 'std', 'count'])
        # ci95_hi = []
        # ci95_lo = []
        # import math
        # for i in _subset_agg.index:
        #     m, c, s = _subset_agg.loc[i]
        #     ci95_hi.append(m + 1.96 * s / math.sqrt(c))
        #     ci95_lo.append(m - 1.96 * s / math.sqrt(c))
        # _subset_agg['ci_95_hi'] = ci95_hi
        # _subset_agg['ci_95_low'] = ci95_lo
        # _subset_agg[['mean', 'ci_95_hi', 'ci_95_low']].plot()
        # import matplotlib.pyplot as plt
        # plt.show()

        # # # Confidence intervals
        # # # https://stackoverflow.com/questions/53519823/confidence-interval-in-python-dataframe
        # # # http://www.randalolson.com/2012/08/06/statistical-analysis-made-easy-in-python/
        # # compute 95% confidence intervals around the mean
        # CIs = bootstrap.ci(data=_subset_agg['max'], statfunction=np.average)
        # print("Bootstrapped 95% confidence intervals\nLow:", CIs[0], "\nHigh:", CIs[1])
        # _subset_agg['max'].plot()
        # plt.show()
        #
        # _subset['diff'].rolling(window=48 * 7, center=True).sum().plot()
        # plt.show()

        # import statsmodels.stats.api as sms
        # sms.DescrStatsW(diff).tconfint_mean()

        # ci95_hi = []
        # ci95_lo = []
        # sub2 = _subset['diff'].groupby(_subset.index.month).agg(['mean', 'count', 'std'])
        # import math
        # import matplotlib.pyplot as plt
        # for i in sub2.index:
        #     m, c, s = sub2.loc[i]
        #     ci95_hi.append(m + 1.96 * s / math.sqrt(c))
        #     ci95_lo.append(m - 1.96 * s / math.sqrt(c))
        # sub2['ci_95_hi'] = ci95_hi
        # sub2['ci_95_low'] = ci95_lo
        # sub2[['mean', 'ci_95_hi', 'ci_95_low']].plot()
        # plt.show()

        # _subset[['diff', col, 'ci_95_high', 'ci_95_low']].plot()
        # plt.show()

    def insert_flags(self, aux_cols_dict, col):
        """Flag values that are found to be extreme values"""
        filter_extreme = \
            (self.vars_selected_df[col] > self.vars_selected_df[aux_cols_dict['rollmax_col']]) | \
            (self.vars_selected_df[col] < self.vars_selected_df[aux_cols_dict['rollmin_col']])
        self.vars_selected_df.loc[filter_extreme, [aux_cols_dict['flag_col']]] = 1
        extreme_vals = self.vars_selected_df.loc[filter_extreme][col]
        self.vars_selected_df.loc[filter_extreme, [aux_cols_dict['extreme_vals_col']]] = extreme_vals

        filter_ok = \
            (self.vars_selected_df[col] <= self.vars_selected_df[aux_cols_dict['rollmax_col']]) & \
            (self.vars_selected_df[col] >= self.vars_selected_df[aux_cols_dict['rollmin_col']])
        self.vars_selected_df.loc[filter_ok, [aux_cols_dict['flag_col']]] = 0

    def set_aux_cols(self, col):
        """
        Set aux column names for current variable

            ssn ... seasonality

        """
        aux_cols_dict = dict(rollwin_col=(f"_rolling_timewindow_{col[0]}", "[records]"),
                             rollmax_col=(f"_rolling_max_{col[0]}", f"{col[1]}"),
                             rollmin_col=(f"_rolling_min_{col[0]}", f"{col[1]}"),
                             doy_fraction_max_rollmax_col=(f"_doy_max_rolling_max_{col[0]}", f"{col[1]}"),
                             doy_fraction_min_rollmax_col=(f"_doy_min_rolling_max_{col[0]}", f"{col[1]}"),
                             doy_fraction_max_rollmin_col=(f"_doy_max_rolling_min_{col[0]}", f"{col[1]}"),
                             doy_fraction_min_rollmin_col=(f"_doy_min_rolling_min_{col[0]}", f"{col[1]}"),
                             extreme_vals_col=(f"_extreme_vals_{col[0]}", f"{col[1]}"),
                             seasonality_fit=(f"_seasonality_fit_{col[0]}", f"{col[1]}"),
                             seasonality_removed=(f"_ssnr_{col[0]}", f"{col[1]}"),
                             seasonality_removed_nonextr_hi=(f"_ssnr_noexhi_{col[0]}", f"{col[1]}"),
                             seasonality_removed_nonextr_lo=(f"_ssnr_noexlo_{col[0]}", f"{col[1]}"),
                             daily_max_col=(f"_daily_max_{col[0]}", f"{col[1]}"),
                             daily_min_col=(f"_daily_min_{col[0]}", f"{col[1]}"),
                             ssn_fit_daily_max_col=(f"_ssn_fit_daily_max_{col[0]}", f"{col[1]}"),
                             ssn_fit_daily_min_col=(f"_ssn_fit_daily_min_{col[0]}", f"{col[1]}"),
                             ssnr_daily_max_col=(f"_ssnr_daily_max_{col[0]}", f"{col[1]}"),
                             ssnr_daily_min_col=(f"_ssnr_daily_min_{col[0]}", f"{col[1]}"),
                             flag_col=(f"FLAG_{col[0]}", f"[1=extreme]"))
        return aux_cols_dict

    def add_aux_cols(self, aux_cols_dict):
        """Add aux cols for current variable"""
        self.vars_selected_df[aux_cols_dict['rollwin_col']] = np.nan
        self.vars_selected_df[aux_cols_dict['rollmax_col']] = np.nan
        self.vars_selected_df[aux_cols_dict['rollmin_col']] = np.nan
        self.vars_selected_df[aux_cols_dict['doy_fraction_max_rollmax_col']] = np.nan
        self.vars_selected_df[aux_cols_dict['doy_fraction_min_rollmax_col']] = np.nan
        self.vars_selected_df[aux_cols_dict['doy_fraction_max_rollmin_col']] = np.nan
        self.vars_selected_df[aux_cols_dict['doy_fraction_min_rollmin_col']] = np.nan
        self.vars_selected_df[aux_cols_dict['extreme_vals_col']] = np.nan
        self.vars_selected_df[aux_cols_dict['seasonality_fit']] = np.nan
        self.vars_selected_df[aux_cols_dict['seasonality_removed']] = np.nan
        self.vars_selected_df[aux_cols_dict['seasonality_removed_nonextr_hi']] = np.nan
        self.vars_selected_df[aux_cols_dict['seasonality_removed_nonextr_lo']] = np.nan
        self.vars_selected_df[aux_cols_dict['daily_max_col']] = np.nan
        self.vars_selected_df[aux_cols_dict['daily_min_col']] = np.nan
        self.vars_selected_df[aux_cols_dict['ssn_fit_daily_max_col']] = np.nan
        self.vars_selected_df[aux_cols_dict['ssn_fit_daily_min_col']] = np.nan
        self.vars_selected_df[aux_cols_dict['ssnr_daily_max_col']] = np.nan
        self.vars_selected_df[aux_cols_dict['ssnr_daily_min_col']] = np.nan
        self.vars_selected_df[aux_cols_dict['flag_col']] = np.nan
        return aux_cols_dict

    def add_target(self):
        # self.remove_aux_cols_from_selected()
        self.set_target_col_to_add()
        self.add_target_to_selected()
        self.update_selected_vars_list()
        self.calc_outliers()
        self.plot_data()

    def remove_target(self):
        # self.remove_aux_cols_from_selected()
        self.set_target_col_to_remove()
        self.remove_target_from_selected()
        self.update_selected_vars_list()
        empty = self.check_if_selected_empty()
        if empty:
            self.reset_fig()
            self.vars_selected_df = pd.DataFrame()
        else:
            self.plot_data()

    def reset_fig(self):
        for ax in self.fig.axes:
            self.fig.delaxes(ax)
            self.fig.canvas.draw()
            self.fig.canvas.flush_events()

    def remove_prev_qcf_method_columns(self):
        for col in self.vars_selected_df.columns:
            if str(col[0]).startswith('QCF_') | str(col[0]).startswith('_'):
                self.vars_selected_df.drop(axis=1, labels=col, inplace=True)  # Remove aux cols

    def set_target_col_to_add(self):
        """Get column name of target var from available var list"""
        target_var = self.lst_varlist_available.selectedIndexes()
        target_var_ix = target_var[0].row()
        self.target_col = self.col_dict_tuples[target_var_ix]
        self.target_pretty = self.col_list_pretty[target_var_ix]  # Needs new pretty list (screen names)

    def set_target_col_to_remove(self):
        """Get column name of target var from selected var list"""
        target_var = self.lst_varlist_selected.selectedIndexes()
        target_var_ix = target_var[0].row()
        self.target_col = list(self.selected_vars_lookup_dict.keys())[target_var_ix]  # Get key by index
        # self.target_col = self.col_dict_tuples[target_var_ix]
        # self.target_pretty = self.col_list_pretty[target_var_ix]  # Needs new pretty list (screen names)

    def add_target_to_selected(self):
        """Add var from available to selected df and lookup dict"""
        self.selected_vars_lookup_dict[self.target_col] = self.target_pretty
        self.vars_selected_df[self.target_col] = self.vars_available_df[self.target_col]

    # def mark_in_plot(self):
    #     # class_var_col = self.set_class_var_col()
    #     # upper_lim, lower_lim = self.get_settings_from_fields()
    #
    #     marker_data = self.tab_data_df[class_var_col].copy()
    #     self.marker_filter = (marker_data >= lower_lim) & (marker_data <= upper_lim)
    #     self.marker_isset = True
    #     self.ready_to_export = False
    #     self.target_loaded = True
    #     tabs.buildTab.update_btn_status(obj=self)
    #     axes_dict = self.make_axes_dict(fig=self.fig, df=self.vars_selected_df)
    #     self.plot_data(axes_dict=axes_dict, df=self.vars_selected_df)

    def get_col_from_drp_pretty(self, drp):
        """ Set target to selected variable. """
        target_pretty = drp.currentText()
        target_pretty_ix = self.col_list_pretty.index(target_pretty)
        target_col = self.col_dict_tuples[target_pretty_ix]
        return target_col

    def get_settings_from_fields(self):
        self.timewin = int(self.lne_timewin.text())
        self.upper_lim = float(self.lne_upper_nonoutlier_lim.text())
        self.lower_lim = float(self.lne_lower_nonoutlier_lim.text())
        self.nighttime_col = self.get_col_from_drp_pretty(drp=self.drp_define_nighttime_based_on)
        self.separate_day_night = self.drp_separate_day_night.currentText()
        self.set_nighttime_if = self.drp_set_nighttime_if.currentText()
        self.nighttime_threshold = int(self.lne_nighttime_threshold.text())
        self.seasonality_fit_degree = int(self.lne_seasonality_fit_degree.text())

    def keep_marked(self):
        if self.marker_isset:
            self.vars_selected_df[~self.marker_filter] = np.nan
            self.marker_isset = False
            self.ready_to_export = True
            gui.base.buildTab.update_btn_status(obj=self)
            self.plot_data()

    def remove_marked(self):
        if self.marker_isset:
            self.vars_selected_df[self.marker_filter] = np.nan
            self.marker_isset = False
            self.ready_to_export = True
            gui.base.buildTab.update_btn_status(obj=self)
            self.plot_data()

    # def prepare_export(self):
    #     self.remove_aux_cols_from_selected()
    #     export_df = self.vars_selected_df.copy()
    #     export_df.columns = [(str(col[0]) + '+anCF', col[1]) for col in export_df.columns]  # Add suffix
    #     return export_df
    #
    # def get_selected(self, main_df):
    #     """Return modified class data back to main data"""
    #     export_df = self.prepare_export()
    #     main_df = export_to_main(main_df=main_df,
    #                              export_df=export_df,
    #                              tab_data_df=self.tab_data_df)  # Return results to main
    #     return main_df

    def make_dynamic_axes_dict(self):
        selected_vars = self.selected_vars_lookup_dict.keys()  # Does not contain _underscore, flags cols
        gs = gridspec.GridSpec(4, len(selected_vars))  # rows, cols
        gs.update(wspace=0.2, hspace=0.2, left=0.03, right=0.97, top=0.97, bottom=0.03)

        # Make new axes
        axes_dict = {}
        first_ax = -9999
        first_ax_doy = -9999
        first_ax_seasonality_removed_max = -9999
        first_ax_seasonality_removed_min = -9999
        ax_counter = -1
        for ix, col in enumerate(selected_vars):
            if ix == 0:
                ax = self.fig.add_subplot(gs[0, ix])
                ax_seasonality_removed_max = self.fig.add_subplot(gs[1, ix], sharex=ax)
                ax_seasonality_removed_min = self.fig.add_subplot(gs[2, ix], sharex=ax)
                ax_doy = self.fig.add_subplot(gs[3, ix])
                first_ax = ax
                first_ax_doy = ax_doy
                first_ax_seasonality_removed_max = ax_seasonality_removed_max
                first_ax_seasonality_removed_min = ax_seasonality_removed_min
            else:
                ax = self.fig.add_subplot(gs[0, ix])
                ax_seasonality_removed = self.fig.add_subplot(gs[1, ix])
                ax_doy = self.fig.add_subplot(gs[2, ix])
            axes_dict[col] = [ax, ax_seasonality_removed_max, ax_seasonality_removed_min, ax_doy]

        # Format
        for col, ax in axes_dict.items():
            gui.plotfuncs.default_format(ax=ax[0], txt_xlabel=False)

        return axes_dict

    def prepare_fig(self):
        """Prepare figure"""
        for ax in self.fig.axes:
            self.fig.delaxes(ax)  # Delete all axes in figure
        axes_dict = self.make_dynamic_axes_dict()
        color_list = colorwheel_36()
        return axes_dict, color_list

    def plot_data(self):
        axes_dict, color_list = self.prepare_fig()
        # plot_df = self.vars_selected_df[self.selected_vars_lookup_dict.keys()].copy()

        # Measured values with limits and extremes
        current_color = -1
        for col, ax_list in axes_dict.items():
            aux_cols_dict = self.set_aux_cols(col=col)  # Aux colnames for this variable
            current_color += 1
            self.plot_measured_fits(ax=ax_list[0], col=col, aux_cols_dict=aux_cols_dict)
            self.plot_seasonality_removed(ax=ax_list[1], col=aux_cols_dict['ssnr_daily_max_col'], color='red')
            self.plot_seasonality_removed(ax=ax_list[2], col=aux_cols_dict['ssnr_daily_min_col'], color='blue')
            # self.doy_fraction_plot(ax=ax_list[2], aux_cols_dict=aux_cols_dict, color_list=color_list)
            self.fig.canvas.draw()
            self.fig.canvas.flush_events()

    def plot_seasonality_removed(self, ax, col, color):
        ax.plot_date(x=self.vars_selected_df.index,
                     y=self.vars_selected_df[col],
                     color=color, alpha=.9, ls='-', marker='o', markeredgecolor='none',
                     ms=4, zorder=98, label=f"{col[0]} {col[1]}")
        ax.axhline(0, color='black')
        ax.text(0.01, 0.97, f"Removed seasonality {col[0]} {col[1]}",
                size=FONTSIZE_HEADER_AXIS, color=FONTCOLOR_HEADER_AXIS,
                backgroundcolor='none', transform=ax.transAxes, alpha=1,
                horizontalalignment='left', verticalalignment='top', zorder=99)
        gui.plotfuncs.default_grid(ax=ax)
        gui.plotfuncs.default_legend(ax=ax)
        locator = mdates.AutoDateLocator(minticks=3, maxticks=30)
        formatter = mdates.ConciseDateFormatter(locator)
        ax.xaxis.set_major_locator(locator)
        ax.xaxis.set_major_formatter(formatter)

    def plot_measured_fits(self, ax, col, aux_cols_dict):
        ax.plot_date(x=self.vars_selected_df.index,
                     y=self.vars_selected_df[col],
                     color='#78909C', alpha=.9, ls='-', marker='o', markeredgecolor='none',
                     ms=4, zorder=98, label=f"{col[0]} {col[1]}")
        ax.plot_date(x=self.vars_selected_df.index,
                     y=self.vars_selected_df[aux_cols_dict['ssn_fit_daily_max_col']],
                     color='red', alpha=.9, ls='-', marker='o', markeredgecolor='none',
                     ms=4, zorder=98, label="fit to daily maximum")
        ax.plot_date(x=self.vars_selected_df.index,
                     y=self.vars_selected_df[aux_cols_dict['ssn_fit_daily_min_col']],
                     color='blue', alpha=.9, ls='-', marker='o', markeredgecolor='none',
                     ms=4, zorder=98, label=f"fit to daily minimum")
        # ax.plot_date(x=self.vars_selected_df.index,
        #              y=self.vars_selected_df[aux_cols_dict['extreme_vals_col']],
        #              color='white', alpha=.9, ls='-',
        #              marker='o', markeredgecolor='red', ms=4, zorder=99, label='FFU')
        ax.text(0.01, 0.97, f"{col[0]}",
                size=FONTSIZE_HEADER_AXIS, color=FONTCOLOR_HEADER_AXIS,
                backgroundcolor='none', transform=ax.transAxes, alpha=1,
                horizontalalignment='left', verticalalignment='top', zorder=99)
        gui.plotfuncs.default_grid(ax=ax)
        gui.plotfuncs.default_legend(ax=ax)
        locator = mdates.AutoDateLocator(minticks=3, maxticks=30)
        formatter = mdates.ConciseDateFormatter(locator)
        ax.xaxis.set_major_locator(locator)
        ax.xaxis.set_major_formatter(formatter)

    def doy_fraction_plot(self, aux_cols_dict, ax, color_list):
        # DOY fraction plot
        current_color = -1
        isfirst = True
        grouped = self.vars_selected_df.groupby(self.vars_selected_df.index.year)
        for year, year_df in grouped:
            current_color += 1
            ax.plot(year_df[self.doy_fraction_col],
                    year_df[aux_cols_dict['rollmax_col']],
                    color=color_list[current_color + 1], alpha=.9, ls='--',
                    marker=None, markeredgecolor='none', ms=0, zorder=98,
                    label=f"{aux_cols_dict['rollmax_col'][0]}")
            ax.plot(year_df[self.doy_fraction_col],
                    year_df[aux_cols_dict['rollmin_col']],
                    color=color_list[current_color + 2], alpha=.9, ls='--',
                    marker=None, markeredgecolor='none', ms=0, zorder=98,
                    label=f"{aux_cols_dict['rollmin_col'][0]}")
            if isfirst:
                ax.plot(year_df[self.doy_fraction_col],
                        year_df[aux_cols_dict['doy_fraction_max_rollmax_col']],
                        color=color_list[current_color + 3], alpha=.9, ls='-', lw=4,
                        marker=None, markeredgecolor='none', ms=0, zorder=98,
                        label=f"{aux_cols_dict['doy_fraction_max_rollmax_col'][0]}")
                ax.plot(year_df[self.doy_fraction_col],
                        year_df[aux_cols_dict['doy_fraction_min_rollmax_col']],
                        color=color_list[current_color + 4], alpha=.9, ls='-', lw=4,
                        marker=None, markeredgecolor='none', ms=0, zorder=98,
                        label=f"{aux_cols_dict['doy_fraction_min_rollmax_col'][0]}")
                ax.plot(year_df[self.doy_fraction_col],
                        year_df[aux_cols_dict['doy_fraction_max_rollmin_col']],
                        color=color_list[current_color + 5], alpha=.9, ls='-', lw=4,
                        marker=None, markeredgecolor='none', ms=0, zorder=98,
                        label=f"{aux_cols_dict['doy_fraction_max_rollmin_col'][0]}")
                label = str(aux_cols_dict['doy_fraction_min_rollmin_col'][0])
                ax.plot(year_df[self.doy_fraction_col],
                        year_df[aux_cols_dict['doy_fraction_min_rollmin_col']],
                        color=color_list[current_color + 6], alpha=.9, ls='-', lw=4,
                        marker=None, markeredgecolor='none', ms=0, zorder=98,
                        label="{}".format(label))
                isfirst = False
        gui.plotfuncs.default_legend(ax=ax)

    def remove_target_from_selected(self):
        """Remove from lookup dict and df"""
        del self.selected_vars_lookup_dict[self.target_col]
        self.vars_selected_df.drop(self.target_col, axis=1, inplace=True)

    def update_selected_vars_list(self):
        self.lst_varlist_selected.clear()
        for col in self.vars_selected_df.columns:
            if (str(col[0]).startswith('_')) | (str(col[0]).startswith('FLAG_')):
                continue
            item = qw.QListWidgetItem(self.selected_vars_lookup_dict[col])
            self.lst_varlist_selected.addItem(item)

    def check_if_selected_empty(self):
        _list = self.lst_varlist_selected
        if not self.selected_vars_lookup_dict:
            _list.addItem('-empty-')
            empty = True
        else:
            empty = False
        return empty
