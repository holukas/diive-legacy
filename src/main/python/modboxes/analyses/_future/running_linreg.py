import pandas as pd
import statsmodels.formula.api as smf

import gui.elements
import gui.plotfuncs
from gui import elements


class AddControls():
    """
        Creates the gui control elements and their handles for usage.

    """

    def __init__(self, opt_stack, opt_frame, opt_layout, ref_stack):
        self.opt_stack = opt_stack
        self.opt_frame = opt_frame
        self.opt_layout = opt_layout
        self.ref_stack = ref_stack

        self.add_option()
        self.add_refinements()

    def add_option(self):
        # Option Button: [1] Class Finder
        self.opt_btn = elements.add_button_to_grid(grid_layout=self.opt_layout,
                                                   txt='Running Linear Regression', css_id='',
                                                   row=2, col=0, rowspan=1, colspan=1)

        self.opt_stack.addWidget(self.opt_frame)

    def add_refinements(self):
        ref_frame, ref_layout = elements.add_frame_grid()
        self.ref_stack.addWidget(ref_frame)

        gui.elements.add_header_to_grid_top(layout=ref_layout, txt='AN: Running Linear Regression')

        self.ref_drp_ind_var = elements.grd_LabelDropdownPair(txt='Independent Variable',
                                                              css_ids=['', 'cyan'], layout=ref_layout,
                                                              row=1, col=0, orientation='horiz')

        self.ref_lne_win_size = elements.add_label_linedit_pair_to_grid(layout=ref_layout,
                                                                        txt='Window Size (values)',
                                                                        css_ids=['', 'cyan'],
                                                                        orientation='horiz',
                                                                        row=2, col=0)

        self.ref_lne_win_min_vals = elements.add_label_linedit_pair_to_grid(layout=ref_layout,
                                                                            txt='Minimum Values In Window',
                                                                            css_ids=['', 'cyan'],
                                                                            orientation='horiz',
                                                                            row=3, col=0)

        self.ref_lne_win_step_size = elements.add_label_linedit_pair_to_grid(layout=ref_layout,
                                                                             txt='Step Size',
                                                                             css_ids=['', 'cyan'],
                                                                             orientation='horiz',
                                                                             row=4, col=0)

        self.ref_btn_showInPlot = elements.add_button_to_grid(grid_layout=ref_layout,
                                                              txt='Show in Plot', css_id='',
                                                              row=5, col=0, rowspan=1, colspan=2)

        ref_layout.setRowStretch(6, 1)

        return self


class Call:

    def __init__(self, data_df, fig, ax, focus_df, focus_col, win_size, win_min_vals,
                 col_dict_tuples, drp_ind_var, twin_ax, twin_ax_active, win_step_size):
        self.data_df = data_df
        self.window_size = win_size.text()
        self.window_min_vals = win_min_vals.text()
        self.window_step_size = win_step_size.text()
        self.col_dict_tuples = col_dict_tuples
        self.drp_ind_var = drp_ind_var
        self.twin_ax = twin_ax
        self.twin_ax_active = twin_ax_active

        self.fig = fig  # needed for redraw
        self.ax = ax  # adds to existing axis
        self.focus_df = focus_df.copy()
        self.focus_df = self.focus_df[
            [focus_col]]  ## make sure to only have the measured_col in focus_df before analysis
        self.focus_col = focus_col

        # self.class_colname = (class_colname[0] + '_class', class_colname[1])
        # Define MultiIndex column names
        self.marker_col = ('an_runLinRegr', 'marker')

        # self.ax = ModBoxes_shared.remove_prev_lines(ax=self.ax)

        self.get_data()  ## yields self.results_df
        self.show_results_in_plot()

    def get_data(self):
        ind_var_ix = self.drp_ind_var.currentIndex()
        self.ind_var_colname = self.col_dict_tuples[ind_var_ix]
        # self.new_current_class_colname = (ind_var_colname[0] + '_class', ind_var_colname[1])

        # expand focus_df
        self.focus_df[self.ind_var_colname] = self.data_df[self.ind_var_colname]

        self.results_df = self.running_linear_regression()

        # self.focus_df = self.data_df[[self.measured_col, current_class_colname]]
        # self.focus_df.rename(columns={current_class_colname: new_current_class_colname}, inplace=true)

    def running_linear_regression(self):

        self.window_size = int(self.window_size)
        self.window_min_vals = int(self.window_min_vals)
        self.window_step_size = int(self.window_step_size)

        # Data
        _subset_df = self.focus_df.copy()
        _subset_df.columns = ['y', 'X1']

        # X, y
        _subset_y = _subset_df.columns[0]  ## to be predicted
        _subset_X1 = _subset_df.columns[1]  ## independent var 1, predictor

        formula_string = '{} ~ {}'.format(_subset_y, _subset_X1)

        results = smf.ols(formula_string, data=_subset_df).fit()  # -1 removes the intercept
        print(results.summary())
        print('overall r2: {}'.format(results.rsquared))

        coef_ls = []
        r2 = []
        pvalues = []
        this_from = []
        this_to = []
        this_middle_list = []
        self.best_r2 = -9999
        self.best_r2_start = -9999
        self.best_r2_end = -9999
        self.best_r2_middle = -9999
        win_start = []
        win_end = []
        win_span = []
        win_span_half = []  ## for time range plotting, b/c for span 14 days it shall plot 7 days left and 7 right

        for i in range(0, len(_subset_df) - self.window_size, self.window_step_size):

            this_start = _subset_df.iloc[i].name  ## date info stored in name component
            this_end = _subset_df.iloc[i + self.window_size].name
            this_span = this_end - this_start

            if len(_subset_df.loc[this_start:this_end].dropna()) > self.window_min_vals:  # min values
                # if len(_subset_df.ix[i:i + self.window_size].dropna()) > self.window_min_vals:  # min values
                results = smf.ols(formula_string, data=_subset_df.ix[i:i + self.window_size]).fit()
                this_from.append(_subset_df.index[i])
                this_to.append(_subset_df.index[i + self.window_size])
                this_middle = _subset_df.index[int(i + (self.window_size / 2))]
                this_middle_list.append(this_middle)  # index needs to be integer
                this_params = results.params
                this_r2 = results.rsquared
                coef_ls.append(this_params)
                r2.append(this_r2)
                win_start.append(this_start)
                win_end.append(this_end)
                win_span.append(this_span)
                win_span_half.append(this_span / 2)

                # p Value
                if results.pvalues[1] < 0.05:
                    significant = True
                else:
                    significant = False

                if significant == True:
                    pvalues.append(1)
                else:
                    pvalues.append(0)

                if this_r2 > self.best_r2:  # plot best fit in chosen window, plotted is only ONE parameter
                    self.best_r2 = this_r2
                    self.best_r2_start = this_start
                    self.best_r2_end = this_end
                    self.best_r2_middle = this_middle
                    print('new best r2: {} / from {} until {}'.format(
                        self.best_r2, self.best_r2_start, self.best_r2_end))

        # put our results in a dataframe, then we can use .dropna() and fill up empty dates with nan
        this_middle_r2_df = pd.DataFrame({'r2': r2,
                                          'significant<0.05': pvalues,
                                          'win_start': win_start,
                                          'win_end': win_end,
                                          'win_span': win_span,
                                          'win_span_half': win_span_half},
                                         index=pd.to_datetime(this_middle_list))
        this_middle_r2_df = this_middle_r2_df.dropna()

        print("length r2: " + str(len(r2)))
        return this_middle_r2_df

    def show_results_in_plot(self):
        self.color_significant = '#e53935'  # red 500
        self.color_not_significant = '#faeed1'  # bright yellow

        # Check if secondary y-axis already in self.ax
        # If True, remove it
        if gui.plotfuncs.has_twin(self.ax):
            self.twin_ax.remove()

        # Make (new) secondary y-axis
        self.twin_ax = gui.plotfuncs.make_secondary_yaxis(ax=self.ax)

        # Significant regressions
        self.twin_ax.plot_date(self.results_df.index[self.results_df['significant<0.05'] == 1],
                               self.results_df['r2'][self.results_df['significant<0.05'] == 1],
                               zorder=99, label="significant", c=self.color_significant, alpha=0.9,
                               ls='-', lw=0, marker='o', markeredgecolor='none', ms='10')

        self.twin_ax.errorbar(self.results_df.index[self.results_df['significant<0.05'] == 1],
                              self.results_df['r2'][self.results_df['significant<0.05'] == 1],
                              xerr=self.results_df['win_span_half'][self.results_df['significant<0.05'] == 1],
                              alpha=0.2, capsize=0, ls='none', color=self.color_significant, elinewidth=8,
                              label="regression period", zorder=96)

        # Not significant
        self.twin_ax.plot_date(self.results_df.index[self.results_df['significant<0.05'] == 0],
                               self.results_df['r2'][self.results_df['significant<0.05'] == 0],
                               zorder=99, label="significant", c=self.color_not_significant,
                               alpha=0.9, ls='-', lw=0, marker='o', markeredgecolor='none', ms='10')

        self.twin_ax.errorbar(self.results_df.index[self.results_df['significant<0.05'] == 0],
                              self.results_df['r2'][self.results_df['significant<0.05'] == 0],
                              xerr=self.results_df['win_span_half'][self.results_df['significant<0.05'] == 0],
                              alpha=0.2, capsize=0, ls='none', color=self.color_not_significant, elinewidth=8,
                              label="regression period", zorder=96)

        # Best r2
        txt_color = '#b71c1c'  ## red 900
        self.twin_ax.text(self.best_r2_middle, self.best_r2 + 0.02, 'Best r2 = {:.2f}'.format(self.best_r2),
                          horizontalalignment='center', verticalalignment='bottom',
                          # transform=ax.get_xaxis_transform(),
                          size='large', color=txt_color, backgroundcolor='none', weight='bold')

        self.twin_ax.set_ylabel('r2', color='#eab839')
        self.twin_ax.tick_params('y', colors='#eab839')
        self.twin_ax.set_ylim(0, 1)
        self.fig.canvas.draw()  # update plot

    def get_results(self):
        return self.focus_df, self.marker_col, self.twin_ax, self.twin_ax_active
