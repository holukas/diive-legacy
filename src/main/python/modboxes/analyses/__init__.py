from . import aggregator
from . import classfinder
from ._future import _extreme_events_finder, running_linreg
from . import feature_selection
from . import gapfinder
