"""

MULTIPANEL PLOTS
----------------


"""
import matplotlib.dates as mdates
import matplotlib.gridspec as gridspec
# matplotlib.use('Qt5Agg')
import numpy as np
import pandas as pd
from PyQt5 import QtGui
from PyQt5 import QtWidgets as qw

import gui
import logger
from modboxes.plots.styles.LightTheme import *
from pkgs.dfun.frames import export_to_main


# pd.set_option('display.width', 1000)
# pd.set_option('display.max_columns', 15)
# pd.set_option('display.max_rows', 20)


class addContent(gui.base.buildTab):
    """Build new tab and populate with contents"""

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id, tab_template='SVVP')

        # Tab icon
        self.TabWidget.setTabIcon(self.tab_ix, QtGui.QIcon(app_obj.ctx.tab_icon_analyses))

        # Add settings menu contents
        self.drp_class_var, self.lne_lower_class_lim, self.lne_upper_class_lim, \
        self.btn_calc, self.btn_keep_marked, self.btn_remove_marked, \
        self.btn_add_as_new_var = \
            self.add_settings_fields()

        # Add variables required in settings
        self.populate_settings_fields()

        # Add available variables to variable lists
        gui.base.buildTab.populate_variable_list(obj=self)

        # # Add plots
        # self.populate_plot_area()

    def populate_plot_area(self):
        # This is done in Run due to dynamic amount of axes
        pass

    def populate_settings_fields(self):
        # Add all variables to drop-down lists to allow full flexibility yay
        self.drp_class_var.clear()
        for ix, colname_tuple in enumerate(self.tab_data_df.columns):
            self.drp_class_var.addItem(self.col_list_pretty[ix])

        # Set dropdowns to found ix
        default_ix = 0
        self.drp_class_var.setCurrentIndex(default_ix)

        # Update settings text fields
        self.lne_lower_class_lim.setText('0')
        self.lne_upper_class_lim.setText('99')

    def add_settings_fields(self):
        # Settings
        gui.elements.add_header_in_grid_row(layout=self.sett_layout, txt='Settings', row=0)
        # self.sett_layout.setRowStretch(1, 0)  # empty row

        ref_drp_class_var = \
            gui.elements.grd_LabelDropdownPair(txt='Class Variable',
                                               css_ids=['', 'cyan'], layout=self.sett_layout,
                                               row=1, col=0, orientation='horiz')

        ref_lne_lower_class_lim = gui.elements.add_label_linedit_pair_to_grid(layout=self.sett_layout,
                                                                              txt='Lower Class Limit',
                                                                              css_ids=['', 'cyan'],
                                                                              orientation='horiz',
                                                                              row=2, col=0)

        ref_lne_upper_class_lim = gui.elements.add_label_linedit_pair_to_grid(layout=self.sett_layout,
                                                                              txt='Upper Class Limit',
                                                                              css_ids=['', 'cyan'],
                                                                              orientation='horiz',
                                                                              row=3, col=0)

        ref_btn_calc = gui.elements.add_button_to_grid(grid_layout=self.sett_layout,
                                                       txt='Show in Plot', css_id='',
                                                       row=4, col=0, rowspan=1, colspan=2)

        # Keep or remove
        # self.sett_layout.setRowStretch(5, 0)  # empty row
        self.sett_layout.addWidget(qw.QLabel(), 5, 0, 1, 1)  # spacer
        gui.elements.add_header_in_grid_row(layout=self.sett_layout, txt='Marker Options', row=6)

        ref_btn_keep_marked = gui.elements.add_button_to_grid(grid_layout=self.sett_layout,
                                                              txt='Keep Marked Values', css_id='',
                                                              row=7, col=0, rowspan=1, colspan=2)

        ref_btn_remove_marked = gui.elements.add_button_to_grid(grid_layout=self.sett_layout,
                                                                txt='Remove Marked Values', css_id='',
                                                                row=8, col=0, rowspan=1, colspan=2)

        # Add to data
        self.sett_layout.setRowStretch(9, 0)  # empty row

        ref_btn_add_as_new_var = gui.elements.add_button_to_grid(grid_layout=self.sett_layout,
                                                                 txt='+ Add As New Var', css_id='btn_add_as_new_var',
                                                                 row=11, col=0, rowspan=1, colspan=2)

        self.sett_layout.setRowStretch(12, 1)

        return ref_drp_class_var, ref_lne_lower_class_lim, ref_lne_upper_class_lim, \
               ref_btn_calc, ref_btn_keep_marked, ref_btn_remove_marked, ref_btn_add_as_new_var


class Run(addContent):
    target_loaded = False
    marker_isset = False
    ready_to_export = False
    marker_filter = None  # Filter to mark valid data points
    sub_outdir = "analysis_class_finder"

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id)
        logger.log(name='>>> Starting Class Finder', dict={}, highlight=True)  # Log info
        self.sub_outdir = self.project_outdir / self.sub_outdir
        self.selected_vars_lookup_dict = {}
        self.vars_selected_df = pd.DataFrame()
        self.vars_available_df = self.tab_data_df.copy()
        self.set_colnames()
        gui.base.buildTab.update_btn_status(obj=self)

    def add_target(self):
        self.remove_aux_cols_from_selected()
        self.set_target_col_to_add()
        self.add_target_to_selected()
        self.update_selected_vars_list()
        self.plot_data()
        self.target_loaded = True
        gui.base.buildTab.update_btn_status(obj=self)

    def remove_target(self):
        self.remove_aux_cols_from_selected()
        self.set_target_col_to_remove()
        self.remove_target_from_selected()
        self.update_selected_vars_list()
        empty = self.check_if_selected_empty()
        if empty:
            self.ready_to_export = False
            self.target_loaded = False
            self.reset_fig()
        else:
            self.ready_to_export = True
            self.target_loaded = True
            self.plot_data()
        gui.base.buildTab.update_btn_status(obj=self)

    def remove_aux_cols_from_selected(self):
        for aux_col in self.aux_cols:
            if aux_col in self.vars_selected_df.columns:
                self.vars_selected_df.drop(axis=1, labels=aux_col, inplace=True)  # Remove aux cols

    def reset_fig(self):
        for ax in self.fig.axes:
            self.fig.delaxes(ax)
            self.fig.canvas.draw()
            self.fig.canvas.flush_events()

    def calc(self):
        self.init_new_cols()
        self.set_class_var_data_col()
        self.add_class_var_data_to_selected()
        self.calc_upper_lower_lim()
        self.generate_class_flag()
        self.mark_in_plot()

    def mark_in_plot(self):
        self.marker_filter = self.vars_selected_df[self.inclass_flag_col] == True
        self.marker_isset = True
        self.ready_to_export = False
        gui.base.buildTab.update_btn_status(obj=self)
        self.plot_data()

    def generate_class_flag(self):
        """Flag values that are *inside* upper *and* lower class limit"""
        self.vars_selected_df[self.inclass_flag_col] = \
            (self.vars_selected_df[self.class_var_col] <= self.vars_selected_df[self.upper_lim_col]) & \
            (self.vars_selected_df[self.class_var_col] >= self.vars_selected_df[self.lower_lim_col])

    def add_class_var_data_to_selected(self):
        """Add class var data, use constant column name"""
        self.vars_selected_df[self.class_var_col] = self.vars_available_df[self.class_var_data_col]

    def init_new_cols(self):
        self.vars_selected_df[self.upper_lim_col] = np.nan
        self.vars_selected_df[self.lower_lim_col] = np.nan
        self.vars_selected_df[self.class_var_col] = np.nan
        self.vars_selected_df[self.inclass_flag_col] = np.nan

    def set_target_col_to_add(self):
        """Get column name of target var from available var list"""
        target_var = self.lst_varlist_available.selectedIndexes()
        target_var_ix = target_var[0].row()
        self.target_col = self.col_dict_tuples[target_var_ix]
        self.target_pretty = self.col_list_pretty[target_var_ix]  # Needs new pretty list (screen names)

    def set_target_col_to_remove(self):
        """Get column name of target var from selected var list"""
        target_var = self.lst_varlist_selected.selectedIndexes()
        target_var_ix = target_var[0].row()
        self.target_col = list(self.selected_vars_lookup_dict.keys())[target_var_ix]  # Get key by index
        # self.target_col = self.col_dict_tuples[target_var_ix]
        # self.target_pretty = self.col_list_pretty[target_var_ix]  # Needs new pretty list (screen names)

    def add_target_to_selected(self):
        """Add var from available to selected df and lookup dict"""
        self.selected_vars_lookup_dict[self.target_col] = self.target_pretty
        self.vars_selected_df[self.target_col] = self.vars_available_df[self.target_col]

    # def mark_in_plot(self):
    #     # class_var_col = self.set_class_var_col()
    #     # upper_lim, lower_lim = self.get_settings_from_fields()
    #
    #     marker_data = self.tab_data_df[class_var_col].copy()
    #     self.marker_filter = (marker_data >= lower_lim) & (marker_data <= upper_lim)
    #     self.marker_isset = True
    #     self.ready_to_export = False
    #     self.target_loaded = True
    #     tabs.buildTab.update_btn_status(obj=self)
    #     axes_dict = self.make_axes_dict(fig=self.fig, df=self.vars_selected_df)
    #     self.plot_data(axes_dict=axes_dict, df=self.vars_selected_df)

    def calc_upper_lower_lim(self):
        upper_lim, lower_lim = self.get_settings_from_fields()  # Limits are directly read from fields, no calcs
        self.vars_selected_df[self.upper_lim_col] = upper_lim
        self.vars_selected_df[self.lower_lim_col] = lower_lim

    def get_settings_from_fields(self):
        upper_lim = float(self.lne_upper_class_lim.text())
        lower_lim = float(self.lne_lower_class_lim.text())
        return upper_lim, lower_lim

    def set_class_var_data_col(self):
        class_var_ix = self.col_list_pretty.index(self.drp_class_var.currentText())
        self.class_var_data_col = self.col_dict_tuples[class_var_ix]

    def keep_marked(self):
        if self.marker_isset:
            self.vars_selected_df[~self.marker_filter] = np.nan
            self.marker_isset = False
            self.ready_to_export = True
            gui.base.buildTab.update_btn_status(obj=self)
            self.plot_data()

    def remove_marked(self):
        if self.marker_isset:
            self.vars_selected_df[self.marker_filter] = np.nan
            self.marker_isset = False
            self.ready_to_export = True
            gui.base.buildTab.update_btn_status(obj=self)
            self.plot_data()

    def prepare_export(self):
        self.remove_aux_cols_from_selected()
        export_df = self.vars_selected_df.copy()
        export_df.columns = [(str(col[0]) + '+anCF', col[1]) for col in export_df.columns]  # Add suffix
        return export_df

    def get_selected(self, main_df):
        """Return modified class data back to main data"""
        export_df = self.prepare_export()
        main_df = export_to_main(main_df=main_df,
                                 export_df=export_df,
                                 tab_data_df=self.tab_data_df)  # Return results to main
        self.marker_isset = False
        self.ready_to_export = False
        gui.base.buildTab.update_btn_status(obj=self)
        return main_df

    def make_dynamic_axes_dict(self, df):
        gs = gridspec.GridSpec(len(df.columns), 1)  # rows, cols
        gs.update(wspace=0.2, hspace=0.2, left=0.03, right=0.97, top=0.97, bottom=0.03)

        # Make new axes
        axes_dict = {}
        first_ax = -9999
        ax_counter = -1
        for ix, col in enumerate(df):
            if ix == 0:
                ax = self.fig.add_subplot(gs[ix, 0])
                first_ax = ax
            else:
                ax = self.fig.add_subplot(gs[ix, 0], sharex=first_ax)
            axes_dict[col] = ax

        # Format
        for col, ax in axes_dict.items():
            gui.plotfuncs.default_format(ax=ax, txt_xlabel=False)

        return axes_dict

    def make_plot_df(self):
        plot_df_cols = []
        for col in self.vars_selected_df.columns:
            if (str(col[0]).startswith('_aux_')):
                pass
            else:
                plot_df_cols.append(col)
        plot_df = self.vars_selected_df[plot_df_cols].copy()
        return plot_df

    def plot_data(self):
        # Delete all axes in figure
        for ax in self.fig.axes:
            self.fig.delaxes(ax)

        plot_df = self.make_plot_df()
        axes_dict = self.make_dynamic_axes_dict(df=plot_df)

        color_list = colorwheel_36()
        current_color = -1
        for col, ax in axes_dict.items():
            if str(col[0]).startswith('_'):  # Ignore _underscore aux variables
                continue
            current_color += 1
            num_vals = len(plot_df[col].dropna())
            ax.plot_date(x=plot_df.index, y=plot_df[col],
                         color=color_list[current_color], alpha=1, ls='-',
                         marker='o', markeredgecolor='none', ms=4, zorder=98, label='XXX')
            ax.text(0.01, 0.97, f"{col[0]}",
                    size=FONTSIZE_HEADER_AXIS, color=FONTCOLOR_HEADER_AXIS,
                    backgroundcolor='none', transform=ax.transAxes, alpha=1,
                    horizontalalignment='left', verticalalignment='top', zorder=99)

            if self.marker_isset:
                marker_S = plot_df[col].copy()[self.marker_filter]
                num_marked_vals = len(marker_S.dropna())
                perc_marked_vals = (num_marked_vals / num_vals) * 100
                ax.plot_date(x=marker_S.index, y=marker_S,
                             color='#FFEE58', alpha=1, ls='none', mew=1, mec='#607D8B',
                             marker='o', ms=5, zorder=98, label=f"marked values: {num_marked_vals}"
                                                                f" ({perc_marked_vals:.1f}%)")

            gui.plotfuncs.default_grid(ax=ax)
            gui.plotfuncs.default_legend(ax=ax)

            locator = mdates.AutoDateLocator(minticks=3, maxticks=30)
            formatter = mdates.ConciseDateFormatter(locator)
            ax.xaxis.set_major_locator(locator)
            ax.xaxis.set_major_formatter(formatter)

            self.fig.canvas.draw()
            self.fig.canvas.flush_events()

    def remove_target_from_selected(self):
        """Remove from lookup dict and df"""
        del self.selected_vars_lookup_dict[self.target_col]
        self.vars_selected_df.drop(self.target_col, axis=1, inplace=True)

    def update_selected_vars_list(self):
        self.lst_varlist_selected.clear()
        for col in self.vars_selected_df.columns:
            if col not in self.aux_cols:
                item = qw.QListWidgetItem(self.selected_vars_lookup_dict[col])
                self.lst_varlist_selected.addItem(item)

    def check_if_selected_empty(self):
        _list = self.lst_varlist_selected
        if self.vars_selected_df.empty:
            _list.addItem('-empty-')
            empty = True
        else:
            empty = False
        return empty

    def set_colnames(self):
        # Marker for values in the outlier range
        self.class_var_col = (f"_aux_class_var_value", '[aux]')
        self.inclass_flag_col = (f"_aux_QCF_an_class_finder", '[True=in_class]')
        self.upper_lim_col = (f"_aux_upper_limit", '[aux]')
        self.lower_lim_col = (f"_aux_lower_limit", '[aux]')
        self.aux_cols = [self.class_var_col, self.inclass_flag_col,
                         self.upper_lim_col, self.lower_lim_col]
