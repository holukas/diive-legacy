"""

RANDOM FOREST

    very good explanation:
    https://towardsdatascience.com/random-forest-in-python-24d0893d51c0

"""
import datetime
import fnmatch

import matplotlib.gridspec as gridspec
import numpy as np
import pandas as pd
from PyQt5 import QtGui as qtg
from PyQt5 import QtWidgets as qtw
from sklearn import metrics
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import train_test_split

import gui
import gui.add_to_layout
import gui.base
import gui.make
import logger
import modboxes.plots.styles.LightTheme as theme
from pkgs.dfun.frames import flatten_multiindex_all_df_cols
from help.tooltips import gf_random_forest as tooltips
from utils.vargroups import *

pd.set_option('display.width', 1000)
pd.set_option('display.max_columns', 6)


# pd.set_option('display.max_rows', 999)


class addContent(gui.base.buildTab):
    """Build new tab and populate with contents"""

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id, tab_template='SVonVP')

        # Tab icon
        self.TabWidget.setTabIcon(self.tab_ix, qtg.QIcon(app_obj.ctx.tab_icon_gapfilling))

        # Add settings menu contents
        self.drp_target, self.drp_separate_day_night, self.drp_define_nighttime_based_on, \
        self.drp_set_nighttime_if, self.lne_nighttime_threshold, self.lne_num_estimators, \
        self.drp_criterion, self.lne_max_depth, self.lne_min_samples_split, self.lne_min_samples_leaf, \
        self.lne_min_weight_fraction_leaf, self.drp_max_features, self.lne_max_leaf_nodes, \
        self.btn_add_vars_auto, self.btn_train_model, self.btn_fill_gaps, self.btn_add_as_new_var, \
        self.lne_min_impurity_decrease, self.drp_oob_score, self.lne_n_jobs, \
        self.lne_random_state, self.lne_verbose, self.drp_warm_start, self.lne_ccp_alpha, \
        self.lne_max_samples = \
            self.add_settings_fields()

        # # Add variables required in settings
        # self.populate_settings_fields()

        # Add available variables to variable lists
        gui.base.buildTab.populate_variable_list(obj=self)

        # Add plots
        self.axes_dict = self.populate_plot_area()

        # Add special
        self.lbl_statsbox_num_gaps, self.lbl_statsbox_available = self.add_statsboxes()
        self.btn_fill_gaps.setDisabled(True)
        self.btn_add_as_new_var.setDisabled(True)

    def add_statsboxes(self):
        frm_statsboxes_gf = qtw.QFrame()
        grd_statsboxes_gf = qtw.QGridLayout()  # create layout for frame
        frm_statsboxes_gf.setLayout(grd_statsboxes_gf)  # assign layout to frame
        # lyt_vert_figs_tt_gf.setContentsMargins(0, 0, 0, 0)
        lbl_statsbox_num_gaps = gui.elements.add_label_pair_to_grid_layout(txt='Gaps',
                                                                           css_ids=['lbl_statsbox_txt',
                                                                                    'lbl_statsbox_val'],
                                                                           layout=grd_statsboxes_gf,
                                                                           row=0, col=0, orientation='vert')
        lbl_statsbox_num_gaps.setText('-')
        lbl_statsbox_available = gui.elements.add_label_pair_to_grid_layout(txt='Available Rows',
                                                                            css_ids=['lbl_statsbox_txt',
                                                                                     'lbl_statsbox_val'],
                                                                            layout=grd_statsboxes_gf,
                                                                            row=0, col=1, orientation='vert')
        lbl_statsbox_available.setText('-')
        self.lyt_plot_area.addWidget(frm_statsboxes_gf, stretch=1)
        return lbl_statsbox_num_gaps, lbl_statsbox_available

    def populate_plot_area(self):
        return self.add_axes()

    # def populate_settings_fields(self):
    #     self.drp_target.clear()
    #     self.drp_define_nighttime_based_on.clear()
    #
    #     # Add all variables to drop-down lists to allow full flexibility yay
    #     for ix, colname_tuple in enumerate(self.tab_data_df.columns):
    #         self.drp_target.addItem(self.col_list_pretty[ix])
    #         self.drp_define_nighttime_based_on.addItem(self.col_list_pretty[ix])

    def add_axes(self):
        gs = gridspec.GridSpec(4, 1)  # rows, cols
        gs.update(wspace=0.2, hspace=0.2, left=0.03, right=0.97, top=0.97, bottom=0.03)

        ax_tt_feature_importances = self.fig.add_subplot(gs[0, 0])
        ax_tt_measured_and_predicted = self.fig.add_subplot(gs[1, 0])
        ax_gf_measured_and_predicted = self.fig.add_subplot(gs[2, 0])
        ax_gf = self.fig.add_subplot(gs[3, 0],
                                     sharex=ax_gf_measured_and_predicted,
                                     sharey=ax_gf_measured_and_predicted)

        axes_dict = {'ax_gf_measured_and_predicted': ax_gf_measured_and_predicted,
                     'ax_gf': ax_gf,
                     'ax_tt_feature_importances': ax_tt_feature_importances,
                     'ax_tt_measured_and_predicted': ax_tt_measured_and_predicted}

        for key, ax in axes_dict.items():
            gui.plotfuncs.default_format(ax=ax, txt_xlabel=False)

        return axes_dict

    def add_settings_fields(self):
        # Target
        gui.elements.add_header_in_grid_row(
            row=0, layout=self.sett_layout, txt='Target')
        drp_target = gui.elements.grd_LabelDropdownPair(
            txt='Target', css_ids=['', 'cyan'], layout=self.sett_layout,
            row=1, col=0, orientation='horiz')
        gui.add_to_layout.add_spacer_item_to_grid(self.sett_layout, 2, 0)

        # Day / night
        gui.elements.add_header_in_grid_row(
            row=3, layout=self.sett_layout, txt='Day / Night')
        drp_separate_day_night = gui.elements.grd_LabelDropdownPair(
            txt='Separate For Day / Night', css_ids=['', 'cyan'], layout=self.sett_layout,
            row=4, col=0, orientation='horiz')
        drp_separate_day_night.addItems(['Yes', 'No'])
        drp_define_nighttime_based_on = gui.elements.grd_LabelDropdownPair(
            txt='Define Nighttime Based On', css_ids=['', 'cyan'], layout=self.sett_layout,
            row=5, col=0, orientation='horiz')
        drp_set_nighttime_if = gui.elements.grd_LabelDropdownPair(
            txt='Set To Nighttime If', css_ids=['', 'cyan'], layout=self.sett_layout,
            row=6, col=0, orientation='horiz')
        drp_set_nighttime_if.addItems(['Smaller Than Threshold', 'Larger Than Threshold'])
        lne_nighttime_threshold = gui.elements.add_label_linedit_pair_to_grid(
            txt='Threshold', css_ids=['', 'cyan'], layout=self.sett_layout,
            row=7, col=0, orientation='horiz')
        lne_nighttime_threshold.setText('20')
        gui.add_to_layout.add_spacer_item_to_grid(self.sett_layout, 8, 0)

        # MODEL SETTINGS
        # =======================================
        onlyInt = qtg.QIntValidator()
        onlyFloat = qtg.QDoubleValidator()

        gui.elements.add_header_in_grid_row(
            row=9, layout=self.sett_layout, txt='Model Settings')
        args = dict(css_ids=['', 'cyan'], layout=self.sett_layout, col=0, orientation='horiz')

        lne_num_estimators = gui.elements.add_label_linedit_info_triplet_to_grid(
            row=10, txt='Number Of Estimators', txt_info_hover=tooltips._help_lne_num_estimators, **args)
        lne_num_estimators.setText('10')
        lne_num_estimators.setValidator(onlyInt)

        drp_criterion = gui.elements.add_label_dropdown_info_triplet_to_grid(
            row=11, txt='Criterion', txt_info_hover=tooltips._help_drp_criterion, **args)
        drp_criterion.addItems(['Mean Squared Error (mse)', 'Mean Absolute Error (mae)'])

        lne_max_depth = gui.elements.add_label_linedit_info_triplet_to_grid(
            row=12, txt='Max Depth', txt_info_hover=tooltips._help_lne_max_depth, **args)
        lne_max_depth.setText('-9999')  # -9999 corresponds to None
        lne_max_depth.setValidator(onlyInt)

        lne_min_samples_split = gui.elements.add_label_linedit_info_triplet_to_grid(
            row=13, txt='Min Samples Split', txt_info_hover=tooltips._help_lne_min_samples_split, **args)
        lne_min_samples_split.setText('2')  # Int of float
        lne_max_depth.setValidator(onlyFloat)

        lne_min_samples_leaf = gui.elements.add_label_linedit_info_triplet_to_grid(
            row=14, txt='Min Samples Leaf', txt_info_hover=tooltips._help_lne_min_samples_leaf, **args)
        lne_min_samples_leaf.setText('1')  # Int of float
        lne_max_depth.setValidator(onlyFloat)

        lne_min_weight_fraction_leaf = gui.elements.add_label_linedit_info_triplet_to_grid(
            row=15, txt='Min Weight Fraction Leaf', txt_info_hover=tooltips._help_lne_min_weight_fraction_leaf, **args)
        lne_min_weight_fraction_leaf.setText('0.0')
        lne_min_weight_fraction_leaf.setValidator(onlyFloat)

        drp_max_features = gui.elements.add_label_dropdown_info_triplet_to_grid(
            row=16, txt='Max Features', txt_info_hover=tooltips._help_drp_max_features, **args)
        drp_max_features.addItems(['auto', 'sqrt', 'log2'])

        lne_max_leaf_nodes = gui.elements.add_label_linedit_info_triplet_to_grid(
            row=17, txt='Max Leaf Nodes', txt_info_hover=tooltips._help_lne_max_leaf_nodes, **args)
        lne_max_leaf_nodes.setText('-9999')
        lne_max_leaf_nodes.setValidator(onlyInt)

        lne_min_impurity_decrease = gui.elements.add_label_linedit_info_triplet_to_grid(
            row=18, txt='Min Impurity Decrease', txt_info_hover=tooltips._help_lne_min_impurity_decrease, **args)
        lne_min_impurity_decrease.setText('0.0')
        lne_min_impurity_decrease.setValidator(onlyFloat)

        drp_oob_score = gui.elements.add_label_dropdown_info_triplet_to_grid(
            row=19, txt='Out-of-bag Samples', txt_info_hover=tooltips._help_drp_oob_score, **args)
        drp_oob_score.addItems(['False', 'True'])

        lne_n_jobs = gui.elements.add_label_linedit_info_triplet_to_grid(
            row=20, txt='Number Of Jobs', txt_info_hover=tooltips._help_lne_n_jobs, **args)
        lne_n_jobs.setText('-9999')
        lne_n_jobs.setValidator(onlyInt)

        lne_random_state = gui.elements.add_label_linedit_info_triplet_to_grid(
            row=21, txt='Random State', txt_info_hover=tooltips._help_lne_random_state, **args)
        lne_random_state.setText('-9999')
        lne_random_state.setValidator(onlyInt)

        lne_verbose = gui.elements.add_label_linedit_info_triplet_to_grid(
            row=22, txt='Verbose', txt_info_hover=tooltips._help_lne_verbose, **args)
        lne_verbose.setText('0')
        lne_verbose.setValidator(onlyInt)

        drp_warm_start = gui.elements.add_label_dropdown_info_triplet_to_grid(
            row=23, txt='Warm Start', txt_info_hover=tooltips._help_drp_warm_start, **args)
        drp_warm_start.addItems(['False', 'True'])

        lne_ccp_alpha = gui.elements.add_label_linedit_info_triplet_to_grid(
            row=24, txt='Cost-Complexity Pruning', txt_info_hover=tooltips._help_lne_ccp_alpha, **args)
        lne_ccp_alpha.setText('0.0')
        lne_ccp_alpha.setValidator(onlyFloat)

        lne_max_samples = gui.elements.add_label_linedit_info_triplet_to_grid(
            row=25, txt='Max Samples', txt_info_hover=tooltips._help_lne_max_samples, **args)
        lne_max_samples.setText('-9999')
        lne_max_samples.setValidator(onlyFloat)

        gui.add_to_layout.add_spacer_item_to_grid(self.sett_layout, 26, 0)

        # Buttons
        btn_add_vars_auto = gui.elements.add_button_to_grid(
            grid_layout=self.sett_layout, txt='Add All Variables To Selected', css_id='btn_cat_ControlsRun',
            row=27, col=0, rowspan=1, colspan=2)
        btn_train_model = gui.elements.add_button_to_grid(
            grid_layout=self.sett_layout, txt='Train and Test Model', css_id='btn_cat_ControlsRun',
            row=28, col=0, rowspan=1, colspan=2)
        btn_fill_gaps = gui.elements.add_button_to_grid(
            grid_layout=self.sett_layout, txt='Fill Gaps', css_id='btn_cat_ControlsRun',
            row=29, col=0, rowspan=1, colspan=2)
        btn_add_as_new_var = gui.elements.add_button_to_grid(
            grid_layout=self.sett_layout, txt='+ Add As New Var', css_id='btn_add_as_new_var',
            row=30, col=0, rowspan=1, colspan=2)

        self.sett_layout.setRowStretch(31, 2)  # empty row

        return drp_target, drp_separate_day_night, drp_define_nighttime_based_on, \
               drp_set_nighttime_if, lne_nighttime_threshold, lne_num_estimators, \
               drp_criterion, lne_max_depth, lne_min_samples_split, lne_min_samples_leaf, \
               lne_min_weight_fraction_leaf, drp_max_features, lne_max_leaf_nodes, \
               btn_add_vars_auto, btn_train_model, btn_fill_gaps, btn_add_as_new_var, \
               lne_min_impurity_decrease, drp_oob_score, lne_n_jobs, lne_random_state, \
               lne_verbose, drp_warm_start, lne_ccp_alpha, lne_max_samples


class Run(addContent):
    """
    Start random forest
    """
    current_color_ix = -1
    current_marker_ix = -1
    plot_color_list = theme.colors_6()
    plot_marker_list = theme.generate_plot_marker_list()
    feature_gap_num_records = -9999
    grouping_col = ('_group', '[#]')
    year_col = ('_year', '[aux]')
    season_col = ('_season', '[aux]')
    month_col = ('_month', '[aux]')
    weekofyear_col = ('_weekofyear', '[aux]')
    date_col = ('_date', '[aux]')
    dayofyear_col = ('_dayofyear', '[aux]')
    hour_col = ('_hour', '[aux]')
    y_test_pred_col = ('TEST_PREDICTED', '[same-as-target]')
    colorwheel = theme.colors_12(shade=500)
    sub_outdir = "gapfilling_random_forest"

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id)
        logger.log(name='>>> Starting Gap-filling: Random Forest', dict={}, highlight=True)  # Log info
        self.grp_model_dict = {}
        self.rafo_df = pd.DataFrame()
        self.selected_vars_lookup_dict = {}
        self.vars_selected_df = pd.DataFrame()
        self.vars_available_df = self.tab_data_df.copy()
        self.sub_outdir = self.project_outdir / self.sub_outdir
        self.update_refinements()

    def update_refinements(self):
        self.drp_target.clear()
        self.drp_define_nighttime_based_on.clear()

        default_target_ix = default_nighttime_ix = 0
        default_target_ix_found = default_nighttime_ix_found = False

        # Add all variables to drop-down lists to allow full flexibility yay
        for ix, colname_tuple in enumerate(self.tab_data_df.columns):
            self.drp_target.addItem(self.col_list_pretty[ix])
            self.drp_define_nighttime_based_on.addItem(self.col_list_pretty[ix])

            # Check if column appears in the global vars
            if any(fnmatch.fnmatch(colname_tuple[0], vid) for vid in NIGHTTIME_DETECTION):
                default_nighttime_ix = ix if not default_nighttime_ix_found else default_nighttime_ix
                default_nighttime_ix_found = True

        # Set dropdowns to found ix
        self.drp_target.setCurrentIndex(default_target_ix)
        self.drp_define_nighttime_based_on.setCurrentIndex(default_nighttime_ix)

        # # Default values for the text fields
        # self.lne_swin_sim.setText('50')  # W m-2

    def add_var_to_selected(self):
        """Add feature from available to selected"""
        selected_var = self.lst_varlist_available.selectedIndexes()
        selected_var_ix = selected_var[0].row()
        selected_col = self.col_dict_tuples[selected_var_ix]
        selected_pretty = self.col_list_pretty[selected_var_ix]  # Needs new pretty list (screen names)

        # Add to lookup dict and df
        self.selected_vars_lookup_dict[selected_col] = selected_pretty
        self.vars_selected_df[selected_col] = self.vars_available_df[selected_col]
        self.update_varlist_selected(list=self.lst_varlist_selected,
                                     df=self.vars_selected_df,
                                     lookup_dict=self.selected_vars_lookup_dict)

        self.get_settings_from_fields()

    def add_all_vars_to_selected(self):
        for ix in range(self.lst_varlist_available.count()):
            var_pretty = self.lst_varlist_available.item(ix).text()
            var_col = self.col_dict_tuples[ix]

            selection_threshold = (self.target_num_gaps / 100) * 90  # Var must be available in x% of target gaps
            if len(self.vars_available_df[var_col][self.target_gap_locations].dropna()) > selection_threshold:
                self.selected_vars_lookup_dict[var_col] = var_pretty
                self.vars_selected_df[var_col] = self.vars_available_df[var_col]

        self.update_varlist_selected(list=self.lst_varlist_selected,
                                     df=self.vars_selected_df,
                                     lookup_dict=self.selected_vars_lookup_dict)
        self.get_settings_from_fields()

    def remove_var_from_selected(self):
        """Remove feature from available to selected"""
        selected_var = self.lst_varlist_selected.selectedIndexes()
        selected_var_ix = selected_var[0].row()
        selected_col = list(self.selected_vars_lookup_dict.keys())[selected_var_ix]  # Get key by index
        # selected_pretty = self.selected_features_lookup_dict[selected_col]

        # Remove from lookup dict and df
        del self.selected_vars_lookup_dict[selected_col]
        self.vars_selected_df.drop(selected_col, axis=1, inplace=True)
        self.update_varlist_selected(list=self.lst_varlist_selected,
                                     df=self.vars_selected_df,
                                     lookup_dict=self.selected_vars_lookup_dict)

        self.check_if_selected_empty()
        self.get_settings_from_fields()

    def gap_locations(self):
        target_gap_locations = self.tab_data_df[self.target_col].isnull()  # True if gap in measured
        feature_gap_num_records = len(self.vars_selected_df[target_gap_locations].dropna())
        target_num_gaps = target_gap_locations.sum()
        return target_gap_locations, feature_gap_num_records, target_num_gaps

    def fill_statsboxes(self):
        self.lbl_statsbox_num_gaps.setText(f"Gaps in {self.target_col[0]}: {self.target_num_gaps} missing values")
        perc = (self.feature_gap_num_records / self.target_num_gaps) * 100
        self.lbl_statsbox_available.setText(
            f"Gaps in {self.target_col[0]} that can be filled with features: {self.feature_gap_num_records} ({perc:.1f}%)")

    def update_varlist_selected(self, list, df, lookup_dict):
        list.clear()
        for col in df.columns:
            item = qtw.QListWidgetItem(lookup_dict[col])
            list.addItem(item)

    def check_if_selected_empty(self):
        _list = self.lst_varlist_selected
        if self.vars_selected_df.empty:
            _list.addItem('-empty-')

    def add_time_cols(self, df):
        """Add date and time info columns"""
        df[self.date_col] = df.index.strftime('%Y%m%d')  # Format to integer for machine learning
        df[self.year_col] = df.index.year
        df[self.month_col] = df.index.month
        df[self.dayofyear_col] = df.index.dayofyear
        df[self.weekofyear_col] = df.index.isocalendar().week
        df[self.hour_col] = df.index.hour
        return df

    def add_meteorological_season(self, df):
        # Add meteorological seasons
        df[self.season_col] = np.nan
        _season_lut = self.generate_season_type_lut(start_month=12, segments=4)
        found_data_months = df.index.month.unique()
        for found_month in found_data_months:
            assigned_season = int(_season_lut.loc[_season_lut['month'] == found_month, 'season'])
            # tt_df.sort_index(axis=1, inplace=True)  # lexsort for better performance
            df.loc[(df.index.month == found_month),
                   [self.season_col]] = assigned_season
        return df

    def collect_data(self):
        """Collect target and features data, add time cols, seasons and group col"""
        df = self.vars_selected_df.copy()  # Selected features
        df[self.target_col] = self.tab_data_df[self.target_col]  # Target data, i.e. measured data
        df = self.add_time_cols(df=df.copy())
        df = self.add_meteorological_season(df=df.copy())
        df = self.add_grouping_col(df=df.copy())  # Needed for grouping
        return df

    def generate_season_type_lut(self, start_month, segments):
        """ Generate df that contains info which month is assigned to which season (lookup table). """
        season_lut = pd.DataFrame()
        labels = list(range(1, segments + 1))
        start_dt = datetime.date(2000, start_month, 1)
        date_series = pd.date_range(start_dt, periods=12, freq='M')
        season_lut['month'] = date_series.month
        season_lut['month_name'] = date_series.month_name()
        _seasons = pd.cut(season_lut.index, segments, labels=labels)  # How awesome!
        season_lut['season'] = _seasons
        return season_lut

    def get_settings_from_fields(self):

        self.target_col = self.get_col_from_drp_pretty(drp=self.drp_target)
        self.nighttime_col = self.get_col_from_drp_pretty(drp=self.drp_define_nighttime_based_on)
        self.separate_day_night = self.drp_separate_day_night.currentText()
        self.set_nighttime_if = self.drp_set_nighttime_if.currentText()
        self.nighttime_threshold = int(self.lne_nighttime_threshold.text())

        self.target_col = self.get_col_from_drp_pretty(drp=self.drp_target)  # Update target from dropdown selection
        self.target_gap_locations, self.feature_gap_num_records, self.target_num_gaps = self.gap_locations()

        # Model settings
        # see: https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestRegressor.html
        self.num_estimators = int(self.lne_num_estimators.text())

        _criterion = self.drp_criterion.currentText()
        if _criterion == 'Mean Squared Error (mse)':
            self.criterion = 'mse'
        elif _criterion == 'Mean Absolute Error (mae)':
            self.criterion = 'mae'
        else:
            self.criterion = 'mse'

        self.max_depth = None if self.lne_max_depth.text() == '-9999' else int(self.lne_max_depth.text())

        # min_samples_split must be an integer greater than 1 or a float in (0.0, 1.0]
        if float(self.lne_min_samples_split.text()) > 1:
            self.min_samples_split = int(self.lne_min_samples_split.text())
        else:
            self.min_samples_split = abs(float(self.lne_min_samples_split.text()))

        # min_samples_leaf must be at least 1 or in (0, 0.5]
        _min_samples_leaf = abs(float(self.lne_min_samples_leaf.text()))
        if _min_samples_leaf >= 1:
            self.min_samples_leaf = int(_min_samples_leaf)
        elif (_min_samples_leaf > 0.5) & (_min_samples_leaf < 1):
            self.min_samples_leaf = 1  # Set to default
        elif _min_samples_leaf == 0:
            self.min_samples_leaf = 0.0000000001  # Must not be zero, set to very small number

        self.min_weight_fraction_leaf = float(self.lne_min_weight_fraction_leaf.text())
        self.max_features = str(self.drp_max_features.currentText())  # More options in sklearn: int, float, see docs
        self.max_leaf_nodes = None if self.lne_max_leaf_nodes.text() == '-9999' else int(self.lne_max_leaf_nodes.text())
        self.min_impurity_decrease = float(self.lne_min_impurity_decrease.text())
        self.oob_score = False if self.drp_oob_score.currentText() == 'False' else True
        self.n_jobs = None if self.lne_n_jobs.text() == '-9999' else int(self.lne_n_jobs.text())
        self.random_state = None if self.lne_random_state.text() == '-9999' else int(self.lne_random_state.text())
        self.verbose = int(self.lne_verbose.text())
        self.warm_start = False if self.drp_warm_start.currentText() == 'False' else True
        self.ccp_alpha = abs(float(self.lne_ccp_alpha.text()))  # Must be positive float
        self.max_samples = None if self.lne_max_samples.text() == '-9999' else float(self.lne_max_samples.text())

        self.fill_statsboxes()

    def add_grouping_col(self, df):
        """Add flag to indicate group membership"""
        df[self.grouping_col] = np.nan

        # Check if grouping col was also selected as a feature
        delete_grouping_col = True
        if self.nighttime_col in list(self.selected_vars_lookup_dict.keys()):
            delete_grouping_col = False

        if self.separate_day_night == 'Yes':
            nighttime_filter = None
            daytime_filter = None
            df[self.nighttime_col] = self.tab_data_df[self.nighttime_col]  # Add for grouping
            if self.set_nighttime_if == 'Smaller Than Threshold':
                nighttime_filter = df[self.nighttime_col] < self.nighttime_threshold
                daytime_filter = df[self.nighttime_col] >= self.nighttime_threshold
            elif self.set_nighttime_if == 'Larger Than Threshold':
                nighttime_filter = df[self.nighttime_col] > self.nighttime_threshold
                daytime_filter = df[self.nighttime_col] <= self.nighttime_threshold

            df.loc[nighttime_filter, [self.grouping_col]] = 0
            df.loc[daytime_filter, [self.grouping_col]] = 1

            if delete_grouping_col:
                df.drop(self.nighttime_col, axis=1, inplace=True)  # Only needed for grouping
        else:
            df.loc[:, [self.grouping_col]] = 0  # All in the same group
        return df

    def make_model(self, train_test_df):
        """Make model from train and test data"""

        # Split into training and test data
        train_df, test_df = train_test_split(train_test_df, test_size=0.25)
        train_df = train_df.sort_index()
        test_df = test_df.sort_index()

        # Model training
        # --------------
        y_train = train_df[self.target_col]  # Train target data (Series)
        X_train = train_df.drop(self.target_col, axis=1)  # Train feature data (DataFrame)
        model = self.train_model(X_train=X_train.to_numpy(),  # Needs data as arrays (.to_numpy)
                                 y_train=y_train.to_numpy(),
                                 num_estimators=self.num_estimators,
                                 obj=self)
        # Train results: feature importances
        feature_importances_df = pd.DataFrame(index=X_train.columns,
                                              data={'feature_importances': model.feature_importances_})
        feature_importances_df.sort_values('feature_importances', ascending=False, inplace=True)

        # Model testing
        # -------------
        y_test = test_df[self.target_col]  # Test target data (Series)
        X_test = test_df.drop(self.target_col, axis=1)  # Test feature data (DataFrame)

        # Predictions
        y_test_pred, mae, mse, rmse = \
            self.predict(model=model,
                         features=X_test.to_numpy(),
                         target=y_test.to_numpy(),
                         test=True)
        test_results_df = test_df.copy()
        test_results_df[self.y_test_pred_col] = y_test_pred

        # Collect test results
        # --------------------
        test_stats_dict = {'Mean absolute error': mae,
                           'Mean squared error': mse,
                           'Root mean squared error': rmse}
        model_dict = dict(model=model,
                          test_stats_dict=test_stats_dict,
                          test_results_df=test_results_df,
                          feature_importances_df=feature_importances_df)
        return model_dict

    def _keep_complete_rows(self, df):
        # _filter_missing_target = df[self.target_col].isnull()
        # df = df[~_filter_missing_target]
        return df.dropna()  # Keep rows where all variables are available

    def model_training(self):
        """ Train and test (tt) the model with the features in the feature list,
        separately for each group (based on grouping column). """

        # msg = qtw.QMessageBox()
        # msg.setWindowTitle("Random Forest")
        # msg.setText("This is a message box")
        # msg.exec_()

        # Collect data needed for model building and later gap-filling
        self.get_settings_from_fields()
        self.rafo_df = self.collect_data()

        # Complete rows needed for model building
        tt_data_df = self._keep_complete_rows(df=self.rafo_df.copy())

        # Group based on grouping column
        grouped = tt_data_df.groupby(self.grouping_col)
        self.grp_model_dict = {}
        for grp, grp_df in grouped:
            model_dict = self.make_model(train_test_df=grp_df)
            # Add model and other info for this group to dict, for loop through grps
            self.grp_model_dict[grp] = model_dict

        # Plot importances
        self.plot_test_importances(ax=self.axes_dict['ax_tt_feature_importances'],
                                   grp_model_dict=self.grp_model_dict)

        # Plot test predicted values vs measured values
        self.plot_tt_timeseries(grp_model_dict=self.grp_model_dict,  # Contains eg test results for groups
                                measured=tt_data_df[self.target_col],
                                ax=self.axes_dict['ax_tt_measured_and_predicted'])

        self.btn_fill_gaps.setEnabled(True)
        self.btn_add_as_new_var.setDisabled(True)

    @staticmethod
    def quickfill(df, target_col, target_gf_col):
        """Quickly (and "dirtly") fill time series using only timestamp info as features"""
        # filter_nan = df[target_col].isnull()
        _df_no_nan = df[[target_col]].dropna().copy()
        _df_with_nan = df[[target_col]].copy()

        # Make quick model
        tt_measured, tt_features, tt_feature_list = \
            Run.convert_to_arrays(df=_df_no_nan,
                                  target_col=target_col)
        training_features, test_features, training_measured, test_measured = \
            Run.split_into_training_and_testing_sets(features=tt_features,
                                                     target=tt_measured)
        model = \
            Run.train_model(X_train=training_features,
                            y_train=training_measured,
                            num_estimators=10)
        # test_predictions, test_accuracy, test_mean_abs_error = \
        #     Run.predict(model=model, features=test_features,
        #                 measured=test_measured, test=True)

        # Gapfilling
        gap_locs = _df_with_nan[target_col].isnull()  # Get gap locations
        gaps_that_can_be_filled_df = _df_with_nan.loc[gap_locs]
        gaps_measured, gaps_features, gaps_feature_list = Run.convert_to_arrays(df=gaps_that_can_be_filled_df,
                                                                                target_col=target_col)

        gaps_predictions = Run.predict(model=model, features=gaps_features,
                                       target=False, test=False)

        gaps_dates = Run.list_to_datetime(features=gaps_features,
                                          feature_list=gaps_feature_list)
        gaps_filled_df = pd.DataFrame(data={'gaps_dates': gaps_dates,
                                            'gaps_predictions': gaps_predictions})
        gaps_filled_df.set_index('gaps_dates', inplace=True)

        df['gaps_predictions'] = gaps_filled_df['gaps_predictions']
        df[target_gf_col] = df[target_col].fillna(gaps_filled_df['gaps_predictions'])
        return df[target_gf_col]

    def gapfilling(self):
        """Fill gaps in target with information from selected features"""

        # Get features (X) for rows where target is missing and all features are available
        _rafo_df = self.rafo_df.copy()
        _rafo_df = _rafo_df[self.target_gap_locations]  # Locations where target is missing
        X = _rafo_df.drop(self.target_col, axis=1)  # Features (DataFrame), incl. group
        X = X.dropna()  # All features available

        # In each group, use features to predict target in target gaps
        X_grouped = X.groupby(self.grouping_col)
        for X_grp, X_grp_df in X_grouped:
            model = self.grp_model_dict[X_grp]['model']
            gaps_predictions = self.predict(model=model,
                                            features=X_grp_df.to_numpy(),
                                            target=None,
                                            test=False)

            # Predictions as Series
            gaps_predictions_S = pd.Series(index=X_grp_df.index, data=gaps_predictions)

            # Add gap predictions for this group to other data
            self.rafo_df[('_gaps_predictions', f'[GRP_{X_grp}]')] = gaps_predictions_S

        # Combine into same df (merge)
        self.set_colnames()
        self.rafo_df = self.init_cols(df=self.rafo_df.copy())
        self.rafo_df[self.measured_f_col] = self.rafo_df[self.target_col].copy()  # Will be filled

        # Collect all gaps predictions in one column
        for col in self.rafo_df.columns:
            if col[0] == '_gaps_predictions':
                self.rafo_df[self.merged_gaps_predictions_col] = \
                    self.rafo_df[self.merged_gaps_predictions_col].combine_first(self.rafo_df[col])

        predictions_ix = self.rafo_df[self.merged_gaps_predictions_col].dropna().index  # Index of all gapfilled values
        self.rafo_df.loc[predictions_ix, [self.measured_qcf_col]] = 1  # Predictions exist, i.e. gapfilled values
        self.rafo_df.loc[~self.target_gap_locations, [self.measured_qcf_col]] = 0  # Originally measured values

        self.rafo_df[self.measured_f_col] = \
            self.rafo_df[self.measured_f_col].combine_first(self.rafo_df[self.merged_gaps_predictions_col])

        # Plot
        self.plot_measured_and_gaps_predicted_ts(df=self.rafo_df,
                                                 ax=self.axes_dict['ax_gf_measured_and_predicted'],
                                                 measured_col=self.target_col,
                                                 gap_pred_col=self.merged_gaps_predictions_col)
        self.plot_gf_ts(df=self.rafo_df,
                        ax=self.axes_dict['ax_gf'],
                        gf_col=self.measured_f_col)
        self.btn_add_as_new_var.setEnabled(True)

        # # TODO Decision tree
        # https://towardsdatascience.com/random-forest-in-python-24d0893d51c0
        # self.plot_decision_tree(model=rf, lst_Features=lst_Features)

    def combine_tt_lists_into_df(self, feature_list, features, test_features, measured, test_predictions):
        # Measured data
        dates = self.list_to_datetime(features=features, feature_list=feature_list)  # Dates of training values
        measured_df = pd.DataFrame(data={'date': dates, 'measured': measured})  # Dataframe with true values and dates
        measured_df.set_index('date', inplace=True)

        # Predicted data
        dates = self.list_to_datetime(features=test_features, feature_list=feature_list)  # Dates of predictions
        predicted_df = pd.DataFrame(data={'date': dates, 'predicted': test_predictions})  # Predictions & dates
        predicted_df.set_index('date', inplace=True)
        predicted_df.sort_index(inplace=True)

        # Combined measured and predicted data
        df = measured_df.copy()
        df['predicted'] = predicted_df['predicted']
        return df

    @staticmethod
    def list_to_datetime(features, feature_list):
        years = features[:, feature_list.index('year')]
        months = features[:, feature_list.index('month')]
        days = features[:, feature_list.index('day')]
        hours = features[:, feature_list.index('hours')]
        minutes = features[:, feature_list.index('minutes')]

        # List and then convert to datetime object
        dates = [f"{str(int(year))}-{str(int(month))}-{str(int(day))} {str(int(hour))}:{str(int(minute))}"
                 for year, month, day, hour, minute in zip(years, months, days, hours, minutes)]
        dates = [datetime.datetime.strptime(date, '%Y-%m-%d %H:%M') for date in dates]
        return dates

    # def plot_tt_performance_scatter(self, ax_acc, ax_mae, accuracy, mean_abs_error):
    #     ax_acc.clear()
    #     ax_mae.clear()
    #     performance_df = pd.DataFrame()
    #     performance_df.loc[0, 'accuracy_mape'] = accuracy
    #     self.plot_scatter(df=performance_df, ax=ax_acc,
    #                       plot_col='accuracy_mape', title_txt='Accuracy - Mean Absolute Percentage Error (MAPE)')
    #
    #     performance_df.loc[0, 'mean_abs_error'] = mean_abs_error
    #     self.plot_scatter(df=performance_df, ax=ax_mae,
    #                       plot_col='mean_abs_error', title_txt='Mean Absolute Error (mae)')

    def variable_importances(self, model, feature_list):
        # Get numerical feature importances
        importances = list(model.feature_importances_)

        # List of tuples with variable and importance
        feature_importances = [(feature, round(importance, 2)) for feature, importance in
                               zip(feature_list, importances)]

        # Sort the feature importances by most important first
        feature_importances = sorted(feature_importances, key=lambda x: x[1],
                                     reverse=True)

        # Print out the feature and importances
        [print('Variable: {:20} Importance: {}'.format(*pair)) for pair in feature_importances]
        return feature_importances

    @staticmethod
    def train_model(X_train, y_train, num_estimators, obj):
        """ Instantiate the model, and fit (scikit-learn’s name for training) the model on the training data """

        # Instantiate model with x decision trees
        model = RandomForestRegressor(n_estimators=num_estimators,
                                      criterion=obj.criterion,
                                      max_depth=obj.max_depth,
                                      min_samples_split=obj.min_samples_split,
                                      min_samples_leaf=obj.min_samples_leaf,
                                      min_weight_fraction_leaf=obj.min_weight_fraction_leaf,
                                      max_features=obj.max_features,
                                      max_leaf_nodes=obj.max_leaf_nodes,
                                      min_impurity_decrease=obj.min_impurity_decrease,
                                      min_impurity_split=None,
                                      bootstrap=True,
                                      oob_score=obj.oob_score,
                                      n_jobs=obj.n_jobs,
                                      random_state=obj.random_state,
                                      verbose=obj.verbose,
                                      warm_start=obj.warm_start,
                                      ccp_alpha=obj.ccp_alpha,
                                      max_samples=obj.max_samples)

        model.fit(X_train, y_train)  # Train the model on training data

        # # https://towardsdatascience.com/feature-selection-using-random-forest-26d7b747597f
        # # TODO
        # sel = SelectFromModel(RandomForestClassifier(n_estimators=100))
        # sel.fit(train_features, train_labels)
        # sel.get_support()

        return model

    @staticmethod
    def predict(model: RandomForestRegressor,
                features: np.array,
                target: np.array,
                test: bool):
        # Use the forest's predict method on the test data
        # see: https://medium.com/ampersand-academy/random-forest-regression-using-python-sklearn-from-scratch-9ad7cf2ec2bb
        # see: https://towardsdatascience.com/random-forest-in-python-24d0893d51c0
        y_pred = model.predict(features)

        if test:
            # Evaluating the Algorithm
            mae = metrics.mean_absolute_error(target, y_pred)  # Mean absolute error
            mse = metrics.mean_squared_error(target, y_pred)  # Mean squared error
            rmse = np.sqrt(metrics.mean_squared_error(target, y_pred))  # Root mean squared error

            # print('Mean Absolute Error:', )
            # print('Mean Squared Error:', )
            # print('Root Mean Squared Error:', )

            # # Calculate the absolute errors
            # errors = abs(y_pred - target)
            # mean_abs_error = np.mean(errors)
            # print('Mean Absolute Error:', round(mean_abs_error, 2),
            #       'degrees.')  # Print out the mean absolute error (mae)

            # # Calculate mean absolute percentage error (MAPE)
            # mape = 100 * (errors / measured)
            # accuracy = 100 - np.mean(mape)
            # print('Accuracy:', round(accuracy, 2), '%.')

            return y_pred, mae, mse, rmse
        else:
            return y_pred

    @staticmethod
    def split_into_training_and_testing_sets(features, target):
        """ Split data into training and testing sets (using Skicit-learn). """
        train_features, test_features, train_measured, test_measured = \
            train_test_split(features, target,
                             test_size=0.25,
                             random_state=42)
        # Shape of data1
        # We expect the training features number of columns to match the
        # testing feature number of columns and the number of rows to match
        # for the respective training and testing features and the labels.
        print('Training Features Shape:', train_features.shape)
        print('Training Measured Shape:', train_measured.shape)
        print('Testing Features Shape:', test_features.shape)
        print('Testing Labels Shape:', test_measured.shape)
        return train_features, test_features, train_measured, test_measured

    @staticmethod
    def convert_to_arrays(df, target_col):
        """
        Use numpy to convert to arrays and separate the data into
        features and target
        """

        # Get target from df, convert to array and remove from df
        target_arr = df[target_col].to_numpy()  # Target are the values we want to predict
        df = df.drop(target_col, axis=1)  # Remove the labels from the features
        features_arr = df.to_numpy()  # Convert to numpy array

        # Flatten column names, stored as string instead of tuples, for conversion to array
        df = flatten_multiindex_all_df_cols(df=df)

        # Column names are now flattened, i.e. string instead of MultiIndex tuples
        features_list = list(df.columns)
        features_timestamp = df.index

        # target = np.array(df[target_col])  # Labels are the values we want to predict
        # features = np.array(df)

        return target_arr, features_arr, features_list, features_timestamp

    def get_filled(self, main_df, results_df):
        """
        Filled timeseries back to data_df

        The quality flag is converted from strings (LIP1, LIP5, ...) to numbers,
        then they can be directly plotted.

        A similar approach in used in MDS gap-filling.
        """

        # Make subset of columns that are then put into data_df
        data_cols_for_data_df = results_df[[self.measured_f_col, self.measured_qcf_col]]
        _data_cols_for_data_df = data_cols_for_data_df.copy()

        # Make sure columns are MultiIndex
        _data_cols_for_data_df.columns = pd.MultiIndex.from_tuples(_data_cols_for_data_df.columns)

        # # Remove all non-numeric characters from the quality string
        # _data_cols_for_data_df[self.measured_fqual_col] = \
        #     _data_cols_for_data_df[self.measured_fqual_col].str.replace(r'\D', '')  # \D removes string characters

        # # Make sure the column is numeric
        # _data_cols_for_data_df[self.measured_fqual_col] = pd.to_numeric(_data_cols_for_data_df[self.measured_fqual_col])

        # Add gap-filled data to main data_df
        main_df[self.measured_f_col] = np.nan
        main_df[self.measured_qcf_col] = np.nan
        main_df = main_df.combine_first(_data_cols_for_data_df)

        # # Show gap-filled time series in plot
        # self.plot_ts(df=add_to_df, ax=self.axes_dict['ax_ts_filled'], plot_col=self.var_f_col,
        #              title_txt=f"Gap-filled Timeseries {self.var_f_col[0]}")

        self.btn_add_as_new_var.setDisabled(True)

        return main_df

    def init_cols(self, df):
        df[self.measured_f_col] = np.nan
        df[self.measured_qcf_col] = np.nan
        df[self.merged_gaps_predictions_col] = np.nan
        return df

    def set_colnames(self):
        suffix = '+gfRF'
        self.measured_f_col = (self.target_col[0] + suffix, self.target_col[1])  # col that is filled
        self.measured_qcf_col = ("_QCF_" + self.measured_f_col[0], '[0=measured]')  # fill quality
        self.merged_gaps_predictions_col = ("_merged_gaps_predictions_" + self.measured_f_col[0], self.target_col[1])

    def make_gaps_df(self, df):
        """
        Make df that contains only rows that miss the measurement but has all features available.
        """
        filter_nan = df[self.target_col].isnull()  # Detect missing values in measured
        gaps_df = df[filter_nan]  # Remove rows with missing measurements
        # gaps_df = gaps_df.drop(self.target_col, axis=1)  # Remove measurements col
        # gaps_df = gaps_df.dropna()
        return df[self.target_gap_locations]

    def get_col_from_drp_pretty(self, drp):
        """ Set target to selected variable. """
        target_pretty = drp.currentText()
        target_pretty_ix = self.col_list_pretty.index(target_pretty)
        target_col = self.col_dict_tuples[target_pretty_ix]
        return target_col

    def detect_features(self, source_df, min_available=0.67):
        """ Assemble the df that contains all the needed data. """
        total_cols = len(source_df.columns)
        total_len = len(source_df.index)
        df = pd.DataFrame()

        # Check if available column data is above the threshold (67% available needed)
        for col in source_df.columns:
            this_len = len(source_df[col].dropna().index)
            available = this_len / total_len
            if available >= min_available:
                df[col] = source_df[col]

        accepted_cols = len(df.columns)
        accepted_len = len(df.index)
        accepted_len_no_nan = len(df.dropna().index)

        print(f"Accepted {accepted_cols} of {total_cols} columns.")
        print(f"Input data contains {accepted_len_no_nan} of {accepted_len} data rows where all records are available.")

        # measured_pretty_ix = self.col_list_pretty.index(self.measured_col)
        # self.measured_col = self.col_dict_tuples[measured_pretty_ix]
        # from inout.DataFunctions import flatten_multiindex_all_df_cols
        # df = flatten_multiindex_all_df_cols(df=df)

        # df = self.make_aux_cols(df=df)
        return df

    # def plot_decision_tree(self, model, feature_list):
    #     tree = model.estimators_[0]  # Pull out one tree from the forest
    #     export_graphviz(tree, out_file='tree.dot', feature_names=feature_list, rounded=True,
    #                     precision=1)
    #     (graph,) = pydot.graph_from_dot_file('tree.dot')  # Use dot file to create a graph
    #     graph.write_png('tree.png')  # Write graph to a png file

    def plot_test_importances(self, ax, grp_model_dict, show_how_many=10):
        ax.clear()
        # num_groups = len(grp_model_dict)
        # importances_daytime = importances_daytime[0:show_how_many]

        # Get feature importances for each group
        feature_importances_df = pd.DataFrame()
        grp_counter = 0
        for grp in grp_model_dict:
            grp_counter += 1
            _df = grp_model_dict[grp]['feature_importances_df']
            _df.rename(columns={'feature_importances': f'feature_importances_group_{grp}'}, inplace=True)
            feature_importances_df = pd.concat([feature_importances_df, _df], axis=1)

            # grp_col = f"feature_importances_group_{grp}"
            # _df[grp_col] = _df['feature_importances']
            # _df.drop(['feature_importances'], axis=1, inplace=True)
            # for pair in grp_dict['feature_importances']:
            #     feature_importances_df.loc[pair[0], grp] = pair[1]  # Collect results in df

        num_bars_per_tick = len(feature_importances_df.columns)
        bars_width = 0.6  # Total width of all bars
        bars_start = bars_width / 2 / 2  # Start of the first of multiple bars
        bar_width = bars_width / num_bars_per_tick  # Width of one of multiple bars
        nudge_pos = bar_width

        pos_xticks = []
        labels_xticks = []
        for ix, var in enumerate(feature_importances_df.index):
            grp_counter = -1
            color_counter = -1
            for grp in feature_importances_df.columns:
                grp_counter += 1
                color_counter += 1
                label = f"Group {grp}" if ix == 0 else None
                ax.bar((ix - bars_start) + (nudge_pos * grp_counter), feature_importances_df.loc[[var], grp],
                       align='center', alpha=1, width=bar_width, color=self.colorwheel[color_counter], label=label)
            pos_xticks.append(ix)
            labels_xticks.append((var[0]))

        gui.plotfuncs.default_legend(ax=ax, loc='upper right', bbox_to_anchor=(1, 1))
        ax.set_xticks(pos_xticks)
        ax.set_xticklabels(labels_xticks)
        self.fig.canvas.draw()
        self.fig.canvas.flush_events()

    def plot_measured_and_gaps_predicted_ts(self, df, ax, measured_col, gap_pred_col):
        """ Plot measured and filled gaps. """

        ax.clear()

        ax.plot_date(x=df.index, y=df[measured_col],
                     color='#78909C', alpha=0.5, ls='-',
                     marker='o', markeredgecolor='none', ms=6, zorder=98, label='measured')

        # Group based on grouping column
        grouped = df.groupby(self.grouping_col)
        for grp, grp_df in grouped:
            _filter_this_grp = df[self.grouping_col] == grp
            gap_pred = grp_df.loc[_filter_this_grp, [gap_pred_col]]
            ax.plot_date(x=gap_pred.index, y=gap_pred,
                         color=self.colorwheel[int(grp)], alpha=0.7, ls='none',
                         marker='o', markeredgecolor='none', ms=6, zorder=98,
                         label=f"predicted in group {grp}")

        ax.text(0.01, 0.97, 'Measured and Gaps Predicted Data',
                size=theme.FONTSIZE_HEADER_AXIS, color=theme.FONTCOLOR_HEADER_AXIS,
                backgroundcolor='none', transform=ax.transAxes, alpha=1,
                horizontalalignment='left', verticalalignment='top')

        ax.set_xlim(df.index[0], df.index[-1])

        self.fig.canvas.draw()
        self.fig.canvas.flush_events()

    def plot_gf_ts(self, df, ax, gf_col):
        """ Plot measured and filled gaps. """

        ax.clear()

        ax.plot_date(x=df.index, y=df[gf_col],
                     color='#78909C', alpha=0.5, ls='-',
                     marker='o', markeredgecolor='none', ms=6, zorder=98, label='measured')

        ax.text(0.01, 0.97, 'Gap-filled Timeseries',
                size=theme.FONTSIZE_HEADER_AXIS, color=theme.FONTCOLOR_HEADER_AXIS,
                backgroundcolor='none', transform=ax.transAxes, alpha=1,
                horizontalalignment='left', verticalalignment='top')

        ax.set_xlim(df.index[0], df.index[-1])

        self.fig.canvas.draw()
        self.fig.canvas.flush_events()

    def plot_tt_timeseries(self, grp_model_dict, measured: pd.Series, ax):
        """ Plot measured and filled gaps. """
        test_mean_abs_error = ""
        test_mean_squared_error = ""
        test_root_mean_squared_error = ""

        ax.clear()

        ax.plot_date(x=measured.index, y=measured,
                     color='#607D8B', alpha=0.3, ls='-',
                     marker='o', markeredgecolor='none', ms=6, zorder=98, label='measured')

        grp_counter = -1
        color_counter = -1
        for grp in grp_model_dict:
            test_results_df = grp_model_dict[grp]['test_results_df']
            test_stats_dict = grp_model_dict[grp]['test_stats_dict']
            grp_counter += 1
            color_counter += 1

            # ax.plot_date(x=test_results_df.index, y=test_results_df[self.target_col],
            #              color='#607D8B', alpha=0.3, ls='-',
            #              marker='o', markeredgecolor='none', ms=6, zorder=98, label='measured')

            ax.plot_date(x=test_results_df.index, y=test_results_df[self.y_test_pred_col],
                         color=self.colorwheel[color_counter], alpha=0.8, ls='none',
                         marker='o', markeredgecolor='none', ms=6, zorder=99, label='predicted')

            test_mean_abs_error = test_mean_abs_error \
                                  + f"[GRP {grp:.0f}]: {test_stats_dict['Mean absolute error']:.2f}    "
            test_mean_squared_error = test_mean_squared_error \
                                      + f"[GRP {grp:.0f}]: {test_stats_dict['Mean squared error']:.2f}    "
            test_root_mean_squared_error = test_root_mean_squared_error \
                                           + f"[GRP {grp:.0f}]: {test_stats_dict['Root mean squared error']:.2f}    "

        stats_txt = \
            f"Performance\n" \
            f"Mean Absolute Error: {test_mean_abs_error}\n" \
            f"Mean Squared Error: {test_mean_squared_error}\n" \
            f"Root mean Squared Error: {test_root_mean_squared_error}\n"
        ax.text(0.99, 0.97, stats_txt, size=theme.FONTSIZE_HEADER_AXIS,
                color=theme.FONTCOLOR_HEADER_AXIS, backgroundcolor='none', transform=ax.transAxes,
                alpha=1, horizontalalignment='right', verticalalignment='top')

        ax.text(0.01, 0.97, 'Training and Testing', size=theme.FONTSIZE_HEADER_AXIS,
                color=theme.FONTCOLOR_HEADER_AXIS, backgroundcolor='none', transform=ax.transAxes,
                alpha=1, horizontalalignment='left', verticalalignment='top')

        # ax.set_xlim(df.index[0], df.index[-1])

        self.fig.canvas.draw()
        self.fig.canvas.flush_events()
