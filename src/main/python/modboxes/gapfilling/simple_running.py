import matplotlib.dates as mdates
import matplotlib.gridspec as gridspec
import numpy as np
import pandas as pd
from PyQt5 import QtGui as qtg

import gui
import gui.add_to_layout
import gui.base
import gui.make
import logger
import modboxes.plots.styles.LightTheme as theme
from pkgs.dfun.frames import export_to_main


# pd.set_option('display.max_rows', 50)
# pd.set_option('display.max_columns', 10)
# pd.set_option('display.width', 1000)

class addContent(gui.base.buildTab):
    """Build new tab and populate with contents"""

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id, tab_template='SVP')

        # Tab icon
        self.TabWidget.setTabIcon(self.tab_ix, qtg.QIcon(app_obj.ctx.tab_icon_gapfilling))

        # Add settings menu contents
        self.lne_timewin_len, self.lne_timewin_min_vals, self.drp_method, \
        self.btn_calc, self.btn_add_as_new_var = \
            self.add_settings_fields()

        # Add variables required in settings
        self.populate_settings_fields()

        # Add available variables to variable lists
        gui.base.buildTab.populate_variable_list(obj=self)

        # # Add plots
        # self.axes_dict = self.populate_plot_area()

        # # Add special
        # self.lbl_statsbox_num_gaps, self.lbl_statsbox_available = self.add_statsboxes()

    def populate_plot_area(self):
        return self.add_axes()

    def populate_settings_fields(self):
        self.lne_timewin_len.setText("144")
        self.lne_timewin_min_vals.setText("100")

    def add_axes(self):
        pass

    def add_settings_fields(self):
        gui.elements.add_header_subheader_to_grid_top(layout=self.sett_layout,
                                                      txt=["Gap-filling",
                                                           "Simple Running"])
        gui.add_to_layout.add_spacer_item_to_grid(layout=self.sett_layout, row=1, col=0)

        gui.elements.add_header_in_grid_row(layout=self.sett_layout, txt="Time Window", row=2)

        lne_timewin_len = gui.elements.add_label_linedit_pair_to_grid(
            txt='Size (values)', css_ids=['', 'cyan'], layout=self.sett_layout,
            row=3, col=0, orientation='horiz')

        lne_timewin_min_vals = gui.elements.add_label_linedit_pair_to_grid(
            txt='Minimum Number Of Values', css_ids=['', 'cyan'], layout=self.sett_layout,
            row=4, col=0, orientation='horiz')

        gui.add_to_layout.add_spacer_item_to_grid(layout=self.sett_layout, row=5, col=0)

        drp_method = gui.elements.grd_LabelDropdownPair(
            txt='Method', css_ids=['', ''], layout=self.sett_layout,
            row=5, col=0, orientation='horiz')
        drp_method.addItem('Median')
        drp_method.addItem('Mean')
        drp_method.addItem('Max')
        drp_method.addItem('Min')

        gui.add_to_layout.add_spacer_item_to_grid(layout=self.sett_layout, row=6, col=0)

        btn_calc = gui.elements.add_button_to_grid(
            grid_layout=self.sett_layout, txt='Preview', css_id='',
            row=7, col=0, rowspan=1, colspan=2)

        gui.add_to_layout.add_spacer_item_to_grid(layout=self.sett_layout, row=8, col=0)

        btn_add_as_new_var = gui.elements.add_button_to_grid(grid_layout=self.sett_layout,
                                                             txt='+ Add As New Var',
                                                             css_id='btn_add_as_new_var',
                                                             row=9, col=0, rowspan=1, colspan=2)

        self.sett_layout.setRowStretch(10, 2)  # empty row

        # progress_bar = qw.QProgressBar()
        # progress_bar.setAlignment(qtc.Qt.AlignCenter)
        # progress_bar.setFormat('setup ...')
        # progress_bar.setMaximum(100)
        # self.sett_layout.addWidget(progress_bar, 29, 0, 1, 2)

        return lne_timewin_len, lne_timewin_min_vals, drp_method, \
               btn_calc, btn_add_as_new_var


class Run(addContent):
    sub_outdir = "gapfilling_simple_running"
    short_id = "gfSIRU"

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id)
        logger.log(name='>>> Starting Gap-filling: Simple Running', dict={}, highlight=True)  # Log info
        self.sub_outdir = self.project_outdir / self.sub_outdir
        self.class_df = pd.DataFrame()
        self.target_col = None
        self.target_loaded = False
        self.filter_gaps = None  # Gap locations
        self.gapfilled_available = False
        self.ready_to_export = False
        self.btn_add_as_new_var.setDisabled(True)

        self.set_colnames()
        self.axes_dict = self.make_axes_dict()

    def select_target(self):
        """Select target var from list"""
        self.set_target_col()
        self.class_df = self.init_class_df()
        self.update_fields()
        self.init_new_cols()
        self.target_loaded = True
        self.gapfilled_available = False
        self.ready_to_export = False
        self.btn_add_as_new_var.setDisabled(True)
        self.plot_data()

    def set_colnames(self):
        """Set names of columns that are added to DataFrame."""
        self.running_vals_col = ('_running_values', '[aux]')
        self.gap_vals_col = ('_gap_values', '[aux]')
        self.target_gf_col = ('_target_gapfilled', '[aux]')

    def init_new_cols(self):
        self.class_df[self.running_vals_col] = np.nan
        self.class_df[self.gap_vals_col] = np.nan
        self.class_df[self.target_gf_col] = np.nan

    def calc(self):
        self.get_settings_from_fields()

        # Detect missing values in measured
        self.filter_gaps = self.class_df[self.target_col].isnull()

        # Rolling object, same for all methods
        rolling_obj = self.class_df[self.target_col].rolling(self.timewin_len,
                                                             min_periods=self.timewin_min_vals,
                                                             center=True)

        if self.method == 'Median':
            # Running median based on measured values
            self.class_df[self.running_vals_col] = rolling_obj.median()

        elif self.method == 'Mean':
            # Running mean based on measured values
            self.class_df[self.running_vals_col] = rolling_obj.mean()

        elif self.method == 'Max':
            # Running max based on measured values
            self.class_df[self.running_vals_col] = rolling_obj.max()

        elif self.method == 'Min':
            # Running min based on measured values
            self.class_df[self.running_vals_col] = rolling_obj.min()

        # Running median for values that are missing in measured
        self.class_df[self.gap_vals_col] = self.class_df[self.running_vals_col][self.filter_gaps]

        # Fill gaps in measured with running median
        self.class_df[self.target_gf_col] = \
            self.class_df[self.target_col].fillna(self.class_df[self.running_vals_col])

        self.gapfilled_available = True
        self.ready_to_export = True
        self.btn_add_as_new_var.setEnabled(True)

        self.plot_data()

    def plot_data(self):
        # Delete all axes in figure
        for ax in self.fig.axes:
            self.fig.delaxes(ax)

        self.axes_dict = self.make_axes_dict()
        ax = self.axes_dict['ax_main']

        df = self.class_df
        col = self.target_col
        legend_lns = []

        ax.text(0.01, 0.97, f"{col[0]}",
                size=theme.FONTSIZE_HEADER_AXIS, color=theme.FONTCOLOR_HEADER_AXIS,
                backgroundcolor='none', transform=ax.transAxes, alpha=1,
                horizontalalignment='left', verticalalignment='top', zorder=99)

        if not self.gapfilled_available:
            lns = ax.plot_date(x=df.index, y=df[col],
                               color='#546E7A', alpha=1, ls='-',
                               marker='o', markeredgecolor='none', ms=4, zorder=98,
                               label=f"{col[0]} {col[1]}")
            legend_lns.append(lns[0])

        if self.gapfilled_available:
            # marker_S = df[self.target_col].copy()[self.marker_filter]

            # Gap-filled target
            _series = self.class_df[self.target_gf_col].dropna()
            num_vals = len(_series)
            lns = ax.plot_date(x=_series.index, y=_series,
                               color='#546E7A', alpha=1, ls='-',
                               marker='o', markeredgecolor='none', ms=4, zorder=98,
                               label=f"gap-filled {self.target_col[0]} {self.target_col[1]} "
                                     f"({num_vals} values)")
            legend_lns.append(lns[0])

            # Running values, using selected method
            _series = self.class_df[self.running_vals_col].dropna()
            num_vals = len(_series)
            lns = ax.plot_date(x=_series.index, y=_series,
                               color='#ef5350', alpha=1, ls='none', mew=1, mec='#4FC3F7',
                               marker='o', ms=2, zorder=98, label=f'running {self.method} ({num_vals} values)')
            legend_lns.append(lns[0])

            # Gap-filled values, using selected method
            _series = self.class_df[self.running_vals_col][self.filter_gaps].dropna()
            num_vals = len(_series)
            lns = ax.plot_date(x=_series.index, y=_series,
                               color='#ef5350', alpha=1, ls='none', mew=1, mec='#e53935',
                               marker='o', ms=5, zorder=98,
                               label=f'gap-filled values using {self.method} ({num_vals} values)')
            legend_lns.append(lns[0])

        gui.plotfuncs.default_grid(ax=ax)
        gui.plotfuncs.default_legend(ax=ax, from_line_collection=True, line_collection=legend_lns)

        locator = mdates.AutoDateLocator(minticks=3, maxticks=30)
        formatter = mdates.ConciseDateFormatter(locator)
        ax.xaxis.set_major_locator(locator)
        ax.xaxis.set_major_formatter(formatter)

        self.fig.canvas.draw()
        self.fig.canvas.flush_events()

    def make_axes_dict(self):
        gs = gridspec.GridSpec(1, 1)  # rows, cols
        gs.update(wspace=0.2, hspace=0.2, left=0.03, right=0.96, top=0.96, bottom=0.03)
        ax_main = self.fig.add_subplot(gs[0, 0])
        axes_dict = {'ax_main': ax_main}
        for key, ax in axes_dict.items():
            gui.plotfuncs.default_format(ax=ax, txt_xlabel=False)
        return axes_dict

    def get_settings_from_fields(self):
        self.timewin_min_vals = int(self.lne_timewin_min_vals.text())
        self.timewin_len = int(self.lne_timewin_len.text())
        self.method = self.drp_method.currentText()

        # Restrict
        if self.timewin_min_vals >= self.timewin_len:
            self.timewin_min_vals = self.timewin_len

    def init_class_df(self):
        return self.tab_data_df[[self.target_col]].copy()

    def update_fields(self):
        pass

    def set_target_col(self):
        """Get column name of target var from var list"""
        target_var = self.lst_varlist_available.selectedIndexes()
        target_var_ix = target_var[0].row()
        self.target_col = self.col_dict_tuples[target_var_ix]

    def get_selected(self, main_df):
        """Return modified class data back to main data"""
        export_df = self.prepare_export()
        main_df = export_to_main(main_df=main_df,
                                 export_df=export_df,
                                 tab_data_df=self.tab_data_df)  # Return results to main
        self.btn_add_as_new_var.setDisabled(True)
        return main_df

    def prepare_export(self):
        export_df = self.class_df[[self.target_gf_col]].copy()  # Gap-filled target
        outlier_removed_col = (self.target_col[0] + f"+{self.short_id}", self.target_col[1])
        export_df[outlier_removed_col] = export_df[self.target_gf_col]
        export_df = export_df[[outlier_removed_col]]
        return export_df
