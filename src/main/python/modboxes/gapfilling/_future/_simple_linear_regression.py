import gui.elements
import gui.plotfuncs
import logger
from gui import elements
from modboxes.gapfilling import _shared
from modboxes.plots.styles.LightTheme import *

info_module = 'Gap-filling / Simple Linear Regression'


class AddControls():
    """
        Creates the gui control elements and their handles for usage.

    """

    def __init__(self, opt_stack, opt_frame, opt_layout, ref_stack):
        self.opt_stack = opt_stack
        self.opt_frame = opt_frame
        self.opt_layout = opt_layout
        self.ref_stack = ref_stack

        self.add_option()
        self.add_refinements()

    def add_option(self):
        # Option Button
        self.opt_btn = elements.add_button_to_grid(grid_layout=self.opt_layout,
                                                   txt='Simple Linear Regression', css_id='',
                                                   row=4, col=0, rowspan=1, colspan=1)
        self.opt_stack.addWidget(self.opt_frame)

    def add_refinements(self):
        ref_frame, ref_layout = elements.add_frame_grid()
        self.ref_stack.addWidget(ref_frame)

        gui.elements.add_header_to_grid_top(layout=ref_layout, txt='GF: Simple Linear Regression')

        self.ref_drp_ind_var = elements.grd_LabelDropdownPair(txt='Independent Variable',
                                                              css_ids=['', ''],
                                                              layout=ref_layout,
                                                              row=1, col=0,
                                                              orientation='horiz')

        self.ref_btn_showInPlot = elements.add_button_to_grid(grid_layout=ref_layout,
                                                              txt='Show in Plot', css_id='',
                                                              row=2, col=0, rowspan=1, colspan=2)

        ref_layout.setRowStretch(3, 1)


class Call:

    def __init__(self, data_df, drp_ind_var_col, fig, ax, focus_df, focus_col, col_dict_tuples):

        self.data_df = data_df.copy()
        self.fig = fig  # needed for redraw
        self.ax = ax  # adds to existing axis
        self.focus_df = focus_df.copy()
        self.focus_col = focus_col
        self.focus_df = self.focus_df[[self.focus_col]]  # Basically resets focus_df, deletes prev aux columns
        self.drp_ind_var_col = drp_ind_var_col
        self.col_dict_tuples = col_dict_tuples

        # Define MultiIndex column names
        focus_name = self.focus_col[0]
        self.focus_col_gf = (f"{focus_name}_f", f"{self.focus_col[1]}")
        self.predicted_vals_col = (f"{focus_name}_predicted", '[aux]')
        self.gap_values_col = (f"{focus_name}_gap_vals", '[aux]')

        self.ax = gui.plotfuncs.remove_prev_lines(ax=self.ax)

        try:
            self.get_data()
            self.gapfilling_simplelinregr()
        except:
            pass

    def get_data(self):
        ind_var_ix = self.drp_ind_var_col.currentIndex()
        self.ind_var_col = self.col_dict_tuples[ind_var_ix]
        self.focus_df[self.ind_var_col] = self.data_df[self.ind_var_col]
        self.k, self.d, self.predicted_values, self.rsquared, self.rsquared_adj, self.results = \
            pkgs.dfun.regression.linear(df=self.focus_df)

    def gapfilling_simplelinregr(self):

        # Predicted values from simple linear regression
        # self.focus_df[self.predicted_vals] = self.predicted_values
        self.focus_df[self.predicted_vals_col] = self.focus_df[self.ind_var_col] * self.k + self.d

        # Detect missing values in measured
        filter_nan = self.focus_df[self.focus_col].isnull()
        # Fitted values for data gaps in measured
        self.focus_df[self.gap_values_col] = self.focus_df[self.predicted_vals_col][filter_nan]

        # Fill gaps in measured with predicted values
        self.focus_df[self.focus_col_gf] = self.focus_df[self.focus_col].fillna(self.focus_df[self.predicted_vals_col])

        _shared.show_in_plot(self=self, additional_line=False)

        info_txt = self.results_as_txt(filter_nan=filter_nan)
        self.ax.text(0.05, 0.95, info_txt,
                     horizontalalignment='left', verticalalignment='top', transform=self.ax.transAxes,
                     size=FONTSIZE_INFO_DEFAULT, color=FONTCOLOR_LABELS_AXIS, backgroundcolor='none')

        r2adj = int(self.rsquared_adj * 100)
        self.ax.text(0.95, 0.95, 'r$^2$ adj. = {} %'.format(r2adj),
                     horizontalalignment='right', verticalalignment='top', transform=self.ax.transAxes,
                     size=FONTSIZE_INFO_LARGE, color=COLOR_TXT_INFO_LARGE, backgroundcolor=COLOR_BG_INFO_LARGE)

        self.fig.canvas.draw()  # Update plot

    def results_as_txt(self, filter_nan):
        """
        Collect some stats for output

        Parameters
        ----------
        filter_nan: bool
                    Here used for filtering Series for *numeric* values (note the ~)

        Returns
        -------
        info_txt: str
        """
        # Check number of values for measured and predicted
        num_vals_measured = self.focus_df[self.focus_col][~filter_nan].size
        num_vals_predicted = self.focus_df[self.predicted_vals_col].dropna().size
        num_vals_predicted_in_gaps = self.focus_df[self.gap_values_col].dropna().size

        info_txt = \
            f"using {self.ind_var_col[0]} to predict {self.focus_col[0]}\n" \
            f"y = {self.k:.3f}x + {self.d:.3f}\n" \
            f"r2 = {self.rsquared:.3f} (r2 adj = {self.rsquared_adj:.3f})\n" \
            f"before gap-filling: {num_vals_measured} values\n" \
            f"predicted: {num_vals_predicted} values\n" \
            f"predicted gaps: {num_vals_predicted_in_gaps} values\n"

        logger.log(name=info_module, dict={'info': info_txt}, highlight=False)  # Log info
        return info_txt

    def get_results(self):
        return self.focus_df, self.focus_col_gf
