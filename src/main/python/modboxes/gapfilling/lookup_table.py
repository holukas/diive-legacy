"""

MARGINAL DISTRIBUTION SAMPLING (MDS)
Gap-filling after Reichstein et al (2005)

Reference: https://doi.org/10.1111/j.1365-2486.2005.001002.x

    Make new tab for MDS gap-filling

    Create a new tab and create an instance based on current data. Important: the
    instance gets current data at the time when the instance is created (instance data_df).
    Basically this means that the tab works 'isolated' from the VARIABLES tab. For example,
    if a variables is deleted from the list of variables in the VARIABLES tab, then this
    change will not show up in the 'isolated' MDS tab: it will still work with the instance data_df.
    However, when a variable is gap-filled in the MDS tab, then the gap-filled time series is
    automatically inserted in the main data_df and will show up in the VARIABLES tab.


"""

import fnmatch
import time

import matplotlib.gridspec as gridspec
import numpy as np
import pandas as pd
from PyQt5 import QtCore as qtc
from PyQt5 import QtGui
from PyQt5 import QtWidgets as qw

import gui.base
import gui.elements
import gui.plotfuncs
import logger
import pkgs.dfun.frames
from modboxes.plots.styles.LightTheme import *
from utils.vargroups import *


class addContent(gui.base.buildTab):
    """Build new tab and populate with contents"""

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id, tab_template='SP')

        # Add settings menu contents
        self.drp_flux, self.drp_swin, self.drp_ta, self.drp_vpd, self.lne_swin_sim, \
        self.lne_ta_sim, self.lne_vpd_sim, \
        self.btn_start_qual_a, self.btn_start_qual_b, self.btn_start_qual_c, self.lne_time_win_days, \
        self.lne_time_win_hours, self.progress_bar, self.btn_init, self.btn_calc_uncertainty, \
        self.btn_add_as_new_var = \
            self.add_settings_fields()

        # Add variables required in settings
        self.populate_settings_fields()

        # # Add available variables to variable lists
        # tabs.buildTab.populate_variable_list(obj=self)

        # # Add plots
        # self.axes_dict = self.populate_plot_area()

        # # Add special
        # self.lbl_statsbox_num_gaps, self.lbl_statsbox_available = self.add_statsboxes()

    # def add_statsboxes(self):
    #     frm_statsboxes_gf = qw.QFrame()
    #     grd_statsboxes_gf = qw.QGridLayout()  # create layout for frame
    #     frm_statsboxes_gf.setLayout(grd_statsboxes_gf)  # assign layout to frame
    #     # lyt_vert_figs_tt_gf.setContentsMargins(0, 0, 0, 0)
    #     lbl_statsbox_num_gaps = gui_elements.add_label_pair_to_grid_layout(txt='Gaps',
    #                                                                        css_ids=['lbl_statsbox_txt',
    #                                                                                 'lbl_statsbox_val'],
    #                                                                        layout=grd_statsboxes_gf,
    #                                                                        row=0, col=0, orientation='vert')
    #     lbl_statsbox_num_gaps.setText('-')
    #     lbl_statsbox_available = gui_elements.add_label_pair_to_grid_layout(txt='Available Rows',
    #                                                                         css_ids=['lbl_statsbox_txt',
    #                                                                                  'lbl_statsbox_val'],
    #                                                                         layout=grd_statsboxes_gf,
    #                                                                         row=0, col=1, orientation='vert')
    #     lbl_statsbox_available.setText('-')
    #     self.lyt_plot_area.addWidget(frm_statsboxes_gf, stretch=1)
    #     return lbl_statsbox_num_gaps, lbl_statsbox_available

    def populate_plot_area(self):
        return self.add_axes()

    def populate_settings_fields(self):
        self.drp_flux.clear()
        self.drp_swin.clear()
        self.drp_ta.clear()
        self.drp_vpd.clear()

        # Add all variables to drop-down lists to allow full flexibility yay
        for ix, colname_tuple in enumerate(self.tab_data_df.columns):
            self.drp_flux.addItem(self.col_list_pretty[ix])
            self.drp_swin.addItem(self.col_list_pretty[ix])
            self.drp_ta.addItem(self.col_list_pretty[ix])
            self.drp_vpd.addItem(self.col_list_pretty[ix])
            # q = qw.QListWidgetItem(self.col_list_pretty[ix])
            # self.lst_Features.addItem(q)  # add column name to list
            # background_color = QColor()
            # background_color.setNamedColor('#f44336')
            # q.setBackground(background_color)
            # q.setBackground(QtCore.Qt.blue)  # works but is overwritten by css file

        # Set dropdowns to found ix
        self.drp_flux.setCurrentIndex(0)
        self.drp_swin.setCurrentIndex(0)
        self.drp_ta.setCurrentIndex(0)
        self.drp_vpd.setCurrentIndex(0)

    def add_axes(self):
        pass

    def add_settings_fields(self):
        # ----------------------------------------------------------------------------
        # DATA
        gui.elements.add_header_in_grid_row(layout=self.sett_layout, txt='Data', row=0)

        # VARIABLE SELECTION
        drp_swin = elements.grd_LabelDropdownPair(txt='Short-wave Radiation (W m-2)',
                                                  css_ids=['', 'cyan'],
                                                  layout=self.sett_layout,
                                                  row=1, col=0,
                                                  orientation='horiz')
        self.sett_layout.addWidget(qw.QLabel(), 2, 0, 1, 1)  # spacer

        drp_ta = elements.grd_LabelDropdownPair(txt='Air Temperature (°C)',
                                                css_ids=['', 'cyan'],
                                                layout=self.sett_layout,
                                                row=3, col=0,
                                                orientation='horiz')
        self.sett_layout.addWidget(qw.QLabel(), 4, 0, 1, 1)  # spacer

        drp_vpd = elements.grd_LabelDropdownPair(txt='VPD (Pa)',
                                                 css_ids=['', 'cyan'],
                                                 layout=self.sett_layout,
                                                 row=5, col=0,
                                                 orientation='horiz')
        self.sett_layout.addWidget(qw.QLabel(), 6, 0, 1, 1)  # spacer

        drp_flux = elements.grd_LabelDropdownPair(txt='Flux',
                                                  css_ids=['', 'cyan'],
                                                  layout=self.sett_layout,
                                                  row=7, col=0,
                                                  orientation='horiz')

        self.sett_layout.addWidget(qw.QLabel(), 8, 0, 1, 1)  # spacer

        # ----------------------------------------------------------------------------
        # METHODS
        gui.elements.add_header_in_grid_row(layout=self.sett_layout, txt='Methods', row=9)
        self.sett_layout.addWidget(qw.QLabel(), 10, 0, 1, 1)  # spacer

        # Class similarities
        lne_swin_sim = elements.add_label_linedit_pair_to_grid(txt='Class Similarity (W m-2) ±',
                                                               css_ids=['', 'cyan'],
                                                               layout=self.sett_layout, row=11, col=0,
                                                               orientation='horiz')
        lne_ta_sim = elements.add_label_linedit_pair_to_grid(txt='Class Similarity (°C) ±',
                                                             css_ids=['', 'cyan'],
                                                             layout=self.sett_layout, row=12, col=0,
                                                             orientation='horiz')
        lne_vpd_sim = elements.add_label_linedit_pair_to_grid(txt='Class Similarity (Pa) ±',
                                                              css_ids=['', 'cyan'],
                                                              layout=self.sett_layout, row=13, col=0,
                                                              orientation='horiz')
        self.sett_layout.addWidget(qw.QLabel(), 14, 0, 1, 1)  # spacer

        lne_time_win_days = elements.add_label_linedit_pair_to_grid(txt='Time Window (Days) ±',
                                                                    css_ids=['', 'cyan'],
                                                                    layout=self.sett_layout, row=15, col=0,
                                                                    orientation='horiz')
        lne_time_win_hours = elements.add_label_linedit_pair_to_grid(txt='Time Window (Hours) ±',
                                                                     css_ids=['', 'cyan'],
                                                                     layout=self.sett_layout, row=16, col=0,
                                                                     orientation='horiz')
        self.sett_layout.addWidget(qw.QLabel(), 17, 0, 1, 1)  # spacer

        # ----------------------------------------------------------------------------
        # INITIATE
        btn_init = elements.add_button_to_grid(grid_layout=self.sett_layout,
                                               txt='Initiate Selected Data', css_id='btn_cat_ControlsRun',
                                               row=21, col=0, rowspan=1, colspan=2)

        # ----------------------------------------------------------------------------
        # RUN
        btn_calc_uncertainty = elements.add_button_to_grid(grid_layout=self.sett_layout,
                                                           txt='Calculate Uncertainty',
                                                           css_id='btn_cat_ControlsRun',
                                                           row=22, col=0, rowspan=1, colspan=2)

        self.sett_layout.setRowStretch(23, 2)  # empty row

        # ----------------------------------------------------------------------------
        # RUN
        btn_start_qual_a = elements.add_button_to_grid(grid_layout=self.sett_layout,
                                                       txt='Start Gap-filling: Quality A',
                                                       css_id='btn_cat_ControlsRun',
                                                       row=24, col=0, rowspan=1, colspan=2)

        btn_start_qual_b = elements.add_button_to_grid(grid_layout=self.sett_layout,
                                                       txt='Start Gap-filling: Quality B',
                                                       css_id='btn_cat_ControlsRun',
                                                       row=25, col=0, rowspan=1, colspan=2)

        btn_start_qual_c = elements.add_button_to_grid(grid_layout=self.sett_layout,
                                                       txt='Start Gap-filling: Quality C',
                                                       css_id='btn_cat_ControlsRun',
                                                       row=26, col=0, rowspan=1, colspan=2)

        btn_add_as_new_var = elements.add_button_to_grid(grid_layout=self.sett_layout,
                                                         txt='+ Add As New Var',
                                                         css_id='btn_add_as_new_var',
                                                         row=27, col=0, rowspan=1, colspan=2)

        self.sett_layout.setRowStretch(28, 2)  # empty row

        progress_bar = qw.QProgressBar()
        progress_bar.setAlignment(qtc.Qt.AlignCenter)
        progress_bar.setFormat('setup ...')
        # progress_bar.setMaximum(100)
        self.sett_layout.addWidget(progress_bar, 29, 0, 1, 2)

        self.sett_layout.setRowStretch(30, 2)  # empty row

        return drp_flux, drp_swin, drp_ta, drp_vpd, lne_swin_sim, lne_ta_sim, lne_vpd_sim, \
               btn_start_qual_a, btn_start_qual_b, btn_start_qual_c, lne_time_win_days, \
               lne_time_win_hours, progress_bar, btn_init, btn_calc_uncertainty, btn_add_as_new_var


class Run(addContent):
    current_color_ix = -1
    current_marker_ix = -1
    plot_color_list = colors_6()
    plot_marker_list = generate_plot_marker_list()
    sub_outdir = "gapfilling_lookup_table"

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id)

        # Tab icon
        self.TabWidget.setTabIcon(self.tab_ix, QtGui.QIcon(app_obj.ctx.tab_icon_gapfilling))

        logger.log(name='>>> Starting Gap-filling: Lookup Table', dict={}, highlight=True)  # Log info
        self.sub_outdir = self.project_outdir / self.sub_outdir
        self.update_refinements()

    def init_data(self):
        # self.update_refinements()
        self.btn_add_as_new_var.setDisabled(True)
        self.get_settings_from_fields()
        self.axes_dict = self.make_axes_dict()
        self.mds_df = self.make_mds_df()  # gaps will be filled in this df
        self.lut_df = self.mds_df.copy()  # for LUT data, stays the same
        self.gaps_df = self.mds_df.copy()  # for detecting gaps, will get shorter as gaps are filled
        self.gap_count_on_init = self.setup_progress_bar(df=self.lut_df, col_with_gaps=self.flux_data_col)
        self.plot_lut_df_with_gaps(df=self.lut_df, ax=self.axes_dict['ax_ts'], plot_col=self.flux_data_col,
                                   title_txt='Measured Values\n{} ({} values missing)'.format(self.flux_data_col[0],
                                                                                              self.gap_count_on_init))

        self.plot_lut_df_with_gaps(df=self.lut_df, ax=self.axes_dict['ax_ts_filled_colors'],
                                   plot_col=self.flux_data_col, title_txt='Gap-filling Quality')

        self.plot_lut_df_with_gaps(df=self.lut_df, ax=self.axes_dict['ax_ts_filled'], plot_col=self.flux_f_col,
                                   title_txt=f"Gap-filled Timeseries {self.flux_f_col[0]}")
        self.btn_start_qual_a.setEnabled(True)

    def gapfilling_quality_A(self):
        """Quality A1 - A4."""
        logger.log(name='>>> Gap-filling Quality A', dict={}, highlight=True)  # Log info

        # SWIN, TA, VPD, NEE available within 7, 14 days
        # Quality A1, A2
        multiplier_list = range(1, 3, 1)  # 1 x 7 days = 7 days
        qual_ids_list = ['A - LUT3 within {} days'.format(x * self.time_win_days) for x in multiplier_list]
        self.lut_looper(required_dict=self.required(which='SWIN, TA, VPD'),
                        win_multiplier_list=multiplier_list, qual_ids_list=qual_ids_list)

        # SWIN, NEE available within 7 days
        # Quality A3
        multiplier_list = [1]  # 1 x 7 days = 7 days
        qual_ids_list = ['A - LUT1 within {} days'.format(x * self.time_win_days) for x in multiplier_list]
        self.lut_looper(required_dict=self.required(which='SWIN'), win_multiplier_list=multiplier_list,
                        qual_ids_list=qual_ids_list)

        # NEE available within |dt| <= 1h on same day
        # Quality A4
        multiplier_list = [0]  # 0 x 7 days = 0 days (on same day)
        qual_ids_list = ['A - MDC within {} days'.format(x * self.time_win_days) for x in multiplier_list]
        self.mdc_looper(time_win_days=multiplier_list, qual_ids_list=qual_ids_list)

        self.btn_add_as_new_var.setEnabled(True)

    def mdc(self, fill_gaps_in_df, time_win_days, time_win_hours, qual_id, ax):

        time_start = time.time()

        # Check where the data gaps for flux_col are
        gaps_df, gap_count_pre = pkgs.dfun.frames.find_nans_in_df_col(df=fill_gaps_in_df,
                                                                      col=self.flux_f_col)  # data w/ flux missing
        # No need to gap-fill if there are no gaps
        if gaps_df.empty:
            return gaps_df

        gaps_df, win_start_dt_col, win_end_dt_col = pkgs.dfun.frames.insert_datetimerange(df=gaps_df,
                                                                                          win_days=time_win_days,
                                                                                          win_hours=time_win_hours)

        gaps_df, win_start_time_col, win_end_time_col = pkgs.dfun.frames.insert_timerange(df=gaps_df,
                                                                                          win_hours=time_win_hours)

        counter = 0
        for ix, row in gaps_df.iterrows():
            counter += 1
            self.update_progress_bar(df=gaps_df, col=self.flux_f_col)

            # Keep only datetime window
            start_dt = row[win_start_dt_col]
            end_dt = row[win_end_dt_col]
            mask = (self.lut_df.index >= start_dt) & (self.lut_df.index <= end_dt)
            lut_df_snippet = self.lut_df.loc[mask]

            # Keep only time (hours) window
            start_time = row[win_start_time_col]
            end_time = row[win_end_time_col]
            mask = (lut_df_snippet.index.time >= start_time) & (lut_df_snippet.index.time <= end_time)
            lut_df_snippet = lut_df_snippet.loc[mask]

            available_flux_values = lut_df_snippet[self.flux_data_col].count()
            fill_val = lut_df_snippet[self.flux_data_col].mean() if available_flux_values >= 2 else np.nan

            if fill_val:
                fill_val_mdc_sd = lut_df_snippet[self.flux_data_col].std()

                gaps_df.loc[ix, self.flux_f_col] = fill_val  # insert found fill val in df
                gaps_df.loc[ix, self.flux_f_lutmdc_sd_col] = fill_val_mdc_sd  # insert found fill val in df
                gaps_df.loc[ix, self.flux_fqual_str_col] = qual_id

                # Filled values are collected in mds_df
                self.mds_df.loc[ix, self.flux_f_col] = fill_val
                self.mds_df.loc[ix, self.flux_f_lutmdc_sd_col] = fill_val_mdc_sd
                self.mds_df.loc[ix, self.flux_fqual_str_col] = qual_id

        self.post_fill(gaps_df=gaps_df, ax=ax, qual_id=qual_id, tic=time_start, gap_count_pre=gap_count_pre)

        return gaps_df

    def plot_gaps_df_filled_values(self, df, ax, filled_color, qual_id, tail, plot_marker):
        # ax.get_legend().remove()
        if tail > 0:
            _df = df[[self.flux_f_col, self.flux_fqual_str_col]].dropna().tail(tail)
        else:
            _df = df
        filter = _df[self.flux_fqual_str_col] == qual_id
        _df = _df[filter]

        ax.plot_date(x=df.index, y=df[self.flux_f_col], color=filled_color, alpha=1, ls='none',
                     marker=plot_marker, markeredgecolor='none', ms=6, zorder=99, label=qual_id)

        ax.errorbar(df.index, df[self.flux_f_col],
                    yerr=df[self.flux_f_lutmdc_sd_col],
                    alpha=0.1, capsize=0, ls='none', color=filled_color, elinewidth=8,
                    label="SD during similar conditions", zorder=96)

        # ax.plot_date(x=df.index, y=df[self.flux_f_lutmdc_sd_col], color=filled_color, alpha=1, ls='none',
        #              marker=plot_marker, markeredgecolor='none', ms=8, zorder=99, label=qual_id)

        # ax.plot_date(x=df.index, y=df[self.flux_f_p75_col], color=filled_color, alpha=1, ls='none',
        #              marker=plot_marker, markeredgecolor='none', ms=8, zorder=99, label=qual_id)

        gui.plotfuncs.default_legend(ax=ax, loc='upper right', bbox_to_anchor=(1, 1))
        self.fig.canvas.draw()
        self.fig.canvas.flush_events()

    def post_fill(self, gaps_df, ax, qual_id, tic, gap_count_pre):
        """Plot filled values and output stats to LogBox."""
        gap_count_post = self.update_progress_bar(df=gaps_df, col=self.flux_f_col)

        self.plot_gaps_df_filled_values(df=gaps_df, ax=ax, filled_color=self.plot_color_list[self.current_color_ix],
                                        plot_marker=self.plot_marker_list[self.current_marker_ix],
                                        qual_id=qual_id, tail=-9999)

        gaps_filled = gap_count_pre - gap_count_post
        gaps_filled_perc = (gaps_filled / gap_count_pre) * 100
        logger.log(name=qual_id, dict={'gaps before': gap_count_pre,
                                       'gaps after': gap_count_post,
                                       'gaps filled': f"{gaps_filled} ({gaps_filled_perc:.1f}%)",
                                       'step runtime': f"{time.time() - tic:.3f}s"},
                   highlight=False)  # Log info

    def update_progress_bar(self, df, col):
        _, gap_count_progress = pkgs.dfun.frames.find_nans_in_df_col(df=df, col=col)
        time.sleep(0.00000001)
        self.progress_bar.setValue(gap_count_progress)
        self.progress_bar.setFormat('{} gaps remaining'.format(gap_count_progress))
        return gap_count_progress

    def mdc_looper(self, time_win_days, qual_ids_list):
        """Expand MDC window automatically."""
        loops = len(qual_ids_list)
        for loop in range(0, loops):
            self.set_marker_style()
            self.gaps_df = self.mdc(fill_gaps_in_df=self.gaps_df, time_win_days=time_win_days[loop],
                                    time_win_hours=self.time_win_hours, qual_id=qual_ids_list[loop],
                                    ax=self.axes_dict['ax_ts_filled_colors'])

    def required(self, which):
        if which == 'SWIN, TA, VPD':
            available_dict = {self.swin_data_col: (self.swin_sim_upperlim_col, self.swin_sim_lowerlim_col),
                              self.ta_data_col: (self.ta_sim_upperlim_col, self.ta_sim_lowerlim_col),
                              self.vpd_data_col: (self.vpd_sim_upperlim_col, self.vpd_sim_lowerlim_col)}
        elif which == 'SWIN':
            available_dict = {self.swin_data_col: (self.swin_sim_upperlim_col, self.swin_sim_lowerlim_col)}

        else:
            available_dict = -9999

        return available_dict

    def lut_looper(self, required_dict, win_multiplier_list, qual_ids_list):
        """Expand LUT window automatically."""
        loops = len(qual_ids_list)
        for loop in range(0, loops):
            self.set_marker_style()

            self.gaps_df = self.lut(available_dict=required_dict,
                                    ax=self.axes_dict['ax_ts_filled_colors'],
                                    time_win_days=self.time_win_days * win_multiplier_list[loop],
                                    time_win_hours=self.time_win_hours,
                                    qual_id=qual_ids_list[loop],
                                    fill_gaps_in_df=self.gaps_df)

    def set_marker_style(self):
        self.current_color_ix, self.current_marker_ix = \
            gui.plotfuncs.set_marker_color(current_color_ix=self.current_color_ix,
                                           plot_color_list=self.plot_color_list,
                                           current_marker_ix=self.current_marker_ix,
                                           plot_marker_list=self.plot_marker_list)

    def lut(self, available_dict, qual_id, time_win_days, time_win_hours, ax, fill_gaps_in_df):

        time_start = time.time()

        # Check where the data gaps for flux_col are
        gaps_df, gap_count_pre = pkgs.dfun.frames.find_nans_in_df_col(df=fill_gaps_in_df,
                                                                      col=self.flux_f_col)  # data w/ flux missing
        # No need to gap-fill if there are no gaps
        if gaps_df.empty:
            return gaps_df

        gaps_df, win_start_col, win_end_col = pkgs.dfun.frames.insert_datetimerange(df=gaps_df,
                                                                                    win_days=time_win_days,
                                                                                    win_hours=time_win_hours)

        # Fill data gaps in gaps_df row-by-row, using info from daterange and the lut_df
        counter = 0
        for ix, row in gaps_df.iterrows():
            # print(ix)
            counter += 1
            self.update_progress_bar(df=gaps_df, col=self.flux_f_col)
            # if counter % 10 == 0:
            #     self.update_progress_bar(df=gaps_df, col=self.flux_f_col)

            start_dt = row[win_start_col]
            end_dt = row[win_end_col]

            lut_df_snippet = pkgs.dfun.frames.df_between_two_dates(df=self.lut_df, dropna=True,
                                                                   dropna_col=self.flux_data_col,
                                                                   start_date=start_dt, end_date=end_dt)

            fill_val, fill_val_lut_sd = self.calc_lut_fill_value_from_similar(df=lut_df_snippet,
                                                                              available_dict=available_dict,
                                                                              row=row)

            if fill_val:
                # Insert fill value in gaps_df
                gaps_df.loc[ix, self.flux_f_col] = fill_val
                gaps_df.loc[ix, self.flux_f_lutmdc_sd_col] = fill_val_lut_sd
                gaps_df.loc[ix, self.flux_fqual_str_col] = qual_id

                # Filled values are collected in mds_df
                self.mds_df.loc[ix, self.flux_f_col] = fill_val
                self.mds_df.loc[ix, self.flux_f_lutmdc_sd_col] = fill_val_lut_sd
                self.mds_df.loc[ix, self.flux_fqual_str_col] = qual_id

                # # Show single fill value in plot (slooooow)
                # ax.plot_date(x=ix, y=fill_val, color='r', alpha=1, ls='--',
                #              marker='o', markeredgecolor='none', ms=8, zorder=99)
                # self.fig.canvas.draw()
                # self.fig.canvas.flush_events()

            # print("{}    {} total values filled    this: {} with data between  {}  and  {}".format(
            #     qual_id, len(gaps_df[self.flux_f_col].dropna()), ix, start_dt, end_dt))

        self.post_fill(gaps_df=gaps_df, ax=ax, qual_id=qual_id, tic=time_start, gap_count_pre=gap_count_pre)

        return gaps_df

    def calc_lut_fill_value_from_similar(self, df, available_dict, row):
        """Find similar conditions in df and calculate flux mean.

        :param df:
        :param available_dict:
        :param row:
        :return:
        """
        _df = df.copy()
        # _df = _df.dropna()
        filter_nan = _df[self.flux_data_col].isnull()  # True if NaN
        _df = _df.loc[~filter_nan]  # Keep False (=values, i.e. not NaN)

        for var, limits in available_dict.items():
            mask = (_df[var] <= row[limits[0]]) & \
                   (_df[var] >= row[limits[1]])
            _df = _df[mask]
            # print("Narrowing down data ... remaining values after {}: {}".format(crit_col[0], len(df)))
        if not _df.empty:
            fill_val = _df[self.flux_data_col].mean()
            fill_val_lut_sd = _df[self.flux_data_col].std()
        else:
            fill_val = fill_val_lut_sd = False

        # print("Fill value: {} / {} / {}".format(fill_val, fill_val_p25, fill_val_p75))
        return fill_val, fill_val_lut_sd

    def plot_lut_df_with_gaps(self, df, ax, plot_col, title_txt):
        ax.clear()
        ax.plot_date(x=df.index, y=df[plot_col], color='#78909C', alpha=0.5, ls='-',
                     marker='o', markeredgecolor='none', ms=6, zorder=98, label='measured')
        gui.plotfuncs.default_legend(ax=ax, loc='upper right', bbox_to_anchor=(1, 1))
        if title_txt:
            ax.text(0.01, 0.97, title_txt,
                    size=FONTSIZE_HEADER_AXIS, color=FONTCOLOR_HEADER_AXIS,
                    backgroundcolor='none', transform=ax.transAxes, alpha=1,
                    horizontalalignment='left', verticalalignment='top')
        self.fig.canvas.draw()
        self.fig.canvas.flush_events()
        return None

    def setup_progress_bar(self, df, col_with_gaps):
        # Progress bar
        _, gap_count_on_init = pkgs.dfun.frames.find_nans_in_df_col(df=df, col=col_with_gaps)
        self.progress_bar.setMaximum(gap_count_on_init)
        self.progress_bar.setValue(gap_count_on_init)
        self.progress_bar.setFormat('{} gaps found'.format(gap_count_on_init))
        return gap_count_on_init

    def get_settings_from_fields(self):
        self.flux_data_col = self.drp_flux.currentText()
        self.ta_data_col = self.drp_ta.currentText()
        self.swin_data_col = self.drp_swin.currentText()
        self.vpd_data_col = self.drp_vpd.currentText()
        flux_pretty_ix = self.col_list_pretty.index(self.flux_data_col)
        swin_pretty_ix = self.col_list_pretty.index(self.swin_data_col)
        ta_pretty_ix = self.col_list_pretty.index(self.ta_data_col)
        vpd_pretty_ix = self.col_list_pretty.index(self.vpd_data_col)
        self.flux_data_col = self.col_dict_tuples[flux_pretty_ix]
        self.swin_data_col = self.col_dict_tuples[swin_pretty_ix]
        self.ta_data_col = self.col_dict_tuples[ta_pretty_ix]
        self.vpd_data_col = self.col_dict_tuples[vpd_pretty_ix]

        self.swin_sim_lim = float(self.lne_swin_sim.text())
        self.ta_sim_lim = float(self.lne_ta_sim.text())
        self.vpd_sim_lim = float(self.lne_vpd_sim.text())
        self.time_win_days = int(self.lne_time_win_days.text())
        self.time_win_hours = int(self.lne_time_win_hours.text())

    def make_mds_df(self):
        """ Assembles the df that contains all the needed data """
        mds_df = self.tab_data_df[[self.flux_data_col, self.swin_data_col, self.ta_data_col, self.vpd_data_col]].copy()
        self.set_colnames()
        mds_df = self.init_aux_cols(df=mds_df)
        mds_df = self.make_aux_cols(df=mds_df)
        return mds_df

    def set_colnames(self):
        suffix = '+gfLUT'
        self.flux_f_col = (self.flux_data_col[0] + suffix, self.flux_data_col[1])  # col that is filled
        self.flux_f_lutmdc_sd_col = \
            (self.flux_data_col[0] + suffix + '_lut_sd', self.flux_data_col[1])  # col that is filled
        self.flux_fqual_str_col = (self.flux_data_col[0] + suffix + 'qual', self.flux_data_col[1])  # fill quality
        self.swin_sim_upperlim_col = (self.swin_data_col[0] + suffix + '_sim_upper_lim', self.swin_data_col[1])
        self.swin_sim_lowerlim_col = (self.swin_data_col[0] + suffix + '_sim_lower_lim', self.swin_data_col[1])
        self.ta_sim_upperlim_col = (self.ta_data_col[0] + suffix + '_sim_upper_lim', self.ta_data_col[1])
        self.ta_sim_lowerlim_col = (self.ta_data_col[0] + suffix + '_sim_lower_lim', self.ta_data_col[1])
        self.vpd_sim_upperlim_col = (self.vpd_data_col[0] + suffix + '_sim_upper_lim', self.vpd_data_col[1])
        self.vpd_sim_lowerlim_col = (self.vpd_data_col[0] + suffix + '_sim_lower_lim', self.vpd_data_col[1])

    def init_aux_cols(self, df):
        _df = df.copy()
        _df[self.flux_f_col] = _df[self.flux_data_col]
        _df[self.flux_f_lutmdc_sd_col] = np.nan
        _df[self.flux_fqual_str_col] = 'M'  # default 0 means measured, also the gaps
        _df[self.swin_sim_upperlim_col] = np.nan
        _df[self.swin_sim_lowerlim_col] = np.nan
        _df[self.ta_sim_upperlim_col] = np.nan
        _df[self.ta_sim_lowerlim_col] = np.nan
        _df[self.vpd_sim_upperlim_col] = np.nan
        _df[self.vpd_sim_lowerlim_col] = np.nan
        return _df

    def make_aux_cols(self, df):
        _df = df.copy()
        _df[self.flux_f_col] = _df[self.flux_data_col]
        _df[self.flux_f_lutmdc_sd_col] = np.nan
        _df[self.flux_fqual_str_col] = 'M'  # default 0 means measured, also the gaps
        _df[self.swin_sim_upperlim_col] = _df[self.swin_data_col] + self.swin_sim_lim
        _df[self.swin_sim_lowerlim_col] = _df[self.swin_data_col] - self.swin_sim_lim
        _df[self.ta_sim_upperlim_col] = _df[self.ta_data_col] + self.ta_sim_lim
        _df[self.ta_sim_lowerlim_col] = _df[self.ta_data_col] - self.ta_sim_lim
        _df[self.vpd_sim_upperlim_col] = _df[self.vpd_data_col] + self.vpd_sim_lim
        _df[self.vpd_sim_lowerlim_col] = _df[self.vpd_data_col] - self.vpd_sim_lim
        return _df

    def make_axes_dict(self):
        gs = gridspec.GridSpec(3, 1)  # rows, cols
        gs.update(wspace=0.2, hspace=0.2, left=0.03, right=0.97, top=0.97, bottom=0.03)
        ax_ts = self.fig.add_subplot(gs[0, 0])
        ax_ts_filled_colors = self.fig.add_subplot(gs[1, 0], sharex=ax_ts, sharey=ax_ts)
        ax_ts_filled = self.fig.add_subplot(gs[2, 0], sharex=ax_ts, sharey=ax_ts)
        axes_dict = {'ax_ts': ax_ts,
                     'ax_ts_filled_colors': ax_ts_filled_colors,
                     'ax_ts_filled': ax_ts_filled}
        for key, ax in axes_dict.items():
            gui.plotfuncs.default_format(ax=ax, txt_xlabel=False)
        return axes_dict

    def update_refinements(self):
        self.drp_flux.clear()
        self.drp_ta.clear()
        self.drp_vpd.clear()

        default_swin_ix = default_ta_ix = default_vpd_ix = default_flux_ix = 0
        default_swin_ix_found = default_ta_ix_found = default_vpd_ix_found = default_flux_ix_found = False

        # Add all variables to drop-down lists to allow full flexibility yay
        for ix, colname_tuple in enumerate(self.tab_data_df.columns):
            # print(colname_tuple)
            self.drp_swin.addItem(self.col_list_pretty[ix])
            self.drp_ta.addItem(self.col_list_pretty[ix])
            self.drp_vpd.addItem(self.col_list_pretty[ix])
            self.drp_flux.addItem(self.col_list_pretty[ix])

            # Check if column appears in the global vars

            if any(fnmatch.fnmatch(colname_tuple[0], vid) for vid in SHORTWAVE_IN):
                default_swin_ix = ix if not default_swin_ix_found else default_swin_ix
                default_swin_ix_found = True

            elif any(fnmatch.fnmatchcase(colname_tuple[0], vid) for vid in AIR_TEMPERATURE_METEO):
                default_ta_ix = ix if not default_ta_ix_found else default_ta_ix
                default_ta_ix_found = True

            elif any(fnmatch.fnmatch(colname_tuple[0], vid) for vid in VPD):
                default_vpd_ix = ix if not default_vpd_ix_found else default_vpd_ix
                default_vpd_ix_found = True

            elif any(fnmatch.fnmatch(colname_tuple[0], vid) for vid in FLUXES_GENERAL_CO2):
                default_flux_ix = ix if not default_flux_ix_found else default_flux_ix
                default_flux_ix_found = True

        # Set dropdowns to found ix
        self.drp_swin.setCurrentIndex(default_swin_ix)
        self.drp_ta.setCurrentIndex(default_ta_ix)
        self.drp_vpd.setCurrentIndex(default_vpd_ix)
        self.drp_flux.setCurrentIndex(default_flux_ix)

        # Default values for the text fields
        self.lne_swin_sim.setText('50')  # W m-2
        self.lne_ta_sim.setText('2.5')  # C°
        self.lne_vpd_sim.setText('500')  # 5 hPa is default for reference, EddyPro output gives Pa
        self.lne_time_win_days.setText('7')
        self.lne_time_win_hours.setText('1')

        # Buttons
        self.btn_start_qual_a.setDisabled(True)
        self.btn_start_qual_b.setDisabled(True)
        self.btn_start_qual_c.setDisabled(True)
        self.btn_calc_uncertainty.setDisabled(True)
        self.btn_add_as_new_var.setDisabled(True)

    def update_dynamic_fields(self):
        self.get_settings_from_fields()

        c, bins = pd.cut(self.tab_data_df[self.swin_data_col], bins=20, retbins=True)
        sim = float(bins[2] - bins[1])
        self.lne_swin_sim.setText(str(sim))

        c, bins = pd.cut(self.tab_data_df[self.ta_data_col], bins=20, retbins=True)
        sim = float(bins[2] - bins[1])
        self.lne_ta_sim.setText(str(sim))

        c, bins = pd.cut(self.tab_data_df[self.vpd_data_col], bins=4000, retbins=True)
        sim = float(bins[2] - bins[1])
        self.lne_vpd_sim.setText(str(sim))

    def get_filled(self, main_df):
        """
        Filled timeseries back to data_df

        The quality flag is converted from strings (A1, A2, ...) to numbers,
        then they can be directly plotted.

        """

        # Make subset of columns that are then put into data_df
        data_cols_for_data_df = self.mds_df[[self.flux_f_col, self.flux_fqual_str_col]]
        _data_cols_for_data_df = data_cols_for_data_df.copy()

        # Fill-quality strings are replaced by integers
        fqual_dict = {'M': 0, 'A': 1, 'B': 2, 'C': 3}
        for str, int in fqual_dict.items():
            _filter = data_cols_for_data_df[self.flux_fqual_str_col].str.startswith(str, na=False)
            _data_cols_for_data_df.loc[_filter, self.flux_fqual_str_col] = int

        # Add gap-filled data to main data_df
        main_df[self.flux_f_col] = np.nan
        main_df[self.flux_fqual_str_col] = np.nan
        main_df = main_df.combine_first(_data_cols_for_data_df)

        # Show gap-filled time series in plot
        self.plot_lut_df_with_gaps(df=main_df, ax=self.axes_dict['ax_ts_filled'], plot_col=self.flux_f_col,
                                   title_txt=f"Gap-filled Timeseries {self.flux_f_col[0]}")

        return main_df


class RunMDS:
    current_color_ix = -1
    current_marker_ix = -1
    plot_color_list = colors_6()
    plot_marker_list = generate_plot_marker_list()

    def __init__(self, mds_tab):

        # General
        self.data_df = mds_tab.tab_data_df  # Instance data as basis for gap-filling
        self.col_dict_tuples = mds_tab.col_dict_tuples
        self.col_list_pretty = mds_tab.col_list_pretty

        # Plot
        self.axes_dict = mds_tab.axes_dict
        self.fig = mds_tab.fig
        self.canvas = mds_tab.canvas

        # Measured variables
        self.flux_data_col = mds_tab.drp_flux.currentText()
        self.ta_data_col = mds_tab.drp_ta.currentText()
        self.swin_data_col = mds_tab.drp_swin.currentText()
        self.vpd_data_col = mds_tab.drp_vpd.currentText()

        # Similarity limits
        self.swin_sim_lim = float(mds_tab.lne_swin_sim.text())
        self.ta_sim_lim = float(mds_tab.lne_ta_sim.text())
        self.vpd_sim_lim = float(mds_tab.lne_vpd_sim.text())

        # Time windows
        self.time_win_days = int(mds_tab.lne_time_win_days.text())
        self.time_win_hours = int(mds_tab.lne_time_win_hours.text())

        # Buttons
        # For progress update of button text
        self.btn_qual_A = mds_tab.btn_start_qual_A
        self.btn_qual_B = mds_tab.btn_start_qual_B
        self.btn_qual_C = mds_tab.btn_start_qual_C
        self.btn_calc_unc = mds_tab.btn_calc_uncertainty
        self.button_orig_text_qual_A = self.btn_qual_A.text()
        self.enable_buttons()

        # Progress bar
        self.progress_bar = mds_tab.progress_bar

        # Prepare data
        self.mds_df = self.make_mds_df()  # gaps will be filled in this df
        self.lut_df = self.mds_df.copy()  # for LUT data, stays the same
        self.gaps_df = self.mds_df.copy()  # for detecting gaps, will get shorter as gaps are filled

        # Show selected flux
        self.gap_count_on_init = self.setup_progress_bar(df=self.lut_df, col_with_gaps=self.flux_data_col)

        self.plot_lut_df_with_gaps(df=self.lut_df, ax=self.axes_dict['ax_ts'], plot_col=self.flux_data_col,
                                   title_txt='Measured Values\n{} ({} values missing)'.format(self.flux_data_col[0],
                                                                                              self.gap_count_on_init))

        self.plot_lut_df_with_gaps(df=self.lut_df, ax=self.axes_dict['ax_ts_filled_colors'],
                                   plot_col=self.flux_data_col, title_txt='Gap-filling Quality')

        self.plot_lut_df_with_gaps(df=self.lut_df, ax=self.axes_dict['ax_ts_filled'], plot_col=self.flux_f_col,
                                   title_txt=f"Gap-filled Timeseries {self.flux_f_col[0]}")

    # def calculate_uncertainty(self):
    #     """Create random gaps in time series and then gap-fill them.
    #
    #
    #     """
    #     unc_mds_df = self.mds_df.copy()
    #     unc_lut_df = unc_mds_df.copy()
    #     unc_gaps_df = unc_mds_df.copy()
    #
    #     # Make random gaps
    #     randgaps_col = (f"{self.flux_data_col[0]}_randgaps", self.flux_data_col[1])
    #     unc_mds_df[randgaps_col] = np.nan
    #     nan_mat = np.random.random(unc_mds_df[self.flux_data_col].size) < 0.1
    #     unc_mds_df[randgaps_col] = unc_mds_df[self.flux_data_col].mask(nan_mat)
    #     print("X")

    def lut_looper(self, required_dict, win_multiplier_list, qual_ids_list):
        """Expand LUT window automatically."""
        loops = len(qual_ids_list)
        for loop in range(0, loops):
            self.set_marker_style()

            self.gaps_df = self.lut(available_dict=required_dict,
                                    ax=self.axes_dict['ax_ts_filled_colors'],
                                    time_win_days=self.time_win_days * win_multiplier_list[loop],
                                    time_win_hours=self.time_win_hours,
                                    qual_id=qual_ids_list[loop],
                                    fill_gaps_in_df=self.gaps_df)

    def set_marker_style(self):
        self.current_color_ix, self.current_marker_ix = \
            gui.plotfuncs.set_marker_color(current_color_ix=self.current_color_ix,
                                           plot_color_list=self.plot_color_list,
                                           current_marker_ix=self.current_marker_ix,
                                           plot_marker_list=self.plot_marker_list)

    def mdc_looper(self, time_win_days, qual_ids_list):
        """Expand MDC window automatically."""
        loops = len(qual_ids_list)
        for loop in range(0, loops):
            self.set_marker_style()
            self.gaps_df = self.mdc(fill_gaps_in_df=self.gaps_df, time_win_days=time_win_days[loop],
                                    time_win_hours=self.time_win_hours, qual_id=qual_ids_list[loop],
                                    ax=self.axes_dict['ax_ts_filled_colors'])

    def gapfilling_quality_A(self):
        """Quality A1 - A4."""
        logger.log(name='>>> Gap-filling Quality A', dict={}, highlight=True)  # Log info

        # SWIN, TA, VPD, NEE available within 7, 14 days
        # Quality A1, A2
        multiplier_list = range(1, 3, 1)  # 1 x 7 days = 7 days
        qual_ids_list = ['A - LUT3 within {} days'.format(x * self.time_win_days) for x in multiplier_list]
        self.lut_looper(required_dict=self.required(which='SWIN, TA, VPD'),
                        win_multiplier_list=multiplier_list, qual_ids_list=qual_ids_list)

        # SWIN, NEE available within 7 days
        # Quality A3
        multiplier_list = [1]  # 1 x 7 days = 7 days
        qual_ids_list = ['A - LUT1 within {} days'.format(x * self.time_win_days) for x in multiplier_list]
        self.lut_looper(required_dict=self.required(which='SWIN'), win_multiplier_list=multiplier_list,
                        qual_ids_list=qual_ids_list)

        # NEE available within |dt| <= 1h on same day
        # Quality A4
        multiplier_list = [0]  # 0 x 7 days = 0 days (on same day)
        qual_ids_list = ['A - MDC within {} days'.format(x * self.time_win_days) for x in multiplier_list]
        self.mdc_looper(time_win_days=multiplier_list, qual_ids_list=qual_ids_list)

    def get_filled(self, main_df):
        """
        Filled timeseries back to data_df

        The quality flag is converted from strings (A1, A2, ...) to numbers,
        then they can be directly plotted.

        """

        # Make subset of columns that are then put into data_df
        data_cols_for_data_df = self.mds_df[[self.flux_f_col, self.flux_fqual_str_col]]
        _data_cols_for_data_df = data_cols_for_data_df.copy()

        # Fill-quality strings are replaced by integers
        fqual_dict = {'M': 0, 'A': 1, 'B': 2, 'C': 3}
        for str, int in fqual_dict.items():
            _filter = data_cols_for_data_df[self.flux_fqual_str_col].str.startswith(str, na=False)
            _data_cols_for_data_df.loc[_filter, self.flux_fqual_str_col] = int

        # Add gap-filled data to main data_df
        main_df[self.flux_f_col] = np.nan
        main_df[self.flux_fqual_str_col] = np.nan
        main_df = main_df.combine_first(_data_cols_for_data_df)

        # Show gap-filled time series in plot
        self.plot_lut_df_with_gaps(df=main_df, ax=self.axes_dict['ax_ts_filled'], plot_col=self.flux_f_col,
                                   title_txt=f"Gap-filled Timeseries {self.flux_f_col[0]}")

        return main_df

    def get_gaps_df(self):
        return self.gaps_df

    def gapfilling_quality_B(self):

        logger.log(name='>>> Gap-filling Quality B', dict={}, highlight=True)  # Log info

        # NEE available within |dt| <= 1h within 1, 2 days
        # Quality B1, B2
        multiplier_list = [1, 2]  # 1 x 7 days = 7 days
        qual_ids_list = ['B - MDC within {} days'.format(x * self.time_win_days) for x in multiplier_list]
        self.mdc_looper(time_win_days=multiplier_list, qual_ids_list=qual_ids_list)

        # SWIN, TA, VPD, NEE available within 21, 28 days
        # Quality B3, B4
        multiplier_list = [3, 4]  # 3 x 7 days = 21 days
        qual_ids_list = ['B - LUT3 within {} days'.format(x * self.time_win_days) for x in multiplier_list]
        self.lut_looper(required_dict=self.required(which='SWIN, TA, VPD'), win_multiplier_list=multiplier_list,
                        qual_ids_list=qual_ids_list)

        # SWIN, NEE available within 14 days
        # Quality B3, B4
        multiplier_list = [2]  # 2 x 7 days = 14 days
        qual_ids_list = ['B - LUT1 within {} days'.format(x * self.time_win_days) for x in multiplier_list]
        self.lut_looper(required_dict=self.required(which='SWIN'), win_multiplier_list=multiplier_list,
                        qual_ids_list=qual_ids_list)

    def gapfilling_quality_C(self):

        logger.log(name='>>> Gap-filling Quality C', dict={}, highlight=True)  # Log info

        # SWIN, TA, VPD, NEE available within 35-140 days
        # Quality C
        multiplier_list = range(5, 21, 1)  # 5 x 7 days = 35 days
        qual_ids_list = ['C - LUT3 within {} days'.format(x * self.time_win_days) for x in multiplier_list]
        self.lut_looper(required_dict=self.required(which='SWIN, TA, VPD'), win_multiplier_list=multiplier_list,
                        qual_ids_list=qual_ids_list)

        # SWIN, NEE available within 21-140 days
        # Quality C
        multiplier_list = range(3, 21, 1)  # 3 x 7 days = 21 days
        qual_ids_list = ['C - LUT1 within {} days'.format(x * self.time_win_days) for x in multiplier_list]
        self.lut_looper(required_dict=self.required(which='SWIN'), win_multiplier_list=multiplier_list,
                        qual_ids_list=qual_ids_list)

    def lut(self, available_dict, qual_id, time_win_days, time_win_hours, ax, fill_gaps_in_df):

        time_start = time.time()

        # Check where the data gaps for flux_col are
        gaps_df, gap_count_pre = pkgs.dfun.frames.find_nans_in_df_col(df=fill_gaps_in_df,
                                                                      col=self.flux_f_col)  # data w/ flux missing
        # No need to gap-fill if there are no gaps
        if gaps_df.empty:
            return gaps_df

        gaps_df, win_start_col, win_end_col = pkgs.dfun.frames.insert_datetimerange(df=gaps_df,
                                                                                    win_days=time_win_days,
                                                                                    win_hours=time_win_hours)

        # Fill data gaps in gaps_df row-by-row, using info from daterange and the lut_df
        counter = 0
        for ix, row in gaps_df.iterrows():
            counter += 1
            self.update_progress_bar(df=gaps_df, col=self.flux_f_col)
            # if counter % 10 == 0:
            #     self.update_progress_bar(df=gaps_df, col=self.flux_f_col)

            start_dt = row[win_start_col]
            end_dt = row[win_end_col]

            lut_df_snippet = pkgs.dfun.frames.df_between_two_dates(df=self.lut_df, dropna=True,
                                                                   dropna_col=self.flux_data_col,
                                                                   start_date=start_dt, end_date=end_dt)

            fill_val, fill_val_lut_sd = self.calc_lut_fill_value_from_similar(df=lut_df_snippet,
                                                                              available_dict=available_dict,
                                                                              row=row)

            if fill_val:
                # Insert fill value in gaps_df
                gaps_df.loc[ix, self.flux_f_col] = fill_val
                gaps_df.loc[ix, self.flux_f_lutmdc_sd_col] = fill_val_lut_sd
                gaps_df.loc[ix, self.flux_fqual_str_col] = qual_id

                # Filled values are collected in mds_df
                self.mds_df.loc[ix, self.flux_f_col] = fill_val
                self.mds_df.loc[ix, self.flux_f_lutmdc_sd_col] = fill_val_lut_sd
                self.mds_df.loc[ix, self.flux_fqual_str_col] = qual_id

                # # Show single fill value in plot (slooooow)
                # ax.plot_date(x=ix, y=fill_val, color='r', alpha=1, ls='--',
                #              marker='o', markeredgecolor='none', ms=8, zorder=99)
                # self.fig.canvas.draw()
                # self.fig.canvas.flush_events()

            # print("{}    {} total values filled    this: {} with data between  {}  and  {}".format(
            #     qual_id, len(gaps_df[self.flux_f_col].dropna()), ix, start_dt, end_dt))

        self.post_fill(gaps_df=gaps_df, ax=ax, qual_id=qual_id, tic=time_start, gap_count_pre=gap_count_pre)

        return gaps_df

    def mdc(self, fill_gaps_in_df, time_win_days, time_win_hours, qual_id, ax):

        time_start = time.time()

        # Check where the data gaps for flux_col are
        gaps_df, gap_count_pre = pkgs.dfun.frames.find_nans_in_df_col(df=fill_gaps_in_df,
                                                                      col=self.flux_f_col)  # data w/ flux missing
        # No need to gap-fill if there are no gaps
        if gaps_df.empty:
            return gaps_df

        gaps_df, win_start_dt_col, win_end_dt_col = pkgs.dfun.frames.insert_datetimerange(df=gaps_df,
                                                                                          win_days=time_win_days,
                                                                                          win_hours=time_win_hours)

        gaps_df, win_start_time_col, win_end_time_col = pkgs.dfun.frames.insert_timerange(df=gaps_df,
                                                                                          win_hours=time_win_hours)

        counter = 0
        for ix, row in gaps_df.iterrows():
            counter += 1
            self.update_progress_bar(df=gaps_df, col=self.flux_f_col)

            # Keep only datetime window
            start_dt = row[win_start_dt_col]
            end_dt = row[win_end_dt_col]
            mask = (self.lut_df.index >= start_dt) & (self.lut_df.index <= end_dt)
            lut_df_snippet = self.lut_df.loc[mask]

            # Keep only time (hours) window
            start_time = row[win_start_time_col]
            end_time = row[win_end_time_col]
            mask = (lut_df_snippet.index.time >= start_time) & (lut_df_snippet.index.time <= end_time)
            lut_df_snippet = lut_df_snippet.loc[mask]

            available_flux_values = lut_df_snippet[self.flux_data_col].count()
            fill_val = lut_df_snippet[self.flux_data_col].mean() if available_flux_values >= 2 else np.nan

            if fill_val:
                fill_val_mdc_sd = lut_df_snippet[self.flux_data_col].std_col()

                gaps_df.loc[ix, self.flux_f_col] = fill_val  # insert found fill val in df
                gaps_df.loc[ix, self.flux_f_lutmdc_sd_col] = fill_val_mdc_sd  # insert found fill val in df
                gaps_df.loc[ix, self.flux_fqual_str_col] = qual_id

                # Filled values are collected in mds_df
                self.mds_df.loc[ix, self.flux_f_col] = fill_val
                self.mds_df.loc[ix, self.flux_f_lutmdc_sd_col] = fill_val_mdc_sd
                self.mds_df.loc[ix, self.flux_fqual_str_col] = qual_id

        self.post_fill(gaps_df=gaps_df, ax=ax, qual_id=qual_id, tic=time_start, gap_count_pre=gap_count_pre)

        return gaps_df

    def post_fill(self, gaps_df, ax, qual_id, tic, gap_count_pre):
        """Plot filled values and output stats to LogBox."""
        gap_count_post = self.update_progress_bar(df=gaps_df, col=self.flux_f_col)

        self.plot_gaps_df_filled_values(df=gaps_df, ax=ax, filled_color=self.plot_color_list[self.current_color_ix],
                                        plot_marker=self.plot_marker_list[self.current_marker_ix],
                                        qual_id=qual_id, tail=-9999)

        gaps_filled = gap_count_pre - gap_count_post
        gaps_filled_perc = (gaps_filled / gap_count_pre) * 100
        logger.log(name=qual_id, dict={'gaps before': gap_count_pre,
                                       'gaps after': gap_count_post,
                                       'gaps filled': f"{gaps_filled} ({gaps_filled_perc:.1f}%)",
                                       'step runtime': f"{time.time() - tic:.3f}s"},
                   highlight=False)  # Log info

    def required(self, which):
        if which == 'SWIN, TA, VPD':
            available_dict = {self.swin_data_col: (self.swin_sim_upperlim_col, self.swin_sim_lowerlim_col),
                              self.ta_data_col: (self.ta_sim_upperlim_col, self.ta_sim_lowerlim_col),
                              self.vpd_data_col: (self.vpd_sim_upperlim_col, self.vpd_sim_lowerlim_col)}
        elif which == 'SWIN':
            available_dict = {self.swin_data_col: (self.swin_sim_upperlim_col, self.swin_sim_lowerlim_col)}

        else:
            available_dict = -9999

        return available_dict

    def calc_lut_fill_value_from_similar(self, df, available_dict, row):
        """Find similar conditions in df and calculate flux mean.

        :param df:
        :param available_dict:
        :param row:
        :return:
        """
        _df = df.copy()
        # _df = _df.dropna()
        filter_nan = _df[self.flux_data_col].isnull()  # True if NaN
        _df = _df.loc[~filter_nan]  # Keep False (=values, i.e. not NaN)

        for var, limits in available_dict.items():
            mask = (_df[var] <= row[limits[0]]) & \
                   (_df[var] >= row[limits[1]])
            _df = _df[mask]
            # print("Narrowing down data ... remaining values after {}: {}".format(crit_col[0], len(df)))
        if not _df.empty:
            fill_val = _df[self.flux_data_col].mean()
            fill_val_lut_sd = _df[self.flux_data_col].std_col()
        else:
            fill_val = fill_val_lut_sd = False

        # print("Fill value: {} / {} / {}".format(fill_val, fill_val_p25, fill_val_p75))
        return fill_val, fill_val_lut_sd

    def make_aux_cols(self, df):
        _df = df.copy()

        suffix = '+gfMDS'

        self.flux_f_col = (self.flux_data_col[0] + suffix, self.flux_data_col[1])  # col that is filled
        self.flux_f_lutmdc_sd_col = \
            (self.flux_data_col[0] + suffix + '_lut_sd', self.flux_data_col[1])  # col that is filled
        self.flux_fqual_str_col = (self.flux_data_col[0] + suffix + 'qual', self.flux_data_col[1])  # fill quality
        self.swin_sim_upperlim_col = (self.swin_data_col[0] + suffix + '_sim_upper_lim', self.swin_data_col[1])
        self.swin_sim_lowerlim_col = (self.swin_data_col[0] + suffix + '_sim_lower_lim', self.swin_data_col[1])
        self.ta_sim_upperlim_col = (self.ta_data_col[0] + suffix + '_sim_upper_lim', self.ta_data_col[1])
        self.ta_sim_lowerlim_col = (self.ta_data_col[0] + suffix + '_sim_lower_lim', self.ta_data_col[1])
        self.vpd_sim_upperlim_col = (self.vpd_data_col[0] + suffix + '_sim_upper_lim', self.vpd_data_col[1])
        self.vpd_sim_lowerlim_col = (self.vpd_data_col[0] + suffix + '_sim_lower_lim', self.vpd_data_col[1])

        _df[self.flux_f_col] = _df[self.flux_data_col]
        _df[self.flux_f_lutmdc_sd_col] = np.nan
        _df[self.flux_fqual_str_col] = 'M'  # default 0 means measured, also the gaps
        _df[self.swin_sim_upperlim_col] = _df[self.swin_data_col] + self.swin_sim_lim
        _df[self.swin_sim_lowerlim_col] = _df[self.swin_data_col] - self.swin_sim_lim
        _df[self.ta_sim_upperlim_col] = _df[self.ta_data_col] + self.ta_sim_lim
        _df[self.ta_sim_lowerlim_col] = _df[self.ta_data_col] - self.ta_sim_lim
        _df[self.vpd_sim_upperlim_col] = _df[self.vpd_data_col] + self.vpd_sim_lim
        _df[self.vpd_sim_lowerlim_col] = _df[self.vpd_data_col] - self.vpd_sim_lim

        return _df

    def make_mds_df(self):
        """ Assembles the df that contains all the needed data """
        print('[{}]'.format(self.make_mds_df.__name__))
        flux_pretty_ix = self.col_list_pretty.index(self.flux_data_col)
        swin_pretty_ix = self.col_list_pretty.index(self.swin_data_col)
        ta_pretty_ix = self.col_list_pretty.index(self.ta_data_col)
        vpd_pretty_ix = self.col_list_pretty.index(self.vpd_data_col)
        self.flux_data_col = self.col_dict_tuples[flux_pretty_ix]
        self.swin_data_col = self.col_dict_tuples[swin_pretty_ix]
        self.ta_data_col = self.col_dict_tuples[ta_pretty_ix]
        self.vpd_data_col = self.col_dict_tuples[vpd_pretty_ix]
        mds_df = self.data_df[[self.flux_data_col, self.swin_data_col, self.ta_data_col, self.vpd_data_col]]
        mds_df = self.make_aux_cols(df=mds_df)
        return mds_df

    def plot_lut_df_with_gaps(self, df, ax, plot_col, title_txt):
        ax.clear()
        ax.plot_date(x=df.index, y=df[plot_col], color='#78909C', alpha=0.5, ls='-',
                     marker='o', markeredgecolor='none', ms=6, zorder=98, label='measured')
        gui.plotfuncs.default_legend(ax=ax, loc='upper right', bbox_to_anchor=(1, 1))
        if title_txt:
            ax.text(0.01, 0.97, title_txt,
                    size=FONTSIZE_HEADER_AXIS, color=FONTCOLOR_HEADER_AXIS,
                    backgroundcolor='none', transform=ax.transAxes, alpha=1,
                    horizontalalignment='left', verticalalignment='top')
        self.fig.canvas.draw()
        self.fig.canvas.flush_events()
        return None

    def plot_gaps_df_filled_values(self, df, ax, filled_color, qual_id, tail, plot_marker):
        # ax.get_legend().remove()
        if tail > 0:
            _df = df[[self.flux_f_col, self.flux_fqual_str_col]].dropna().tail(tail)
        else:
            _df = df
        filter = _df[self.flux_fqual_str_col] == qual_id
        _df = _df[filter]

        ax.plot_date(x=df.index, y=df[self.flux_f_col], color=filled_color, alpha=1, ls='none',
                     marker=plot_marker, markeredgecolor='none', ms=6, zorder=99, label=qual_id)

        ax.errorbar(df.index, df[self.flux_f_col],
                    yerr=df[self.flux_f_lutmdc_sd_col],
                    alpha=0.1, capsize=0, ls='none', color=filled_color, elinewidth=8,
                    label="SD during similar conditions", zorder=96)

        # ax.plot_date(x=df.index, y=df[self.flux_f_lutmdc_sd_col], color=filled_color, alpha=1, ls='none',
        #              marker=plot_marker, markeredgecolor='none', ms=8, zorder=99, label=qual_id)

        # ax.plot_date(x=df.index, y=df[self.flux_f_p75_col], color=filled_color, alpha=1, ls='none',
        #              marker=plot_marker, markeredgecolor='none', ms=8, zorder=99, label=qual_id)

        gui.plotfuncs.default_legend(ax=ax, loc='upper right', bbox_to_anchor=(1, 1))
        self.fig.canvas.draw()
        self.fig.canvas.flush_events()

    def setup_progress_bar(self, df, col_with_gaps):
        # Progress bar
        _, gap_count_on_init = pkgs.dfun.frames.find_nans_in_df_col(df=df, col=col_with_gaps)
        self.progress_bar.setMaximum(gap_count_on_init)
        self.progress_bar.setValue(gap_count_on_init)
        self.progress_bar.setFormat('{} gaps found'.format(gap_count_on_init))
        return gap_count_on_init

    def update_progress_bar(self, df, col):
        _, gap_count_progress = pkgs.dfun.frames.find_nans_in_df_col(df=df, col=col)
        time.sleep(0.00000001)
        self.progress_bar.setValue(gap_count_progress)
        self.progress_bar.setFormat('{} gaps remaining'.format(gap_count_progress))
        return gap_count_progress

    def enable_buttons(self):
        self.btn_qual_A.setEnabled(True)
        self.btn_qual_B.setEnabled(True)
        self.btn_qual_C.setEnabled(True)
        # self.btn_calc_unc.setEnabled(True)  # TODO after implementation
