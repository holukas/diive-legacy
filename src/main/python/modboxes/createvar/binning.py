# -*- coding: utf-8 -*-
import matplotlib.dates as mdates
import matplotlib.gridspec as gridspec
import numpy as np
import pandas as pd
from PyQt5 import QtGui
from PyQt5.QtGui import QIntValidator

import gui
import gui.add_to_layout
import gui.base
import gui.make
import logger
import modboxes.plots.styles.LightTheme as theme
from pkgs.dfun.frames import export_to_main, move_col_to_pos

from help.tooltips import cv_binning as tooltips


class addContent(gui.base.buildTab):
    """Build new tab and populate with contents"""

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id, tab_template='SVPwT')

        # Tab icon
        self.TabWidget.setTabIcon(self.tab_ix, QtGui.QIcon(app_obj.ctx.tab_icon_create_variable))

        # Add settings menu contents
        self.lne_numbins, self.btn_calc, self.btn_add_as_new_var = \
            self.add_settings_fields()

        # # Add variables required in settings
        # self.populate_settings_fields()

        # Add available variables to variable lists
        gui.base.buildTab.populate_variable_list(obj=self)

        # # Add plots
        # self.populate_plot_area()

    # def populate_plot_area(self):
    #     self.axes_dict = self.add_axes()

    # def populate_settings_fields(self):
    #     pass

    # def add_axes(self):
    #     pass

    def add_settings_fields(self):
        gui.elements.add_header_subheader_to_grid_top(layout=self.sett_layout,
                                                      txt=["Binning",
                                                           "Create Variable"])
        gui.add_to_layout.add_spacer_item_to_grid(row=1, col=0, layout=self.sett_layout)

        gui.elements.add_header_in_grid_row(row=2, layout=self.sett_layout, txt="Bins")

        # Units
        lne_numbins = gui.elements.add_label_linedit_info_triplet_to_grid(
            txt='Number Of Bins', css_ids=['', 'cyan'], layout=self.sett_layout, orientation='horiz',
            row=3, col=0, txt_info_hover=tooltips.lne_numbins)
        lne_numbins.setText("10")
        onlyInt = QIntValidator()
        lne_numbins.setValidator(onlyInt)

        # Apply button
        btn_calc = gui.elements.add_button_to_grid(grid_layout=self.sett_layout, txt='Calculate Bins', css_id='',
                                                   row=4, col=0, rowspan=1, colspan=2)
        btn_add_as_new_var = gui.elements.add_button_to_grid(grid_layout=self.sett_layout,
                                                             txt='+ Add As New Var', css_id='btn_add_as_new_var',
                                                             row=5, col=0, rowspan=1, colspan=2)

        self.sett_layout.setRowStretch(6, 1)
        return lne_numbins, btn_calc, btn_add_as_new_var


class Run(addContent):
    converted_exists = False
    sub_outdir = "create_var_binning"

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id)
        logger.log(name='>>> Starting Create Var: Binning', dict={}, highlight=True)  # Log info
        self.sub_outdir = self.project_outdir / self.sub_outdir
        self.class_df = pd.DataFrame()
        self.target_col = None
        self.btn_calc.setDisabled(True)
        self.btn_add_as_new_var.setDisabled(True)
        self.axes_dict = self.make_axes_dict()

    def select_target(self):
        """Select target var from list"""
        self.converted_exists = False
        self.set_target_col()
        self.class_df = self.init_class_df()
        self.plot_data()
        self.btn_calc.setEnabled(True)

    def calc(self):
        self.class_df = self.class_df[[self.target_col]]
        numbins = self.get_settings_from_fields()
        self.set_colnames(new_units=f'[{numbins}bins]')
        self.init_new_cols()
        edges_df = self.calculate_bins(numbins=numbins)
        self.converted_exists = True
        gui.tableview.show_df(df=edges_df, tableview=self.table_view)
        self.plot_data()
        self.btn_add_as_new_var.setEnabled(True)

    def calculate_bins(self, numbins):
        # kudos: https://stackoverflow.com/questions/61778610/output-both-bins-and-labels-column-in-pandas-binning
        assignments, edges = pd.qcut(self.class_df[self.target_col], q=numbins, labels=False, retbins=True)
        self.class_df[self.converted_target_col] = assignments
        _index = range(0, len(edges))
        edges_df = pd.DataFrame(index=_index, columns=['bin_start'], data=edges)
        edges_df['bin_end'] = edges_df['bin_start'].copy().shift(-1)
        edges_df['bin'] = edges_df.index
        edges_df = move_col_to_pos(df=edges_df, colname='bin', pos=0)
        edges_df.drop(edges_df.tail(1).index, inplace=True)  # drop last row
        edges_df['values'] = assignments.value_counts()
        # self.class_df[self.bin_floor] = edges[assignments]
        # self.class_df[self.bin_ceiling] = edges[assignments + 1]
        return edges_df

    def init_class_df(self):
        return self.tab_data_df[[self.target_col]].copy()

    def init_new_cols(self):
        self.class_df[self.converted_target_col] = np.nan
        self.class_df[self.bin_floor] = np.nan
        self.class_df[self.bin_ceiling] = np.nan

    def set_target_col(self):
        """Get column name of target var from var list"""
        target_var = self.lst_varlist_available.selectedIndexes()
        target_var_ix = target_var[0].row()
        self.target_col = self.col_dict_tuples[target_var_ix]

    def get_settings_from_fields(self):
        numbins = int(self.lne_numbins.text())
        return numbins

    def prepare_export(self):
        return self.class_df[[self.converted_target_col]].copy()  # Converted target already has +mfCU suffix

    def get_selected(self, main_df):
        """Return modified class data back to main data"""
        export_df = self.prepare_export()
        main_df = export_to_main(main_df=main_df,
                                 export_df=export_df,
                                 tab_data_df=self.tab_data_df)  # Return results to main
        self.btn_add_as_new_var.setDisabled(True)
        return main_df

    def plot_data(self):
        # Delete all axes in figure
        for ax in self.fig.axes:
            self.fig.delaxes(ax)

        axes_dict = self.make_axes_dict()

        legend_lns = []
        for key, ax in axes_dict.items():
            if key == 'ax_main':
                col = self.target_col
                color = '#546E7A'
                bg_color = '#90A4AE'
            elif key == 'ax_converted':
                if self.converted_exists:  # Check if converted already exists
                    col = self.converted_target_col
                    color = '#7CB342'
                    bg_color = '#AED581'
                else:
                    continue
            else:
                col = '-col-not-found-'
                color = '-col-not-found-'
                bg_color = '-col-not-found-'

            ax.plot_date(x=self.class_df.index, y=self.class_df[col],
                         color=color, alpha=1, ls='-',
                         marker='o', markeredgecolor='none', ms=4, zorder=98, label=f"{col[0]} {col[1]}")

            ax.text(0.01, 0.97, f"{col[0]}    {col[1]}", weight='bold',
                    size=theme.FONTSIZE_HEADER_AXIS_LARGE, color=theme.FONTCOLOR_HEADER_AXIS,
                    backgroundcolor=bg_color, transform=ax.transAxes, alpha=1,
                    horizontalalignment='left', verticalalignment='top', zorder=99)

            ax.text(0.97, 0.03, f"MEAN: {self.class_df[col].mean():.2f}", weight='bold',
                    size=theme.FONTSIZE_HEADER_AXIS_LARGE, color=theme.FONTCOLOR_HEADER_AXIS,
                    backgroundcolor=bg_color, transform=ax.transAxes, alpha=1,
                    horizontalalignment='right', verticalalignment='bottom', zorder=99)

            gui.plotfuncs.default_grid(ax=ax)
            gui.plotfuncs.default_legend(ax=ax, from_line_collection=False, line_collection=legend_lns)

            locator = mdates.AutoDateLocator(minticks=3, maxticks=30)
            formatter = mdates.ConciseDateFormatter(locator)
            ax.xaxis.set_major_locator(locator)
            ax.xaxis.set_major_formatter(formatter)

        self.fig.canvas.draw()
        self.fig.canvas.flush_events()

    def set_colnames(self, new_units):
        self.converted_target_col = (f"{self.target_col[0]}+cvBIN", new_units)
        self.bin_floor = ("_bin_floor", "[aux]")
        self.bin_ceiling = ("_bin_ceiling", "[aux]")

    def make_axes_dict(self):
        gs = gridspec.GridSpec(2, 1)  # rows, cols
        gs.update(wspace=0.2, hspace=0.2, left=0.03, right=0.96, top=0.96, bottom=0.03)
        ax_main = self.fig.add_subplot(gs[0, 0])
        ax_converted = self.fig.add_subplot(gs[1, 0], sharex=ax_main)
        axes_dict = {'ax_main': ax_main, 'ax_converted': ax_converted}
        for key, ax in axes_dict.items():
            gui.plotfuncs.default_format(ax=ax, txt_xlabel=False)
        return axes_dict
