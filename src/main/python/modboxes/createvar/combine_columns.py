# matplotlib.use('Qt5Agg')
import matplotlib.gridspec as gridspec
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from pandas.plotting import register_matplotlib_converters

import gui.add_to_layout
import gui.base
import gui.make
from pkgs.dfun.frames import export_to_main

pd.plotting.register_matplotlib_converters()  # Needed for time plotting
from help import abbreviations
import gui.elements
import gui.plotfuncs
import logger
from modboxes.plots.styles.LightTheme import *
from PyQt5 import QtGui


class addContent(gui.base.buildTab):
    """Build new tab and populate with contents"""

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id, tab_template='SPoVPoVPoV')

        # Tab icon
        self.TabWidget.setTabIcon(self.tab_ix, QtGui.QIcon(app_obj.ctx.tab_icon_create_variable))

        # Add settings menu contents
        self.btn_add_as_new_var = self.add_settings_fields()

        # # Add available variables to variable lists
        # tabs.buildTab.populate_multiple_variable_lists(obj=self,
        #                                                lists=[self.lst_varlist_selected_1,
        #                                                       self.lst_varlist_selected_2])

        # Add plots
        self.axes_dict = self.populate_plot_area()

    def populate_plot_area(self):
        return self.add_axes()

    def add_axes(self):
        # Setup grids for multiple axes
        gs_1 = gridspec.GridSpec(1, 1)  # rows, cols
        gs_1.update(wspace=0.3, hspace=0.3, left=0.03, right=0.97, top=0.97, bottom=0.03)
        ax_1 = self.fig_1.add_subplot(gs_1[0, 0])

        gs_2 = gridspec.GridSpec(1, 1)  # rows, cols
        gs_2.update(wspace=0.3, hspace=0.3, left=0.03, right=0.97, top=0.97, bottom=0.03)
        ax_2 = self.fig_2.add_subplot(gs_2[0, 0], sharex=ax_1, sharey=ax_1)

        gs_3 = gridspec.GridSpec(1, 1)  # rows, cols
        gs_3.update(wspace=0.3, hspace=0.3, left=0.03, right=0.97, top=0.97, bottom=0.03)
        ax_3 = self.fig_3.add_subplot(gs_3[0, 0], sharex=ax_1, sharey=ax_1)

        axes_dict = {'ax_1': ax_1, 'ax_2': ax_2, 'ax_3': ax_3}
        return axes_dict

    def populate_settings_fields(self):
        pass
        # # Add all variables to drop-down lists to allow full flexibility yay
        # self.drp_xaxis.clear()
        # self.drp_yaxis.clear()
        # for ix, colname_tuple in enumerate(self.tab_data_df.columns):
        #     self.drp_xaxis.addItem(self.col_list_pretty[ix])
        #     self.drp_yaxis.addItem(self.col_list_pretty[ix])

    def add_settings_fields(self):
        # Settings
        gui.elements.add_header_subheader_to_grid_top(layout=self.sett_layout,
                                                      row=0, col=0, rowspan=1, colspan=3,
                                                      txt=["Combine Columns",
                                                           "Create Variable"])
        gui.add_to_layout.add_spacer_item_to_grid(layout=self.sett_layout, row=1, col=0)

        btn_add_as_new_var = gui.elements.add_button_to_grid(grid_layout=self.sett_layout,
                                                             txt='+ Add As New Var', css_id='btn_add_as_new_var',
                                                             row=2, col=0, rowspan=1, colspan=2)
        gui.add_to_layout.add_spacer_item_to_grid(layout=self.sett_layout, row=2, col=0)
        self.sett_layout.setRowStretch(3, 2)

        return btn_add_as_new_var


class Run(addContent):
    color_list = colorwheel_36()  # get some colors
    class_id = abbreviations.create_variable_combine_columns
    sub_outdir = "cv_combine_columns"

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id)
        logger.log(name=f">>> Starting Create Variable > Combine Columns {self.class_id}",
                   dict={}, highlight=True)  # Log info
        self.selected_vars_lookup_dict = {}
        self.vars_available_df = self.tab_data_df.copy()
        self.sub_outdir = self.project_outdir / self.sub_outdir
        self.combination_df = pd.DataFrame()
        self.combined_col = None
        self.var_1_series = None
        self.var_2_series = None
        self.var_1_isset = False
        self.var_2_isset = False
        self.lst_varlist_selected_3.setDisabled(True)
        self.btn_varlist_3.setText('Continue Combination ...')
        self.btn_varlist_3.setDisabled(True)
        self.btn_add_as_new_var.setDisabled(True)
        self.set_pretty_colnames_from_df(df=self.vars_available_df)
        self.update_varlists()

    def set_pretty_colnames_from_df(self, df):
        """
        Update list of pretty variable names for lists, and the dict of column name
        tuples for lookup
        """
        self.col_list_pretty = []  # Reset
        self.col_dict_tuples = {}  # Reset
        for ix, colname_tuple in enumerate(df.columns):
            pretty_name = '{}: {}  {}'.format(ix, colname_tuple[0], colname_tuple[1])  # pretty string
            self.col_dict_tuples[ix] = colname_tuple
            self.col_list_pretty.append(pretty_name)

    def update_varlists(self):
        """Add available variables and combination options to lists"""
        self.lst_varlist_selected_1.clear()
        self.lst_varlist_selected_2.clear()
        self.lst_varlist_selected_3.clear()
        gui.base.buildTab.populate_multiple_variable_lists(col_list_pretty=self.col_list_pretty,
                                                           df=self.vars_available_df,
                                                           lists=[self.lst_varlist_selected_1,
                                                                  self.lst_varlist_selected_2])
        self.populate_calc_options()

    def populate_calc_options(self):
        self.lst_varlist_selected_3.clear()
        self.lst_varlist_selected_3.addItem('Addition')
        self.lst_varlist_selected_3.addItem('Subtraction')
        self.lst_varlist_selected_3.addItem('Division')
        self.lst_varlist_selected_3.addItem('Multiplication')
        self.lst_varlist_selected_3.addItem('Keep Overlapping')
        self.lst_varlist_selected_3.addItem('Fill Gaps')

    def get_selected_var(self, varlist):
        """Get column name from dropdown selection"""
        selected_var = varlist.selectedIndexes()
        selected_var_ix = selected_var[0].row()
        selected_col = self.col_dict_tuples[selected_var_ix]
        # selected_pretty = self.col_list_pretty[selected_var_ix]  # Needs new pretty list (screen names)
        return selected_col

    def update_dynamic_fields(self):
        if self.var_1_isset and self.var_2_isset:
            self.lst_varlist_selected_3.setEnabled(True)

    def make_heatmap(self, var_data, ax, fig):
        from modboxes.plots import heatmap
        x, y, z, plot_df = heatmap.Run._prepare(selected_col_data=var_data,
                                                display_type='Time & Date')
        if not var_data.empty:
            # colormap
            if np.nanmin(z) < 0 and np.nanmax(z) > 0:
                cmap_type = 'diverging'
                cmap = plt.get_cmap('RdYlBu_r')  # set colormap, see https://matplotlib.org/users/colormaps.html
            else:
                cmap_type = 'sequential'
                cmap = plt.get_cmap('GnBu')

            z_numvals_unique = len(plot_df['z'].unique())
            if z_numvals_unique > 100:
                vmin = plot_df['z'].quantile(.01)
                vmax = plot_df['z'].quantile(.99)
            else:
                vmin = plot_df['z'].quantile(0)
                vmax = plot_df['z'].quantile(1)

            heatmap.Run.plot_heatmap(x=x, y=y, z=z, ax=ax, cmap=cmap, color_bad='Grey',
                                     info_txt="", vmin=vmin, vmax=vmax, cb_digits_after_comma=1)
            ax.set_facecolor('white')
            ax.set_xticks(['6:00', '12:00', '18:00'])
            ax.set_xticklabels([6, 12, 18])
            gui.plotfuncs.nice_date_ticks(ax=ax, minticks=3, maxticks=7, which='y')
            gui.plotfuncs.default_format(ax=ax, txt_xlabel='Time')
            plt.tight_layout()
            fig.canvas.draw()  # update plot w/ only canvas.draw but NOT with pause() works w/o background flashing

    def update_var_1(self):
        """Set data for variable 1"""
        var_col = self.get_selected_var(varlist=self.lst_varlist_selected_1)
        self.var_1_series = self.vars_available_df[var_col].copy()
        self.var_1_isset = True
        self.make_heatmap(var_data=self.var_1_series,
                          ax=self.axes_dict['ax_1'],
                          fig=self.fig_1)
        self.update_dynamic_fields()

    def update_var_2(self):
        """Set data for variable 2"""
        var_col = self.get_selected_var(varlist=self.lst_varlist_selected_2)
        self.var_2_series = self.vars_available_df[var_col].copy()
        self.var_2_isset = True
        self.make_heatmap(var_data=self.var_2_series,
                          ax=self.axes_dict['ax_2'],
                          fig=self.fig_2)
        self.update_dynamic_fields()

    @staticmethod
    def remove_duplicate_splits_from_string(str, split_char):
        """Shorten string by removing duplicate splits"""
        splits = str.split(split_char)
        newname = ''
        for s in splits:
            if s not in newname:
                newname = newname + s + '_'
        newname = newname[0:-1]
        return newname

    def continue_combination(self):
        """Store current combination in combination df for additional combinations"""

        newname = self.remove_duplicate_splits_from_string(str=self.combined_col[0], split_char='_')
        newcol = (newname, self.combined_col[1])  # New col has shorter name
        self.combination_df[newcol] = self.combination_df[
            self.combined_col].copy()  # Add newcol as column in combined_df
        self.combination_df.drop(self.combined_col, axis=1, inplace=True)  # Drop combined with old name
        self.combined_col = newcol  # Set new name for combined
        self.vars_available_df[self.combined_col] = self.combination_df[
            self.combined_col].copy()  # To selected, for lists
        self.set_pretty_colnames_from_df(df=self.vars_available_df)
        self.update_varlists()
        self.btn_varlist_3.setEnabled(False)
        self.btn_add_as_new_var.setEnabled(False)

    def calc(self):
        option = self.lst_varlist_selected_3.currentItem().text()
        self.combination_df = pd.concat([self.var_1_series, self.var_2_series], axis=1)

        self.combined_col = None
        if option == 'Addition':
            operator_sign = '_added_'
            colname = f"{self.var_1_series.name[0]}{operator_sign}{self.var_2_series.name[0]}{self.class_id}"
            colunits = f"{self.var_1_series.name[1]}{operator_sign}{self.var_2_series.name[1]}"
            self.combined_col = (colname, colunits)
            self.combination_df[self.combined_col] = self.var_1_series.add(self.var_2_series)
        elif option == 'Subtraction':
            operator_sign = '_subtracted_'
            colname = f"{self.var_1_series.name[0]}{operator_sign}{self.var_2_series.name[0]}{self.class_id}"
            colunits = f"{self.var_1_series.name[1]}{operator_sign}{self.var_2_series.name[1]}"
            self.combined_col = (colname, colunits)
            self.combination_df[self.combined_col] = self.var_1_series.sub(self.var_2_series)
        elif option == 'Division':
            operator_sign = '_dividedby_'
            colname = f"{self.var_1_series.name[0]}{operator_sign}{self.var_2_series.name[0]}{self.class_id}"
            colunits = f"{self.var_1_series.name[1]}{operator_sign}{self.var_2_series.name[1]}"
            self.combined_col = (colname, colunits)
            self.combination_df[self.combined_col] = self.var_1_series.divide(self.var_2_series)
        elif option == 'Multiplication':
            operator_sign = '_multiplied_'
            colname = f"{self.var_1_series.name[0]}{operator_sign}{self.var_2_series.name[0]}{self.class_id}"
            colunits = f"{self.var_1_series.name[1]}{operator_sign}{self.var_2_series.name[1]}"
            self.combined_col = (colname, colunits)
            self.combination_df[self.combined_col] = self.var_1_series.multiply(self.var_2_series)
        elif option == 'Keep Overlapping':
            operator_sign = '_overlaps_'
            colname = f"{self.var_1_series.name[0]}{operator_sign}{self.var_2_series.name[0]}{self.class_id}"
            colunits = f"{self.var_1_series.name[1]}{operator_sign}{self.var_2_series.name[1]}"
            self.combined_col = (colname, colunits)
            filter_missing_var_2 = self.var_2_series.isnull()
            self.combination_df[self.combined_col] = self.var_1_series.loc[~filter_missing_var_2]
        elif option == 'Fill Gaps':
            operator_sign = '_filled_with_'
            colname = f"{self.var_1_series.name[0]}{operator_sign}{self.var_2_series.name[0]}{self.class_id}"
            colunits = f"{self.var_1_series.name[1]}{operator_sign}{self.var_2_series.name[1]}"
            self.combined_col = (colname, colunits)
            self.combination_df[self.combined_col] = self.var_1_series.combine_first(self.var_2_series)
        self.make_heatmap(var_data=self.combination_df[self.combined_col], ax=self.axes_dict['ax_3'], fig=self.fig_3)
        self.btn_varlist_3.setEnabled(True)
        self.btn_add_as_new_var.setEnabled(True)

    def prepare_export(self):
        export_df = self.combination_df[[self.combined_col]].copy()
        # outlier_removed_col = (self.target_col[0] + "+odDD", self.target_col[1])
        # export_df[outlier_removed_col] = export_df[self.target_col]
        # export_df = export_df[[outlier_removed_col]]
        return export_df

    def get_selected(self, main_df):
        """Return modified class data back to main data"""
        export_df = self.prepare_export()
        main_df = export_to_main(main_df=main_df,
                                 export_df=export_df,
                                 tab_data_df=self.vars_available_df)  # Return results to main
        self.btn_add_as_new_var.setDisabled(True)
        return main_df

    # def get_settings_from_fields(self):
    #     # Selected var
    #     selected_var = self.lst_varlist_available.selectedIndexes()
    #     selected_var_ix = selected_var[0].row()
    #     self.selected_col = self.col_dict_tuples[selected_var_ix]
    #     self.display_type = self.drp_display_type.currentText()
