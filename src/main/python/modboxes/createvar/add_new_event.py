import numpy as np
from PyQt5 import QtCore as qtc
from PyQt5 import QtWidgets as qw
from PyQt5.QtGui import QIcon

import gui.base
from gui import elements


class PopupWindow(gui.base.CustomQDialogWindow):
    """ Limit dataset to selected time range. """

    def __init__(self, df, ctx):
        super().__init__()
        self.df = df
        self.ctx = ctx
        self.icon = QIcon(self.ctx.tab_icon_create_variable)
        self.docstring = self.__doc__

        # Dialog window
        self.ref_lne_name, self.ref_dte_start, self.ref_dte_end = \
            self.gui()
        self.update_fields()

    def update_fields(self):
        """ Set line edit default entry and add dropdown options. """
        min_data_day_str = self.df.index[0].strftime("%Y-%m-%d")  # str
        max_data_day_str = self.df.index[-1].strftime("%Y-%m-%d")
        min_data_day_qt = qtc.QDate.fromString(min_data_day_str, 'yyyy-MM-dd')  # Qt format
        max_data_day_qt = qtc.QDate.fromString(max_data_day_str, 'yyyy-MM-dd')
        self.ref_dte_start.setDate(min_data_day_qt)
        self.ref_dte_end.setDate(max_data_day_qt)

    def gui(self):
        """
        Construct window GUI.
        """
        header_txt = 'Create Event'
        container_grid = self.setupUi(self, win_title=header_txt, ctx=self.ctx,
                                      icon=self.icon)
        elements.add_header_to_grid_top(layout=container_grid, txt=header_txt)

        # Settings
        ref_lne_name = elements.add_label_linedit_pair_to_grid(txt='Name of Event',
                                                               css_ids=['', 'cyan'],
                                                               layout=container_grid,
                                                               orientation='horiz',
                                                               row=1, col=0)
        ref_dte_start = elements.grd_LabelDateEditPair(txt='Event Start (YYYY-MM-DD)',
                                                       css_ids=['', ''],
                                                       layout=container_grid,
                                                       row=2, col=0)
        ref_dte_end = elements.grd_LabelDateEditPair(txt='Event End (YYYY-MM-DD)',
                                                     css_ids=['', ''],
                                                     layout=container_grid,
                                                     row=3, col=0)

        # Button box: OK and Cancel
        self.button_box = qw.QDialogButtonBox()
        self.button_box.setEnabled(True)
        self.button_box.setStandardButtons(qw.QDialogButtonBox.Cancel | qw.QDialogButtonBox.Ok)
        self.button_box.setObjectName("button_box")
        self.button_box.accepted.connect(self.accept)
        self.button_box.rejected.connect(self.reject)
        container_grid.addWidget(self.button_box, 4, 1, 1, 1)
        container_grid.setRowStretch(5, 1)
        return ref_lne_name, ref_dte_start, ref_dte_end


class Run():

    def __init__(self, obj):
        self.obj = obj
        self.start_date = obj.ref_dte_start.date().toPyDate()
        self.end_date = obj.ref_dte_end.date().toPyDate()
        event_name = obj.ref_lne_name.text()
        self.event_col = (event_name, '[event]')

    def make_new_event_col(self):
        """ Create new column for event, set start-end range to 1, otherwise 0. """
        df = self.obj.df.copy()
        df[self.event_col] = np.nan
        filter = (df.index.date >= self.start_date) & (df.index.date <= self.end_date)
        df.loc[filter, self.event_col] = 1
        df.loc[~filter, self.event_col] = 0
        return df
