"""

LAG FEATURES

"""

# matplotlib.use('Qt5Agg')
import pandas as pd
from PyQt5 import QtGui
from PyQt5 import QtWidgets as qw
from PyQt5.QtGui import QDoubleValidator, QIntValidator

import gui.add_to_layout
import gui.base
import gui.elements
import gui.make
import gui.plotfuncs
import logger
from pkgs.dfun.frames import export_to_main


# pd.set_option('display.width', 1000)
# pd.set_option('display.max_columns', 15)
# pd.set_option('display.max_rows', 999)


class addContent(gui.base.buildTab):
    """Build new tab and populate with contents"""

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id, tab_template='SVonVT')

        # Tab icon
        self.TabWidget.setTabIcon(self.tab_ix, QtGui.QIcon(app_obj.ctx.tab_icon_create_variable))

        # Add settings menu contents
        self.lne_shift, self.btn_create_lagged_versions, self.btn_add_as_new_vars = \
            self.add_settings_fields()

        # # Add variables required in settings
        # self.populate_settings_fields()

        # Add available variables to variable lists
        gui.base.buildTab.populate_variable_list(obj=self)

        # # Add plots
        # self.axes_dict = self.populate_plot_area()

    def populate_plot_area(self):
        pass

    def populate_settings_fields(self):
        pass

    def add_axes(self):
        pass

    def add_settings_fields(self):
        gui.elements.add_header_subheader_to_grid_top(layout=self.sett_layout,
                                                      txt=["Lag Features",
                                                           "Create Variable"])
        gui.add_to_layout.add_spacer_item_to_grid(self.sett_layout, 1, 0)

        gui.elements.add_header_in_grid_row(
            row=2, layout=self.sett_layout, txt='Settings')

        onlyInt = QIntValidator()
        onlyFloat = QDoubleValidator()
        args = dict(css_ids=['', 'cyan'], layout=self.sett_layout, col=0, orientation='horiz')

        lne_shift = gui.elements.add_label_linedit_info_triplet_to_grid(
            row=3, txt='Shift (Steps)', txt_info_hover=_help_lne_shift, **args)
        lne_shift.setText('3')
        lne_shift.setValidator(onlyInt)

        gui.add_to_layout.add_spacer_item_to_grid(self.sett_layout, 4, 0)

        # drp_method = gui_elements.grd_LabelDropdownPair(
        #     txt='Method', css_ids=['', 'cyan'], layout=self.sett_layout,
        #     row=8, col=0, orientation='horiz')
        # gui_elements.add_spacer_item_to_grid(self.sett_layout, 6, 0)
        # drp_method.addItems(['Boruta',
        #                      'Recursive Feature Elimination With Cross-Validation'])

        # Buttons
        btn_create_lagged_versions = gui.elements.add_button_to_grid(
            grid_layout=self.sett_layout, txt='Create Lagged Versions', css_id='btn_cat_ControlsRun',
            row=5, col=0, rowspan=1, colspan=2)
        btn_add_as_new_vars = gui.elements.add_button_to_grid(
            grid_layout=self.sett_layout, txt='+ Add As New Vars', css_id='btn_add_as_new_var',
            row=6, col=0, rowspan=1, colspan=2)

        self.sett_layout.setRowStretch(7, 2)  # empty row

        return lne_shift, btn_create_lagged_versions, btn_add_as_new_vars


class Run(addContent):
    """
    Start random forest
    """
    sub_outdir = "create_variable_lag_features"

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id)
        logger.log(name='>>> Starting Create Variable: Lag Features', dict={}, highlight=True)  # Log info
        self.shift = 1
        self.selected_vars_lookup_dict = {}
        self.vars_selected_df = pd.DataFrame()
        self.variants_df = pd.DataFrame()
        self.sub_outdir = self.project_outdir / self.sub_outdir
        self.btn_create_lagged_versions.setDisabled(True)
        self.btn_add_as_new_vars.setDisabled(True)

    def get_variants(self, main_df):
        export_df = self.variants_df.copy()
        main_df = export_to_main(main_df=main_df,
                                 export_df=export_df,
                                 tab_data_df=self.tab_data_df)  # Return results to main
        self.btn_add_as_new_vars.setDisabled(True)
        return main_df

    def create_lagged_versions(self):
        self.get_settings_from_fields()
        self.variants_df = self.vars_selected_df.copy()
        for col in self.variants_df.columns:
            for shift in range(1, self.shift + 1):
                _shifted_col = (f"{col[0]}-LAG{shift}", f"{col[1]}")
                self.variants_df[_shifted_col] = self.variants_df[col].shift(shift)
        self.show_in_textbox(df=self.variants_df, n=20)
        self.btn_add_as_new_vars.setEnabled(True)

    def add_var_to_selected(self):
        """Add feature from available to selected"""
        selected_var = self.lst_varlist_available.selectedIndexes()
        selected_var_ix = selected_var[0].row()
        selected_col = self.col_dict_tuples[selected_var_ix]
        selected_pretty = self.col_list_pretty[selected_var_ix]  # Needs new pretty list (screen names)

        # Add to lookup dict and df
        self.selected_vars_lookup_dict[selected_col] = selected_pretty
        self.vars_selected_df[selected_col] = self.tab_data_df[selected_col]
        self.update_varlist_selected(list=self.lst_varlist_selected,
                                     df=self.vars_selected_df,
                                     lookup_dict=self.selected_vars_lookup_dict)
        self.btn_create_lagged_versions.setEnabled(True)
        self.btn_add_as_new_vars.setDisabled(True)
        self.show_in_textbox(df=self.vars_selected_df, n=20)

    def remove_var_from_selected(self):
        """Remove feature from available to selected"""
        selected_var = self.lst_varlist_selected.selectedIndexes()
        selected_var_ix = selected_var[0].row()
        selected_col = list(self.selected_vars_lookup_dict.keys())[selected_var_ix]  # Get key by index
        # selected_pretty = self.selected_features_lookup_dict[selected_col]

        # Remove from lookup dict and df
        del self.selected_vars_lookup_dict[selected_col]
        self.vars_selected_df.drop(selected_col, axis=1, inplace=True)
        self.update_varlist_selected(list=self.lst_varlist_selected,
                                     df=self.vars_selected_df,
                                     lookup_dict=self.selected_vars_lookup_dict)

        self.check_if_selected_empty()

    def update_varlist_selected(self, list, df, lookup_dict):
        list.clear()
        for col in df.columns:
            item = qw.QListWidgetItem(lookup_dict[col])
            list.addItem(item)

    def check_if_selected_empty(self):
        _list = self.lst_varlist_selected
        if self.vars_selected_df.empty:
            _list.addItem('-empty-')
            self.btn_create_lagged_versions.setDisabled(True)
            self.btn_add_as_new_vars.setDisabled(True)
            self.txe_textbox.setText("")
        else:
            self.show_in_textbox(df=self.vars_selected_df, n=20)
            # self.txe_textbox.setText(str(self.vars_selected_df..head(30)))

    def show_in_textbox(self, df, n):
        first_n = str(df.head(n))
        last_n = str(df.tail(n))
        txt = f"FIRST {n} DATA LINES:\n" \
              f"==============\n\n" \
              f"{first_n}\n\n\n\n" \
              f"(...)\n\n\n\n" \
              f"LAST {n} DATA LINES:\n" \
              f"==============\n\n" \
              f"{last_n}"
        self.txe_textbox.setText(txt)
        self.txe_textbox.setLineWrapMode(qw.QTextEdit.NoWrap)
        self.txe_textbox.horizontalScrollBar().setValue(0)

    def get_settings_from_fields(self):
        """Get settings from fields and translate to usable formats"""
        self.shift = int(self.lne_shift.text())

    def get_col_from_drp_pretty(self, drp):
        """ Set target to selected variable. """
        target_pretty = drp.currentText()
        target_pretty_ix = self.col_list_pretty.index(target_pretty)
        target_col = self.col_dict_tuples[target_pretty_ix]
        return target_col


_help_lne_shift = \
    "Number of steps to shift the data (...more description soon...)"
