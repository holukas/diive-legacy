"""

DEFINE SEASONS
--------------

"""
import datetime as dt

import matplotlib.gridspec as gridspec
# matplotlib.use('Qt5Agg')
import numpy as np
import pandas as pd
from PyQt5 import QtGui

import gui.base
import gui.elements
import gui.plotfuncs
import logger
from gui import elements
from modboxes.plots.styles.LightTheme import *
from pkgs.dfun.frames import export_to_main


# pd.set_option('display.width', 1000)
# pd.set_option('display.max_columns', 15)
# pd.set_option('display.max_rows', 20)


class addContent(gui.base.buildTab):
    """Build new tab and populate with contents"""

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id, tab_template='SP')

        # Tab icon
        self.TabWidget.setTabIcon(self.tab_ix, QtGui.QIcon(app_obj.ctx.tab_icon_create_variable))

        # Add settings menu contents
        self.btn_apply, self.infobox, self.ref_drp_based_on, self.ref_drp_segments, \
        self.ref_drp_starting_in, self.btn_add_as_new_col = \
            self.add_settings_fields()

        # # Add variables required in settings
        # self.populate_settings_fields()

        # Add plots
        self.axes_dict = self.populate_plot_area()

    def populate_plot_area(self):
        return self.add_axes()

    def populate_settings_fields(self):
        pass

    def add_axes(self):
        gs = gridspec.GridSpec(1, 1)  # rows, cols
        gs.update(wspace=0.2, hspace=0.2, left=0.03, right=0.97, top=0.97, bottom=0.03)
        ax = self.fig.add_subplot(gs[0, 0])
        axes_dict = {'ax': ax}
        for key, ax in axes_dict.items():
            gui.plotfuncs.default_format(ax=ax, txt_xlabel=False)
        return axes_dict

    def add_settings_fields(self):
        gui.elements.add_header_subheader_to_grid_top(layout=self.sett_layout,
                                                      row=0, col=0, rowspan=1, colspan=3,
                                                      txt=["Define Seasons",
                                                           "Create Variable"])

        self.sett_layout.setRowStretch(1, 0)  # empty row

        # Seasons based on
        ref_drp_based_on = elements.grd_LabelDropdownPair(txt='Seasons Based On',
                                                          css_ids=['', 'cyan'],
                                                          layout=self.sett_layout,
                                                          orientation='horiz',
                                                          row=2, col=0,
                                                          colspan=2)
        ref_drp_based_on.addItem('Months')

        # Segments
        ref_drp_segments = elements.grd_LabelDropdownPair(txt='Season Segments',
                                                          css_ids=['', 'cyan'],
                                                          layout=self.sett_layout,
                                                          orientation='horiz',
                                                          row=3, col=0,
                                                          colspan=2)
        ref_drp_segments.addItems(['1', '2', '3', '4', '5', '6'])
        ref_drp_segments.setCurrentIndex(3)  # Default 4 segments

        # Start
        ref_drp_starting_in = elements.grd_LabelDropdownPair(txt='Starting Month',
                                                             css_ids=['', 'cyan'],
                                                             layout=self.sett_layout,
                                                             orientation='horiz',
                                                             row=4, col=0,
                                                             colspan=2)
        month_list = range(1, 13, 1)  # Create items for all 12 months
        month_list = [str(m) for m in month_list]
        ref_drp_starting_in.addItems(month_list)
        ref_drp_starting_in.setCurrentIndex(2)  # Index 2 is March

        btn_apply = elements.add_button_to_grid(grid_layout=self.sett_layout,
                                                txt='Apply',
                                                css_id='btn_cat_ControlsRun',
                                                row=5, col=0, rowspan=1, colspan=3)

        btn_add_as_new_col = elements.add_button_to_grid(grid_layout=self.sett_layout,
                                                         txt='+ Add As New Var',
                                                         css_id='btn_add_as_new_var',
                                                         row=6, col=0, rowspan=1, colspan=3)

        self.sett_layout.setRowStretch(7, 2)  # empty row

        infobox = elements.add_infobox_to_grid(txt='', grid_layout=self.sett_layout, css_id='',
                                               row=8, col=0, rowspan=1, colspan=2)
        self.sett_layout.setRowStretch(8, 1)  # empty row

        return btn_apply, infobox, ref_drp_based_on, ref_drp_segments, ref_drp_starting_in, \
               btn_add_as_new_col


class Run(addContent):
    """
    Divide data into seasons

    Two additional columns with seasonal information are added to the df.
        (1) Season type: the general season type, e.g. 1, 2, 3, ...
        (2) Season: the unique season identifier, combines year and season type info,
                e.g. 2018_1, 2018_2, 2019_1, ...

    """
    grp_season_type_col = (f'GRP_SEASON_TYPE_', '[#]')  # General season type info, e.g. 1, 2, 3, ...
    grp_season_col = (f'GRP_SEASON_', '[#]')  # Unique season identifier, e.g. 2018_1, 2019_1, ...
    season_type_change_col = ('SEASON_TYPE_CHANGE', '[1=yes]')  # Marks rows where new season starts
    season_type_start_dt_col = ('SEASON_TYPE_START', '[-]')
    season_type_end_dt_col = ('SEASON_TYPE_END', '[-]')
    sub_outdir = "create_var_define_seasons"

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id)
        logger.log(name='>>> Starting Create Variable > Define Seasons', dict={}, highlight=True)  # Log info
        self.sub_outdir = self.project_outdir / self.sub_outdir
        self.assignments_df = pd.DataFrame()
        id = self.season_group_id()
        self.grp_season_type_col = (f'{self.grp_season_type_col[0]}{id}', self.grp_season_type_col[1])
        self.grp_season_col = (f'{self.grp_season_col[0]}{id}', self.grp_season_col[1])
        self.btn_add_as_new_col.setDisabled(True)

    def season_group_id(self):
        """Check if there is already season info in the df."""
        collist = self.tab_data_df.columns.to_list()
        occurrences = sum(self.grp_season_type_col[0] in s[0] for s in collist)
        id = occurrences + 1
        logger.log(name='', dict={'Season Group ID': id}, highlight=True)  # Log info
        return id

    def prepare_export(self):
        export_df = self.assignments_df[[self.grp_season_type_col, self.grp_season_col]].copy()
        return export_df

    def get_season_cols(self, main_df):
        """ Insert season column into main data. """
        export_df = self.prepare_export()
        main_df = export_to_main(main_df=main_df,
                                 export_df=export_df,
                                 tab_data_df=self.tab_data_df)  # Return results to main

        # # Make subset of columns that are then put into data_df
        # data_cols_for_data_df = self.assignments_df[[self.grp_season_type_col, self.grp_season_col]]
        # _data_cols_for_data_df = data_cols_for_data_df.copy()
        #
        # # Make sure columns are MultiIndex
        # _data_cols_for_data_df.columns = pd.MultiIndex.from_tuples(_data_cols_for_data_df.columns)
        #
        # # Add gap-filled data to main data_df
        # main_df[self.grp_season_type_col] = np.nan
        # main_df[self.grp_season_col] = np.nan
        # main_df = main_df.combine_first(_data_cols_for_data_df)
        self.btn_add_as_new_col.setDisabled(True)

        return main_df

    def get_settings_from_fields(self):
        self.based_on = self.ref_drp_based_on.currentText()
        self.segments = int(self.ref_drp_segments.currentText())
        self.starting_in = int(self.ref_drp_starting_in.currentText())

    def init_assignments_df(self):
        self.assignments_df = pd.DataFrame(index=self.tab_data_df.index, columns=[self.grp_season_type_col])
        self.assignments_df[self.grp_season_type_col] = np.nan
        self.assignments_df[self.grp_season_col] = np.nan
        self.assignments_df[self.season_type_change_col] = np.nan
        self.assignments_df[self.season_type_start_dt_col] = np.nan
        self.assignments_df[self.season_type_end_dt_col] = np.nan
        self.assignments_df.columns = pd.MultiIndex.from_tuples(self.assignments_df.columns)

    def apply(self):
        """ Create season type and season assignments. """
        self.get_settings_from_fields()
        self.init_assignments_df()

        # Lookup table for season/type assignment
        self.generate_season_type_lut(start_month=self.starting_in,
                                      based_on=self.based_on,
                                      segments=self.segments)

        self.assign_season_types()
        self.assign_seasons()

        # Plot
        axes_dict = self.make_axes_dict(fig=self.fig)
        # TODO hier weiter
        self.plot_seasons(axes_dict=axes_dict, season_type_assignments_df=self.assignments_df)
        self.btn_add_as_new_col.setEnabled(True)

    def assign_season_types(self):
        """Create new dataframe that contains the season type assignment for each row.

        The new assignments_df contains the full resolution index of the main df that
        contains all the data. The index is used to add season type info for each record
        in the main df.

        Examples:
            - 1, 2, 3, ...

        """
        found_data_months = self.assignments_df.index.month.unique()
        for found_month in found_data_months:
            assigned_season = int(self.season_lut.loc[self.season_lut['month'] == found_month, 'season'])
            self.assignments_df.sort_index(axis=1, inplace=True)  # lexsort for better performance
            self.assignments_df.loc[(self.assignments_df.index.month == found_month),
                                    [self.grp_season_type_col]] = assigned_season
        return self.assignments_df

    def assign_seasons(self):
        """Assign season to each row.

        This method uses the general season type info from season_type_assignments_df
        and builds an unique identification float(!) for each season.

        Examples:
            - Season X with data in 2018 becomes 2018.X, whereby X is the season type (1, 2, 3, ...)
            - Season X with data spanning 2 years 2018 and 2019 becomes season 20182019.X

        """
        # Mark change to new season, the subtraction yields
        # - an integer != 0 for rows where season type changes,
        # - 0 for rows where season type did not change,
        # - nan for the first row due to .shift.
        self.assignments_df[self.season_type_change_col] = \
            self.assignments_df[self.grp_season_type_col] \
            - self.assignments_df[self.grp_season_type_col].shift(1)
        self.assignments_df.loc[self.assignments_df[self.season_type_change_col] != 0,
                                [self.season_type_change_col]] = 1  # Mark season change with 1
        first_date = self.assignments_df.iloc[0].name

        # Fill in for first row (would otherwise be nan)
        self.assignments_df.loc[self.assignments_df.index == first_date,
                                [self.season_type_change_col]] = 1

        # Create df that only contains rows where seasons change
        filter = self.assignments_df[self.season_type_change_col] == 1
        season_change_rows_df = self.assignments_df[filter].copy()
        season_change_rows_df.loc[:, [self.season_type_start_dt_col]] = season_change_rows_df.index
        season_change_rows_df.loc[:, [self.season_type_end_dt_col]] = \
            season_change_rows_df[self.season_type_start_dt_col].shift(-1)
        num_season_changes = len(season_change_rows_df)  # Needed to detect last available season

        # Loop through season changes
        season_changes = 0
        for ix, row in season_change_rows_df.iterrows():
            season_changes += 1
            start_dt = row[self.season_type_start_dt_col]
            end_dt = row[self.season_type_end_dt_col]

            # Build unique season string
            # if season_changes < num_season_changes:
            #     unique_years = season_type_assignments_df.loc[start_dt:end_dt].index.year.unique()
            # else:
            #     unique_years = season_type_assignments_df.loc[start_dt:].index.year.unique()  # Last season
            # unique_season = str(unique_years[0])
            # if len(unique_years[1:]) > 0:
            #     for uy in unique_years[1:0]:
            #         unique_season += f'{uy}'
            unique_season = start_dt.strftime('%Y%m%d')
            unique_season += f'.{int(row[self.grp_season_type_col])}'
            unique_season = float(unique_season)  # Needs numeric representation

            if season_changes < num_season_changes:
                self.assignments_df.loc[start_dt:end_dt, [self.grp_season_col]] = unique_season
            else:
                self.assignments_df.loc[start_dt:, [self.grp_season_col]] = unique_season  # Last season

    def generate_season_type_lut(self, start_month, based_on, segments):
        """ Generate df that contains info which month is assigned to which season (lookup table). """
        self.season_lut = pd.DataFrame()
        if based_on == 'Months':
            labels = list(range(1, segments + 1))
            start_dt = dt.date(2000, start_month, 1)
            date_series = pd.date_range(start_dt, periods=12, freq='M')
            self.season_lut['month'] = date_series.month
            self.season_lut['month_name'] = date_series.month_name()
            _seasons = pd.cut(self.season_lut.index, segments, labels=labels)  # How awesome!
            self.season_lut['season'] = _seasons

    def make_axes_dict(self, fig):
        """ Assign data columns to axes. """

        gs = gridspec.GridSpec(2, 1)  # rows, cols
        gs.update(wspace=0.4, hspace=0.4, left=0.1, right=0.9, top=0.9, bottom=0.1)

        # Delete all axes in figure
        for ax in self.fig.axes:
            self.fig.delaxes(ax)

        ax = fig.add_subplot(gs[0, 0])
        axes_dict = {}
        axes_dict['ax'] = ax
        self.fig.canvas.draw()
        self.fig.canvas.flush_events()

        # Format
        for col, ax in axes_dict.items():
            gui.plotfuncs.default_format(ax=ax, txt_xlabel=False)

        return axes_dict

    def plot_seasons(self, season_type_assignments_df, axes_dict):
        """Plot seasons."""
        colors = colors_12(500)
        for col, ax in axes_dict.items():
            counter_season = -1
            grouped_df = season_type_assignments_df.groupby(self.grp_season_col)
            for season_key, unique_season_df in grouped_df:
                counter_season += 1

                this_season_type = int(list(set(unique_season_df[self.grp_season_type_col]))[0])

                ax.plot_date(x=unique_season_df.index,
                             y=unique_season_df[self.grp_season_type_col],
                             alpha=1, ls='-',
                             zorder=98, label='Season',
                             marker='s',
                             color=colors[this_season_type],
                             lw=WIDTH_LINE_DEFAULT,
                             markeredgecolor=COLOR_MARKER_DEFAULT_EDGE,
                             ms=SIZE_MARKER_DEFAULT)

                ax.text(unique_season_df.index[0],
                        unique_season_df[self.grp_season_type_col][0],
                        f'{season_key}',
                        horizontalalignment='left',
                        verticalalignment='bottom',
                        size=FONTSIZE_INFO_DEFAULT,
                        color=FONTCOLOR_LABELS_AXIS,
                        backgroundcolor='none',
                        zorder=99)

        self.fig.canvas.draw()
        self.fig.canvas.flush_events()
