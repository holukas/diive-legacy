import fnmatch

import math
import matplotlib.gridspec as gridspec
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from PyQt5 import QtWidgets as qw
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar

import gui.elements
import gui.plotfuncs
from gui import elements
from utils.vargroups import *


class AddControls():
    """
        Creates the gui control elements and their handles for usage.

    """

    def __init__(self, opt_stack, opt_frame, opt_layout, ref_stack, ctx):
        self.opt_stack = opt_stack
        self.opt_frame = opt_frame
        self.opt_layout = opt_layout
        self.ref_stack = ref_stack
        self.ctx = ctx
        self.add_option()
        self.add_refinements()
        # self.get_handles()

    def get_handles(self):
        return self.btn_hrt_opt_StationarityM98, \
               self.btn_hrt_ref_StationarityM98_showInNewTab

    def add_option(self):
        # Option Button: [3] Running Median
        self.btn_hrt_opt_StationarityM98 = elements.add_button_to_grid(grid_layout=self.opt_layout,
                                                                       txt='Stationarity (Mahrt 1998)', css_id='',
                                                                       row=1, col=0, rowspan=1, colspan=1)
        self.opt_stack.addWidget(self.opt_frame)

    def add_refinements(self):
        ref_frame, ref_layout = elements.add_frame_grid()
        self.ref_stack.addWidget(ref_frame)

        gui.elements.add_header_to_grid_top(layout=ref_layout, txt='HRT: Stationarity (Mahrt 1998)')

        self.btn_hrt_ref_StationarityM98_showInNewTab = \
            elements.add_iconbutton_to_grid(grid_layout=ref_layout,
                                            txt='Show in New Tab', css_id='',
                                            row=1, col=0, rowspan=1, colspan=2,
                                            icon=self.ctx.icon_open_in_new_tab)

        ref_layout.setRowStretch(2, 1)

        return None


class MakeNewTab:
    """
        Connection between the main gui tab and the newly added tab.
        Creates the new tab for the plots and then adds the plots control elements
        (buttons, dropdowns) to the new tab.
    """

    def __init__(self, TabWidget, data_df, colname_pretty_string, new_tab_id):

        self.TabWidget = TabWidget
        self.colname_pretty_string = colname_pretty_string
        self.data_df = data_df

        self.title = f"STATIONARITY M98 {new_tab_id}"  # used in tab name and header

        # Create tab menu (refinements)
        self.drp_u_var, self.drp_v_var, self.drp_w_var, self.btn_init_wind_vars, self.ref_frame, \
        self.drp_flux_scalar_var = \
            self.create_ref_tab_menu()

        # Create and add new tab
        self.canvas, self.figure, self.axes_dict = self.create_tab()

        # Update the gui dropdown menus of the new tab
        self.update_refinements()

    def get_handles(self):
        return self.drp_u_var, self.drp_v_var, self.drp_w_var, self.btn_init_wind_vars, \
               self.figure, self.axes_dict, self.drp_flux_scalar_var

    def create_tab(self):
        tabContainerVertLayout, tab_ix = \
            self.TabWidget.add_new_tab(title=self.title)
        self.TabWidget.setCurrentIndex(tab_ix)  ## select newly created tab

        # HEADER (top): Create and add
        elements.add_label_to_layout(txt=self.title, css_id='lbl_Header2', layout=tabContainerVertLayout)

        # MENU (left): Stack for refinement menu
        left_tabMenu_ref_stack = qw.QStackedWidget()
        left_tabMenu_ref_stack.setProperty('labelClass', '')
        left_tabMenu_ref_stack.setContentsMargins(0, 0, 0, 0)
        left_tabMenu_ref_stack.addWidget(self.ref_frame)

        # PLOT & NAVIGATION CONTROLS (right frame)
        # in horizontal layout

        # Figure (right frame)
        tabFigure = plt.Figure(facecolor='white')
        # tabFigure = plt.Figure(facecolor='#29282d')
        tabCanvas = FigureCanvas(tabFigure)

        # Make axes
        axes_dict = self.make_axes_dict(tabFigure=tabFigure)

        # Navigation (right frame)
        tabToolbar = NavigationToolbar(tabCanvas, parent=self.TabWidget)

        # Frame for figure & navigation (right frame)
        right_frm_fig_nav = qw.QFrame()
        lytVert_fig_nav = qw.QVBoxLayout()  # create layout for frame
        right_frm_fig_nav.setLayout(lytVert_fig_nav)  # assign layout to frame
        lytVert_fig_nav.setContentsMargins(0, 0, 0, 0)
        lytVert_fig_nav.addWidget(tabCanvas)  # add widget to layout
        lytVert_fig_nav.addWidget(tabToolbar)

        # ASSEMBLE MENU AND PLOT
        # Splitter for menu on the left and plot on the right
        tabSplitter = qw.QSplitter()
        tabSplitter.addWidget(left_tabMenu_ref_stack)  ## add ref menu first (left)
        tabSplitter.addWidget(right_frm_fig_nav)  ## add plot & navigation second (right)
        tabSplitter.setStretchFactor(0, 1)
        tabSplitter.setStretchFactor(1, 2)  ## stretch right more than left
        tabContainerVertLayout.addWidget(tabSplitter, stretch=1)  ## add Splitter to tab layout

        return tabCanvas, tabFigure, axes_dict

    def make_axes_dict(self, tabFigure):

        gs = gridspec.GridSpec(4, 4)  # rows, cols
        gs.update(wspace=0.1, hspace=0.2, left=0.03, right=0.97, top=0.97, bottom=0.03)

        # plots
        ax_u = tabFigure.add_subplot(gs[0, 0])
        ax_v = tabFigure.add_subplot(gs[0, 1])
        ax_w = tabFigure.add_subplot(gs[0, 2])
        ax_flux_scalar = tabFigure.add_subplot(gs[0, 3])

        ax_u_rot = tabFigure.add_subplot(gs[1, 0])
        ax_v_rot = tabFigure.add_subplot(gs[1, 1])
        ax_w_rot = tabFigure.add_subplot(gs[1, 2])

        ax_w_rot_turb = tabFigure.add_subplot(gs[2, 2])
        ax_flux_scalar_turb = tabFigure.add_subplot(gs[2, 3])

        ax_lag_search = tabFigure.add_subplot(gs[3, 0:2])

        axes_dict = {
            'ax_u': ax_u,
            'ax_v': ax_v,
            'ax_w': ax_w,
            'ax_flux_scalar': ax_flux_scalar,
            'ax_u_rot': ax_u_rot,
            'ax_v_rot': ax_v_rot,
            'ax_w_rot': ax_w_rot,
            'ax_w_rot_turb': ax_w_rot_turb,
            'ax_flux_scalar_turb': ax_flux_scalar_turb,
            'ax_lag_search': ax_lag_search
        }

        for key, ax in axes_dict.items():
            gui.plotfuncs.default_format(ax=ax, txt_xlabel=False)

        return axes_dict

    def create_ref_tab_menu(self):
        ref_frame, ref_layout = elements.add_frame_grid()

        # ----------------------------------------------------------------------------
        # Wind Variables
        # ----------------------------------------------------------------------------
        gui.elements.add_header_in_grid_row(layout=ref_layout, txt='Select Wind Components', row=0)

        drp_u_var = elements.grd_LabelDropdownPair(txt='u (m s-1)',
                                                   css_ids=['', 'cyan'],
                                                   layout=ref_layout,
                                                   row=1, col=0,
                                                   orientation='horiz')

        drp_v_var = elements.grd_LabelDropdownPair(txt='v (m s-1)',
                                                   css_ids=['', 'cyan'],
                                                   layout=ref_layout,
                                                   row=2, col=0,
                                                   orientation='horiz')

        drp_w_var = elements.grd_LabelDropdownPair(txt='w (m s-1)',
                                                   css_ids=['', 'cyan'],
                                                   layout=ref_layout,
                                                   row=3, col=0,
                                                   orientation='horiz')

        ref_layout.addWidget(qw.QLabel(), 4, 0, 1, 1)  # spacer

        # ----------------------------------------------------------------------------
        # Flux Variable
        # ----------------------------------------------------------------------------
        gui.elements.add_header_in_grid_row(layout=ref_layout, txt='Select Flux', row=5)

        drp_flux_scalar_var = elements.grd_LabelDropdownPair(txt='Flux Scalar',
                                                             css_ids=['', 'cyan'],
                                                             layout=ref_layout,
                                                             row=6, col=0,
                                                             orientation='horiz')

        ref_layout.addWidget(qw.QLabel(), 7, 0, 1, 1)  # spacer

        # ----------------------------------------------------------------------------
        # RUN
        # ----------------------------------------------------------------------------
        btn_init_wind_vars = elements.add_button_to_grid(grid_layout=ref_layout,
                                                         txt='Initialze Wind Variables',
                                                         css_id='btn_cat_ControlsRun',
                                                         row=8, col=0, rowspan=1, colspan=2)

        # ----------------------------------------------------------------------------
        # STRETCH
        # ----------------------------------------------------------------------------
        ref_layout.setRowStretch(9, 1)  # empty row
        # ref_layout.setColumnStretch(2, 1)  # empty row

        return drp_u_var, drp_v_var, drp_w_var, btn_init_wind_vars, ref_frame, drp_flux_scalar_var

    def update_refinements(self):
        self.drp_u_var.clear()
        self.drp_v_var.clear()
        self.drp_w_var.clear()
        self.drp_flux_scalar_var.clear()

        # Select first var if no default var is found in global vars
        default_u_ix = default_v_ix = default_w_ix = default_flux_scalar_ix = 0

        # Add all variables to drop-down lists to allow full flexibility yay
        for ix, colname_tuple in enumerate(self.data_df.columns):
            self.drp_u_var.addItem(self.colname_pretty_string[ix])
            self.drp_v_var.addItem(self.colname_pretty_string[ix])
            self.drp_w_var.addItem(self.colname_pretty_string[ix])
            self.drp_flux_scalar_var.addItem(self.colname_pretty_string[ix])

            # Check if column appears in the global vars
            if any(fnmatch.fnmatch(colname_tuple[0], vid)
                   for vid in WIND_U_HIRES):
                default_u_ix = ix
            elif any(fnmatch.fnmatch(colname_tuple[0], vid)
                     for vid in WIND_V_HIRES):
                default_v_ix = ix
            elif any(fnmatch.fnmatch(colname_tuple[0], vid)
                     for vid in WIND_W_HIRES):
                default_w_ix = ix
            elif any(fnmatch.fnmatch(colname_tuple[0], vid)
                     for vid in FLUX_SCALARS_CO2_HIRES):
                default_flux_scalar_ix = ix

        # Set dropdowns to found ix
        self.drp_u_var.setCurrentIndex(default_u_ix)
        self.drp_v_var.setCurrentIndex(default_v_ix)
        self.drp_w_var.setCurrentIndex(default_w_ix)
        self.drp_flux_scalar_var.setCurrentIndex(default_flux_scalar_ix)
        return None


class Run:

    # TODO but the rotation is applied sample-wise; TODO still 6-hour df
    # https://www.licor.com/env/support/EddyPro/topics/anemometer-tilt-correction.html
    # https://www.thespruceeats.com/the-greek-alphabet-1705558
    # https://stackoverflow.com/questions/10057854/inverse-of-tan-in-python-tan-1

    def __init__(self, data_df, drp_u, drp_v, drp_w, col_list_pretty, col_dict_tuples, fig, axes_dict,
                 drp_flux_scalar):
        super(Run, self).__init__()

        self.data_df = data_df.copy()
        self.col_list_pretty = col_list_pretty
        self.col_dict_tuples = col_dict_tuples
        self.fig = fig  # needed for redraw
        self.axes_dict = axes_dict  # adds to existing axis
        self.drp_u = drp_u
        self.drp_v = drp_v
        self.drp_w = drp_w
        self.drp_flux_scalar = drp_flux_scalar

        # Collect wind data in separate df
        self.df, self.u_col, self.v_col, self.w_col, self.flux_scalar_col = self.make_df()

        # Prepare df for results
        self.u_temp_col, self.v_temp_col, self.w_temp_col, self.u_rot_col, self.v_rot_col, self.w_rot_col, \
        self.w_rot_col_mean, self.w_rot_col_turb, self.flux_scalar_col_mean, self.flux_scalar_col_turb, \
        self.flux_scalar_col_flux = \
            self.init_new_cols()

        # Calculate rotation angles from mean wind
        self.angle_r1, self.angle_r2 = self.rot_angles_from_mean_wind()

        # Rotate high-resolution wind data
        self.df = self.rotate_wind()

        # Turbulent fluctuations
        self.calculate_turbulent_fluctuations()

        # Find max cov
        cov_max_shift, cov_max, lag_search_df = self.find_max_cov()

        # Shift to found max
        scalar_data_shifted = self.df[self.flux_scalar_col_turb].shift(int(cov_max_shift))
        cov = self.df[self.w_rot_col_turb].cov(scalar_data_shifted)
        # self.df[self.flux_scalar_col_flux] = self.df[self.w_rot_col_turb].cov(scalar_data_shifted)
        # cov and cov_max must be the same value

        # Plot
        plot_dict = self.make_plot_dict()
        self.make_plot(plot_dict=plot_dict)

        lag_search_df[['cov_abs', 'cov']].plot(ax=self.axes_dict['ax_lag_search'])
        self.fig.canvas.draw()
        self.fig.canvas.flush_events()

    def calculate_turbulent_fluctuations(self):

        self.df[self.w_rot_col_mean] = self.df[self.w_rot_col].mean()
        self.df[self.flux_scalar_col_mean] = self.df[self.flux_scalar_col].mean()

        self.df[self.w_rot_col_turb] = self.df[self.w_rot_col] - self.df[self.w_rot_col_mean]
        self.df[self.flux_scalar_col_turb] = self.df[self.flux_scalar_col] - self.df[self.flux_scalar_col_mean]
        return self

    def find_max_cov(self):
        """
        Find maximum absolute covariance between turbulent wind data
        and turbulent scalar data.
        """

        lag_search_df = pd.DataFrame()
        lag_search_df['shift'] = range(-200, 201)
        lag_search_df['cov'] = np.nan

        for ix, row in lag_search_df.iterrows():
            scalar_data_shifted = self.df[self.flux_scalar_col_turb].shift(int(row['shift']))
            cov = self.df[self.w_rot_col_turb].cov(scalar_data_shifted)
            lag_search_df.loc[lag_search_df['shift'] == row['shift'], 'cov'] = cov

        lag_search_df['cov_abs'] = lag_search_df['cov'].abs()
        cov_max_ix = lag_search_df['cov_abs'].idxmax()
        cov_max_shift = lag_search_df.iloc[cov_max_ix]['shift']
        cov_max = lag_search_df.iloc[cov_max_ix]['cov']

        return cov_max_shift, cov_max, lag_search_df

        # flux = self.df[self.flux_scalar_col_turb].shift(self.df[])
        # self.df[self.flux_scalar_col_flux] = self.df[self.w_rot_col_turb].cov(self.df[self.flux_scalar_col_turb])

    def rotate_wind(self):
        """
        Use rotation angles from mean wind to perform double rotation
        on high-resolution wind data
        """

        # Perform first rotation of coordinate system
        # Make v component zero --> mean of high-res v_temp_col becomes zero (or very close to)
        self.df[self.u_temp_col] = self.df[self.u_col] * math.cos(self.angle_r1) \
                                   + self.df[self.v_col] * math.sin(self.angle_r1)
        self.df[self.v_temp_col] = -self.df[self.u_col] * math.sin(self.angle_r1) \
                                   + self.df[self.v_col] * math.cos(self.angle_r1)
        self.df[self.w_temp_col] = self.df[self.w_col]

        # Perform second rotation of coordinate system
        # Make w component zero --> mean of high-res w_rot_col becomes zero (or very close to)
        self.df[self.u_rot_col] = self.df[self.u_temp_col] * math.cos(self.angle_r2) \
                                  + self.df[self.w_temp_col] * math.sin(self.angle_r2)
        self.df[self.v_rot_col] = self.df[self.v_temp_col]
        self.df[self.w_rot_col] = -self.df[self.u_temp_col] * math.sin(self.angle_r2) \
                                  + self.df[self.w_temp_col] * math.cos(self.angle_r2)

        return self.df

    def rot_angles_from_mean_wind(self):
        """
        Calculate rotation angles for double rotation from mean wind

        The rotation angles are calculated from mean wind, but are later
        applied sample-wise to the full high-resolution data (typically 20Hz
        for wind data).

        Note that rotation angles are given in radians.

        First rotation angle:
            thita = tan-1 (v_mean / u_mean)

        Second rotation angle:
            phi = tan-1 (w_temp / u_temp)

        """

        u_mean = self.df[self.u_col].mean()
        v_mean = self.df[self.v_col].mean()
        w_mean = self.df[self.w_col].mean()

        # First rotation angle, in radians
        angle_r1 = math.atan(v_mean / u_mean)

        # Perform first rotation of coordinate system for mean wind
        # Make v component of mean wind zero --> v_temp becomes zero
        u_temp = u_mean * math.cos(angle_r1) + v_mean * math.sin(angle_r1)
        v_temp = -u_mean * math.sin(angle_r1) + v_mean * math.cos(angle_r1)
        w_temp = w_mean

        # Second rotation angle, in radians
        angle_r2 = math.atan(w_temp / u_temp)

        # For calculating the rotation angles, it is not necessary to perform the second
        # rotation of the coordinate system for mean wind
        # Make v component zero, vm = 0
        # u_rot = u_temp * math.degrees(math.cos(angle_r2)) + w_temp * math.degrees(math.sin(angle_r2))
        # v_rot = v_temp
        # w_rot = -u_temp * math.degrees(math.sin(angle_r2)) + w_temp * math.degrees(math.cos(angle_r2))

        return angle_r1, angle_r2

    def make_plot_dict(self):
        # Collect vars and ax on which they are plotted so we can loop
        plot_dict = {
            self.u_col: self.axes_dict['ax_u'],
            self.v_col: self.axes_dict['ax_v'],
            self.w_col: self.axes_dict['ax_w'],
            self.flux_scalar_col: self.axes_dict['ax_flux_scalar'],
            self.u_rot_col: self.axes_dict['ax_u_rot'],
            self.v_rot_col: self.axes_dict['ax_v_rot'],
            self.w_rot_col: self.axes_dict['ax_w_rot'],
            self.w_rot_col_turb: self.axes_dict['ax_w_rot_turb'],
            self.flux_scalar_col_turb: self.axes_dict['ax_flux_scalar_turb']
        }
        return plot_dict

    def make_plot(self, plot_dict):

        for col, ax in plot_dict.items():
            ax.clear()

            # Plot data
            ax.plot_date(x=self.df.index, y=self.df[col], color='#78909C', alpha=0.5, ls='-',
                         marker='o', markeredgecolor='none', ms=2, zorder=98, label='measured')

            # Zero line if needed
            if (self.df[col].min() < 0) and (self.df[col].max() > 0):
                ax.axhline(y=0, color='#444444', ls='-', lw=1, alpha=0.8, zorder=97)

            # Show mean line
            ax.axhline(y=self.df[col].mean(), color='#29B6F6', ls='-', lw=2, alpha=0.7, zorder=99, label='mean')

            # Add texts
            ax.text(0.02, 0.96, '{}'.format(col[0]),
                    size=14, color='#FFCA28', backgroundcolor='none', transform=ax.transAxes, alpha=0.6,
                    horizontalalignment='left', verticalalignment='top')

            gui.plotfuncs.default_legend(ax=ax, loc='upper right', bbox_to_anchor=(1, 1))

            self.fig.canvas.draw()
            self.fig.canvas.flush_events()

    def init_new_cols(self):
        """ Add new wind rotation columns to df as empty """

        # Temp vars used during wind rotation
        u_temp_col = (self.u_col[0] + '_temp', self.u_col[1])
        v_temp_col = (self.v_col[0] + '_temp', self.v_col[1])
        w_temp_col = (self.w_col[0] + '_temp', self.w_col[1])
        self.df[u_temp_col] = self.df[v_temp_col] = self.df[w_temp_col] = np.nan

        # Rotated wind
        u_rot_col = (self.u_col[0] + '_rot', self.u_col[1])
        v_rot_col = (self.v_col[0] + '_rot', self.v_col[1])
        w_rot_col = (self.w_col[0] + '_rot', self.w_col[1])
        self.df[u_rot_col] = self.df[v_rot_col] = self.df[w_rot_col] = np.nan

        w_rot_col_mean = (self.w_col[0] + '_rot_mean', self.w_col[1])
        w_rot_col_turb = (self.w_col[0] + '_rot_turb', self.w_col[1])

        flux_scalar_col_mean = (self.flux_scalar_col[0] + '_rot_mean', self.flux_scalar_col[1])
        flux_scalar_col_turb = (self.flux_scalar_col[0] + '_rot_turb', self.flux_scalar_col[1])

        flux_scalar_col_flux = (self.flux_scalar_col[0] + '_rot_turb_flux', self.flux_scalar_col[1])

        return u_temp_col, v_temp_col, w_temp_col, u_rot_col, v_rot_col, w_rot_col, \
               w_rot_col_mean, w_rot_col_turb, flux_scalar_col_mean, flux_scalar_col_turb, flux_scalar_col_flux

    def make_df(self):
        # Wind components
        u_ix = self.drp_u
        v_ix = self.drp_v
        w_ix = self.drp_w
        flux_scalar_ix = self.drp_flux_scalar
        u_col = self.col_dict_tuples[u_ix]
        v_col = self.col_dict_tuples[v_ix]
        w_col = self.col_dict_tuples[w_ix]
        flux_scalar_col = self.col_dict_tuples[flux_scalar_ix]

        # Drop duplicate column names from df
        # Necessary in case e.g. u is the same as v
        plot_list = [u_col, v_col, w_col, flux_scalar_col]
        plot_list_no_duplicates = []
        for ix, x in enumerate(plot_list):
            if plot_list[ix] in plot_list_no_duplicates:
                pass
            else:
                plot_list_no_duplicates.append(plot_list[ix])
        df = self.data_df[plot_list_no_duplicates]
        # # Elegant solution to check for duplicates in column index, but slow
        # # https://stackoverflow.com/questions/54373153/removing-duplicate-columns-from-a-pandas-dataframe
        # plot_df = plot_df.T.drop_duplicates().T
        df = df.dropna()
        return df, u_col, v_col, w_col, flux_scalar_col
