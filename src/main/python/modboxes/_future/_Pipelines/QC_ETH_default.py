"""

========================================
Quality Control _Pipelines //////////////
========================================

Chain various methods and functions together.

For QC flags, first a standardized column name is created so they can be easily found
by a distinct prefix 'qcflag_'. Then, all 'qcflag_' columns are added to the main data_df
where all variables are stored.


"""

import fnmatch

import gui.elements
import logger
from gui import elements
from utils.vargroups import *


class AddControls():
    """
    Creates the gui control elements and their handles for usage.
    """

    def __init__(self, opt_stack, opt_frame, opt_layout, ref_stack):
        self.opt_stack = opt_stack
        self.opt_frame = opt_frame
        self.opt_layout = opt_layout
        self.ref_stack = ref_stack

        self.add_option()
        self.add_refinements()
        # self.get_handles()

    def get_handles(self):
        return self.btn_pn_opt_PipeQC, \
               self.drp_pn_ref_PipeQC_flux, \
               self.drp_pn_ref_PipeQC_foundFlag_SSITC, \
               self.drp_pn_ref_PipeQC_foundFlag_OutAbsLim, \
               self.drp_pn_ref_PipeQC_foundFlag_scf, \
               self.btn_pn_ref_PipeQC_generateOverallFlag

    def add_option(self):
        # Option Button: [3] Running Median
        self.btn_pn_opt_PipeQC = elements.add_button_to_grid(grid_layout=self.opt_layout,
                                                             txt='QC Pipeline - ETH Default', css_id='',
                                                             row=1, col=0, rowspan=1, colspan=1)
        self.opt_stack.addWidget(self.opt_frame)

    def add_refinements(self):
        ref_frame, ref_layout = elements.add_frame_grid()
        self.ref_stack.addWidget(ref_frame)

        gui.elements.add_header_to_grid_top(layout=ref_layout, txt='PN: QC Pipeline - ETH Default')

        self.drp_pn_ref_PipeQC_flux = elements.grd_LabelDropdownPair(txt='Generate Overall QC Flag For Flux',
                                                                     css_ids=['', 'cyan'],
                                                                     layout=ref_layout,
                                                                     row=1, col=0,
                                                                     orientation='horiz')

        self.drp_pn_ref_PipeQC_foundFlag_SSITC = elements.grd_LabelDropdownPair(txt='SSITC: EddyPro Default Flag',
                                                                                css_ids=['', 'cyan'],
                                                                                layout=ref_layout,
                                                                                row=2, col=0,
                                                                                orientation='horiz')

        self.drp_pn_ref_PipeQC_foundFlag_OutAbsLim = elements.grd_LabelDropdownPair(txt='Outlier: Absolute Limits',
                                                                                    css_ids=['', 'cyan'],
                                                                                    layout=ref_layout,
                                                                                    row=3, col=0,
                                                                                    orientation='horiz')

        self.drp_pn_ref_PipeQC_foundFlag_scf = elements.grd_LabelDropdownPair(txt='SCF: Spectral Correction Factor',
                                                                              css_ids=['', 'cyan'],
                                                                              layout=ref_layout,
                                                                              row=4, col=0,
                                                                              orientation='horiz')

        self.btn_pn_ref_PipeQC_generateOverallFlag = elements.add_button_to_grid(grid_layout=ref_layout,
                                                                                 txt='Generate Overall QC Flag',
                                                                                 css_id='',
                                                                                 row=5, col=0, rowspan=1, colspan=2)

        ref_layout.setRowStretch(6, 1)

        return self


class Call:
    def __init__(self, data_df, drp_selected_flux, collist_pretty, coldict_tuples, drp_ssitc, drp_scf, drp_abslim):
        """ XXX """
        logger.log(name='_Pipelines / Amp QC Flag', dict={}, highlight=True)  # Log info

        self.data_df = data_df
        self.coldict_tuples = coldict_tuples
        self.collist_pretty = collist_pretty

        self.flux_col = self.get_colname(pretty_str=drp_selected_flux)
        self.ssitc_col = self.get_colname(pretty_str=drp_ssitc)
        self.abslim_col = self.get_colname(pretty_str=drp_abslim)
        self.scf_col = self.get_colname(pretty_str=drp_scf)

        self.qc_settings = self.get_qc_settings()

    def get_colname(self, pretty_str):
        """
        Get column name of selected variable

        In e.g. dropdown menus, not the real column name is used to show variable
        names. Instead, these menus show a more readable variable name (pretty_str).
        This method checks the list of "readable variable names" and then translates
        the selected variable to their respective "real" column name. The real name
        is then used to directly access the data in the DataFrame.
        """
        colname_pretty_ix = self.collist_pretty.index(pretty_str)  # Index in list of all variables
        # Full column name (tuple), to directly access data in df
        colname_tuple = self.coldict_tuples[colname_pretty_ix]
        return colname_tuple

    def get_qc_settings(self):
        qc_settings_dict = {'scf_upper_limit': 99999,
                            'scf_lower_limit': 2}

        # Absolute flux limits
        if any(fnmatch.fnmatchcase(self.flux_col[0], ids) for ids in FLUXES_GENERAL_CO2):
            detected_flux = 'CO2 flux'
            qc_settings_dict['abs_upper_limit'] = 50
            qc_settings_dict['abs_lower_limit'] = -50
        elif any(fnmatch.fnmatchcase(self.flux_col[0], ids) for ids in FLUXES_GENERAL_H):
            detected_flux = 'H (sensible heat flux)'
            qc_settings_dict['abs_upper_limit'] = 800
            qc_settings_dict['abs_lower_limit'] = -200
        elif any(fnmatch.fnmatchcase(self.flux_col[0], ids) for ids in FLUXES_GENERAL_LE):
            detected_flux = 'LE (latent evaporation flux)'
            qc_settings_dict['abs_upper_limit'] = 1000
            qc_settings_dict['abs_lower_limit'] = -200
        elif any(fnmatch.fnmatchcase(self.flux_col[0], ids) for ids in FLUXES_GENERAL_H2O):
            detected_flux = 'H2O flux'
            qc_settings_dict['abs_upper_limit'] = 50
            qc_settings_dict['abs_lower_limit'] = -20
        elif any(fnmatch.fnmatchcase(self.flux_col[0], ids) for ids in FLUXES_GENERAL_ET):
            detected_flux = 'ET flux'
            qc_settings_dict['abs_upper_limit'] = 3
            qc_settings_dict['abs_lower_limit'] = -1
        elif any(fnmatch.fnmatchcase(self.flux_col[0], ids) for ids in FLUXES_GENERAL_N2O):
            detected_flux = 'N2O flux'
            qc_settings_dict['abs_upper_limit'] = 0.1
            qc_settings_dict['abs_lower_limit'] = -0.05
        elif any(fnmatch.fnmatchcase(self.flux_col[0], ids) for ids in FLUXES_GENERAL_CH4):
            detected_flux = 'CH4 flux'
            qc_settings_dict['abs_upper_limit'] = 0.8
            qc_settings_dict['abs_lower_limit'] = -0.4
        else:
            detected_flux = '-none-'

        logger.log(name='', dict={'selected flux column': self.flux_col,
                                  'detected flux': detected_flux}, highlight=False)  # Log info
        logger.log(name='', dict=qc_settings_dict, highlight=False)  # Log info
        return qc_settings_dict
