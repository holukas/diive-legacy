# -*- coding: utf-8 -*-
"""

OFF-SEASON UPTAKE CORRECTION
============================
Abbreviation: OSUC

Needed inputs in original R code:
---------------------------------
    Fc = tmp$co2_flux
    qc = tmp$co2_molar_density
    u = tmp$wind_speed
    ustar = tmp$u.
    Tv = tmp$sonic_temperature
    rho.v = tmp$water_vapor_density
    rho.a = tmp$air_density
    fraction = 0.085
    subset=!is.na(tmp$status_code_75_mean)

Original R code:

        # Author:			Werner Eugster
        # Date:				2016-09-14

        setwd("A:/FLUXES/CH-DAV/__PI dataset production")

        M_AIR <- 0.028965						# molar mass of dry air	[kg/mol]
        M_H2O <- 0.018							# molar mass of water vapor	[kg/mol]


                # -------------------------------------------------------------------
                # The 10_FINAL files have 3 header lines, where the 1st line is the
                # header and lines 2 and 3 have to be skipped. This function reads
                # such a file.

        read.FINAL <- function (infile) {
            header <- read.csv(infile, nrow=1, skip=2)
            data <- read.csv(infile, header=FALSE, skip=3)
            data[data==-9999] <- NA
            colnames(data) <- colnames(header)
            #data$TIMESTAMP <- as.POSIXlt(data$TIMESTAMP, tz="UTC")
            return(data)
        }

                # -------------------------------------------------------------------
                # For the Davos fluxes we still lack a Burba correction
                # The function below uses the equation that we used for Seebodenalp
                # and published in Rogiers et al. (2008) Global Change Biology, but
                # with the default correction fraction published by JÃ¤rvi et al. (2009)
                # Boreal Environmental Research
                # GCB: doi: 10.1111/j.1365-2486.2008.01680.x
                # BER: Vol 14: 499--514

        Burba.correction <- function (Fc, qc, u, ustar, Tv, rho.v, rho.a, fraction=0.085, subset=NULL) {
                # Fc = measured CO2 flux (Âµmol/m2/s)
                # qc = CO2 density  (Âµmol/m3)
            # u  = horizontal wind speed (m/s)
            # ustar = friction velocity (m/s)
            # Tv = virtual sonic temperature (K)

                # ra = aerodynamic resistance which is computed from horizontal wind
                #     speed and ustar, hence these two in the arguments list
                # Ta = air temperature (Â°C)
                # Ts = instrument surface temperature computed from Ta
                # rho.v = water vapor density (kg/m3)
                # rho.d = dry air density (kg/m3)
                # fraction = fraction of the correction that Burba suggests for a
                #Â      vertically mounted open-path Licor 7500
                # subset = if NULL, all records are given a Burba correction, but
                # 		if this is a logical vector, then only the records given in
                #			subset will be used (important for year 2005)
                #
                # the units that EddyPro output provides are:
                # air_density kg+1m-3
                # co2_flux Âµmol+1s-1m-2
                # co2_molar_density mmol+1m-3
                # h2o_molar_density mmol+1m-3
                # sonic_temperature K
                # specific_humidity kg+1kg-1
                # u_rot m+1s-1
                # wind_speed m+1s-1
                # water_vapor_density kg+1m-3
                # u. m+1s-1 (this is u*)


          if(fraction < 0 | fraction > 1){
            stop("Burba.correction: fraction must be in the range 0 to 1")
          }
            # Fc already is in Âµmol/m2/s
          qc <- qc*1000				# from mmol/m3 to Âµmol/m3
          Ta <- Tv - 273.15
          Ts <- 0.0025*Ta*Ta + 0.90*Ta + 2.07
          ra <- u/ustar/ustar
          rho.d <- rho.a + rho.v*(M_AIR/M_H2O -1)
          Fc.corr <- fraction * ((Ts-Ta)*qc)/(ra*(Ta+273.15))*(1+1.6077*rho.v/rho.d)
          if(!is.null(subset)){
            Fc.corr[!subset] <- 0
          }
          Fc.corr <- Fc + Fc.corr
          return(Fc.corr)
        }

                # -------------------------------------------------------------------


                #Â (1) CHAMAU (formerly DAVOS)

        DATADIR <- "2005-2016_IRGA75_Level-1_merged"
        FILES <- dir(DATADIR)

        counter <- 1
        for(file in FILES){
          print(sprintf("reading %s ...", file))
          tmp <- read.FINAL(sprintf("%s/%s", DATADIR, file))
                # apply the Burba correction if needed
          if("status_code_75_mean" %in% names(tmp)){
            tmp$co2_flux_Burba <- Burba.correction(tmp$co2_flux, tmp$co2_molar_density, tmp$wind_speed, tmp$u., tmp$sonic_temperature, tmp$water_vapor_density, tmp$air_density, fraction=0.085, subset=!is.na(tmp$status_code_75_mean))
            tmp$co2_flux <- tmp$co2_flux_Burba
          }
          OUTFILE <- sub(".csv", "-Burba.csv", file)
          ii <- grep("Burba", names(tmp))
          if(length(ii)==1){
            tmp <- tmp[,-ii]
          }
          write.csv(tmp, OUTFILE, quote=FALSE, row.names=FALSE)
            counter <- counter +1
        }


"""

import fnmatch
import os
from pathlib import Path

import matplotlib.dates as mdates
import matplotlib.gridspec as gridspec
import numpy as np
import pandas as pd
from PyQt5 import QtGui
from PyQt5 import QtWidgets as qtw
from PyQt5.QtGui import QDoubleValidator, QIntValidator
from scipy.optimize import minimize_scalar

import gui.add_to_layout
import gui.base
import gui.make
from pkgs import dfun
import pkgs.dfun.frames
import pkgs.dfun.times
import gui
import gui.tableview
import logger
import modboxes.plots.styles.LightTheme as theme
from utils.vargroups import *


class addContent(gui.base.buildTab):
    """Build new tab and populate with contents"""

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id, tab_template='SVaVaP')

        # Tab icon
        self.TabWidget.setTabIcon(self.tab_ix, QtGui.QIcon(app_obj.ctx.tab_icon_eddy_covariance))

        # Add settings menu contents
        self.btn_apply, self.btn_add_as_new_var, self.drp_co2_molar_density_col, \
        self.drp_horiz_windspeed_col, self.drp_ustar_col, self.drp_virt_sonic_temp_col, \
        self.drp_water_vapor_density_col, self.drp_air_density_col, \
        self.btn_optimize, self.lne_num_bootstrap_runs, self.lne_num_class_var_classes, \
        self.tvw_corr_fractions, self.btn_load_cf_from_file, self.btn_save_cf_to_file, \
        self.drp_separate_day_night, self.drp_define_nighttime_based_on, self.drp_set_nighttime_if, \
        self.lne_nighttime_threshold, self.drp_class_var_col = \
            self.add_settings_fields()

        # Add variables required in settings
        self.populate_settings_fields()

        # # Add available variables to variable lists
        gui.base.buildTab.populate_multiple_variable_lists(col_list_pretty=self.col_list_pretty,
                                                           df=self.tab_data_df,
                                                           lists=[self.lst_varlist_target,
                                                                  self.lst_varlist_reference])

        # # Add plots
        # self.populate_plot_area()

    def populate_plot_area(self):
        pass

    def populate_settings_fields(self):
        # self.drp_co2_measured_flux_col.clear()
        self.drp_co2_molar_density_col.clear()
        self.drp_horiz_windspeed_col.clear()
        self.drp_ustar_col.clear()
        self.drp_virt_sonic_temp_col.clear()
        self.drp_water_vapor_density_col.clear()
        self.drp_air_density_col.clear()
        self.drp_define_nighttime_based_on.clear()
        self.drp_class_var_col.clear()

        default_co2_measured_flux_ix \
            = default_co2_molar_density_col_ix \
            = default_horiz_windspeed_ix \
            = default_ustar_ix \
            = default_virt_sonic_temp_ix \
            = default_water_vapor_density_ix \
            = default_air_density_ix \
            = default_define_nighttime_based_on_ix \
            = default_class_var_ix \
            = 0

        for ix, col in enumerate(self.tab_data_df):
            pretty = self.col_list_pretty[ix]
            self.drp_ustar_col.addItem(pretty)
            self.drp_air_density_col.addItem(pretty)
            self.drp_horiz_windspeed_col.addItem(pretty)
            self.drp_water_vapor_density_col.addItem(pretty)
            self.drp_co2_molar_density_col.addItem(pretty)
            self.drp_virt_sonic_temp_col.addItem(pretty)
            self.drp_define_nighttime_based_on.addItem(pretty)
            self.drp_class_var_col.addItem(pretty)

            # Check if column appears in the global vars
            if any(fnmatch.fnmatch(col[0], vid) for vid in FLUXES_GENERAL_CO2):
                default_co2_measured_flux_ix = ix
            if any(fnmatch.fnmatch(col[0], vid) for vid in MOLAR_DENSITY_CO2):
                default_co2_molar_density_col_ix = ix
            if any(fnmatch.fnmatch(col[0], vid) for vid in WIND_SPEED):
                default_horiz_windspeed_ix = ix
                default_class_var_ix = ix
            # if any(re.match(col[0], vid) for vid in USTAR_EDDYPRO):
            #     default_ustar_ix = ix
            if any(fnmatch.fnmatch(col[0], vid) for vid in TEMP_SONIC_VIRT):
                default_virt_sonic_temp_ix = ix
            if any(fnmatch.fnmatch(col[0], vid) for vid in H2O_VAPOR_DENSITY):
                default_water_vapor_density_ix = ix
            if any(fnmatch.fnmatch(col[0], vid) for vid in AIR_DENSITY):
                default_air_density_ix = ix
            if any(fnmatch.fnmatch(col[0], vid) for vid in NIGHTTIME_DETECTION):
                default_define_nighttime_based_on_ix = ix

        # Pre-selected variables in dropdowns
        # self.drp_co2_measured_flux_col.setCurrentIndex(default_co2_measured_flux_ix)
        self.drp_co2_molar_density_col.setCurrentIndex(default_co2_molar_density_col_ix)
        self.drp_horiz_windspeed_col.setCurrentIndex(default_horiz_windspeed_ix)
        self.drp_class_var_col.setCurrentIndex(default_class_var_ix)
        self.drp_ustar_col.setCurrentIndex(default_ustar_ix)
        self.drp_virt_sonic_temp_col.setCurrentIndex(default_virt_sonic_temp_ix)
        self.drp_water_vapor_density_col.setCurrentIndex(default_water_vapor_density_ix)
        self.drp_air_density_col.setCurrentIndex(default_air_density_ix)
        self.drp_define_nighttime_based_on.setCurrentIndex(default_define_nighttime_based_on_ix)

    def add_axes(self):
        pass

    def add_settings_fields(self):
        onlyDouble = QDoubleValidator()  # Allow only doubles as input (floats, integers, no text)
        onlyInt = QIntValidator()  # Allow only integers as input

        gui.elements.add_header_subheader_to_grid_top(
            layout=self.sett_layout,
            row=0, col=0, rowspan=1, colspan=3,
            txt=["Off-season Uptake Correction",
                 "Eddy Covariance"])
        gui.add_to_layout.add_spacer_item_to_grid(
            row=1, col=2, layout=self.sett_layout)

        # Input variables
        # ===============
        gui.elements.add_header_in_grid_row(
            row=2, layout=self.sett_layout, txt='Input Variables', col=0)

        drp_co2_molar_density_col = gui.elements.grd_LabelDropdownPair(
            txt='OPEN PATH: CO2 Molar Density (mmol m-3)', css_ids=['lbl_bold1', ''], layout=self.sett_layout,
            row=3, col=0, orientation='horiz')

        drp_water_vapor_density_col = gui.elements.grd_LabelDropdownPair(
            txt='OPEN PATH: Water Vapor Density (kg m-3)', css_ids=['lbl_bold1', ''], layout=self.sett_layout,
            row=4, col=0, orientation='horiz')

        drp_horiz_windspeed_col = gui.elements.grd_LabelDropdownPair(
            txt='Horizontal Wind Speed (m s-1)', css_ids=['lbl_bold1', ''], layout=self.sett_layout,
            row=5, col=0, orientation='horiz')

        drp_ustar_col = gui.elements.grd_LabelDropdownPair(
            txt='Friction velocity, ustar (m s-1)', css_ids=['lbl_bold1', ''], layout=self.sett_layout,
            row=6, col=0, orientation='horiz')

        drp_virt_sonic_temp_col = gui.elements.grd_LabelDropdownPair(
            txt='Virtual Sonic Temperature (K)', css_ids=['lbl_bold1', ''], layout=self.sett_layout,
            row=7, col=0, orientation='horiz')

        drp_air_density_col = gui.elements.grd_LabelDropdownPair(
            txt='Air Density (kg m-3)', css_ids=['lbl_bold1', ''], layout=self.sett_layout,
            row=8, col=0, orientation='horiz')

        gui.add_to_layout.add_spacer_item_to_grid(
            row=9, col=0, layout=self.sett_layout)

        # Day / night
        gui.elements.add_header_in_grid_row(
            row=10, layout=self.sett_layout, txt='Day / Night')
        drp_separate_day_night = gui.elements.grd_LabelDropdownPair(
            txt='Separate For Day / Night', css_ids=['', 'cyan'], layout=self.sett_layout,
            row=11, col=0, orientation='horiz')
        drp_separate_day_night.addItems(['Yes', 'No'])
        drp_separate_day_night.setDisabled(True)  # Disabled for now
        drp_define_nighttime_based_on = gui.elements.grd_LabelDropdownPair(
            txt='Define Nighttime Based On', css_ids=['', 'cyan'], layout=self.sett_layout,
            row=12, col=0, orientation='horiz')
        drp_set_nighttime_if = gui.elements.grd_LabelDropdownPair(
            txt='Set To Nighttime If', css_ids=['', 'cyan'], layout=self.sett_layout,
            row=13, col=0, orientation='horiz')
        drp_set_nighttime_if.addItems(['Smaller Than Threshold', 'Larger Than Threshold'])
        lne_nighttime_threshold = gui.elements.add_label_linedit_pair_to_grid(
            txt='Threshold', css_ids=['', 'cyan'], layout=self.sett_layout,
            row=14, col=0, orientation='horiz')
        lne_nighttime_threshold.setText('20')
        gui.add_to_layout.add_spacer_item_to_grid(self.sett_layout, 15, 0)

        # Settings
        # ========
        gui.elements.add_header_in_grid_row(
            row=16, layout=self.sett_layout, txt='Settings')

        lne_num_bootstrap_runs = gui.elements.add_label_linedit_pair_to_grid(
            row=17, col=0, txt='Number Of Bootstrap Runs',
            css_ids=['', 'cyan'], layout=self.sett_layout, orientation='horiz', )
        lne_num_bootstrap_runs.setText("3")
        lne_num_bootstrap_runs.setValidator(onlyInt)

        drp_class_var_col = gui.elements.grd_LabelDropdownPair(
            txt='Class Variable', css_ids=['', 'cyan'], layout=self.sett_layout,
            row=18, col=0, orientation='horiz')

        lne_num_class_var_classes = gui.elements.add_label_linedit_pair_to_grid(
            row=19, col=0, txt='Number Of Class Variable Classes',
            css_ids=['', 'cyan'], layout=self.sett_layout, orientation='horiz', )
        lne_num_class_var_classes.setText("7")
        lne_num_class_var_classes.setValidator(onlyInt)

        gui.add_to_layout.add_spacer_item_to_grid(
            row=20, col=0, layout=self.sett_layout)

        # Correction fractions
        # ====================
        gui.elements.add_header_in_grid_row(
            row=21, layout=self.sett_layout, txt='Correction Fractions')

        # args = dict(css_ids=['', 'cyan'], layout=self.sett_layout, col=0, orientation='horiz')

        # Add tableview
        tvw_corr_fractions = gui.tableview.add_to_grid(
            row=22, col=0, rowspan=1, colspan=3, layout=self.sett_layout)

        # txe_corr_fractions = gui_elements.add_textedit_to_grid(
        #     row=15, col=0, layout=self.sett_layout, rowspan=1, colspan=2)

        gui.add_to_layout.add_spacer_item_to_grid(
            row=23, col=0, layout=self.sett_layout)

        # Sub-layout for correction fraction buttons
        _frame = qtw.QFrame()
        _grid_layout = qtw.QGridLayout()
        _frame.setLayout(_grid_layout)
        btn_optimize = gui.elements.add_button_to_grid(
            row=0, col=0, grid_layout=_grid_layout, txt='Optimize', css_id='btn_teal', rowspan=1,
            colspan=1)
        btn_load_cf_from_file = gui.elements.add_button_to_grid(
            row=0, col=1, grid_layout=_grid_layout, txt='Load From File', css_id='', rowspan=1,
            colspan=1)
        btn_save_cf_to_file = gui.elements.add_button_to_grid(
            row=0, col=2, grid_layout=_grid_layout, txt='Save To File', css_id='', rowspan=1,
            colspan=1)
        gui.add_to_layout.add_spacer_item_to_grid(
            row=0, col=3, layout=_grid_layout, width=100, height=10)
        self.sett_layout.addWidget(_frame, 23, 0, 1, 3)

        gui.add_to_layout.add_spacer_item_to_grid(
            row=24, col=0, layout=self.sett_layout)
        btn_apply = gui.elements.add_button_to_grid(
            row=25, col=0, grid_layout=self.sett_layout, txt='Apply', css_id='', rowspan=1, colspan=2)
        btn_add_as_new_var = gui.elements.add_button_to_grid(
            row=26, col=0, grid_layout=self.sett_layout, txt='+ Add As New Var', css_id='btn_add_as_new_var',
            rowspan=1, colspan=2)

        self.sett_layout.setRowStretch(27, 1)
        return btn_apply, btn_add_as_new_var, drp_co2_molar_density_col, drp_horiz_windspeed_col, \
               drp_ustar_col, drp_virt_sonic_temp_col, drp_water_vapor_density_col, drp_air_density_col, \
               btn_optimize, lne_num_bootstrap_runs, lne_num_class_var_classes, tvw_corr_fractions, \
               btn_load_cf_from_file, btn_save_cf_to_file, \
               drp_separate_day_night, drp_define_nighttime_based_on, drp_set_nighttime_if, \
               lne_nighttime_threshold, drp_class_var_col


class Run(addContent):
    converted_exists = False
    sub_outdir = "eddy_covariance_offseason_uptake_correction"
    M_AIR = 0.028965  # molar mass of dry air	[kg/mol]
    M_H2O = 0.018  # molar mass of water vapor	[kg/mol]
    FREEZE_POINT_KELVIN = 273.15  # corresponds to 0°C

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id)
        logger.log(name='>>> Starting Eddy Covariance > Off-season Uptake Correction', dict={},
                   highlight=True)  # Log info
        self.sub_outdir = self.project_outdir / self.sub_outdir
        self.class_df = pd.DataFrame()
        self.correction_fractions_df = pd.DataFrame()
        self.axes_dict = self.make_axes_dict()
        self.target_col = None
        self.reference_col = None
        self.target_corrected_flux_col = None

        self.target_loaded = False
        self.target_corrected_loaded = False
        self.reference_loaded = False
        self.optimization_performed = False

        self.btn_optimize.setDisabled(True)
        self.btn_load_cf_from_file.setDisabled(True)
        self.btn_save_cf_to_file.setDisabled(True)
        self.btn_apply.setDisabled(True)
        self.btn_add_as_new_var.setDisabled(True)
        self.lst_varlist_reference.setDisabled(True)

    def enable_disable_buttons(self):
        if self.target_loaded:
            self.lst_varlist_reference.setEnabled(True)
            self.btn_load_cf_from_file.setEnabled(True)
        else:
            self.lst_varlist_reference.setDisabled(True)
            self.btn_load_cf_from_file.setDisabled(True)

        if self.reference_loaded:
            pass

        if self.target_loaded and self.reference_loaded:
            self.btn_optimize.setEnabled(True)
            self.btn_save_cf_to_file.setEnabled(True)
        else:
            self.btn_optimize.setDisabled(True)
            self.btn_save_cf_to_file.setDisabled(True)

        if self.optimization_performed:
            self.btn_apply.setEnabled(True)
            self.btn_save_cf_to_file.setEnabled(True)
        else:
            self.btn_apply.setDisabled(True)
            self.btn_save_cf_to_file.setDisabled(True)

        if self.target_corrected_loaded:
            self.btn_add_as_new_var.setEnabled(True)
        else:
            self.btn_add_as_new_var.setDisabled(True)

    def select_target(self):
        """Select target var from list, in this case the open path fluxes"""
        self.remove_target()  # Reset target data
        self.set_target_col()
        self.class_df = self.init_class_df()
        if self.reference_loaded:  # Add reference data that was already selected
            self.class_df[self.reference_col] = self.tab_data_df[[self.reference_col]].copy()
        self.target_loaded = True
        self.target_corrected_loaded = False
        self.optimization_performed = False
        self.enable_disable_buttons()
        self.plot_data()

    def select_reference(self):
        """Select reference var from list, in this case the closed path fluxes"""
        self.remove_reference()  # Reset reference; remove previous reference data
        self.set_reference_col()  # Set new reference
        self.class_df[self.reference_col] = self.tab_data_df[[self.reference_col]].copy()
        if self.target_loaded:  # Add reference data that was already selected
            self.class_df[self.target_col] = self.tab_data_df[[self.target_col]].copy()
        self.reference_loaded = True
        self.target_corrected_loaded = False
        self.optimization_performed = False
        self.enable_disable_buttons()
        self.plot_data()

    def set_reference_col(self):
        """Get column name of reference var from reference var list"""
        reference_var = self.lst_varlist_reference.selectedIndexes()
        reference_var_ix = reference_var[0].row()
        self.reference_col = self.col_dict_tuples[reference_var_ix]

    def remove_reference(self):
        """Remove previous reference data from class_df"""
        if self.reference_loaded:
            self.class_df.drop(self.reference_col, axis=1, inplace=True)

    def set_target_col(self):
        """Get column name of target var from target var list"""
        target_var = self.lst_varlist_target.selectedIndexes()
        target_var_ix = target_var[0].row()
        self.target_col = self.col_dict_tuples[target_var_ix]

    def remove_target(self):
        """Remove previous target data from class_df"""
        if self.target_loaded:
            self.class_df.drop(self.target_col, axis=1, inplace=True)

    def get_settings_from_fields(self):
        self.co2_molar_density_col = self.get_col_from_drp_pretty(drp=self.drp_co2_molar_density_col)
        self.horiz_windspeed_col = self.get_col_from_drp_pretty(drp=self.drp_horiz_windspeed_col)
        self.class_var_col = self.get_col_from_drp_pretty(drp=self.drp_class_var_col)
        self.ustar_col = self.get_col_from_drp_pretty(drp=self.drp_ustar_col)
        self.virt_sonic_temp_col = self.get_col_from_drp_pretty(drp=self.drp_virt_sonic_temp_col)
        self.water_vapor_density_col = self.get_col_from_drp_pretty(drp=self.drp_water_vapor_density_col)
        self.air_density_col = self.get_col_from_drp_pretty(drp=self.drp_air_density_col)
        self.num_bootstrap_runs = int(self.lne_num_bootstrap_runs.text())
        self.num_class_var_classes = int(self.lne_num_class_var_classes.text())

        self.nighttime_col = self.get_col_from_drp_pretty(drp=self.drp_define_nighttime_based_on)
        # self.separate_day_night = self.drp_separate_day_night.currentText()
        self.set_nighttime_if = self.drp_set_nighttime_if.currentText()
        self.nighttime_threshold = int(self.lne_nighttime_threshold.text())

    def get_col_from_drp_pretty(self, drp):
        """ Set target to selected variable. """
        target_pretty = drp.currentText()
        target_pretty_ix = self.col_list_pretty.index(target_pretty)
        target_col = self.col_dict_tuples[target_pretty_ix]
        return target_col

    def set_colnames(self):
        self.target_corrected_flux_col = (f"{self.target_col[0]}+OSUC",
                                          self.target_col[1])
        self.class_var_group_col = ('classvar_group', '[group#]')
        self.corr_fraction_col = ('corr_fraction', '[#]')
        # self.rmse = ('rmse', '[corr-reference]')
        self.abc_results_col = ('abc_results', '[aux]')  # Intermediate step in correction equation
        self.is_daytime_col = ('is_daytime', '[1=yes]')

    def init_class_df(self):
        return self.tab_data_df[[self.target_col]].copy()
        # self.class_df[self.co2_corrected_flux_col] = np.nan

    def expand_class_df(self):
        """Add required vars to df"""
        cols = [self.co2_molar_density_col,
                self.horiz_windspeed_col,
                self.ustar_col,
                self.virt_sonic_temp_col,
                self.water_vapor_density_col,
                self.air_density_col,
                self.nighttime_col,
                self.class_var_col]
        for col in cols:
            self.class_df[col] = self.tab_data_df[col].copy()

    def apply_correction_fraction(self):
        """Apply correction fractions"""
        self.get_settings_from_fields()
        self.set_colnames()
        self.expand_class_df()
        self.class_df[self.class_var_group_col] = np.nan
        self.class_df[self.is_daytime_col] = np.nan
        self.class_df[self.corr_fraction_col] = np.nan
        self.add_daytime_group_col()

        # For each data point, look-up temperature group (separate for day/night)
        # and fraction bootstrap median for the group
        for ix, row in self.correction_fractions_df.iterrows():
            _filter_group = row
            _filter_group = (self.class_df[self.is_daytime_col] == row['daytime']) \
                            & (self.class_df[self.class_var_col] >= row['min_class_var']) \
                            & (self.class_df[self.class_var_col] <= row['max_class_var'])
            self.class_df.loc[_filter_group, self.class_var_group_col] = row['class_var_group']
            self.class_df.loc[_filter_group, self.corr_fraction_col] = row['fraction_median']

        self.class_df[self.abc_results_col] = self.calc_params(df=self.class_df)

        # fraction = float(self.lne_corr_fraction.text())
        self.class_df[('fccorr', '[aux]')] = self.class_df[self.abc_results_col] \
            .multiply(self.class_df[self.corr_fraction_col])
        self.class_df[self.target_corrected_flux_col] = self.class_df[self.target_col] \
                                                        + self.class_df[('fccorr', '[aux]')]

        # # Root mean square error: reference - corrected target
        # self.class_df[self.rmse] = np.sqrt((self.class_df[self.target_corrected_flux_col]
        #                                     - self.class_df[self.reference_col]) ** 2)
        #
        # # RMSE average per group
        # self.correction_fractions_df['rmse_final'] = np.nan
        # _grouped = self.class_df.groupby(self.temp_group)
        # for _group, _group_df in _grouped:
        #     self.correction_fractions_df.loc[_group, 'rmse_final'] = _group_df[self.rmse].mean()

        self.target_corrected_loaded = True
        self.enable_disable_buttons()
        self.plot_data()

    def add_daytime_group_col(self):
        """Add column that indicates if daytime (1) or nighttime (0)"""
        nighttime_filter = None
        daytime_filter = None
        self.class_df[self.nighttime_col] = self.tab_data_df[self.nighttime_col]  # Add for grouping
        if self.set_nighttime_if == 'Smaller Than Threshold':
            nighttime_filter = self.class_df[self.nighttime_col] < self.nighttime_threshold
            daytime_filter = self.class_df[self.nighttime_col] >= self.nighttime_threshold
        elif self.set_nighttime_if == 'Larger Than Threshold':
            nighttime_filter = self.class_df[self.nighttime_col] > self.nighttime_threshold
            daytime_filter = self.class_df[self.nighttime_col] <= self.nighttime_threshold

        self.class_df.loc[nighttime_filter, [self.is_daytime_col]] = 0
        self.class_df.loc[daytime_filter, [self.is_daytime_col]] = 1

    def init_corr_fractions_df(self):
        """Initialize df that collects results for correction fractions
        - Needs to be initialized with a Multiindex
        - Multiindex consists of two indices: (1) daytime and (2) sonic temperature class
        """

        _list_class_var_classes = [*range(0, self.num_class_var_classes)]
        _iterables = [[1, 0], _list_class_var_classes]
        _multi_ix = pd.MultiIndex.from_product(_iterables, names=["daytime_ix", "sonic_temperature_class_ix"])
        corr_fractions_df = pd.DataFrame(index=_multi_ix)
        return corr_fractions_df

    def optimize(self):
        """Divide dataset into sonic temperature groups and bootstrap groups
        for the calculation of correction fraction medians"""

        self.get_settings_from_fields()
        self.set_colnames()
        self.expand_class_df()
        self.class_df[self.is_daytime_col] = np.nan
        self.correction_fractions_df = self.init_corr_fractions_df()

        # Add two grouping cols: daytime yes/no and class variable groups
        self.add_daytime_group_col()
        groups_df = self.class_df.dropna().copy()  # New df for grouping

        # (1) Separate into daytime and nighttime data
        _grouped_by_daynighttime = groups_df.groupby(self.is_daytime_col)
        for _group_daynighttime, _group_daynighttime_df in _grouped_by_daynighttime:

            # (2) Divide data into x class variable classes w/ same number of values
            _group_daynighttime_df[self.class_var_group_col] = \
                pd.qcut(_group_daynighttime_df[self.class_var_col],
                        q=self.num_class_var_classes, labels=False)

            # (3) Group by sonic temperature group membership
            _grouped_by_class_var = _group_daynighttime_df.groupby(self.class_var_group_col)

            for _group_class_var, _group_class_var_df in _grouped_by_class_var:
                print(_group_class_var_df)

                # Bootstrap group data
                bts_fractions = []
                bts_sum_of_squares = []
                num_vals = []
                for bts_run in range(0, self.num_bootstrap_runs):
                    num_rows = int(len(_group_class_var_df.index))
                    bts_sample_df = _group_class_var_df.sample(n=num_rows, replace=True)  # Take sample w/ replacement
                    bts_sample_df.sort_index(inplace=True)
                    bts_sample_df[self.abc_results_col] = self.calc_params(df=bts_sample_df)
                    result = self.optimize_fraction(df=bts_sample_df)
                    fraction = result.x
                    sum_of_squares = result.fun
                    bts_fractions.append(fraction)
                    bts_sum_of_squares.append(sum_of_squares)
                    num_vals.append(bts_sample_df[self.class_var_col].count())

                # ix_slice = pd.IndexSlice  # Create an object to more easily perform multi-index slicing
                # self.correction_fractions_df.loc[ix_slice[_group_daynighttime, _group_temp], f'daytime'] =\
                #     _group_daynighttime

                # self.correction_fractions_df = pd.DataFrame()  # Collects correction fraction aggs for each group
                location = tuple([_group_daynighttime, _group_class_var])
                self.correction_fractions_df.loc[location, f'daytime'] = _group_daynighttime
                self.correction_fractions_df.loc[location, f'class_var_group'] = _group_class_var
                self.correction_fractions_df.loc[location, f'min_class_var'] = _group_class_var_df[
                    self.class_var_col].min()
                self.correction_fractions_df.loc[location, f'max_class_var'] = _group_class_var_df[
                    self.class_var_col].max()
                self.correction_fractions_df.loc[location, f'fraction_median'] = np.median(bts_fractions)
                self.correction_fractions_df.loc[location, f'sum_of_squares_median'] = np.median(bts_sum_of_squares)
                self.correction_fractions_df.loc[location, f'num_vals_avg'] = np.mean(num_vals)
                self.correction_fractions_df.loc[location, f'fraction_Q25'] = np.quantile(bts_fractions, 0.25)
                self.correction_fractions_df.loc[location, f'fraction_Q75'] = np.quantile(bts_fractions, 0.75)
                self.correction_fractions_df.loc[location, f'bootstrap_runs'] = self.num_bootstrap_runs

        gui.tableview.show_df(df=self.correction_fractions_df, tableview=self.tvw_corr_fractions)

        self.optimization_performed = True
        self.plot_data()
        # self.btn_apply.setEnabled(True)
        self.enable_disable_buttons()

    def calc_params(self, df):
        # qc = CO2 density  (Âµmol/m3)
        # qc <- qc*1000 from mmol/m3 to µmol/m3
        # From mmol m-3 --> umol m-3
        qc = df[self.co2_molar_density_col].multiply(1000)

        # Ta <- Tv - 273.15
        ta = df[self.virt_sonic_temp_col].sub(273.15)

        # Ts = instrument surface temperature computed from Ta
        # Ts <- 0.0025*Ta*Ta + 0.90*Ta + 2.07
        ta_power2 = np.power((ta), 2)
        ts = ta_power2.multiply(0.0025) + ta.multiply(0.90) + 2.07

        # ra = aerodynamic resistance which is computed from horizontal wind speed and ustar
        # ra <- u/ustar/ustar
        u = df[self.horiz_windspeed_col].copy()
        ustar = df[self.ustar_col].copy()
        ra = u / ustar / ustar

        # rho.v = water vapor density (kg/m3)
        # rho.d = dry air density (kg/m3)
        # rho.a = air density (kg/m3)
        # rho.d <- rho.a + rho.v*(M_AIR/M_H2O -1)
        rhov = df[self.water_vapor_density_col].copy()
        rhoa = df[self.air_density_col].copy()
        rhod = rhoa + rhov.multiply(self.M_AIR / self.M_H2O - 1)

        #                             a      /        b         *           c
        # Fc.corr <- fraction * ((Ts-Ta)*qc) / (ra*(Ta+273.15)) * (1+1.6077*rho.v/rho.d)
        a = (ts - ta) * qc
        b = ra * (ta + 273.15)
        c = 1 + 1.6077 * rhov / rhod
        abc = a / b * c
        # fccorr = abc.multiply(self.corr_fraction)
        return abc

    def calc_sumofsquares(self, fraction, abc, target, reference):
        fccorr = abc.multiply(fraction)
        corrected = target + fccorr
        diff2 = np.sqrt((corrected - reference) ** 2)
        sum_of_squares = diff2.sum()
        # print(f"fraction: {fraction}  sos: {sum_of_squares}")
        return sum_of_squares

    def optimize_fraction(self, df):
        """Optimize fraction by minimizing sum of squares b/w corrected target and reference"""
        optimize_df = df[[self.target_col, self.reference_col, self.abc_results_col]].dropna().copy()
        target = optimize_df[self.target_col].copy()
        reference = optimize_df[self.reference_col].copy()
        abc = optimize_df[self.abc_results_col].copy()
        result = minimize_scalar(self.calc_sumofsquares, args=(abc, target, reference))
        return result

    def prepare_export(self):
        return self.class_df[[self.target_corrected_flux_col]].copy()

    def get_selected(self, main_df):
        """Return modified class data back to main data"""
        export_df = self.prepare_export()
        main_df = pkgs.dfun.frames.export_to_main(main_df=main_df,
                                                  export_df=export_df,
                                                  tab_data_df=self.tab_data_df)  # Return results to main
        self.btn_add_as_new_var.setDisabled(True)
        return main_df

    def plot_target(self, plot_df):
        _stats = plot_df[self.target_col].median()

        self.axes_dict['ax_reference_target'].plot_date(
            x=plot_df.index, y=plot_df[self.target_col], color='#90A4AE',
            alpha=1, ls='-', marker='o', markeredgecolor='none', ms=4, zorder=98,
            label=f"TARGET: {self.target_col}  median: {_stats}")

        self.axes_dict['ax_cumu'].plot_date(
            x=plot_df.index, y=plot_df[self.target_col].cumsum(), color='#90A4AE',
            alpha=1, ls='-', marker='o', markeredgecolor='none', ms=4, zorder=98,
            label=f"TARGET: {self.target_col}  median: {_stats}")

    def plot_reference(self, plot_df):
        _stats = plot_df[self.reference_col].median()
        self.axes_dict['ax_reference_target'].plot_date(
            x=plot_df.index, y=plot_df[self.reference_col], color='#424242',
            alpha=1, ls='-', marker='o', markeredgecolor='none', ms=4, zorder=98,
            label=f"REFERENCE: {self.reference_col}  median: {_stats}")
        self.axes_dict['ax_cumu'].plot_date(
            x=plot_df.index, y=plot_df[self.reference_col].cumsum(), color='#424242',
            alpha=1, ls='-', marker='o', markeredgecolor='none', ms=4, zorder=98,
            label=f"REFERENCE: {self.reference_col}  median: {_stats}")

    def plot_target_corrected(self, plot_df):
        # _stats = plot_df[self.reference_col].median()

        plot_df['_cumsum'] = plot_df[self.target_corrected_flux_col].cumsum()
        _group_colors_dict = theme.colorwheel_36_wider()
        marker_cycle = {'day': 'o', 'night': 's'}

        # Separate groups for daytime and nighttime data
        _daynight_grouped = plot_df.groupby(self.is_daytime_col)
        for _daynight_group_ix, _daynight_group_df in _daynight_grouped:
            _time = 'day' if _daynight_group_ix == 1 else 'night'

            _temp_grouped = _daynight_group_df.groupby(self.class_var_group_col)
            for _temp_group, _temp_group_df in _temp_grouped:
                # Time series
                # rmse_avg = self.correction_fractions_df.loc[_group, 'rmse_final']
                color = _group_colors_dict[_temp_group] if _daynight_group_ix == 1 else 'white'
                markeredgecolor = 'none' if _daynight_group_ix == 1 else _group_colors_dict[_temp_group]
                self.axes_dict['ax_reference_target'].plot_date(
                    x=_temp_group_df.index, y=_temp_group_df[self.target_corrected_flux_col], color=color,
                    alpha=1, ls='none', marker='o', markeredgecolor=markeredgecolor, ms=4, zorder=98,
                    label=f"CORRECTED TARGET: {_time} class variable group {_temp_group:.0f}")

                # Cumulative
                self.axes_dict['ax_cumu'].plot_date(
                    x=_temp_group_df.index, y=_temp_group_df['_cumsum'], color=color,
                    # x=plot_df.index, y=plot_df['_cumsum'], color='#FF7043',
                    alpha=1, ls='', marker='o', markeredgecolor=markeredgecolor, ms=4, zorder=98,
                    label=f"CORRECTED TARGET: {_time} class variable group {_temp_group:.0f}")
                self.axes_dict['ax_cumu'].legend(loc='lower left')

    def plot_target_vs_ref_scatter(self, ax, ref_x, target_y, label, color):
        ax.plot(ref_x, target_y,
                color='none', alpha=.1, ls='none', marker='o',
                markeredgecolor=color, ms=4, zorder=98, label=label)

        # todo

    def plot_data(self):
        # axes_dict = self.make_axes_dict()
        plot_df = self.class_df.copy()

        # Reset axes
        for ax_id, ax in self.axes_dict.items():
            ax.lines = []
            ax.collections = []

        # Plot target
        if self.target_loaded:
            self.plot_target(plot_df=plot_df)

        # Plot reference
        if self.reference_loaded:
            self.plot_reference(plot_df=plot_df)
            self.plot_target_vs_ref_scatter(ax=self.axes_dict['ax_ref_vs_target_scatter'],
                                            ref_x=plot_df[self.reference_col],
                                            target_y=plot_df[self.target_col],
                                            label="before correction", color='grey')

        # Plot correction fractions from optimization
        if self.optimization_performed:
            self.plot_corr_fractions(ax=self.axes_dict['ax_corr_fraction'],
                                     df=self.correction_fractions_df)

        # Plot corrected target
        if self.target_corrected_loaded:
            self.plot_target_corrected(plot_df=plot_df)

        if self.target_corrected_loaded and self.reference_loaded:
            self.plot_target_vs_ref_scatter(ax=self.axes_dict['ax_ref_vs_target_scatter'],
                                            ref_x=plot_df[self.reference_col],
                                            target_y=plot_df[self.target_corrected_flux_col],
                                            label="after correction", color='#FF7043')

        for ix, ax in self.axes_dict.items():
            gui.plotfuncs.default_grid(ax=ax)
            gui.plotfuncs.default_legend(ax=ax, ncol=2, loc='best')

        locator = mdates.AutoDateLocator(minticks=3, maxticks=30)
        formatter = mdates.ConciseDateFormatter(locator)
        self.axes_dict['ax_reference_target'].xaxis.set_major_locator(locator)
        self.axes_dict['ax_cumu'].xaxis.set_major_formatter(formatter)

        self.fig.canvas.draw()
        self.fig.canvas.flush_events()

    def plot_corr_fractions(self, ax, df):
        """Plot correction fractions for day and night"""

        marker_cycle = {'day': 'o', 'night': 's'}
        color_cycle = {'day': '#FF9800', 'night': '#3F51B5'}  # Orange 500 / Indigo 500

        xmax = -9999
        xmin = 9999
        ymax = -9999
        ymin = 9999

        # Separate groups for daytime and nighttime data
        _daynight_grouped = df.groupby('daytime')
        for _daynight_group_ix, _daynight_group_df in _daynight_grouped:
            _time = 'day' if _daynight_group_ix == 1 else 'night'

            ax.plot(_daynight_group_df['min_class_var'], _daynight_group_df['fraction_median'],
                    color=color_cycle[_time], alpha=1, ls='-', marker=marker_cycle[_time],
                    markeredgecolor='none', ms=4, zorder=98, label=f'{_time}: median from bootstrap')
            ax.fill_between(_daynight_group_df['min_class_var'],
                            _daynight_group_df['fraction_Q75'], _daynight_group_df['fraction_Q25'],
                            color=color_cycle[_time], alpha=0.2, lw=0)

            xmax = _daynight_group_df['min_class_var'].max() if _daynight_group_df[
                                                                    'min_class_var'].max() > xmax else xmax
            xmin = _daynight_group_df['min_class_var'].min() if _daynight_group_df[
                                                                    'min_class_var'].min() < xmin else xmin
            ymax = _daynight_group_df['fraction_Q75'].max() if _daynight_group_df['fraction_Q75'].max() > ymax else ymax
            ymin = _daynight_group_df['fraction_Q25'].min() if _daynight_group_df['fraction_Q25'].min() < ymin else ymin

        ax.set_xlim(xmin, xmax)
        ax.set_ylim(ymin, ymax)
        ax.set_xlabel('class variable (units)', color='black', fontsize=6, fontweight='bold')
        ax.set_ylabel('correction fraction', color='black', fontsize=6, fontweight='bold')

    def make_axes_dict(self):
        # Delete all axes in figure
        for ax in self.fig.axes:
            self.fig.delaxes(ax)
        gs = gridspec.GridSpec(3, 2)  # rows, cols
        gs.update(wspace=0.2, hspace=0.3, left=0.05, right=0.96, top=0.96, bottom=0.03)
        ax_reference_target = self.fig.add_subplot(gs[0, 0:3])
        ax_cumu = self.fig.add_subplot(gs[1, 0:3], sharex=ax_reference_target)
        ax_corr_fraction = self.fig.add_subplot(gs[2, 0])
        ax_ref_vs_target_scatter = self.fig.add_subplot(gs[2, 1])
        axes_dict = {'ax_reference_target': ax_reference_target,
                     'ax_cumu': ax_cumu,
                     'ax_corr_fraction': ax_corr_fraction,
                     'ax_ref_vs_target_scatter': ax_ref_vs_target_scatter}
        for key, ax in axes_dict.items():
            gui.plotfuncs.default_format(ax=ax, txt_xlabel=False)

        args = dict(fontsize=theme.FONTSIZE_HEADER_AXIS_LARGE, color=theme.FONTCOLOR_HEADER_AXIS, loc='left')
        axes_dict['ax_reference_target'].set_title("Time Series", **args)
        axes_dict['ax_cumu'].set_title("Cumulative", **args)
        axes_dict['ax_corr_fraction'].set_title("Correction Fractions Per Variable Class", **args)
        axes_dict['ax_ref_vs_target_scatter'].set_title("Reference vs Target", **args)

        return axes_dict

    def save_cf_to_file(self):
        """Export correction fractions to csv file"""
        if not Path.is_dir(self.sub_outdir):
            logger.log(name=f"Creating folder {self.sub_outdir} ...", dict={}, highlight=False)  # Log info
            os.makedirs(self.sub_outdir)
        suffix = pkgs.dfun.times.make_timestamp_suffix()
        now_time_dt, _ = pkgs.dfun.times.get_current_time()
        now_time_str = now_time_dt.strftime("%Y%m%d%H%M%S")
        outfile = self.sub_outdir / f"offseason_uptake_correction_fractions_{now_time_str}.csv"
        self.correction_fractions_df.to_csv(outfile, index=False)
        self.enable_disable_buttons()

    def load_cf_from_file(self):
        filename = pkgs.dfun.files.select_single_source_file(start_dir=self.project_outdir,
                                                             text="Select Correction Fractions File")
        self.correction_fractions_df = pd.read_csv(filename)
        gui.tableview.show_df(df=self.correction_fractions_df, tableview=self.tvw_corr_fractions)
        self.optimization_performed = True
        self.plot_data()
        # self.btn_apply.setEnabled(True)
        self.enable_disable_buttons()
