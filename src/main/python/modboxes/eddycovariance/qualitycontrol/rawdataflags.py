"""

Raw data flags from EddyPro

    EddyPro outputs the raw data flags as 0 or 1, whereby 1 can correspond to
    bad data (if the selected flag is a hard flag) or to OK data (if the selected
    flag is a soft flag).

    EddyPro Statistical Flags (Raw Data)
        consists of
        -- <statflag>, e.g. spikes_hf
        -- <flag variable>, e.g. 8u/v/w/ts/co2/h2o/ch4/none

    -- 8 digits:
    spikes_hf                   8u/v/w/ts/co2/h2o/ch4/none	    800000099
    amplitude_resolution_hf     8u/v/w/ts/co2/h2o/ch4/none	    800000099
    drop_out_hf	                8u/v/w/ts/co2/h2o/ch4/none	    800000099
    absolute_limits_hf	        8u/v/w/ts/co2/h2o/ch4/none	    800000199
    skewness_kurtosis_hf	    8u/v/w/ts/co2/h2o/ch4/none	    800000099
    skewness_kurtosis_sf	    8u/v/w/ts/co2/h2o/ch4/none	    800011199
    discontinuities_hf	        8u/v/w/ts/co2/h2o/ch4/none	    800000000
    discontinuities_sf	        8u/v/w/ts/co2/h2o/ch4/none	    800000000

    -- 4 digits:
    timelag_hf	                8co2/h2o/ch4/none	            81000
    timelag_sf	                8co2/h2o/ch4/none	            81100

    -- 1 digit:
    attack_angle_hf	            8aa	                            80
    non_steady_wind_hf	        8U	                            80


"""
import numpy as np

class Completeness():
    """
    Number of records
    """

    def __init__(self, drp_completeness_flag_col, lne_completeness_flag0_lim, lne_completeness_flag1_lim,
                 col_list_pretty, col_dict_tuples, vars_selected_df, tab_data_df):
        # Get settings
        flag_pretty = drp_completeness_flag_col.currentText()
        flag_pretty_idx = col_list_pretty.index(flag_pretty)
        self.flag_col = col_dict_tuples[flag_pretty_idx]
        self.flag0_lim = float(lne_completeness_flag0_lim.text())
        self.flag1_lim = float(lne_completeness_flag1_lim.text())

        self.tab_data_df = tab_data_df
        self.vars_selected_df = vars_selected_df.copy()

    def add_qcf_scf(self):
        self._set_colnames()
        self._init_new_cols()
        self._add_aux_cols()
        self._calc_qcfcol()
        return self.vars_selected_df

    def _set_colnames(self):
        self.values_col = (f"_AUX_COMPLETENESS_VALS_{self.flag_col[0]}", f"{self.flag_col[1]}")
        self.flag0_lim_col = (f"_AUX_COMPLETENESS_FLAG0_LIM_{self.flag_col[0]}", f"{self.flag_col[1]}")
        self.flag1_lim_col = (f"_AUX_COMPLETENESS_FLAG1_LIM_{self.flag_col[0]}", f"{self.flag_col[1]}")
        self.qcf_col = (f"_QCF_COMPLETENESS_{self.flag_col[0]}", "[2=bad]")

    def _init_new_cols(self):
        self.vars_selected_df[self.values_col] = np.nan
        self.vars_selected_df[self.flag0_lim_col] = np.nan
        self.vars_selected_df[self.flag1_lim_col] = np.nan
        self.vars_selected_df[self.qcf_col] = np.nan

    def _add_aux_cols(self):
        self.vars_selected_df[self.flag0_lim_col] = self.flag0_lim
        self.vars_selected_df[self.flag1_lim_col] = self.flag1_lim
        self.vars_selected_df[self.values_col] = self.tab_data_df[self.flag_col]

    def _calc_qcfcol(self):
        # All averaging intervals missing more than the flag 1 limit are rejected
        flag_filter = (self.vars_selected_df[self.values_col] < self.vars_selected_df[self.flag1_lim_col])
        self.vars_selected_df.loc[flag_filter, [self.qcf_col]] = 2

        #
        flag_filter = (self.vars_selected_df[self.values_col] >= self.vars_selected_df[self.flag1_lim_col]) & \
                      (self.vars_selected_df[self.values_col] <= self.vars_selected_df[self.flag0_lim_col])
        self.vars_selected_df.loc[flag_filter, [self.qcf_col]] = 1
        flag_filter = (self.vars_selected_df[self.values_col] > self.vars_selected_df[self.flag0_lim_col])
        self.vars_selected_df.loc[flag_filter, [self.qcf_col]] = 0





class RawDataFlag8:
    """8-digit flag code"""

    def __init__(self, drp_statflag, drp_flagvar, col_list_pretty, col_dict_tuples, vars_selected_df, tab_data_df):
        self.col_list_pretty = col_list_pretty
        self.col_dict_tuples = col_dict_tuples
        self.vars_selected_df = vars_selected_df
        self.tab_data_df = tab_data_df
        self.statflag_col = self._get_var_col_from_drp_pretty(drp=drp_statflag)
        self.flagvar_pretty = drp_flagvar.currentText()

    def get_qcf_rawdata_flag(self):
        self._get_flagvar()
        self._set_colnames()
        self._init_new_cols()
        self._add_aux_cols()
        self._prepare_extraction()
        self._insert_flagvar_flag()
        self._calc_qcfcol()
        return self.vars_selected_df

    def _get_flagvar(self):
        # Get the index of the flag var in the flag integer
        # The flag integer looks like this: 800000199
        # Its units look like this:         8u/v/w/ts/co2/h2o/ch4/none
        # This means: e.g. H2O has index 5 in the flag integer
        # selected_flagvar:  e.g. '5: h2o'
        _flagvar = self.flagvar_pretty.split(' ')  # ['5:', 'h2o']
        self.flagvar_ix = int(_flagvar[0].split(':')[0])  # '5'
        self.flagvar = _flagvar[1]  # 'h2o'

    def _set_colnames(self):
        self.multiflags_col = (f"_AUX_RAWDATA_{self.statflag_col[0]}_VALS_{self.flagvar}", f"{self.statflag_col[1]}")
        self.flag_col = (f"_AUX_RAWDATA_{self.statflag_col[0]}_{self.flagvar}", f"[flag]")
        self.qcf_col = (f"_QCF_RAWDATA_{self.statflag_col[0]}_{self.flagvar}", "[2=bad]")

    def _get_var_col_from_drp_pretty(self, drp):
        pretty = drp.currentText()
        pretty_idx = self.col_list_pretty.index(pretty)
        var_col = self.col_dict_tuples[pretty_idx]
        return var_col

    def _calc_qcfcol(self):
        # Set bad values = 2 in case of hard flags
        filter_rejectval = self.vars_selected_df[self.flag_col] == 1  # Bad values
        filter_keepval = self.vars_selected_df[self.flag_col] == 0  # Good values
        filter_missingval = (self.vars_selected_df[self.flag_col] == 9)  # Missing values

        if '_hf' in str(self.statflag_col[0]):
            self.vars_selected_df.loc[filter_rejectval, [self.qcf_col]] = 2  # for hard flags, 1=bad becomes 2=bad
        else:
            self.vars_selected_df.loc[filter_rejectval, [self.qcf_col]] = 1  # for soft flags, 1=OK, no 2

        self.vars_selected_df.loc[filter_keepval, [self.qcf_col]] = 0  # flag within limits, good values
        self.vars_selected_df.loc[filter_missingval, [self.qcf_col]] = np.nan  # Missing values stay missing

    def _add_aux_cols(self):
        self.vars_selected_df[self.multiflags_col] = self.tab_data_df[self.statflag_col].copy()

    def _init_new_cols(self):
        self.vars_selected_df[self.multiflags_col] = np.nan
        self.vars_selected_df[self.flag_col] = np.nan
        self.vars_selected_df[self.qcf_col] = np.nan

    def _insert_flagvar_flag(self):
        # Make column that contains only the flag var flag values
        # str.get() method is used to get element at the passed position
        # kudos: https://www.geeksforgeeks.org/python-pandas-series-str-get/
        self.vars_selected_df[self.multiflags_col] = self.vars_selected_df[self.multiflags_col].str[1:]
        self.vars_selected_df[self.flag_col] = \
            self.vars_selected_df[self.multiflags_col].str.get(self.flagvar_ix)
        self.vars_selected_df[self.flag_col] = \
            self.vars_selected_df[self.flag_col].astype(int)

    def _prepare_extraction(self):
        flag_missing_str = self._generate_flag_missing_str()

        # Set missing data to 9-digit eddypro flag_code code, same number of digits needed in all rows
        self.vars_selected_df[self.multiflags_col] = self.vars_selected_df[self.multiflags_col].fillna(flag_missing_str)

        # Convert to str so required flag position can be extracted by index
        self.vars_selected_df[self.multiflags_col] = self.vars_selected_df[self.multiflags_col].astype(str)

    def _generate_flag_missing_str(self):
        # Get units (flag vars) for selected flag
        # Cut off first number in units row (the '8'), e.g. units 8u/v/w/ts/co2/h2o/ch4/none
        qcflag_vars = self.statflag_col[1]
        qcflag_vars = qcflag_vars[1:]
        qcflag_vars = qcflag_vars.split('/')  # Split

        # Number of flag variables, needed for missing flag values
        num_qcflag_vars = len(qcflag_vars) + 1
        flag_missing_str = int('9' * num_qcflag_vars)  # +1 b/c of leading '8' in flag code
        return flag_missing_str
