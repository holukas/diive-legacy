"""

EDDY COVARIANCE QUALITY CONTROL
-------------------------------
Using output in the EddyPro *_full_output_* file


"""
import fnmatch
import math
import os
from difflib import SequenceMatcher  # Calculates string similarity ratio
from pathlib import Path

import matplotlib.dates as mdates
import matplotlib.gridspec as gridspec
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from PyQt5 import QtGui
from PyQt5 import QtWidgets as qw

import gui
import gui.add_to_layout
import gui.base
import gui.make
import utils.vargroups
import logger
import modboxes.eddycovariance.qualitycontrol.fluxflags as flf
import modboxes.eddycovariance.qualitycontrol.rawdataflags as rdf
import modboxes.plots.styles.LightTheme as theme
from pkgs.dfun import frames
from pkgs.dfun.times import get_current_time
from help import infotooltips
from modboxes.plots import heatmap


# pd.set_option('display.width', 1000)
# pd.set_option('display.max_columns', 15)
# pd.set_option('display.max_rows', 20)

class addContent(gui.base.buildTab):
    """Build new tab and populate with contents"""

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id, tab_template='SVonVPT')

        # Tab icon
        self.TabWidget.setTabIcon(self.tab_ix, QtGui.QIcon(app_obj.ctx.tab_icon_eddy_covariance))

        # Add settings menu contents
        self.drp_ssitc_flag_col, self.lne_ssitc_flag1_lim, self.lne_ssitc_flag0_lim, \
        self.btn_add_as_new_var, self.btn_calc, \
        self.drp_scf_flag_col, self.lne_scf_flag1_lim, self.lne_scf_flag0_lim, \
        self.drp_abslim_flag_col, self.lne_abslim_flag0_upper_lim, self.lne_abslim_flag0_lower_lim, \
        self.drp_ssga_flag_col, self.drp_ssga_type, self.lne_ssga_threshold, \
        self.drp_stability_flag_col, self.lne_stability_flag0_upper_lim, self.lne_stability_flag0_lower_lim, \
        self.chk_ssitc_flag, self.chk_scf_flag, self.chk_ssga_flag, self.chk_abslim_flag, self.chk_stability_flag, \
        self.chk_rawdata_spikes_flag, self.drp_rawdata_spikes_flagvar, self.drp_rawdata_spikes_statflag, \
        self.drp_rawdata_amplres_statflag_col, self.drp_rawdata_amplres_flagvar_col, self.chk_rawdata_amplres_flag, \
        self.drp_rawdata_dropout_statflag_col, self.drp_rawdata_dropout_flagvar_col, self.chk_rawdata_dropout_flag, \
        self.drp_rawdata_abslim_statflag_col, self.drp_rawdata_abslim_flagvar_col, self.chk_rawdata_abslim_flag, \
        self.drp_rawdata_skewkurt_hf_statflag_col, self.drp_rawdata_skewkurt_hf_flagvar_col, self.chk_rawdata_skewkurt_hf_flag, \
        self.drp_rawdata_skewkurt_sf_statflag_col, self.drp_rawdata_skewkurt_sf_flagvar_col, self.chk_rawdata_skewkurt_sf_flag, \
        self.drp_rawdata_discont_hf_statflag_col, self.drp_rawdata_discont_hf_flagvar_col, self.chk_rawdata_discont_hf_flag, \
        self.drp_rawdata_discont_sf_statflag_col, self.drp_rawdata_discont_sf_flagvar_col, self.chk_rawdata_discont_sf_flag, \
        self.drp_rawdata_aa_statflag_col, self.drp_rawdata_aa_flagvar_col, self.chk_rawdata_aa_flag, \
        self.drp_rawdata_nonsteadyu_statflag_col, self.drp_rawdata_nonsteadyu_flagvar_col, self.chk_rawdata_nonsteadyu_flag, \
        self.btn_uncheck_all, self.btn_check_all, \
        self.chk_completeness_flag, self.drp_completeness_flag_col, self.lne_completeness_flag0_lim, \
        self.lne_completeness_flag1_lim = \
            self.add_settings_fields()

        # # Add variables required in settings
        # self.populate_settings_fields()

        # Add available variables to variable lists
        gui.base.buildTab.populate_variable_list(obj=self)

        # # Add plots
        # self.populate_plot_area()

    # def populate_plot_area(self):
    #     # This is done in Run due to dynamic amount of axes
    #     pass

    # def populate_settings_fields(self):
    #     pass

    def add_settings_fields(self):
        gui.elements.add_header_subheader_to_grid_top(
            row=0, col=0, rowspan=1, colspan=3,
            txt=["Flux Quality Control", "Eddy Covariance"], layout=self.sett_layout, )
        gui.add_to_layout.add_spacer_item_to_grid(layout=self.sett_layout, row=1, col=0)

        gui.elements.add_header_in_grid_row(
            layout=self.sett_layout, txt='Flux Flags',
            row=2, col=0, colspan=3)

        # EddyPro default flag (SSITC)
        # ============================
        chk_ssitc_flag = gui.elements.add_checkbox_to_grid(
            row=3, col=0, layout=self.sett_layout)
        chk_ssitc_flag.setCheckState(2)
        drp_ssitc_flag_col = gui.elements.grd_LabelDropdownPair(
            txt='SSITC (0-1-2)', css_ids=['lbl_bold1', ''], layout=self.sett_layout,
            row=3, col=1, orientation='horiz')
        drp_ssitc_flag_col.setToolTip(infotooltips.ssitc)
        lne_ssitc_flag0_lim, lne_ssitc_flag1_lim = gui.elements.add_two_subgrid_lnes_to_grid(
            add_to_layout=self.sett_layout, lbl_flag_a='Flag 0: Value <', lbl_flag_b='Flag 1: Value <',
            row=4, col=2)
        lne_ssitc_flag0_lim.setText('1')
        lne_ssitc_flag1_lim.setText('2')

        # Spectral correction factor (scf)
        # ================================
        chk_scf_flag = gui.elements.add_checkbox_to_grid(
            row=5, col=0, layout=self.sett_layout)
        chk_scf_flag.setCheckState(2)
        drp_scf_flag_col = gui.elements.grd_LabelDropdownPair(
            txt='Spectral Correction Factor (scf)', css_ids=['lbl_bold1', ''], layout=self.sett_layout,
            row=5, col=1, orientation='horiz')
        drp_scf_flag_col.setToolTip("...soon...")
        lne_scf_flag0_lim, lne_scf_flag1_lim = gui.elements.add_two_subgrid_lnes_to_grid(
            add_to_layout=self.sett_layout, lbl_flag_a='Flag 0: Value <', lbl_flag_b='Flag 1: Value <',
            row=6, col=2)
        lne_scf_flag0_lim.setText('2')
        lne_scf_flag1_lim.setText('4')

        # Absolute Limits
        # ===============
        chk_abslim_flag = gui.elements.add_checkbox_to_grid(
            layout=self.sett_layout,
            row=7, col=0)
        chk_abslim_flag.setCheckState(2)
        drp_abslim_flag_col = gui.elements.grd_LabelDropdownPair(
            txt='Absolute Limits',
            css_ids=['lbl_bold1', ''], layout=self.sett_layout,
            row=7, col=1, orientation='horiz')
        drp_abslim_flag_col.setToolTip("...soon...")
        lne_abslim_flag0_upper_lim, lne_abslim_flag0_lower_lim = gui.elements.add_two_subgrid_lnes_to_grid(
            add_to_layout=self.sett_layout, lbl_flag_a='Flag 0: Value <=', lbl_flag_b='Flag 0: Value >=',
            row=8, col=2)

        # Signal Strength Gas Analyzer
        # ============================
        chk_ssga_flag = gui.elements.add_checkbox_to_grid(
            row=9, col=0, layout=self.sett_layout)
        chk_ssga_flag.setCheckState(2)
        drp_ssga_flag_col = gui.elements.grd_LabelDropdownPair(
            txt='Signal Strength', css_ids=['lbl_bold1', ''],
            layout=self.sett_layout,
            row=9, col=1, orientation='horiz')
        drp_ssga_flag_col.setToolTip("...soon...")
        drp_ssga_type, lne_ssga_threshold = gui.elements.add_subgrid_drp_lne_to_grid(
            add_to_layout=self.sett_layout, lbl_flag_a='Threshold', lbl_drp='Reject Values',
            row=10, col=2)
        drp_ssga_type.addItems(['Smaller Than Threshold', 'Larger Than Threshold'])
        lne_ssga_threshold.setText('50')

        # Stability
        # =========
        chk_stability_flag = gui.elements.add_checkbox_to_grid(
            row=11, col=0, layout=self.sett_layout)
        chk_stability_flag.setCheckState(0)
        drp_stability_flag_col = gui.elements.grd_LabelDropdownPair(
            txt='Stability', css_ids=['lbl_bold1', ''], layout=self.sett_layout,
            row=11, col=1, orientation='horiz')
        drp_stability_flag_col.setToolTip("...soon...")
        lne_stability_flag0_upper_lim, lne_stability_flag0_lower_lim = gui.elements.add_two_subgrid_lnes_to_grid(
            add_to_layout=self.sett_layout, lbl_flag_a='Flag 0: Value <=', lbl_flag_b='Flag 0: Value >=',
            row=12, col=2)
        lne_stability_flag0_upper_lim.setText('2')
        lne_stability_flag0_lower_lim.setText('-2')

        # Raw Data Flags
        # ==============
        gui.add_to_layout.add_spacer_item_to_grid(
            row=13, col=0, layout=self.sett_layout)
        gui.elements.add_header_in_grid_row(
            layout=self.sett_layout, txt='Raw Data Checks',
            row=14, col=0, colspan=3)

        # Completeness
        chk_completeness_flag = gui.elements.add_checkbox_to_grid(
            row=15, col=0, layout=self.sett_layout)
        chk_completeness_flag.setCheckState(2)
        drp_completeness_flag_col = gui.elements.grd_LabelDropdownPair(
            txt='Completeness', css_ids=['lbl_bold1', ''], layout=self.sett_layout,
            row=15, col=1, orientation='horiz')
        drp_completeness_flag_col.setToolTip("Flag 0: 1% missing\nFlag1: 3% missing")
        lne_completeness_flag0_lim, lne_completeness_flag1_lim = gui.elements.add_two_subgrid_lnes_to_grid(
            add_to_layout=self.sett_layout, lbl_flag_a='Flag 0: Value >', lbl_flag_b='Flag 1: Value >=',
            row=16, col=2)
        # lne_completeness_flag0_lim.setText('1')  dynamically filled
        # lne_completeness_flag1_lim.setText('2')  dynamically filled

        # Raw Data Flags (8-digits)
        # -------------------------
        # Spikes (hf)
        drp_rawdata_spikes_statflag_col, drp_rawdata_spikes_flagvar_col, chk_rawdata_spikes_flag = \
            self.add_rawdata_flag8(txt='Spikes', row=17, txt_tooltip="", checkstate=2)

        # Amplitude resolution (hf)
        drp_rawdata_amplres_statflag_col, drp_rawdata_amplres_flagvar_col, chk_rawdata_amplres_flag = \
            self.add_rawdata_flag8(txt='Amplitude Resolution', row=18, txt_tooltip="", checkstate=0)

        # Drop out (hf)
        drp_rawdata_dropout_statflag_col, drp_rawdata_dropout_flagvar_col, chk_rawdata_dropout_flag = \
            self.add_rawdata_flag8(txt='Drop-out', row=19, txt_tooltip="", checkstate=2)

        # Absolute Limits (hf)
        drp_rawdata_abslim_statflag_col, drp_rawdata_abslim_flagvar_col, chk_rawdata_abslim_flag = \
            self.add_rawdata_flag8(txt='Absolute Limits', row=20, txt_tooltip="", checkstate=2)

        # Skewness / kurtosis (hf)
        drp_rawdata_skewkurt_hf_statflag_col, drp_rawdata_skewkurt_hf_flagvar_col, chk_rawdata_skewkurt_hf_flag = \
            self.add_rawdata_flag8(txt='Skewness / Kurtosis (hf)', row=21, txt_tooltip="", checkstate=0)

        # Skewness / kurtosis (sf)
        drp_rawdata_skewkurt_sf_statflag_col, drp_rawdata_skewkurt_sf_flagvar_col, chk_rawdata_skewkurt_sf_flag = \
            self.add_rawdata_flag8(txt='Skewness / Kurtosis (sf)', row=22, txt_tooltip="", checkstate=0)

        # Discontinuities (hf)
        drp_rawdata_discont_hf_statflag_col, drp_rawdata_discont_hf_flagvar_col, chk_rawdata_discont_hf_flag = \
            self.add_rawdata_flag8(txt='Discontinuities (hf)', row=23, txt_tooltip="", checkstate=0)

        # Discontinuities (sf)
        drp_rawdata_discont_sf_statflag_col, drp_rawdata_discont_sf_flagvar_col, chk_rawdata_discont_sf_flag = \
            self.add_rawdata_flag8(txt='Discontinuities (sf)', row=24, txt_tooltip="", checkstate=0)

        # Raw Data Flags (1-digit)
        # =========================
        # Attack angle (hf)
        drp_rawdata_aa_statflag_col, drp_rawdata_aa_flagvar_col, chk_rawdata_aa_flag = \
            self.add_rawdata_flag8(txt='Attack Angle', row=25, txt_tooltip="", checkstate=0)

        # Non-steady wind (hf)
        drp_rawdata_nonsteadyu_statflag_col, drp_rawdata_nonsteadyu_flagvar_col, chk_rawdata_nonsteadyu_flag = \
            self.add_rawdata_flag8(txt='Non-steady Wind', row=26, txt_tooltip="", checkstate=0)

        self.sett_layout.setRowStretch(27, 1)

        # Buttons
        # =======
        gui.add_to_layout.add_spacer_item_to_grid(layout=self.sett_layout, row=28, col=1)
        sublyt = gui.elements.add_sublayout_to_grid(grid_layout=self.sett_layout,
                                                    row=29, col=0, rowspan=1, colspan=3)
        btn_check_all = gui.elements.add_simplebutton_to_layout(layout=sublyt, style='teal',
                                                                txt='Check All')
        btn_uncheck_all = gui.elements.add_simplebutton_to_layout(layout=sublyt, style='red',
                                                                  txt='Uncheck All')

        # Buttons added below var lists:
        btn_calc = gui.elements.add_button_to_layout(layout=self.lyt_varlists,
                                                     txt='Calculate Combined Flag', css_id='')
        btn_add_as_new_var = gui.elements.add_button_to_layout(layout=self.lyt_varlists,
                                                               txt='+ Add Quality Flag: -', css_id='btn_add_as_new_var')

        return drp_ssitc_flag_col, lne_ssitc_flag1_lim, lne_ssitc_flag0_lim, \
               btn_add_as_new_var, btn_calc, \
               drp_scf_flag_col, lne_scf_flag1_lim, lne_scf_flag0_lim, \
               drp_abslim_flag_col, lne_abslim_flag0_upper_lim, lne_abslim_flag0_lower_lim, \
               drp_ssga_flag_col, drp_ssga_type, lne_ssga_threshold, \
               drp_stability_flag_col, lne_stability_flag0_upper_lim, lne_stability_flag0_lower_lim, \
               chk_ssitc_flag, chk_scf_flag, chk_ssga_flag, chk_abslim_flag, chk_stability_flag, \
               chk_rawdata_spikes_flag, drp_rawdata_spikes_flagvar_col, drp_rawdata_spikes_statflag_col, \
               drp_rawdata_amplres_statflag_col, drp_rawdata_amplres_flagvar_col, chk_rawdata_amplres_flag, \
               drp_rawdata_dropout_statflag_col, drp_rawdata_dropout_flagvar_col, chk_rawdata_dropout_flag, \
               drp_rawdata_abslim_statflag_col, drp_rawdata_abslim_flagvar_col, chk_rawdata_abslim_flag, \
               drp_rawdata_skewkurt_hf_statflag_col, drp_rawdata_skewkurt_hf_flagvar_col, chk_rawdata_skewkurt_hf_flag, \
               drp_rawdata_skewkurt_sf_statflag_col, drp_rawdata_skewkurt_sf_flagvar_col, chk_rawdata_skewkurt_sf_flag, \
               drp_rawdata_discont_hf_statflag_col, drp_rawdata_discont_hf_flagvar_col, chk_rawdata_discont_hf_flag, \
               drp_rawdata_discont_sf_statflag_col, drp_rawdata_discont_sf_flagvar_col, chk_rawdata_discont_sf_flag, \
               drp_rawdata_aa_statflag_col, drp_rawdata_aa_flagvar_col, chk_rawdata_aa_flag, \
               drp_rawdata_nonsteadyu_statflag_col, drp_rawdata_nonsteadyu_flagvar_col, chk_rawdata_nonsteadyu_flag, \
               btn_uncheck_all, btn_check_all, \
               chk_completeness_flag, drp_completeness_flag_col, lne_completeness_flag0_lim, lne_completeness_flag1_lim

    def add_rawdata_flag8(self, txt, row, txt_tooltip=None, checkstate=0):
        drp_statflag_col = \
            gui.elements.grd_LabelDropdownPair(txt=txt, css_ids=['lbl_bold1', ''], layout=self.sett_layout,
                                               row=row, col=1, orientation='horiz')
        if txt_tooltip:
            drp_statflag_col.setToolTip(txt_tooltip)
        drp_flagvar_col = gui.elements.grd_add_dropdown(row=row, col=3, layout=self.sett_layout)
        chk_flag = gui.elements.add_checkbox_to_grid(row=row, col=0, layout=self.sett_layout)
        chk_flag.setCheckState(checkstate)
        return drp_statflag_col, drp_flagvar_col, chk_flag


class Run(addContent):
    flag_marker_color_list = theme.colorwheel_36_wider()
    marker_filter = None  # Filter to mark valid data points
    sub_outdir = "ec_quality_control"

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id)
        logger.log(name='>>> Starting EC Quality Control', dict={}, highlight=True)  # Log info
        self.sub_outdir = self.project_outdir / self.sub_outdir
        self.marker_isset = False
        self.focus_col = None
        self.focus_pretty_ix = None
        self.focus_pretty = None
        self.qcf_agg_col = None
        self.selected_vars_lookup_dict = {}
        self.vars_selected_df = pd.DataFrame()
        self.qcf_counts_df = pd.DataFrame()  # Number of occurrences of qcf flags
        self.vars_available_df = self.tab_data_df.copy()
        self.populate_settings_fields()
        self.update_dynamic_fields()

        self.txe_textbox.setText('Results will be displayed here once available.')

        self.btn_calc.setDisabled(True)
        self.btn_add_as_new_var.setDisabled(True)

    def calc(self):
        self.txe_textbox.clear()
        self.remove_prev_qcf_method_columns()  # Reset columns
        self.add_qcf_method_columns()
        self.qcf_col, self.qcf_agg_col, self.qcf_agg_hf_col, self.qcf_agg_sf_col = self.calc_qcf()

        # Count occurrences of the different flag values
        _subset_df = self._create_qcf_subset()
        self.qcf_counts_df = frames.count_unique_values(df=_subset_df)

        self.results_in_textarea()
        self.plot_data()
        self.btn_add_as_new_var.setEnabled(True)
        self.update_btn_txt()

    def results_in_textarea(self):

        # Total, missing and available records
        filter_nan = self.vars_selected_df[self.target_col].isnull()
        records_total = len(self.vars_selected_df[self.target_col])
        records_available = len(self.vars_selected_df[self.target_col][~filter_nan])
        records_missing = len(self.vars_selected_df[self.target_col][filter_nan])
        self.txe_textbox.append(f"<b>QC Results</b>")
        self.txe_textbox.append(f"FOCUS VARIABLE")
        self.txe_textbox.append(f"<b>{self.target_col[0]}</b>  {self.target_col[1]}")
        self.txe_textbox.append(f"<b>Start Date:</b>  {self.vars_selected_df[self.target_col].index[0]}")
        self.txe_textbox.append(f"<b>End Date:</b>  {self.vars_selected_df[self.target_col].index[-1]}")
        self.txe_textbox.append(f"<b>{records_total}</b> total records")
        self.txe_textbox.append(f"<b>{records_missing}</b> missing records")
        self.txe_textbox.append(f"<b>{records_available}</b> available records (100%)")
        self.txe_textbox.append("\n")

        # Make subset containing single flags and overall flag
        flagcols = []
        for col in self.vars_selected_df.columns:
            if str(col[0]).startswith('_QCF') \
                    & (col != self.qcf_agg_sf_col) \
                    & (col != self.qcf_agg_hf_col) \
                    & (col != self.qcf_agg_col):
                flagcols.append(col)
        _subset = self.vars_selected_df[flagcols].copy()
        _subset[self.qcf_col] = self.vars_selected_df[self.qcf_col]

        # Stats for single flags
        for col in _subset.columns:
            if '_QCF_MISSING_' in col[0]:  # Ignore missing values flag
                continue
            self.txe_textbox.append(f"<b>{col[0]}</b>\n    {col[1]}")
            unique_flags = _subset[col].unique()
            for uf in unique_flags:
                if math.isnan(uf) == False:  # Ignores NaNs
                    filter_uf = _subset[col] == uf
                    num_target_vals = self.vars_selected_df[self.target_col][filter_uf].count()
                    perc_filter = (num_target_vals / records_available) * 100
                    self.txe_textbox.append(f"... Flag <b>{uf:.0f}</b>: {num_target_vals} ({perc_filter:.1f}%)")

            self.txe_textbox.append('\n')

    def list_all_chk(self):
        chk_list = []
        for a in self.__dir__():
            if a.startswith('chk_'):
                chk_list.append(a)
        return chk_list

    def uncheck_all_chk(self):
        """Uncheck all checkboxes"""
        chk_list = self.list_all_chk()
        for chk in chk_list:
            chk_widget = getattr(self, chk)
            chk_widget.setCheckState(0)

    def check_all_chk(self):
        """Check all checkboxes"""
        chk_list = self.list_all_chk()
        for chk in chk_list:
            chk_widget = getattr(self, chk)
            chk_widget.setCheckState(2)

    def update_btn_txt(self):
        self.btn_add_as_new_var.setText(f"+ Add Quality Flag: {self.qcf_col[0]}")

    def _get_var_col_from_pretty(self, pretty):
        pretty_idx = self.col_list_pretty.index(pretty)
        var_col = self.col_dict_tuples[pretty_idx]
        return var_col

    def _find_most_similar_item_in_dropdown(self, drp, reference):
        """Find the element in dropdown most similar to reference"""
        highest_similarity = 0
        highest_similarity_item_in_drp_ix = 0
        highest_similarity_item_in_drp = ""

        drp_items = [drp.itemText(i) for i in range(drp.count())]  # All items in dropdown list
        for ix, i in enumerate(drp_items):
            similarity = SequenceMatcher(None, i, reference).ratio()
            if similarity > highest_similarity:
                highest_similarity = similarity
                highest_similarity_item_in_drp_ix = ix
                highest_similarity_item_in_drp = i
            # print(f"{i}: {similarity}")
        # print(f"{highest_similarity_item_in_drp_ix}: {highest_similarity_item_in_drp}: {highest_similarity}")
        return highest_similarity_item_in_drp_ix

    def update_ssitc(self):
        """Update SSITC dropdown"""
        highsim_ix = \
            self._find_most_similar_item_in_dropdown(drp=self.drp_ssitc_flag_col, reference=self.focus_pretty)
        self.drp_ssitc_flag_col.setCurrentIndex(highsim_ix)

    def update_abslim(self, init=True):
        """
        Update absolute limits

        Parameters
        ----------
        init:   bool
                True if new focus var is selected, determines limits for selected focus.
                False if another var than the focus var is selected.

        """

        if init:
            self.drp_abslim_flag_col.setCurrentIndex(self.focus_pretty_ix)
        else:
            pass
        var_col = self._get_var_col_from_pretty(pretty=self.drp_abslim_flag_col.currentText())
        upper_lim = np.ceil(self.tab_data_df[var_col].quantile(0.999))
        lower_lim = np.ceil(self.tab_data_df[var_col].quantile(0.001))
        self.lne_abslim_flag0_upper_lim.setText(f"{upper_lim:.0f}")
        self.lne_abslim_flag0_lower_lim.setText(f"{lower_lim:.0f}")

    def update_completeness(self):
        # Completeness, set if col was found
        if self.drp_completeness_flag_col.currentText():
            var_col = self._get_var_col_from_pretty(pretty=self.drp_completeness_flag_col.currentText())
            median = self.tab_data_df[var_col].median()  # Just an estimate what might be full number of records
            flag0_lim = int(median / 100 * 99)  # 1% missing
            flag1_lim = int(median / 100 * 97)  # 3% missing
            self.lne_completeness_flag0_lim.setText(f"{flag0_lim:.0f}")  # <
            self.lne_completeness_flag1_lim.setText(f"{flag1_lim:.0f}")  # <=

    def update_rawdata_spikes(self):
        self.update_rawdata_flagvars(self.drp_rawdata_spikes_statflag, self.drp_rawdata_spikes_flagvar)

    def update_rawdata_amplres(self):
        self.update_rawdata_flagvars(self.drp_rawdata_amplres_statflag_col, self.drp_rawdata_amplres_flagvar_col)

    def update_rawdata_dropout(self):
        self.update_rawdata_flagvars(self.drp_rawdata_dropout_statflag_col, self.drp_rawdata_dropout_flagvar_col)

    def update_rawdata_abslim(self):
        self.update_rawdata_flagvars(self.drp_rawdata_abslim_statflag_col, self.drp_rawdata_abslim_flagvar_col)

    def update_rawdata_skewkurt_hf(self):
        self.update_rawdata_flagvars(self.drp_rawdata_skewkurt_hf_statflag_col,
                                     self.drp_rawdata_skewkurt_hf_flagvar_col)

    def update_rawdata_skewkurt_sf(self):
        self.update_rawdata_flagvars(self.drp_rawdata_skewkurt_sf_statflag_col,
                                     self.drp_rawdata_skewkurt_sf_flagvar_col)

    def update_rawdata_discont_hf(self):
        self.update_rawdata_flagvars(self.drp_rawdata_discont_hf_statflag_col,
                                     self.drp_rawdata_discont_hf_flagvar_col)

    def update_rawdata_discont_sf(self):
        self.update_rawdata_flagvars(self.drp_rawdata_discont_sf_statflag_col,
                                     self.drp_rawdata_discont_sf_flagvar_col)

    def update_rawdata_attack_angle(self):
        self.update_rawdata_flagvars(self.drp_rawdata_aa_statflag_col, self.drp_rawdata_aa_flagvar_col)

    def update_rawdata_nonsteadyu(self):
        self.update_rawdata_flagvars(self.drp_rawdata_nonsteadyu_statflag_col,
                                     self.drp_rawdata_nonsteadyu_flagvar_col)

    def update_dynamic_fields(self):
        """Update fields when e.g. a new focus var was selected, make field suggestions"""
        # Find item most similar to focus_col
        if self.focus_col:
            self.update_ssitc()
            self.update_abslim(init=True)
            self.update_completeness()
            self.update_rawdata_spikes()
            self.update_rawdata_amplres()
            self.update_rawdata_dropout()
            self.update_rawdata_abslim()
            self.update_rawdata_skewkurt_hf()
            self.update_rawdata_skewkurt_sf()
            self.update_rawdata_discont_hf()
            self.update_rawdata_discont_sf()
            self.update_rawdata_attack_angle()
            self.update_rawdata_nonsteadyu()

    def update_rawdata_flagvars(self, drp_statflag, drp_flagvar):
        """Populate flagvar dropdowns with info from statflag units"""
        if not drp_statflag:  # If empty
            return None
        drp_flagvar.clear()
        var_col = self._get_var_col_from_pretty(pretty=drp_statflag.currentText())
        flagvars = var_col[1]  # Get units (flag vars) for selected flag
        flagvars = flagvars[1:]  # Cut off first number in units row (the '8'), e.g. units 8u/v/w/ts/co2/h2o/ch4/none
        flagvars = flagvars.split('/')
        suggested_ix = 0
        for ix, flagvar in enumerate(flagvars):
            item = '{}: {}'.format(ix, flagvar)
            drp_flagvar.addItem(item)

            # Try to pre-select relevant flagvar
            if not self.focus_col:
                continue
            else:
                if flagvar in self.focus_col[0]:
                    suggested_ix = ix
        drp_flagvar.setCurrentIndex(suggested_ix)

    def populate_settings_fields(self):
        # EddyPro default flag (SSITC) (qc_*)
        self.drp_ssitc_flag_col.clear()
        self.drp_scf_flag_col.clear()
        self.drp_abslim_flag_col.clear()
        self.drp_completeness_flag_col.clear()

        for idx, col in enumerate(self.tab_data_df):
            pretty = self.col_list_pretty[idx]

            # SSITC (needs qc_*)
            if any(fnmatch.fnmatch(col[0], ids) for ids in utils.vargroups.QCFLAGS_EDDYPRO_SSITC):
                self.drp_ssitc_flag_col.addItem(pretty)

            # SCF (needs *_scf)
            if any(fnmatch.fnmatch(col[0], ids) for ids in utils.vargroups.QCFLAGS_EDDYPRO_SCF):
                self.drp_scf_flag_col.addItem(pretty)

            # Absolute Limits (needs all)
            self.drp_abslim_flag_col.addItem(pretty)
            default_ix = 0
            self.drp_abslim_flag_col.setCurrentIndex(default_ix)  # Set dropdowns to found ix

            # Completeness
            if any(fnmatch.fnmatch(col[0], ids) for ids in utils.vargroups.QCFLAGS_EDDYPRO_RAWDATA_COMPLETENESS):
                self.drp_completeness_flag_col.addItem(pretty)

            # Signal Strength GA
            if any(fnmatch.fnmatch(col[0], ids) for ids in utils.vargroups.SIGNAL_STRENGTH_GA):
                self.drp_ssga_flag_col.addItem(pretty)

            # Stability Parameter
            if any(fnmatch.fnmatch(col[0], ids) for ids in utils.vargroups.MONIN_OBUKHOV_STABILITY):
                self.drp_stability_flag_col.addItem(pretty)

            # Raw Data Spikes hf
            if any(fnmatch.fnmatch(col[0], ids) for ids in utils.vargroups.QCFLAGS_EDDYPRO_RAWDATA_SPIKES):
                self.drp_rawdata_spikes_statflag.addItem(pretty)

            # Raw Data Amplitude Resolution hf
            if any(fnmatch.fnmatch(col[0], ids) for ids in utils.vargroups.QCFLAGS_EDDYPRO_RAWDATA_AMPLRES):
                self.drp_rawdata_amplres_statflag_col.addItem(pretty)

            # Raw Data Drop-out hf
            if any(fnmatch.fnmatch(col[0], ids) for ids in utils.vargroups.QCFLAGS_EDDYPRO_RAWDATA_DROPOUT):
                self.drp_rawdata_dropout_statflag_col.addItem(pretty)

            # Raw Data Absolute Limits hf
            if any(fnmatch.fnmatch(col[0], ids) for ids in utils.vargroups.QCFLAGS_EDDYPRO_RAWDATA_ABSLIM):
                self.drp_rawdata_abslim_statflag_col.addItem(pretty)

            # Raw Data Skewness / Kurtosis hf
            if any(fnmatch.fnmatch(col[0], ids) for ids in utils.vargroups.QCFLAGS_EDDYPRO_RAWDATA_SKEWKURT_HF):
                self.drp_rawdata_skewkurt_hf_statflag_col.addItem(pretty)

            # Raw Data Skewness / Kurtosis sf
            if any(fnmatch.fnmatch(col[0], ids) for ids in utils.vargroups.QCFLAGS_EDDYPRO_RAWDATA_SKEWKURT_SF):
                self.drp_rawdata_skewkurt_sf_statflag_col.addItem(pretty)

            # Raw Data Discontinuities hf
            if any(fnmatch.fnmatch(col[0], ids) for ids in utils.vargroups.QCFLAGS_EDDYPRO_RAWDATA_DISCONT_HF):
                self.drp_rawdata_discont_hf_statflag_col.addItem(pretty)

            # Raw Data Discontinuities sf
            if any(fnmatch.fnmatch(col[0], ids) for ids in utils.vargroups.QCFLAGS_EDDYPRO_RAWDATA_DISCONT_SF):
                self.drp_rawdata_discont_sf_statflag_col.addItem(pretty)

            # Attack angle
            if any(fnmatch.fnmatch(col[0], ids) for ids in utils.vargroups.QCFLAGS_EDDYPRO_RAWDATA_ATTACKANGLE):
                self.drp_rawdata_aa_statflag_col.addItem(pretty)

            # Non-steady wind
            if any(fnmatch.fnmatch(col[0], ids) for ids in utils.vargroups.QCFLAGS_EDDYPRO_RAWDATA_NONSTEADYWIND):
                self.drp_rawdata_nonsteadyu_statflag_col.addItem(pretty)

    def set_focus(self):
        """Set focus to the first variables that was selected"""
        self.focus_col = list(self.selected_vars_lookup_dict)[0]
        self.focus_pretty_ix = list(self.col_dict_tuples.keys())[
            list(self.col_dict_tuples.values()).index(self.focus_col)]
        self.focus_pretty = self.col_list_pretty[self.focus_pretty_ix]

    def add_target(self):
        # self.remove_prev_qcf_method_columns()  # Reset columns
        self.set_target_col_to_add()
        self.add_target_to_selected()
        self.set_focus()
        self.update_dynamic_fields()
        self.update_selected_vars_list()
        self.plot_data()
        self.btn_calc.setEnabled(True)

    def remove_target(self):
        # self.remove_aux_cols_from_selected()
        self.set_target_col_to_remove()
        self.remove_target_from_selected()
        self.update_selected_vars_list()
        empty = self.check_if_selected_empty()
        if empty:
            self.reset_fig()
            self.webview_sankey.setHtml('')
            self.btn_calc.setDisabled(True)
            self.btn_add_as_new_var.setDisabled(True)
            self.btn_add_as_new_var.setText(f"+ Add Quality Flag")
            self.vars_selected_df = pd.DataFrame()
        else:
            self.set_focus()
            self.plot_data()

    def remove_prev_qcf_method_columns(self):
        for col in self.vars_selected_df.columns:
            if str(col[0]).startswith('QCF_') | str(col[0]).startswith('_'):
                self.vars_selected_df.drop(axis=1, labels=col, inplace=True)  # Remove aux cols

    def reset_fig(self):
        for ax in self.fig.axes:
            self.fig.delaxes(ax)
            self.fig.canvas.draw()
            self.fig.canvas.flush_events()

    def set_target_col_to_add(self):
        """Get column name of target var from available var list"""
        target_var = self.lst_varlist_available.selectedIndexes()
        target_var_ix = target_var[0].row()
        self.target_col = self.col_dict_tuples[target_var_ix]
        self.target_pretty = self.col_list_pretty[target_var_ix]  # Needs new pretty list (screen names)

    def set_target_col_to_remove(self):
        """Get column name of target var from selected var list"""
        target_var = self.lst_varlist_selected.selectedIndexes()
        target_var_ix = target_var[0].row()
        self.target_col = list(self.selected_vars_lookup_dict.keys())[target_var_ix]  # Get key by index
        # self.target_col = self.col_dict_tuples[target_var_ix]
        # self.target_pretty = self.col_list_pretty[target_var_ix]  # Needs new pretty list (screen names)

    def add_target_to_selected(self):
        """Add var from available to selected df and lookup dict"""
        self.selected_vars_lookup_dict[self.target_col] = self.target_pretty
        self.vars_selected_df[self.target_col] = self.vars_available_df[self.target_col]

    def prepare_export(self):
        export_cols = [self.qcf_col]
        for col in self.vars_selected_df.columns:
            if str(col[0]).startswith('_QCF'):
                export_cols.append(col)
        export_df = self.vars_selected_df[export_cols].copy()
        export_df.columns = pd.MultiIndex.from_tuples(export_df.columns)
        # self.remove_prev_qcf_method_columns()
        # export_df = self.vars_selected_df.copy()
        # export_df.columns = [(str(col[0]) + '+anCF', col[1]) for col in export_df.columns]  # Add suffix
        # todo solved? hier weiter raw data flags haben noch probleme mit nans, sollen missing sein und nicht 0-1-2
        return export_df

    def get_selected(self, main_df):
        """Return modified class data back to main data"""
        export_df = self.prepare_export()
        main_df = frames.export_to_main(main_df=main_df,
                                        export_df=export_df,
                                        tab_data_df=self.tab_data_df)  # Return results to main
        self.btn_add_as_new_var.setDisabled(True)
        return main_df

    def make_dynamic_axes_dict(self):
        """Generate axes for focus variable and other selected variables"""
        # Get selected variables for subset, ignore aux columns
        subset_df = self.vars_selected_df[list(self.selected_vars_lookup_dict)].copy()

        gs = gridspec.GridSpec(len(subset_df.columns) + 1, 3)  # rows, cols
        gs.update(wspace=0.2, hspace=0.2, left=0.04, right=0.97, top=0.97, bottom=0.03)

        # Add plots for selected vars
        axes_dict = {}
        first_ax = -9999
        ix = 0
        for ix, col in enumerate(subset_df):
            if ix == 0:
                ax = self.fig.add_subplot(gs[ix, 0:2])
                first_ax = ax
            else:
                ax = self.fig.add_subplot(gs[ix, 0:2], sharex=first_ax)
            axes_dict[col] = ax

        # Add heatmap plot
        axes_dict['heatmap'] = self.fig.add_subplot(gs[0:ix + 1, 2])

        # Add bar plot
        # _from = ix+1
        # _to=
        axes_dict['bars'] = self.fig.add_subplot(gs[ix + 1, 0:3])

        # Format
        for col, ax in axes_dict.items():
            gui.plotfuncs.default_format(ax=ax, txt_xlabel=False)

        return axes_dict

    def plot_data(self):
        # Delete all axes in figure
        for ax in self.fig.axes:
            self.fig.delaxes(ax)
        axes_dict = self.make_dynamic_axes_dict()
        current_color = -1

        # Plot selected vars
        counter = -1
        lines = []
        for col, ax in axes_dict.items():
            if (col == 'heatmap') | (col == 'bars'):
                continue  # Will be plotted later
            counter += 1
            current_color += 1
            line, = ax.plot_date(x=self.vars_selected_df.index, y=self.vars_selected_df[col],
                                 color='black', alpha=1, ls='-',
                                 marker='o', markeredgecolor='none', ms=4, zorder=98, label=f"{col[0]}")
            lines.append(line)

            # Plot the individual flag markers
            flag_marker_size = 4
            flag_marker_color_idx = -1
            for flagcol in self.vars_selected_df.columns:
                if str(flagcol[0]).startswith('_QCF_AGG'):
                    continue  # Will be plotted later
                if str(flagcol[0]).startswith('_QCF_'):
                    print(flagcol)
                    flag_marker_size += 3
                    flag_marker_color_idx += 1
                    shared_args = dict(mec=self.flag_marker_color_list[flag_marker_color_idx],
                                       ls='none', zorder=98, mfc='none', ms=flag_marker_size)

                    # Rejected values
                    filter_bad_vals = self.vars_selected_df[flagcol] == 2
                    if filter_bad_vals.sum() > 0:
                        x = self.vars_selected_df.loc[filter_bad_vals].index
                        y = self.vars_selected_df.loc[filter_bad_vals, [col]].copy()
                        # In case there is a flag value but no measured value:
                        if len(y.dropna()) > 0:
                            pass
                        else:
                            y = y.fillna(self.vars_selected_df[col].max())
                        label = "FLAG: {} ({} bad values)".format(flagcol[0], filter_bad_vals.sum())
                        # label = f"{flagcol[0]} ({filter_bad_vals.sum()} bad values)"
                        line, = ax.plot_date(x=x, y=y, alpha=.8, marker='D', mew=2,
                                             label=label, **shared_args)
                        lines.append(line)

                    # OK values
                    filter_ok_vals = self.vars_selected_df[flagcol] == 1
                    if filter_ok_vals.sum() > 0:
                        x = self.vars_selected_df.loc[filter_ok_vals].index
                        y = self.vars_selected_df.loc[filter_ok_vals, [col]]
                        # In case there is a flag value but no measured value:
                        if len(y.dropna()) > 0:
                            pass
                        else:
                            y = y.fillna(self.vars_selected_df[col].max())
                        line, = ax.plot_date(x=x, y=y, alpha=.5, marker='s', mew=1,
                                             label=f"FLAG: {flagcol[0]} ({filter_ok_vals.sum()} OK values)",
                                             **shared_args)
                        lines.append(line)

            gui.plotfuncs.default_grid(ax=ax)
            locator = mdates.AutoDateLocator(minticks=3, maxticks=30)
            formatter = mdates.ConciseDateFormatter(locator)
            ax.xaxis.set_major_locator(locator)
            ax.xaxis.set_major_formatter(formatter)
            if counter == 0:
                gui.plotfuncs.default_legend(ax=ax, labelspacing=1,
                                             from_line_collection=True, line_collection=lines)
                ax.text(0.02, 0.95, f"FOCUS: {col[0]}",
                        size=theme.FONTSIZE_HEADER_AXIS, color=theme.FONTCOLOR_HEADER_AXIS,
                        backgroundcolor='#FFE082', transform=ax.transAxes, alpha=1,
                        horizontalalignment='left', verticalalignment='top', zorder=99)
            else:
                ax.text(0.01, 0.97, f"{col[0]}",
                        size=theme.FONTSIZE_HEADER_AXIS, color=theme.FONTCOLOR_HEADER_AXIS,
                        backgroundcolor='none', transform=ax.transAxes, alpha=1,
                        horizontalalignment='left', verticalalignment='top', zorder=99)
        # self.fig.canvas.draw()
        # self.fig.canvas.flush_events()

        self.plot_heatmap(ax=axes_dict['heatmap'])
        self.plot_bars(ax=axes_dict['bars'])

        self.fig.canvas.draw()
        self.fig.canvas.flush_events()

    def _show_hide_plot(self, ax):
        """Check if there is data for the plot in the df"""
        show_plot = False
        for flagcol in self.vars_selected_df.columns:
            if '_QCF_AGG' in flagcol[0]:
                show_plot = True
                break
        if show_plot:
            ax.set_visible(True)
        else:
            ax.set_visible(False)  # Hide if no data
        return show_plot

    def _create_qcf_subset(self):
        """Collect all QCF *AND* _QCF columns in subset"""
        _subset_df = pd.DataFrame()
        qcf_cols = []  # Subset
        for flagcol in self.vars_selected_df.columns:
            if str(flagcol[0]).startswith('QCF_') | str(flagcol[0]).startswith('_QCF_'):
                qcf_cols.append(flagcol)
        _subset_df = self.vars_selected_df[qcf_cols].copy()

        # Find flag for missing values
        _missing_col = None
        for flagcol in _subset_df:
            if str(flagcol[0]).startswith('_QCF_MISSING_'):
                _missing_col = flagcol
                break

        _filter_missing = _subset_df[_missing_col] == 2
        _subset_df = _subset_df[~_filter_missing]

        return _subset_df

    def plot_bars(self, ax):
        """Plot bars of aggregated QC flag"""
        _show = self._show_hide_plot(ax=ax)
        if not _show:
            return None

        _plot_df = frames.flatten_multiindex_all_df_cols(df=self.qcf_counts_df.copy())  # Flatten multiindex to one row
        labels = list(_plot_df.columns)  # Use column names as labels
        _bottom = np.nan  # Needed for stacking multiple bars on top of each other

        for flag, row in _plot_df.iterrows():
            _flag_counts = _plot_df.loc[flag].replace(np.nan, 0)  # Needs 0 for correct counts
            if flag == 0:
                ax.bar(labels, _flag_counts, width=0.8, label=flag)
                for bar_ix, bar in enumerate(ax.patches):
                    # kudos: https://www.pythoncharts.com/matplotlib/stacked-bar-charts-labels/

                    # Show flag 0 (best) counts in plot
                    ax.text(bar.get_x() + bar.get_width() / 2, bar.get_height() / 2,
                            f"{round(bar.get_height())}", ha='center', color='w', weight='bold', size=6)

                    # Show labels *inside* plot
                    ax.text(bar.get_x() + bar.get_width() / 10, bar.get_y() / 2,
                            f"{labels[bar_ix]}", ha='left', color='white', weight='bold', size=7, rotation=90)

                _bottom = _flag_counts
            else:
                ax.bar(labels, _flag_counts, width=0.8, bottom=_bottom, label=flag)
                _bottom = _flag_counts + _bottom

        ax.text(0.02, 0.95, f"Quality flags",
                size=theme.FONTSIZE_HEADER_AXIS, color=theme.FONTCOLOR_HEADER_AXIS,
                backgroundcolor='#FFE082', transform=ax.transAxes, alpha=1,
                horizontalalignment='left', verticalalignment='top', zorder=99)
        ax.set_xticklabels([])
        gui.plotfuncs.default_legend(ax=ax)
        gui.plotfuncs.default_format(ax=ax, txt_xlabel='flag', txt_ylabel='counts')

    def plot_heatmap(self, ax):
        """Plot heatmap of aggregated QC flag"""
        _show = self._show_hide_plot(ax=ax)
        if not _show:
            return None

        # Show missing values as missing data points in heatmap, not as color
        apply_filter_missing_values = False
        filter_missing_values = None
        for flagcol in self.vars_selected_df.columns:
            if str(flagcol[0]).startswith('_QCF_MISSING_'):
                missing_flag = self.vars_selected_df[flagcol]
                filter_missing_values = missing_flag == 2
                apply_filter_missing_values = True
                break

        for flagcol in self.vars_selected_df.columns:
            if str(flagcol[0]).startswith('QCF_'):
                selected_col_data = self.vars_selected_df[flagcol].copy()
                if apply_filter_missing_values:
                    selected_col_data[filter_missing_values] = np.nan

                x, y, z, plot_df = heatmap.Run._prepare(selected_col_data, display_type='Time & Date')
                cmap = plt.get_cmap('Reds')
                heatmap.Run.plot_heatmap(x=x, y=y, z=z, ax=ax, cmap=cmap, color_bad='Grey',
                                         info_txt="", vmin=selected_col_data.min(), vmax=selected_col_data.max(),
                                         cb_digits_after_comma=0)
                ax.set_facecolor('white')
                ax.set_xticks(['6:00', '12:00', '18:00'])
                ax.set_xticklabels([6, 12, 18])
                gui.plotfuncs.nice_date_ticks(ax=ax, minticks=3, maxticks=7, which='y')
                gui.plotfuncs.default_format(ax=ax, txt_xlabel='Time')

                ax.text(0.05, 0.97, "QCF - Overall Flag",
                        size=theme.FONTSIZE_HEADER_AXIS_SMALL, color=theme.FONTCOLOR_HEADER_AXIS,
                        backgroundcolor='white', transform=ax.transAxes, alpha=.7,
                        horizontalalignment='left', verticalalignment='top', zorder=100,
                        fontweight='bold')

                # Overall quality
                filter_rejected = selected_col_data > 1
                _selected_col_data = selected_col_data.copy()
                num_records = len(_selected_col_data)
                _selected_col_data[filter_rejected] = 2
                worst_quality = num_records * 2  # If all flags would be 2
                flagsum = _selected_col_data.sum()
                overall_quality = (1 - (flagsum / worst_quality)) * 100
                avg_quality_per_record = flagsum / num_records

                ax.text(0.9, 0.05,
                        f"Number Of Records: {num_records}\n"
                        f"Average Flag Per Record: {avg_quality_per_record:.2f}\n"
                        f"Overall Quality Index: {overall_quality:.2f}%",
                        size=theme.FONTSIZE_HEADER_AXIS_SMALL, color=theme.FONTCOLOR_HEADER_AXIS,
                        backgroundcolor='white', transform=ax.transAxes, alpha=.7,
                        horizontalalignment='right', verticalalignment='bottom', zorder=100,
                        fontweight='bold')

    def remove_target_from_selected(self):
        """Remove from lookup dict and df"""
        del self.selected_vars_lookup_dict[self.target_col]
        self.vars_selected_df.drop(self.target_col, axis=1, inplace=True)

    def update_selected_vars_list(self):
        """
        Check the df for selected variables and add to list and lookup

        This ignores all auxiliary columns that are also part of the df,
        namely all columns that start with underscore '_' of columns that
        are a quality flag 'QCF_'.

        """
        self.lst_varlist_selected.clear()
        for col in self.vars_selected_df.columns:
            if str(col[0]).startswith('_') | str(col[0]).startswith('QCF_'):
                continue
            item = qw.QListWidgetItem(self.selected_vars_lookup_dict[col])
            self.lst_varlist_selected.addItem(item)

    def check_if_selected_empty(self):
        _list = self.lst_varlist_selected
        # if self.vars_selected_df.empty:
        if not self.selected_vars_lookup_dict:  # Check if empty
            _list.addItem('-empty-')
            empty = True
        else:
            empty = False
        return empty

    def add_qcf_method_columns(self):
        """Calculate and add the different flags"""
        shared_args = dict(col_list_pretty=self.col_list_pretty,
                           col_dict_tuples=self.col_dict_tuples,
                           tab_data_df=self.tab_data_df)

        # Flux flags
        self.vars_selected_df = \
            flf.MissingValuesFocus(focus_col=self.focus_col,
                                   vars_selected_df=self.vars_selected_df,
                                   tab_data_df=self.tab_data_df).add_qcf_missing()

        if self.chk_ssitc_flag.isChecked():
            self.vars_selected_df = \
                flf.SSITC(drp_ssitc_flag_col=self.drp_ssitc_flag_col,
                          lne_ssitc_flag0_lim=self.lne_ssitc_flag0_lim,
                          lne_ssitc_flag1_lim=self.lne_ssitc_flag1_lim,
                          vars_selected_df=self.vars_selected_df,
                          **shared_args).add_qcf_ssitc()

        if self.chk_scf_flag.isChecked():
            self.vars_selected_df = \
                flf.SpectralCorrectionFactor(drp_scf_flag_col=self.drp_scf_flag_col,
                                             lne_scf_flag0_lim=self.lne_scf_flag0_lim,
                                             lne_scf_flag1_lim=self.lne_scf_flag1_lim,
                                             vars_selected_df=self.vars_selected_df,
                                             **shared_args).add_qcf_scf()

        if self.chk_abslim_flag.isChecked():
            self.vars_selected_df = \
                flf.AbsoluteLimits(drp_abslim_flag_col=self.drp_abslim_flag_col,
                                   lne_abslim_flag0_upper_lim=self.lne_abslim_flag0_upper_lim,
                                   lne_abslim_flag0_lower_lim=self.lne_abslim_flag0_lower_lim,
                                   vars_selected_df=self.vars_selected_df,
                                   **shared_args).add_qcf_abslim()

        if self.chk_ssga_flag.isChecked():
            self.vars_selected_df = \
                flf.GaSignalStrength(drp_ssga_flag_col=self.drp_ssga_flag_col,
                                     drp_ssga_type=self.drp_ssga_type,
                                     lne_ssga_threshold=self.lne_ssga_threshold,
                                     vars_selected_df=self.vars_selected_df,
                                     **shared_args).add_qcf_abslim()

        if self.chk_stability_flag.isChecked():
            self.vars_selected_df = \
                flf.StabilityParameter(drp_stability_flag_col=self.drp_stability_flag_col,
                                       lne_stability_flag0_upper_lim=self.lne_stability_flag0_upper_lim,
                                       lne_stability_flag0_lower_lim=self.lne_stability_flag0_lower_lim,
                                       vars_selected_df=self.vars_selected_df,
                                       **shared_args).add_qcf_abslim()

        # Raw data flags
        if self.chk_completeness_flag.isChecked():
            self.vars_selected_df = \
                rdf.Completeness(drp_completeness_flag_col=self.drp_completeness_flag_col,
                                 lne_completeness_flag0_lim=self.lne_completeness_flag0_lim,
                                 lne_completeness_flag1_lim=self.lne_completeness_flag1_lim,
                                 vars_selected_df=self.vars_selected_df,
                                 **shared_args).add_qcf_scf()

        # Raw data flags 8-digits or 2-digits
        # Spikes
        if self.chk_rawdata_spikes_flag.isChecked():
            self.vars_selected_df = \
                rdf.RawDataFlag8(drp_statflag=self.drp_rawdata_spikes_statflag,
                                 drp_flagvar=self.drp_rawdata_spikes_flagvar,
                                 vars_selected_df=self.vars_selected_df,
                                 **shared_args).get_qcf_rawdata_flag()

        # Amplitude resolution
        if self.chk_rawdata_amplres_flag.isChecked():
            self.vars_selected_df = \
                rdf.RawDataFlag8(drp_statflag=self.drp_rawdata_amplres_statflag_col,
                                 drp_flagvar=self.drp_rawdata_amplres_flagvar_col,
                                 vars_selected_df=self.vars_selected_df,
                                 **shared_args).get_qcf_rawdata_flag()

        # Drop-out
        if self.chk_rawdata_dropout_flag.isChecked():
            self.vars_selected_df = \
                rdf.RawDataFlag8(drp_statflag=self.drp_rawdata_dropout_statflag_col,
                                 drp_flagvar=self.drp_rawdata_dropout_flagvar_col,
                                 vars_selected_df=self.vars_selected_df,
                                 **shared_args).get_qcf_rawdata_flag()

        # Drop-out
        if self.chk_rawdata_abslim_flag.isChecked():
            self.vars_selected_df = \
                rdf.RawDataFlag8(drp_statflag=self.drp_rawdata_abslim_statflag_col,
                                 drp_flagvar=self.drp_rawdata_abslim_flagvar_col,
                                 vars_selected_df=self.vars_selected_df,
                                 **shared_args).get_qcf_rawdata_flag()

        # Skewness / kurtosis (hard flag)
        if self.chk_rawdata_skewkurt_hf_flag.isChecked():
            self.vars_selected_df = \
                rdf.RawDataFlag8(drp_statflag=self.drp_rawdata_skewkurt_hf_statflag_col,
                                 drp_flagvar=self.drp_rawdata_skewkurt_hf_flagvar_col,
                                 vars_selected_df=self.vars_selected_df,
                                 **shared_args).get_qcf_rawdata_flag()

        # Skewness / kurtosis (soft flag)
        if self.chk_rawdata_skewkurt_sf_flag.isChecked():
            self.vars_selected_df = \
                rdf.RawDataFlag8(drp_statflag=self.drp_rawdata_skewkurt_sf_statflag_col,
                                 drp_flagvar=self.drp_rawdata_skewkurt_sf_flagvar_col,
                                 vars_selected_df=self.vars_selected_df,
                                 **shared_args).get_qcf_rawdata_flag()

        # Discontinuities (hard flag)
        if self.chk_rawdata_discont_hf_flag.isChecked():
            self.vars_selected_df = \
                rdf.RawDataFlag8(drp_statflag=self.drp_rawdata_discont_hf_statflag_col,
                                 drp_flagvar=self.drp_rawdata_discont_hf_flagvar_col,
                                 vars_selected_df=self.vars_selected_df,
                                 **shared_args).get_qcf_rawdata_flag()

        # Discontinuities (soft flag)
        if self.chk_rawdata_discont_sf_flag.isChecked():
            self.vars_selected_df = \
                rdf.RawDataFlag8(drp_statflag=self.drp_rawdata_discont_sf_statflag_col,
                                 drp_flagvar=self.drp_rawdata_discont_sf_flagvar_col,
                                 vars_selected_df=self.vars_selected_df,
                                 **shared_args).get_qcf_rawdata_flag()

        # Attack angle
        if self.chk_rawdata_aa_flag.isChecked():
            self.vars_selected_df = \
                rdf.RawDataFlag8(drp_statflag=self.drp_rawdata_aa_statflag_col,
                                 drp_flagvar=self.drp_rawdata_aa_flagvar_col,
                                 vars_selected_df=self.vars_selected_df,
                                 **shared_args).get_qcf_rawdata_flag()

        # Non-steady wind
        if self.chk_rawdata_nonsteadyu_flag.isChecked():
            self.vars_selected_df = \
                rdf.RawDataFlag8(drp_statflag=self.drp_rawdata_nonsteadyu_statflag_col,
                                 drp_flagvar=self.drp_rawdata_nonsteadyu_flagvar_col,
                                 vars_selected_df=self.vars_selected_df,
                                 **shared_args).get_qcf_rawdata_flag()

    def collect_qcf_method_cols(self):
        qcf_method_cols = []
        # Search flag cols
        for col in self.vars_selected_df.columns:
            if str(col[0]).startswith('_QCF_'):
                qcf_method_cols.append(col)
        qcf_flags_df = self.vars_selected_df[qcf_method_cols].copy()
        return qcf_flags_df

    def calc_qcf(self):
        """Calculate overall QCF from the individual method flags"""
        qcf_flags_df = self.collect_qcf_method_cols()

        # Agg flag columns
        qcf_agg_hf_col = (f"_QCF_AGG_HARDFLAGS_{self.focus_col[0]}", "[>1=bad]")
        qcf_agg_sf_col = (f"_QCF_AGG_SOFTFLAGS_{self.focus_col[0]}", "[>2=bad]")
        qcf_agg_col = (f"_QCF_AGG_{self.focus_col[0]}", "[hf+sf]")
        qcf_col = (f"QCF_{self.focus_col[0]}", "[2=bad]")
        qcf_flags_df[qcf_agg_hf_col] = np.nan
        qcf_flags_df[qcf_agg_sf_col] = np.nan
        qcf_flags_df[qcf_agg_col] = np.nan
        qcf_flags_df[qcf_col] = np.nan

        # Sum of hard flags
        # Hard flags have value 2 and are rejected.
        qcf_flags_hardflags_df = qcf_flags_df[qcf_flags_df > 1]
        qcf_flags_df[qcf_agg_hf_col] = qcf_flags_hardflags_df.sum(axis=1)

        # Sum of soft flags
        # 1 or 2 soft flags per record = OK, 3+ soft flags = bad data, reject
        # Necessary b/c two soft flags are still acceptable as OK data. However, since
        # the sum of two soft flags yields 2 and therefore has the same value as one
        # hard flag, this distinction necessary.
        qcf_flags_softflags_df = qcf_flags_df[qcf_flags_df == 1]
        qcf_flags_df[qcf_agg_sf_col] = qcf_flags_softflags_df.sum(axis=1)

        # Sum of all flags (soft and hard)
        qcf_flags_df[qcf_agg_col] = qcf_flags_df[qcf_agg_hf_col] \
                                    + qcf_flags_df[qcf_agg_sf_col]

        # QCF, overall flag 0-1-2
        # =======================
        # flag 0 for records where sum of all flags = 0
        setto0 = qcf_flags_df[qcf_agg_col] == 0
        qcf_flags_df.loc[setto0, [qcf_col]] = 0

        # flag 2 for records where sum of soft flags > 3
        setto2 = qcf_flags_df[qcf_agg_sf_col] > 3
        qcf_flags_df.loc[setto2, [qcf_col]] = 2

        # flag 1 for records where sum of soft flags = 1 or 2 or 3
        setto1 = (qcf_flags_df[qcf_agg_sf_col] == 1) \
                 | (qcf_flags_df[qcf_agg_sf_col] == 2) \
                 | (qcf_flags_df[qcf_agg_sf_col] == 3)
        qcf_flags_df.loc[setto1, [qcf_col]] = 1

        # flag 2 for records where sum of hard flags > 1
        setto2 = qcf_flags_df[qcf_agg_hf_col] > 1
        qcf_flags_df.loc[setto2, [qcf_col]] = 2

        # Update flags in selected
        self.vars_selected_df[qcf_agg_hf_col] = qcf_flags_df[qcf_agg_hf_col].copy()
        self.vars_selected_df[qcf_agg_sf_col] = qcf_flags_df[qcf_agg_sf_col].copy()
        self.vars_selected_df[qcf_agg_col] = qcf_flags_df[qcf_agg_col].copy()
        self.vars_selected_df[qcf_col] = qcf_flags_df[qcf_col].copy()
        return qcf_col, qcf_agg_col, qcf_agg_hf_col, qcf_agg_sf_col

    def save_to_txt_file(self):
        """Export correction fractions to csv file"""
        if not Path.is_dir(self.sub_outdir):
            logger.log(name=f"Creating folder {self.sub_outdir} ...", dict={}, highlight=False)  # Log info
            os.makedirs(self.sub_outdir)
        now_time_dt, _ = get_current_time()
        now_time_str = now_time_dt.strftime("%Y%m%d%H%M%S")
        if self.focus_col:
            outfile = self.sub_outdir / f"{now_time_str}_ec_quality_control_{self.focus_col[0]}.txt"
        else:
            outfile = self.sub_outdir / f"{now_time_str}_ec_quality_control_notes.txt"

        txt = self.txe_textbox.toPlainText()

        text_file = open(outfile, "w")
        n = text_file.write(txt)
        text_file.close()

        # self.correction_fractions_df.to_csv(outfile, index=False)
        # self.enable_disable_buttons()

        # todo
        pass
