import numpy as np


class SSITC():
    """
    EddyPro Default Flag (SSITC)
    """

    def __init__(self, drp_ssitc_flag_col, lne_ssitc_flag0_lim, lne_ssitc_flag1_lim,
                 col_list_pretty, col_dict_tuples, vars_selected_df, tab_data_df):
        # Get settings
        ssitc_flag_pretty = drp_ssitc_flag_col.currentText()
        ssitc_flag_pretty_idx = col_list_pretty.index(ssitc_flag_pretty)
        self.flag_col = col_dict_tuples[ssitc_flag_pretty_idx]
        self.flag0_lim = int(lne_ssitc_flag0_lim.text())
        self.flag1_lim = int(lne_ssitc_flag1_lim.text())

        self.tab_data_df = tab_data_df

        self.vars_selected_df = vars_selected_df.copy()

    def add_qcf_ssitc(self):
        self._set_colnames()
        self._init_new_cols()
        self._add_aux_cols()
        self._calc_qcfcol()
        return self.vars_selected_df

    def _set_colnames(self):
        self.values_col = (f"_AUX_SSITC_VALS_{self.flag_col[0]}", f"{self.flag_col[1]}")
        self.flag0_lim_col = (f"_AUX_SSITC_FLAG0_LIM_{self.flag_col[0]}", f"{self.flag_col[1]}")
        self.flag1_lim_col = (f"_AUX_SSITC_FLAG1_LIM_{self.flag_col[0]}", f"{self.flag_col[1]}")
        self.qcf_col = (f"_QCF_SSITC_{self.flag_col[0]}", "[2=bad]")

    def _init_new_cols(self):
        self.vars_selected_df[self.values_col] = np.nan
        self.vars_selected_df[self.flag0_lim_col] = np.nan
        self.vars_selected_df[self.flag1_lim_col] = np.nan
        self.vars_selected_df[self.qcf_col] = np.nan

    def _add_aux_cols(self):
        self.vars_selected_df[self.flag0_lim_col] = self.flag0_lim
        self.vars_selected_df[self.flag1_lim_col] = self.flag1_lim
        self.vars_selected_df[self.values_col] = self.tab_data_df[self.flag_col]

    def _calc_qcfcol(self):
        flag_filter = (self.vars_selected_df[self.values_col] >= self.vars_selected_df[self.flag1_lim_col])
        self.vars_selected_df.loc[flag_filter, [self.qcf_col]] = 2

        # Here < is needed because there is only 0-1-2 in the _lim_col_data
        # and we give the non-closed limit, i.e. value must be < than limit.
        flag_filter = (self.vars_selected_df[self.values_col] < self.vars_selected_df[self.flag1_lim_col])
        self.vars_selected_df.loc[flag_filter, [self.qcf_col]] = 1
        flag_filter = (self.vars_selected_df[self.values_col] < self.vars_selected_df[self.flag0_lim_col])
        self.vars_selected_df.loc[flag_filter, [self.qcf_col]] = 0


class SpectralCorrectionFactor():
    """
    Spectral Correction Factor (scf)
    """

    def __init__(self, drp_scf_flag_col, lne_scf_flag0_lim, lne_scf_flag1_lim,
                 col_list_pretty, col_dict_tuples, vars_selected_df, tab_data_df):
        # Get settings
        flag_pretty = drp_scf_flag_col.currentText()
        flag_pretty_idx = col_list_pretty.index(flag_pretty)
        self.flag_col = col_dict_tuples[flag_pretty_idx]
        self.flag0_lim = float(lne_scf_flag0_lim.text())
        self.flag1_lim = float(lne_scf_flag1_lim.text())

        self.tab_data_df = tab_data_df
        self.vars_selected_df = vars_selected_df.copy()

    def add_qcf_scf(self):
        self._set_colnames()
        self._init_new_cols()
        self._add_aux_cols()
        self._calc_qcfcol()
        return self.vars_selected_df

    def _set_colnames(self):
        self.values_col = (f"_AUX_SCF_VALS_{self.flag_col[0]}", f"{self.flag_col[1]}")
        self.flag0_lim_col = (f"_AUX_SCF_FLAG0_LIM_{self.flag_col[0]}", f"{self.flag_col[1]}")
        self.flag1_lim_col = (f"_AUX_SCF_FLAG1_LIM_{self.flag_col[0]}", f"{self.flag_col[1]}")
        self.qcf_col = (f"_QCF_SCF_{self.flag_col[0]}", "[2=bad]")

    def _init_new_cols(self):
        self.vars_selected_df[self.values_col] = np.nan
        self.vars_selected_df[self.flag0_lim_col] = np.nan
        self.vars_selected_df[self.flag1_lim_col] = np.nan
        self.vars_selected_df[self.qcf_col] = np.nan

    def _add_aux_cols(self):
        self.vars_selected_df[self.flag0_lim_col] = self.flag0_lim
        self.vars_selected_df[self.flag1_lim_col] = self.flag1_lim
        self.vars_selected_df[self.values_col] = self.tab_data_df[self.flag_col]

    def _calc_qcfcol(self):
        flag_filter = (self.vars_selected_df[self.values_col] > self.vars_selected_df[self.flag1_lim_col])
        self.vars_selected_df.loc[flag_filter, [self.qcf_col]] = 2

        # Here <= is needed because the _lim_col data already takes < into account
        flag_filter = (self.vars_selected_df[self.values_col] <= self.vars_selected_df[self.flag1_lim_col])
        self.vars_selected_df.loc[flag_filter, [self.qcf_col]] = 1
        flag_filter = (self.vars_selected_df[self.values_col] <= self.vars_selected_df[self.flag0_lim_col])
        self.vars_selected_df.loc[flag_filter, [self.qcf_col]] = 0


class MissingValuesFocus():
    """
    Check missing values
    """

    def __init__(self, focus_col, vars_selected_df, tab_data_df):
        # Get settings
        self.flag_col = focus_col
        self.tab_data_df = tab_data_df
        self.vars_selected_df = vars_selected_df.copy()

    def add_qcf_missing(self):
        self._set_colnames()
        self._init_new_cols()
        self._add_aux_cols()
        self._calc_qcfcol()
        return self.vars_selected_df

    def _set_colnames(self):
        self.values_col = (f"_AUX_MISSING_VALS_{self.flag_col[0]}", f"{self.flag_col[1]}")
        self.qcf_col = (f"_QCF_MISSING_{self.flag_col[0]}", "[2=missing]")

    def _init_new_cols(self):
        self.vars_selected_df[self.values_col] = np.nan
        self.vars_selected_df[self.qcf_col] = np.nan

    def _add_aux_cols(self):
        self.vars_selected_df[self.values_col] = self.tab_data_df[self.flag_col]

    def _calc_qcfcol(self):
        flag_filter = self.vars_selected_df[self.values_col].isnull()
        self.vars_selected_df.loc[flag_filter, [self.qcf_col]] = 2
        self.vars_selected_df.loc[~flag_filter, [self.qcf_col]] = 0


class AbsoluteLimits():
    """
    Flag based on absolute limits in selected column
    """

    def __init__(self, drp_abslim_flag_col, lne_abslim_flag0_upper_lim, lne_abslim_flag0_lower_lim,
                 col_list_pretty, col_dict_tuples, vars_selected_df, tab_data_df):
        # Get settings
        flag_pretty = drp_abslim_flag_col.currentText()
        flag_pretty_idx = col_list_pretty.index(flag_pretty)
        self.flag_col = col_dict_tuples[flag_pretty_idx]
        self.flag0_upper_lim = float(lne_abslim_flag0_upper_lim.text())
        self.flag0_lower_lim = float(lne_abslim_flag0_lower_lim.text())

        self.tab_data_df = tab_data_df
        self.vars_selected_df = vars_selected_df.copy()

    def add_qcf_abslim(self):
        self._set_colnames()
        self._init_new_cols()
        self._add_aux_cols()
        self._calc_qcfcol()
        return self.vars_selected_df

    def _set_colnames(self):
        self.values_col = (f"_AUX_ABSLIM_VALS_{self.flag_col[0]}", f"{self.flag_col[1]}")
        self.flag0_upper_lim_col = (f"_AUX_ABSLIM_FLAG0_UPPER_LIM_{self.flag_col[0]}", f"{self.flag_col[1]}")
        self.flag0_lower_lim_col = (f"_AUX_ABSLIM_FLAG1_LOWER_LIM_{self.flag_col[0]}", f"{self.flag_col[1]}")
        self.qcf_col = (f"_QCF_ABSLIM_{self.flag_col[0]}", "[2=bad]")

    def _init_new_cols(self):
        self.vars_selected_df[self.values_col] = np.nan
        self.vars_selected_df[self.flag0_upper_lim_col] = np.nan
        self.vars_selected_df[self.flag0_lower_lim_col] = np.nan
        self.vars_selected_df[self.qcf_col] = np.nan

    def _add_aux_cols(self):
        self.vars_selected_df[self.flag0_upper_lim_col] = self.flag0_upper_lim
        self.vars_selected_df[self.flag0_lower_lim_col] = self.flag0_lower_lim
        self.vars_selected_df[self.values_col] = self.tab_data_df[self.flag_col]

    def _calc_qcfcol(self):
        flag_filter = \
            (self.vars_selected_df[self.values_col] >= self.vars_selected_df[self.flag0_lower_lim_col]) & \
            (self.vars_selected_df[self.values_col] <= self.vars_selected_df[self.flag0_upper_lim_col])
        self.vars_selected_df.loc[flag_filter, [self.qcf_col]] = 0

        flag_filter = \
            (self.vars_selected_df[self.values_col] > self.vars_selected_df[self.flag0_upper_lim_col])
        self.vars_selected_df.loc[flag_filter, [self.qcf_col]] = 2

        flag_filter = \
            (self.vars_selected_df[self.values_col] < self.vars_selected_df[self.flag0_lower_lim_col])
        self.vars_selected_df.loc[flag_filter, [self.qcf_col]] = 2


class GaSignalStrength():
    """
    Signal strength of gas analyzer
    """

    def __init__(self, drp_ssga_flag_col, drp_ssga_type, lne_ssga_threshold,
                 col_list_pretty, col_dict_tuples, vars_selected_df, tab_data_df):
        # Get settings
        flag_pretty = drp_ssga_flag_col.currentText()
        flag_pretty_idx = col_list_pretty.index(flag_pretty)
        self.flag_col = col_dict_tuples[flag_pretty_idx]
        self.type = drp_ssga_type.currentText()
        self.threshold = float(lne_ssga_threshold.text())

        self.tab_data_df = tab_data_df
        self.vars_selected_df = vars_selected_df.copy()

    def add_qcf_abslim(self):
        self._set_colnames()
        self._init_new_cols()
        self._add_aux_cols()
        self._calc_qcfcol()
        return self.vars_selected_df

    def _set_colnames(self):
        self.values_col = (f"_AUX_SSGA_VALS_{self.flag_col[0]}", f"{self.flag_col[1]}")
        self.threshold_col = (f"_AUX_SSGA_THRESHOLD_{self.flag_col[0]}", f"{self.flag_col[1]}")
        self.qcf_col = (f"_QCF_SSGA_{self.flag_col[0]}", "[2=bad]")

    def _init_new_cols(self):
        self.vars_selected_df[self.values_col] = np.nan
        self.vars_selected_df[self.threshold_col] = np.nan
        self.vars_selected_df[self.qcf_col] = np.nan

    def _add_aux_cols(self):
        self.vars_selected_df[self.threshold_col] = self.threshold
        self.vars_selected_df[self.values_col] = self.tab_data_df[self.flag_col]

    def _calc_qcfcol(self):
        if self.type == 'Smaller Than Threshold':
            flag_filter = self.vars_selected_df[self.values_col] < self.vars_selected_df[self.threshold_col]
            self.vars_selected_df.loc[flag_filter, [self.qcf_col]] = 2
            flag_filter = self.vars_selected_df[self.values_col] >= self.vars_selected_df[self.threshold_col]
            self.vars_selected_df.loc[flag_filter, [self.qcf_col]] = 0
        elif self.type == 'Larger Than Threshold':
            flag_filter = self.vars_selected_df[self.values_col] > self.vars_selected_df[self.threshold_col]
            self.vars_selected_df.loc[flag_filter, [self.qcf_col]] = 2
            flag_filter = self.vars_selected_df[self.values_col] <= self.vars_selected_df[self.threshold_col]
            self.vars_selected_df.loc[flag_filter, [self.qcf_col]] = 0
        else:
            flag_filter = '-option-does-not-exist-'


class StabilityParameter():
    """
    Flag based on absolute limits in selected column
    """

    def __init__(self, drp_stability_flag_col, lne_stability_flag0_upper_lim, lne_stability_flag0_lower_lim,
                 col_list_pretty, col_dict_tuples, vars_selected_df, tab_data_df):
        # Get settings
        flag_pretty = drp_stability_flag_col.currentText()
        flag_pretty_idx = col_list_pretty.index(flag_pretty)
        self.flag_col = col_dict_tuples[flag_pretty_idx]
        self.flag0_upper_lim = float(lne_stability_flag0_upper_lim.text())
        self.flag0_lower_lim = float(lne_stability_flag0_lower_lim.text())

        self.tab_data_df = tab_data_df
        self.vars_selected_df = vars_selected_df.copy()

    def add_qcf_abslim(self):
        self._set_colnames()
        self._init_new_cols()
        self._add_aux_cols()
        self._calc_qcfcol()
        return self.vars_selected_df

    def _set_colnames(self):
        self.values_col = (f"_AUX_STABILITY_VALS_{self.flag_col[0]}", f"{self.flag_col[1]}")
        self.flag0_upper_lim_col = (f"_AUX_STABILITY_FLAG0_UPPER_LIM_{self.flag_col[0]}", f"{self.flag_col[1]}")
        self.flag0_lower_lim_col = (f"_AUX_STABILITY_FLAG1_LOWER_LIM_{self.flag_col[0]}", f"{self.flag_col[1]}")
        self.qcf_col = (f"_QCF_STABILITY_{self.flag_col[0]}", "[2=bad]")

    def _init_new_cols(self):
        self.vars_selected_df[self.values_col] = np.nan
        self.vars_selected_df[self.flag0_upper_lim_col] = np.nan
        self.vars_selected_df[self.flag0_lower_lim_col] = np.nan
        self.vars_selected_df[self.qcf_col] = np.nan

    def _add_aux_cols(self):
        self.vars_selected_df[self.flag0_upper_lim_col] = self.flag0_upper_lim
        self.vars_selected_df[self.flag0_lower_lim_col] = self.flag0_lower_lim
        self.vars_selected_df[self.values_col] = self.tab_data_df[self.flag_col]

    def _calc_qcfcol(self):
        flag_filter = \
            (self.vars_selected_df[self.values_col] >= self.vars_selected_df[self.flag0_lower_lim_col]) & \
            (self.vars_selected_df[self.values_col] <= self.vars_selected_df[self.flag0_upper_lim_col])
        self.vars_selected_df.loc[flag_filter, [self.qcf_col]] = 0

        flag_filter = \
            (self.vars_selected_df[self.values_col] > self.vars_selected_df[self.flag0_upper_lim_col])
        self.vars_selected_df.loc[flag_filter, [self.qcf_col]] = 2

        flag_filter = \
            (self.vars_selected_df[self.values_col] < self.vars_selected_df[self.flag0_lower_lim_col])
        self.vars_selected_df.loc[flag_filter, [self.qcf_col]] = 2
