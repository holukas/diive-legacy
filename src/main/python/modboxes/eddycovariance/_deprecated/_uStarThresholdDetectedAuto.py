import numpy as np
import pandas as pd

import gui.elements
import gui.plotfuncs
import modboxes.plots._shared
from gui import elements


class AddControls():

    def __init__(self, opt_stack, opt_frame, opt_layout, ref_stack):
        self.opt_stack = opt_stack
        self.opt_frame = opt_frame
        self.opt_layout = opt_layout
        self.ref_stack = ref_stack

        self.add_option()
        self.add_refinements()

    def add_option(self):
        self.opt_btn = elements.add_button_to_grid(grid_layout=self.opt_layout,
                                                   txt='u* Threshold (Detected, Auto)',
                                                   css_id='',
                                                   row=4, col=0, rowspan=1, colspan=1)
        self.opt_stack.addWidget(self.opt_frame)

    def add_refinements(self):
        ref_frame, ref_layout = elements.add_frame_grid()
        self.ref_stack.addWidget(ref_frame)

        gui.elements.add_header_to_grid_top(layout=ref_layout, txt='QC: u* Threshold (Detected, Auto)')

        self.ref_drp_ustar_auto_col = elements.grd_LabelDropdownPair(txt='u*',
                                                                     css_ids=['', ''],
                                                                     layout=ref_layout,
                                                                     row=1, col=0,
                                                                     orientation='horiz')

        self.ref_btn_show_in_plot = elements.add_button_to_grid(grid_layout=ref_layout,
                                                                txt='Show in Plot', css_id='',
                                                                row=2, col=0, rowspan=1, colspan=2)

        ref_layout.setRowStretch(5, 1)


class Call:
    color = '#e24d42'  # orange red

    def __init__(self, drp_qc_ustar, data_df, col_list_pretty, col_dict_tuples, fig, ax, focus_df, focus_col):
        self.fig = fig  # needed for redraw
        self.ax = ax  # adds to existing axis
        self.ax = gui.plotfuncs.remove_prev_lines(ax=self.ax)

        self.data_df = data_df
        self.focus_df = focus_df.copy()
        self.focus_col = focus_col

        self.drp_qcflag_pretty = drp_qc_ustar
        self.col_list_pretty = col_list_pretty
        self.col_dict_tuples = col_dict_tuples

        self.run()

    def run(self):
        self.qcflag_col = self.get_flag_col()
        self.rejectval_col = self.set_colnames(qcflag_col=self.qcflag_col)
        self.create_new_cols()
        self.get_flag_data()
        self.focus_df = self.insert_flag_filter_col()
        self.show_in_plot()

    def insert_flag_filter_col(self):
        """Insert new column in df that marks which flag values are considered bad values."""
        _df = self.focus_df.copy()
        filter_reject_val = _df[self.qcflag_col] == 2  # True = bad values
        _df[self.rejectval_col] = filter_reject_val  # Bool
        return _df

    def get_flag_data(self):
        """Fill copy contents of qcflag to column."""
        self.focus_df[self.qcflag_col] = self.data_df[self.qcflag_col]

    def create_new_cols(self):
        """Expand focus_df with new (still empty) columns."""
        self.focus_df[self.qcflag_col] = np.nan
        self.focus_df[self.rejectval_col] = np.nan

    @staticmethod
    def set_colnames(qcflag_col):
        """Set names of columns that are added to DataFrame."""
        rejectval_col = (f"{qcflag_col[0]}_rejectval", '[true/false]')
        return rejectval_col

    def get_flag_col(self):
        """Get column name for selected QC flag."""
        qcflag_pretty_str = self.drp_qcflag_pretty
        # index in list of all variables
        qcflag_pretty_ix = self.col_list_pretty.index(qcflag_pretty_str)
        # full column name (tuple), can be used to directly access data in df
        qcflag_col = self.col_dict_tuples[qcflag_pretty_ix]
        return qcflag_col

    def show_in_plot(self):
        # time focus_series are added to an existing plot, no new axis

        plot_df = pd.DataFrame()
        plot_df[self.focus_col] = self.focus_df[self.focus_col]
        plot_df[self.rejectval_col] = self.focus_df[self.rejectval_col]
        filter_rejectval = plot_df[self.rejectval_col]
        plot_df['bad_values'] = plot_df[self.focus_col][filter_rejectval]

        # Marker points
        self.prev_line = modboxes.Plots._shared.add_to_existing_ax(self.fig, add_to_ax=self.ax, x=plot_df.index,
                                                                   y=plot_df['bad_values'], linestyle='', linewidth=0)
        # Show number of marked values, if available
        gui.plotfuncs.show_txt_in_ax_bad_values(fig=self.fig, ax=self.ax,
                                                df=plot_df, sum_for_col=self.rejectval_col)

    def get_results(self):
        return self.focus_df, self.rejectval_col
