"""

Overall QC Flags, by combining multiple quality criteria into one flag

"""
import numpy as np
import pandas as pd

import gui.elements
import gui.plotfuncs
import modboxes.plots._shared
from gui import elements


class AddControls():
    """
    Create GUI control elements and their handles for usage
    """

    def __init__(self, opt_stack, opt_frame, opt_layout, ref_stack):
        self.opt_stack = opt_stack
        self.opt_frame = opt_frame
        self.opt_layout = opt_layout
        self.ref_stack = ref_stack

        self.add_option()
        self.add_refinements()

    def add_option(self):
        self.opt_btn = elements.add_button_to_grid(grid_layout=self.opt_layout,
                                                   txt='Amp QC Flag (Combined Flags)', css_id='',
                                                   row=1, col=0, rowspan=1, colspan=1)
        self.opt_stack.addWidget(self.opt_frame)

    def add_refinements(self):
        ref_frame, ref_layout = elements.add_frame_grid()
        self.ref_stack.addWidget(ref_frame)

        gui.elements.add_header_to_grid_top(layout=ref_layout, txt='QC: Amp QC Flag (Combined Flags)')

        self.ref_drp_select_flag = \
            elements.grd_LabelDropdownPair(txt='Select Amp QC Flag',
                                           css_ids=['', ''],
                                           layout=ref_layout,
                                           row=1, col=0,
                                           orientation='horiz')

        self.ref_lne_upper_lim = \
            elements.add_label_linedit_pair_to_grid(txt='Upper Limit - Reject Values <=',
                                                    css_ids=['', 'cyan'], layout=ref_layout,
                                                    row=2, col=0, orientation='horiz')

        self.ref_lne_lower_lim = \
            elements.add_label_linedit_pair_to_grid(txt='Lower Limit - Reject Values >=',
                                                    css_ids=['', 'cyan'], layout=ref_layout,
                                                    row=3, col=0, orientation='horiz')

        self.ref_btn_show_in_plot = \
            elements.add_button_to_grid(grid_layout=ref_layout, css_id='',
                                        txt='Show Rejected Values In Plot',
                                        row=4, col=0, rowspan=1, colspan=2)

        ref_layout.setRowStretch(5, 1)


class Call:

    def __init__(self, data_df, fig, ax, focus_df, focus_col, lower_lim, upper_lim,
                 drp_qcflag_pretty, col_list_pretty, col_dict_tuples):
        self.fig = fig  # Needed for redraw
        self.ax = ax  # Adds to existing axis
        self.ax = gui.plotfuncs.remove_prev_lines(ax=self.ax)

        self.data_df = data_df.copy()
        self.focus_df = focus_df.copy()
        self.focus_col = focus_col

        self.lower_lim = lower_lim
        self.upper_lim = upper_lim
        self.lower_lim = self.upper_lim if self.lower_lim > self.upper_lim else self.lower_lim

        self.drp_qcflag_pretty = drp_qcflag_pretty
        self.col_list_pretty = col_list_pretty
        self.col_dict_tuples = col_dict_tuples

        self.run()

    def run(self):
        self.qcflag_col = self.get_flag_col()
        self.rejectval_col = self.set_colnames(qcflag_col=self.qcflag_col)
        self.create_new_cols()  # Expand focus_df
        self.get_flag_data()
        self.focus_df = self.insert_flag_filter_col()  # Check which qcflag values mark bad data
        self.show_in_plot()

    @staticmethod
    def set_colnames(qcflag_col):
        """ Set names of columns that are added to DataFrame """
        rejectval_col = (f"{qcflag_col[0]}_rejectval", '[true/false]')
        return rejectval_col

    def create_new_cols(self):
        """ Expand focus_df with new (still empty) columns """
        self.focus_df[self.rejectval_col] = np.nan

    def get_flag_col(self):
        """ Get column name for selected QC flag """
        qcflag_pretty_ix = self.col_list_pretty.index(self.drp_qcflag_pretty)  # Index in list of all variables
        qcflag_col = self.col_dict_tuples[qcflag_pretty_ix]  # Full column name (tuple)
        return qcflag_col

    def get_flag_data(self):
        """ Fill qcflag data """
        self.focus_df[self.qcflag_col] = self.data_df[self.qcflag_col]
        return None

    def insert_flag_filter_col(self):
        """
        Insert new column in df that marks which flag values are considered bad values

        df already contains the data (measured_col) and the qc flag for the data (qcflag_col).
        Here, an additional boolean column (rejectval_col) is added that shows which flag values of
        qcflag_col are considered bad data (True / False), based on settings lower_limit and upper_limit.

        Returns
        -------
        DataFrame with new bool column rejectval_col, which marks all values within the
        selected limits as bad (True), outside the limits as OK (False).
        """
        _df = self.focus_df.copy()
        filter_reject_val = (_df[self.qcflag_col] >= self.lower_lim) & \
                            (_df[self.qcflag_col] <= self.upper_lim)
        _df[self.rejectval_col] = filter_reject_val  # Bool
        return _df

    def show_in_plot(self):
        """ Show bad values in plot """
        # Prepare separate df that contains only data needed for plotting
        plot_df = pd.DataFrame()
        plot_df[self.focus_col] = self.focus_df[self.focus_col]
        filter_rejectval = self.focus_df[self.rejectval_col]
        plot_df['bad_values'] = plot_df[self.focus_col][filter_rejectval]

        # Marker points
        self.prev_line = modboxes.Plots._shared.add_to_existing_ax(self.fig, add_to_ax=self.ax, x=plot_df.index,
                                                                   y=plot_df['bad_values'],
                                                                   linestyle='', linewidth=0)

    def get_results(self):
        """ Return all results """
        return self.focus_df, self.rejectval_col
