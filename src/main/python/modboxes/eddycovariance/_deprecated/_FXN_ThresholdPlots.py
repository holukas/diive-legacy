"""

THRESHOLD PLOTS FLUXNET
-----------------------
#PLOTHRE

"""

import matplotlib.gridspec as gridspec
# matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from PyQt5 import QtWidgets as qw
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar

import gui.elements
from gui import elements
from gui.plotfuncs import default_format


class MakeNewTab:
    def __init__(self, TabWidget, focus_name, col_list_pretty, new_tab_id, col_dict_tuples, data_df):
        self.TabWidget = TabWidget
        self.focus_name = focus_name
        self.colname_pretty_string = col_list_pretty
        self.col_dict_tuples = col_dict_tuples
        self.data_df = data_df

        linebreak = '\n'
        self.title = f"Ustar Threshold{linebreak}Plots (FXN) {new_tab_id}"

        # Create tab menu (refinements)
        self.drp_flux, self.drp_partitioning_method, self.drp_ustar_method, \
        self.drp_mark_gapfilled_vals, self.ref_frame, self.btn_show_in_plot = \
            self.create_ref_tab_menu()

        # Create and add new tab
        self.canvas, self.fig, self.ax = self.create_tab()

        # Update the gui dropdown menus of the new tab
        self.update_refinements()

    def create_tab(self):
        tabContainerVertLayout, tab_ix = \
            self.TabWidget.add_new_tab(title=self.title)
        self.TabWidget.setCurrentIndex(tab_ix)  ## select newly created tab

        # HEADER (top): Create and add
        elements.add_label_to_layout(txt=self.title, css_id='lbl_Header2', layout=tabContainerVertLayout)

        # MENU (left): Stack for refinement menu
        tabMenu_ref_stack = qw.QStackedWidget()
        tabMenu_ref_stack.setProperty('labelClass', '')
        tabMenu_ref_stack.setContentsMargins(0, 0, 0, 0)
        tabMenu_ref_stack.addWidget(self.ref_frame)

        # PLOT & NAVIGATION CONTROLS (right)
        # in horizontal layout

        # Figure (right)
        tabFigure = plt.Figure(facecolor='white')
        # tabFigure = plt.Figure(facecolor='#29282d')
        tabCanvas = FigureCanvas(tabFigure)
        # todo Setup grid for multiple axes if needed at some point
        gs = gridspec.GridSpec(1, 1)  # rows, cols
        gs.update(wspace=0.3, hspace=0.3, left=0.03, right=0.97, top=0.97, bottom=0.03)
        # 3 axes w/ different percentile limits
        tabAx = tabFigure.add_subplot(gs[0, 0])
        default_format(ax=tabAx, txt_xlabel='-', txt_ylabel='-', txt_ylabel_units='-', label_color='#eab839', fontsize='large')

        # Navigation (right)
        tabToolbar = NavigationToolbar(tabCanvas, parent=self.TabWidget)

        # Frame for figure & navigation (right)
        frm_fig_nav = qw.QFrame()
        lytVert_fig_nav = qw.QVBoxLayout()  # create layout for frame
        frm_fig_nav.setLayout(lytVert_fig_nav)  # assign layout to frame
        lytVert_fig_nav.setContentsMargins(0, 0, 0, 0)
        lytVert_fig_nav.addWidget(tabCanvas)  # add widget to layout
        lytVert_fig_nav.addWidget(tabToolbar)

        # ASSEMBLE MENU AND PLOT
        # Splitter for menu on the left and plot on the right
        tabSplitter = qw.QSplitter()
        tabSplitter.addWidget(tabMenu_ref_stack)  ## add ref menu first (left)
        tabSplitter.addWidget(frm_fig_nav)  ## add plot & navigation second (right)
        tabSplitter.setStretchFactor(0, 0)
        tabSplitter.setStretchFactor(1, 1)
        tabContainerVertLayout.addWidget(tabSplitter, stretch=1)  ## add Splitter to tab layout
        return tabCanvas, tabFigure, tabAx

    def create_ref_tab_menu(self):
        ref_frame, ref_layout = elements.add_frame_grid()

        gui.elements.add_header_to_grid_top(layout=ref_layout, txt='PL: Scatter')

        # Select Flux
        drp_flux = elements.grd_LabelDropdownPair_dropEnabled(txt='Flux',
                                                              css_ids=['', ''],
                                                              layout=ref_layout,
                                                              row=1, col=0,
                                                              orientation='horiz')
        drp_flux.addItem('NEE')
        drp_flux.addItem('GPP')
        drp_flux.addItem('RECO')

        # Select Ustar Method
        drp_ustar_method = elements.grd_LabelDropdownPair_dropEnabled(txt='Ustar Threshold',
                                                                      css_ids=['', ''],
                                                                      layout=ref_layout,
                                                                      row=2, col=0,
                                                                      orientation='horiz')
        drp_ustar_method.addItem('CUT: Constant Ustar Threshold Across Years')
        drp_ustar_method.addItem('VUT: Variable Ustar Threshold For Each Year')

        # Select Ustar Method
        drp_partitioning_method = elements.grd_LabelDropdownPair_dropEnabled(
            txt='Partitioning Method (for GPP or RECO)',
            css_ids=['', ''],
            layout=ref_layout,
            row=3, col=0,
            orientation='horiz')
        drp_partitioning_method.addItem('DT: Daytime Partitioning Method')
        drp_partitioning_method.addItem('NT: Nighttime Partitioning Method')

        # Mark Gap-filled Values
        drp_mark_gapfilled_vals = \
            elements.grd_LabelDropdownPair_dropEnabled(txt='Mark Gap-filled Values',
                                                       css_ids=['', ''],
                                                       layout=ref_layout,
                                                       row=4, col=0,
                                                       orientation='horiz')
        drp_mark_gapfilled_vals.addItem('No')
        drp_mark_gapfilled_vals.addItem('Yes')

        btn_show_in_plot = elements.add_button_to_grid(grid_layout=ref_layout,
                                                       txt='Show in Plot', css_id='',
                                                       row=5, col=0, rowspan=1, colspan=2)

        ref_layout.setRowStretch(6, 1)

        return drp_flux, drp_partitioning_method, drp_ustar_method, drp_mark_gapfilled_vals, \
               ref_frame, btn_show_in_plot

    def update_refinements(self):
        # not needed b/c dropdowns are not filled dynamically
        pass


class Run:

    def __init__(self, tab_instance):
        self.fig = tab_instance.fig
        self.ax = tab_instance.axes_dict
        self.data_df = tab_instance.tab_data_df

        # Settings
        self.drp_flux = tab_instance.drp_flux
        self.drp_ustar_method = tab_instance.drp_ustar_method
        self.drp_partitioning_method = tab_instance.drp_partitioning_method
        self.drp_mark_gapfilled_vals = tab_instance.drp_mark_gapfilled_vals

        self.col_dict_tuples = tab_instance.col_dict_tuples
        self.btn_show_in_plot = tab_instance.btn_calc

        # self.ax.clear()  ## remove previous data before showing new data
        # fluxes_df, qc_df = self.get_data()
        # ax = self.make_dynamic_axis_grid()  ## make the axes array
        # self.plot_threshold_plots(fluxes_df=fluxes_df,
        #                           qc_df=qc_df,
        #                           ax=self.ax)

    def get_settings(self):
        flux = self.drp_flux.currentText()
        ustar_method = self.drp_ustar_method.currentText()[0:3]
        partitioning_method = self.drp_partitioning_method.currentText()[0:2]
        mark_gapfilled_vals = self.drp_mark_gapfilled_vals.currentText()
        return flux, ustar_method, partitioning_method, mark_gapfilled_vals

    def get_data(self):
        flux, ustar_method, partitioning_method, _ = self.get_settings()

        # Limit data to selected flux and ustar filtering method
        _plot_df = self.data_df.filter(axis=1, regex='{}'.format(flux))  # Search for flux columns
        _plot_df = _plot_df.filter(axis=1, regex='{}'.format(ustar_method))  ## ustar method

        # Limit GPP or RECO to selected partitioning method
        if flux in ['GPP', 'RECO']:
            _plot_df = _plot_df.filter(axis=1, regex='{}'.format(partitioning_method))  # Partitioning method
        fluxes_df = _plot_df.filter(axis=1, regex='^((?!SE|RANDUNC|JOINTUNC|QC).)*$')  # Exclude certain patterns
        qc_df = _plot_df.filter(axis=1, regex='_QC')  # Only QC flag included
        return fluxes_df, qc_df

    def plot_threshold_plots(self):

        self.ax.clear()
        fluxes_df, qc_df = self.get_data()
        flux, ustar_method, partitioning_method, mark_gapfilled_vals = self.get_settings()

        try:
            xmin = fluxes_df.index[0]
            xmax = fluxes_df.index[-1]
            unique_years = fluxes_df.index.year.unique()

            for ix, year in enumerate(unique_years):

                # -------------------------------------------------------------
                # FIRST PLOT FLUXES_EDDYPRO ONLY //////////////////////////////////////
                # -------------------------------------------------------------

                # Data for this year
                _fluxes_df_subset_year_cumsum = fluxes_df.loc[(fluxes_df.index.year == year)]
                _qc_df_subset_year = qc_df.loc[(qc_df.index.year == year)]  ## QC flags for this year, plots & stats

                # Cumulative fluxes
                _fluxes_df_subset_year_cumsum = _fluxes_df_subset_year_cumsum.cumsum()

                ax = _fluxes_df_subset_year_cumsum.plot(ax=self.ax)  ## plot fluxes

                # ADD QC INFO
                # -----------
                if mark_gapfilled_vals == 'Yes':
                    _qc_columns = _qc_df_subset_year.columns  ## for deleting later

                    # merge fluxes and flags
                    _filtered_df = pd.concat([_fluxes_df_subset_year_cumsum, _qc_df_subset_year], axis=1)
                    for colname in _filtered_df.columns:
                        if '_QC' not in colname[0]:
                            qc_colname = (colname[0] + '_QC', colname[1])
                            _filtered_df[colname].loc[_filtered_df[qc_colname] > 0] = np.nan
                    _filtered_df.drop(axis=1, labels=_qc_columns,
                                      inplace=True)  ## remove qc columns, not needed anymore
                    _filtered_df.plot(ax=ax, color='red', alpha=1, legend=None)

                # FORMAT PLOT
                # -----------
                # Grid and legend
                gui.plotfuncs.default_grid(ax=ax)
                gui.plotfuncs.default_legend(ax=ax, bbox_to_anchor=(0.02, 0.98))

                default_format(ax=ax, txt_xlabel='DATE', txt_ylabel_units='DATE',
                               txt_ylabel=f'{flux} with {ustar_method} and {partitioning_method}',
                               label_color='#eab839', fontsize='large')

                if flux == 'NEE':
                    txt = f'{year} {flux} with {ustar_method}'
                else:
                    txt = f'{year} {flux} with {ustar_method} and {partitioning_method} '
                ax.text(0.5, 0.98, txt,
                        horizontalalignment='center', verticalalignment='top', transform=ax.transAxes,
                        size='x-large', color='#999c9f', backgroundcolor='none')

                # Some info
                info_txt = ''

                if self.drp_flux == 'NEE':

                    qc_stats_gapfilled_dict = {}

                    # Gap-filled values in %
                    for colname in _qc_df_subset_year:
                        total = _qc_df_subset_year[colname].count()
                        gapfilled = _qc_df_subset_year[colname].loc[_qc_df_subset_year[colname] > 0].count()
                        # measured = _qc_df_subset_year[colname].loc[_qc_df_subset_year[colname] == 0].count()
                        stat = (gapfilled / total) * 100
                        qc_stats_gapfilled_dict[colname] = stat

                    for col in _fluxes_df_subset_year_cumsum.columns:
                        qc_statsname_in_dict = (col[0] + '_QC', col[1])
                        gapfilled_values = qc_stats_gapfilled_dict[qc_statsname_in_dict]
                        info_txt = '{}{}: {:.0f} ({:.0f} % gap-filled)\n'.format(
                            info_txt, col[0], _fluxes_df_subset_year_cumsum.iloc[-1][col], gapfilled_values)

                else:

                    for col in _fluxes_df_subset_year_cumsum.columns:
                        info_txt = '{}{}: {:.0f}\n'.format(
                            info_txt, col[0], _fluxes_df_subset_year_cumsum.iloc[-1][col])

                ax.text(0.01, 0.2, info_txt,
                        horizontalalignment='left', verticalalignment='bottom', transform=ax.transAxes,
                        size='large', color='#999c9f', backgroundcolor='none')

                ax.set_xlim(xmin=xmin, xmax=xmax)

                self.fig.canvas.draw()  ## needed to update plot with new data

        except:
            pass

    # def make_dynamic_axis_grid(self):
    #     pass
    #
    #     # numRows = 1
    #     # numColumns = 1
    #     # gs = gridspec.GridSpec(numRows, numColumns)  # rows, cols
    #     # gs.update(wspace=0.3, hspace=0.3, left=0.03, right=0.97, top=0.97, bottom=0.03)
    #     # ax = self.fig.add_subplot(gs[0, 0])
    #
    #     # # Dynamically setup grid for multiple axes as needed
    #     # # https://stackoverflow.com/questions/44159397/matplotlib-dynamic-number-of-subplot
    #     # unique_years = df.index.year.unique()
    #     # numPlots = len(unique_years)  ## number of plots
    #     # max_numColumns_perRow = 4
    #     # numRows = math.ceil(numPlots / max_numColumns_perRow)  ## round up
    #     # numColumns = numPlots if numPlots < max_numColumns_perRow else max_numColumns_perRow
    #     # gs = gridspec.GridSpec(numRows, numColumns)  # rows, cols
    #     # gs.update(wspace=0.3, hspace=0.3, left=0.03, right=0.97, top=0.97, bottom=0.03)
    #     # # Create the axes in the grid and store each axis in a list
    #     # # The number of axes and is the same as the number of unique years in the data
    #     # ax_grid = []
    #     # for n in range(numPlots):
    #     #     in_col = n
    #     #     in_row = 0
    #     #     in_row = in_row + 1 if in_col > (numColumns - 1) else in_row
    #     #     in_col = 0 if in_col > (numColumns - 1) else in_col
    #     #     ax = self.fig.add_subplot(gs[in_row:in_row + 1, in_col:in_col + 1])
    #     #     ax_grid.append(ax)
    #
    #     # return ax

#         qc_colname = (colname[0] + '_QC', colname[1])
# for col in fluxes_df:
#     # ax.plot_date(fluxgaps_df.index, fluxgaps_df[col], alpha=1,
#     #              marker='o', c='white', zorder=101, markersize=10)
#     ax.plot_date(x=fluxgaps_df.index, y=fluxgaps_df[col], color='white', alpha=0.9, ls='-',
#                  marker='o', markeredgecolor='none', ms=20, zorder=101, label='test')


# # Get min and max of fluxes cumsums for this year
# _ymin = _fluxes_df_subset_year_cumsum.iloc[-1, :].min()
# _ymax = _fluxes_df_subset_year_cumsum.iloc[-1, :].max()
#
# # Get overall min and max (over all years)
# if ix == 0:
#     ymax = _ymax
#     if self.drp_flux == 'NEE':
#         ymin = _ymin
# else:
#     ymax = _ymax if _ymax > ymax else ymax
#     if self.drp_flux == 'NEE':
#         ymin = _ymin if _ymin < ymin else ymin
