import gui
import modboxes.plots.styles.LightTheme as theme


class AddControls():
    """
        Creates the gui control elements and their handles for usage.

    """

    def __init__(self, opt_stack, opt_frame, opt_layout, ref_stack):
        self.opt_stack = opt_stack
        self.opt_frame = opt_frame
        self.opt_layout = opt_layout
        self.ref_stack = ref_stack

        self.add_option()
        self.add_refinements()

    def add_option(self):
        # Option Button: [3] Running Median
        self.opt_btn = gui.elements.add_button_to_grid(grid_layout=self.opt_layout,
                                                       txt='Save Figure', css_id='',
                                                       row=2, col=0, rowspan=1, colspan=1)
        self.opt_stack.addWidget(self.opt_frame)

    def add_refinements(self):
        ref_frame, ref_layout = gui.elements.add_frame_grid()
        self.ref_stack.addWidget(ref_frame)

        gui.elements.add_header_to_grid_top(layout=ref_layout, txt='EX: Save Figure')

        self.ref_drp_style = gui.elements.grd_LabelDropdownPair(txt='Style',
                                                                css_ids=['', 'cyan'],
                                                                layout=ref_layout,
                                                                row=1, col=0,
                                                                orientation='horiz')
        self.ref_drp_style.addItem('Light')

        self.ref_btn_save_figure = gui.elements.add_button_to_grid(grid_layout=ref_layout,
                                                                   txt='Export', css_id='',
                                                                   row=2, col=0, rowspan=1, colspan=2)

        ref_layout.setRowStretch(3, 1)


class Call():
    timestamp_col = ('TIMESTAMP', '[yyyy-mm-dd HH:MM:SS]')

    def __init__(self, df, id, button, style, ax, fig, gs, focus_col, ctx, outdir, version_info):
        self.ax = ax
        self.fig = fig
        self.gs = gs
        self.df = df
        self.id = id
        self.style = style
        self.button = button
        self.button_orig_text = button.text()
        self.focus_col = focus_col
        self.ctx = ctx
        self.version_info = version_info
        self.outdir = outdir

    def export(self, fig_out):
        # First, check if output directory exists
        pkgs.dfun.files.verify_dir(dir=self.outdir)

        # DIIVE info
        info = self.version_info
        # info = self.ctx.build_settings  # deprecated
        amp_str = r'$\bf{}$'.format(info['app_name'])
        version_str = r'$\bf{}$'.format(info['version'])
        amp_version_str = f"generated using {amp_str} {version_str}"
        self.add_fig_text(txt=amp_version_str, position='top right')

        # Filename
        filename_out = f"{self.focus_col[0]}_{self.focus_col[1]}_{self.id}.png"
        filepath_out = self.outdir / filename_out
        exported_str = f"exported to {filepath_out}"
        self.add_fig_text(txt=exported_str, position='bottom right')

        # Variable
        # var_str = r'$\bf{}$'.format(self.measured_col[0])
        var_str = '{}'.format(self.focus_col[0])
        self.add_fig_text(txt=var_str, position='top left')

        # Save
        fig_out.savefig(filepath_out, format='png', bbox_inches='tight', facecolor='w',
                        transparent=True, dpi=150)

        gui.elements.btn_txt_live_update(btn=self.button, txt=self.button_orig_text, perc=-9999)

    def add_fig_text(self, txt, position):

        ha = 'right'
        va = 'top'
        x = y = 1
        backgroundcolor = 'none'

        if position == 'top right':
            x = y = 1
            size = 9
        elif position == 'bottom right':
            x = 1
            y = 0
            va = 'bottom'
            size = 7
        elif position == 'top left':
            x = 0
            y = 1
            va = 'top'
            ha = 'left'
            size = 20

        self.fig.text(x, y, txt,
                      horizontalalignment=ha, verticalalignment=va,
                      transform=self.fig.transFigure, size=size, color=theme.COLOR_TXT_LEGEND,
                      backgroundcolor=backgroundcolor)

        # self.fig.tight_layout()
        # self.gs.tight_layout(self.fig)

        # im = plt.imread(self.ctx.icon_amp)
        # # self.fig.imshow(im, aspect='auto', extent=(0.4, 0.6, .5, .7), zorder=-1)
        # fig_out.figimage(im, 10, 10, zorder=100, alpha=1)
