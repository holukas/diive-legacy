import modboxes.plots._shared


def show_in_plot(self, additional_line):
    # time focus_series are added to an existing plot, no new axis

    if additional_line:
        # Auxiliary lines
        marker = ''
        modboxes.Plots._shared.add_to_existing_ax(self.fig, add_to_ax=self.axes_dict, x=self.focus_df.index,
                                                  y=self.focus_df[self.running_median], linestyle='--', linewidth=1)

    # Auxiliary lines
    modboxes.Plots._shared.add_to_existing_ax(self.fig, add_to_ax=self.axes_dict, x=self.focus_df.index, y=self.focus_df[self.gap_values],
                                              linestyle='', linewidth=3)
