import os
import zipfile as zf

import pandas as pd
from PyQt5 import QtWidgets as qw
from PyQt5.QtGui import QIcon

import gui.base
import pkgs.dfun.frames
import pkgs.dfun.times
import gui


class PopupWindow(gui.base.CustomQDialogWindow):
    """ Limit dataset to selected time range. """

    def __init__(self, df, ctx, run_id, first_file_freq, project_outdir):
        super().__init__()
        self.df = df
        self.ctx = ctx
        self.icon = QIcon(self.ctx.tab_icon_export)
        self.run_id = run_id
        self.orig_freq = first_file_freq
        self.outdir = project_outdir

        self.docstring = self.__doc__

        # Dialog window
        self.ref_drp_agg, self.ref_lne_freq_duration, self.ref_drp_freq, self.ref_lne_min_vals, \
        self.ref_lne_custom_prefix, self.ref_drp_one_file_per, self.ref_drp_file_type, \
        self.ref_drp_add_timestamp, self.ref_drp_main_timestamp = \
            self.gui()
        self.update_fields()

    def update_fields(self):
        """ Set defaults. """
        self.ref_drp_add_timestamp.setEnabled(True)
        self.ref_drp_main_timestamp.setEnabled(True)

        if (self.ref_drp_freq.currentText() != 'Original') \
                | (self.ref_drp_agg.currentText() != 'Original'):
            self.ref_drp_add_timestamp.setDisabled(True)
            self.ref_drp_main_timestamp.setDisabled(True)

    def gui(self):
        """
        Construct window GUI.
        """
        header_txt = 'Export Dataset'
        container_grid = self.setupUi(self, win_title=header_txt, ctx=self.ctx, icon=self.icon)
        gui.elements.add_header_to_grid_top(layout=container_grid, txt=header_txt)

        # Settings
        # Frequency
        ref_lne_freq_duration, ref_drp_freq \
            = gui.elements.grd_LabelLineeditDropdownTriplet(txt='Frequency',
                                                            css_ids=['', 'cyan', 'cyan'],
                                                            layout=container_grid,
                                                            orientation='horiz',
                                                            row=2, col=0)
        ref_lne_freq_duration.setText('1')
        ref_drp_freq.addItem('Original')
        ref_drp_freq.addItem('Minute(s)')
        ref_drp_freq.addItem('Hourly')
        ref_drp_freq.addItem('Daily')
        ref_drp_freq.addItem('Weekly')
        ref_drp_freq.addItem('Monthly')
        ref_drp_freq.addItem('Yearly')

        # Method
        ref_drp_agg = gui.elements.grd_LabelDropdownPair(txt='Aggregation',
                                                         css_ids=['', 'cyan'],
                                                         layout=container_grid,
                                                         orientation='horiz',
                                                         row=3, col=0)
        ref_drp_agg.addItem('Original')
        ref_drp_agg.addItem('Mean')
        ref_drp_agg.addItem('Median')
        ref_drp_agg.addItem('SD')
        ref_drp_agg.addItem('Minimum')
        ref_drp_agg.addItem('Maximum')
        ref_drp_agg.addItem('Count')
        ref_drp_agg.addItem('Sum')
        ref_drp_agg.addItem('Variance')

        # Required Minimum Values for e.g. calculating valid mean value
        ref_lne_min_vals = \
            gui.elements.add_label_linedit_pair_to_grid(txt='Minimum Values',
                                                        css_ids=['', 'cyan'],
                                                        layout=container_grid,
                                                        orientation='horiz',
                                                        row=4, col=0)
        ref_lne_min_vals.setText('0')

        # Split into files
        ref_drp_one_file_per = gui.elements.grd_LabelDropdownPair(txt='One File Per',
                                                                  css_ids=['', 'cyan'],
                                                                  layout=container_grid,
                                                                  row=5, col=0,
                                                                  orientation='horiz')
        ref_drp_one_file_per.addItem('Dataset')
        ref_drp_one_file_per.addItem('Year')
        ref_drp_one_file_per.addItem('Month')

        # Output file format
        ref_drp_file_type = gui.elements.grd_LabelDropdownPair(txt='File Type',
                                                               css_ids=['', 'cyan'],
                                                               layout=container_grid,
                                                               row=6, col=0,
                                                               orientation='horiz')
        ref_drp_file_type.addItem('*.diive.csv (2-row header, with variable names and units)')
        ref_drp_file_type.addItem('*.csv (1-row header, with variable names and units)')
        ref_drp_file_type.addItem('*.csv (1-row header, with variable names)')
        # self.ref_drp_file_type.addItem('*.amp (2-row header, compressed, experimental)')
        # self.ref_drp_file_type.addItem('*.zip (2-row header, compressed)')
        container_grid.setRowStretch(7, 1)

        # Output file format
        ref_drp_main_timestamp = gui.elements.grd_LabelDropdownPair(txt='Main Timestamp Format',
                                                                    css_ids=['', 'cyan'],
                                                                    layout=container_grid,
                                                                    row=8, col=0,
                                                                    orientation='horiz')
        ref_drp_main_timestamp.addItem('Middle Of Record (yyyy-mm-dd HH:MM:SS)')
        ref_drp_main_timestamp.addItem('End Of Record (yyyy-mm-dd HH:MM:SS)')
        ref_drp_main_timestamp.addItem('End Of Record (yyyymmddHHMMSS)')
        ref_drp_main_timestamp.addItem('End Of Record, No Seconds (yyyy-mm-dd HH:MM)')
        ref_drp_main_timestamp.addItem('End Of Record, No Seconds (yyyymmddHHMM)')
        ref_drp_main_timestamp.addItem('Start Of Record (yyyy-mm-dd HH:MM:SS)')
        ref_drp_main_timestamp.addItem('Start Of Record (yyyymmddHHMMSS)')
        ref_drp_main_timestamp.addItem('Start Of Record, No Seconds (yyyy-mm-dd HH:MM)')
        ref_drp_main_timestamp.addItem('Start Of Record, No Seconds (yyyymmddHHMMSS)')
        container_grid.setRowStretch(9, 1)

        ref_drp_add_timestamp = gui.elements.grd_LabelDropdownPair(txt='Add Additional Timestamp',
                                                                   css_ids=['', 'cyan'],
                                                                   layout=container_grid,
                                                                   row=10, col=0,
                                                                   orientation='horiz')
        ref_drp_add_timestamp.addItem('No')
        ref_drp_add_timestamp.addItem('End Of Record (yyyy-mm-dd HH:MM:SS)')
        ref_drp_add_timestamp.addItem('End Of Record, No Seconds (yyyy-mm-dd HH:MM)')
        ref_drp_add_timestamp.addItem('Start Of Record (yyyy-mm-dd HH:MM:SS)')
        ref_drp_add_timestamp.addItem('Start Of Record, No Seconds (yyyy-mm-dd HH:MM)')

        # Required Minimum Values for e.g. calculating valid mean value
        ref_lne_custom_prefix = \
            gui.elements.add_label_linedit_pair_to_grid(txt='Output File Prefix',
                                                        css_ids=['', 'cyan'],
                                                        layout=container_grid,
                                                        orientation='horiz',
                                                        row=12, col=0)
        ref_lne_custom_prefix.setText('')

        # Button box: OK and Cancel
        self.button_box = qw.QDialogButtonBox()
        self.button_box.setEnabled(True)
        self.button_box.setStandardButtons(qw.QDialogButtonBox.Cancel | qw.QDialogButtonBox.Ok)
        self.button_box.setObjectName("button_box")
        self.button_box.accepted.connect(self.accept)
        self.button_box.rejected.connect(self.reject)
        container_grid.addWidget(self.button_box, 13, 1, 1, 1)
        container_grid.setRowStretch(14, 1)
        return ref_drp_agg, ref_lne_freq_duration, ref_drp_freq, \
               ref_lne_min_vals, ref_lne_custom_prefix, ref_drp_one_file_per, ref_drp_file_type, \
               ref_drp_add_timestamp, ref_drp_main_timestamp


class Run():
    filename_suffix = ''

    def __init__(self, obj):

        self.df = obj.df.copy()

        self.drp_one_file_per = obj.ref_drp_one_file_per
        self.drp_filetype = obj.ref_drp_file_type
        self.lne_to_duration = obj.ref_lne_freq_duration
        self.drp_to_freq = obj.ref_drp_freq
        self.drp_agg_method = obj.ref_drp_agg
        self.lne_min_vals = obj.ref_lne_min_vals
        self.lne_custom_prefix = obj.ref_lne_custom_prefix
        self.drp_main_timestamp = obj.ref_drp_main_timestamp
        self.drp_add_timestamp = obj.ref_drp_add_timestamp

        self.id = obj.run_id
        self.orig_freq = obj.orig_freq
        self.outdir = obj.outdir

        # Add info about duration to freq_str, e.g. '3T' for mean values over 3 minutes
        to_freq = self.drp_to_freq.currentText()
        to_duration = self.lne_to_duration.text()
        self.to_freq = pkgs.dfun.times.generate_freq_str(to_freq=to_freq)
        if self.to_freq != 'Original':
            self.to_freq_str = '{}{}'.format(to_duration, self.to_freq)

        # # Detect unique years, needs shifted timestamp when timestamp denotes end of row period
        # _df = DataFunctions.shift_timestamp(df=self.df, freq=self.orig_freq)
        self.unique_years = self.df.index.year.unique()

        #             df=self.data_df, id=self.run_id,
        #             button=self.modbox_export.VariablesToFiles.ref_btn_export,
        #             one_file_per=self.modbox_export.VariablesToFiles.ref_drp_one_file_per.currentText(),
        #             filetype=self.modbox_export.VariablesToFiles.ref_drp_file_type.currentText(),
        #             to_duration=int(self.modbox_export.VariablesToFiles.ref_lne_freq_duration.text()),
        #             to_freq=self.modbox_export.VariablesToFiles.ref_drp_freq.currentText(),
        #             agg_method=self.modbox_export.VariablesToFiles.ref_drp_agg.currentText(),
        #             min_vals=int(self.modbox_export.VariablesToFiles.ref_lne_min_vals.text()),
        #             orig_freq=self.first_file_freq,
        #             custom_prefix=self.modbox_export.VariablesToFiles.ref_lne_custom_prefix.text(),
        #             outdir=self.project_outdir,
        #             out_timestamp_convention=self.modbox_export.VariablesToFiles.ref_drp_timestamp_convention.currentText()).export()

    def resample_if_needed(self, agg_method, min_vals, to_duration):
        # Resample data if needed
        if (agg_method == 'Original') | (self.to_freq == 'Original'):
            # In case any 'Original': freq_str and agg_method are ignored
            resampled_df = self.df.copy()
            self.filename_suffix = 'Original-{}'.format(self.orig_freq)
            timestamp_info_df = pd.DataFrame()  # todo at some point
        else:
            resampled_df, timestamp_info_df = \
                pkgs.dfun.frames.resample_df(df=self.df,
                                             freq_str=self.to_freq_str,
                                             agg_method=agg_method,
                                             min_vals=min_vals,
                                             to_freq_duration=to_duration,
                                             to_freq=self.to_freq)
            self.filename_suffix = '{}-{}'.format(agg_method, self.to_freq_str)
        return resampled_df, timestamp_info_df

    def export(self):
        """
        Export as standardized output file(s).
        """
        # Get settings
        one_file_per = self.drp_one_file_per.currentText()
        agg_method = self.drp_agg_method.currentText()
        min_vals = int(self.lne_min_vals.text())
        to_duration = self.lne_to_duration.text()
        filetype = self.drp_filetype.currentText()
        custom_prefix = self.lne_custom_prefix.text()
        format_main_timestamp = self.drp_main_timestamp.currentText()
        add_timestamp = self.drp_add_timestamp.currentText()

        _out_df = -9999

        resampled_df, timestamp_info_df = self.resample_if_needed(agg_method=agg_method,
                                                                  min_vals=min_vals,
                                                                  to_duration=to_duration)

        resampled_df = self.format_main_timestamp(df=resampled_df, format=format_main_timestamp)

        # resampled_df = DataFunctions.insert_drop_timestamp_col(df=resampled_df,
        #                                                        timestamp_colname=timestamp_colname)

        # Make sure to fill NAs with missing code
        resampled_df.fillna(-9999, inplace=True)

        if (add_timestamp != 'No') | (agg_method != 'Original') | (self.to_freq != 'Original'):
            # Only for original data
            resampled_df = self.insert_additional_timestamps(df=resampled_df, add_timestamp=add_timestamp)

        # Do not split, generate only one file with all variables
        if one_file_per == 'Dataset':
            filename_out_no_ext = 'Dataset_{}_{}'.format(self.id, self.filename_suffix)
            self.gates(df=resampled_df,
                       filename_out_no_ext=filename_out_no_ext,
                       timestamp_info_df=timestamp_info_df,
                       filetype=filetype,
                       custom_prefix=custom_prefix)

        # Split into yearly files
        elif one_file_per == 'Year':
            file = 0
            for year in self.unique_years:
                file += 1
                filter = (resampled_df.index.year == year)
                _out_df = resampled_df[filter]
                # _out_df = DataFunctions.insert_drop_timestamp_col(df=_out_df)
                filename_out_no_ext = 'Year-{:0>4}_{}_{}'.format(year, self.id, self.filename_suffix)
                self.gates(df=_out_df,
                           filename_out_no_ext=filename_out_no_ext,
                           timestamp_info_df=timestamp_info_df,
                           filetype=filetype,
                           custom_prefix=custom_prefix)

        # Split into monthly files
        elif one_file_per == 'Month':
            file = 0
            for year in self.unique_years:
                filter = (resampled_df.index.year == year)
                _year_df = resampled_df[filter]
                unique_year_months = _year_df.index.month.unique()
                for month in unique_year_months:
                    file += 1
                    filter = (resampled_df.index.year == year) & (resampled_df.index.month == month)
                    _out_df = resampled_df.loc[filter]
                    # _out_df = DataFunctions.insert_drop_timestamp_col(df=_out_df)
                    filename_out_no_ext = 'Month-{:0>4}-{:0>2}_{}_{}.csv'.format(year, month, self.id,
                                                                                 self.filename_suffix)
                    self.gates(df=_out_df,
                               filename_out_no_ext=filename_out_no_ext,
                               timestamp_info_df=timestamp_info_df,
                               filetype=filetype,
                               custom_prefix=custom_prefix)

    def format_main_timestamp(self, df, format):
        """Add timestamps in addition to the default timestamp"""

        half_freq = df.index.freq / 2
        timestamp_colname = None
        format_str = None
        operation = None

        if format == 'Middle Of Record (yyyy-mm-dd HH:MM:SS)':
            # Default timestamp
            timestamp_colname = df.index.name
            format_str = "%Y-%m-%d %H:%M:%S"
            operation = None

        elif format == 'End Of Record (yyyy-mm-dd HH:MM:SS)':
            timestamp_colname = ("TIMESTAMP", "[end, yyyy-mm-dd HH:MM:SS]")
            format_str = "%Y-%m-%d %H:%M:%S"
            operation = 'add'
        elif format == 'End Of Record (yyyymmddHHMMSS)':
            timestamp_colname = ("TIMESTAMP", "[end, yyyymmddHHMMSS]")
            format_str = "%Y%m%d%H%M%S"
            operation = 'add'
        elif format == 'End Of Record, No Seconds (yyyy-mm-dd HH:MM)':
            timestamp_colname = ("TIMESTAMP", "[end, yyyy-mm-dd HH:MM]")
            format_str = "%Y-%m-%d %H:%M"
            operation = 'add'
        elif format == 'End Of Record, No Seconds (yyyymmddHHMM)':
            timestamp_colname = ("TIMESTAMP", "[end, yyyymmddHHMM]")
            format_str = "%Y%m%d%H%M"
            operation = 'add'

        elif format == 'Start Of Record (yyyy-mm-dd HH:MM:SS)':
            timestamp_colname = ("TIMESTAMP", "[start, yyyy-mm-dd HH:MM:SS]")
            format_str = "%Y-%m-%d %H:%M:%S"
            operation = 'subtract'
        elif format == 'Start Of Record (yyyymmddHHMMSS)':
            timestamp_colname = ("TIMESTAMP", "[start, yyyymmddHHMMSS]")
            format_str = "%Y%m%d%H%M%S"
            operation = 'subtract'
        elif format == 'Start Of Record, No Seconds (yyyy-mm-dd HH:MM)':
            timestamp_colname = ("TIMESTAMP", "[start, yyyy-mm-dd HH:MM]")
            format_str = "%Y-%m-%d %H:%M"
            operation = 'subtract'
        elif format == 'Start Of Record, No Seconds (yyyymmddHHMM)':
            timestamp_colname = ("TIMESTAMP", "[start, yyyymmddHHMM]")
            format_str = "%Y%m%d%H%M"
            operation = 'subtract'

        df.insert(0, timestamp_colname, value=df.index)  # Index col to normal data col
        # df.drop([timestamp_colname], axis=1, inplace=True)
        if operation == 'add':
            df[timestamp_colname] = df[timestamp_colname].add(half_freq)
        elif operation == 'subtract':
            df[timestamp_colname] = df[timestamp_colname].sub(half_freq)
        else:
            pass
        df[timestamp_colname] = pd.to_datetime(df[timestamp_colname])  # To be safe
        df[timestamp_colname] = df[timestamp_colname].dt.strftime(format_str)  # Gives *string* for export
        # df.set_index(timestamp_colname, inplace=True, drop=True)

        return df

    def insert_additional_timestamps(self, df, add_timestamp):
        """Add timestamps in addition to the default timestamp"""

        if add_timestamp == 'End Of Record (yyyy-mm-dd HH:MM:SS)':
            outcol = ("_TIMESTAMP_END", "[yyyy-mm-dd HH:MM:SS]")
            df[outcol] = df.index
            half_freq = df.index.freq / 2
            df[outcol] = df[outcol].add(half_freq)
            df[outcol] = pd.to_datetime(df[outcol])  # To be safe
            df[outcol] = df[outcol].dt.strftime("%Y-%m-%d %H:%M:%S")  # Gives *string* for export

        elif add_timestamp == 'End Of Record, No Seconds (yyyy-mm-dd HH:MM)':
            outcol = ("_TIMESTAMP_END", "[yyyy-mm-dd HH:MM]")
            df[outcol] = df.index
            half_freq = df.index.freq / 2
            df[outcol] = df[outcol].add(half_freq)
            df[outcol] = pd.to_datetime(df[outcol])  # To be safe
            df[outcol] = df[outcol].dt.strftime("%Y-%m-%d %H:%M")  # Gives *string* for export

        elif add_timestamp == 'Start Of Record (yyyy-mm-dd HH:MM:SS)':
            outcol = ("_TIMESTAMP_START", "[yyyy-mm-dd HH:MM:SS]")
            df[outcol] = df.index
            half_freq = df.index.freq / 2
            df[outcol] = df[outcol].sub(half_freq)
            df[outcol] = pd.to_datetime(df[outcol])  # To be safe
            df[outcol] = df[outcol].dt.strftime("%Y-%m-%d %H:%M:%S")  # Gives *string* for export

        elif add_timestamp == 'Start Of Record, No Seconds (yyyy-mm-dd HH:MM)':
            outcol = ("_TIMESTAMP_START", "[yyyy-mm-dd HH:MM]")
            df[outcol] = df.index
            half_freq = df.index.freq / 2
            df[outcol] = df[outcol].sub(half_freq)
            df[outcol] = pd.to_datetime(df[outcol])  # To be safe
            df[outcol] = df[outcol].dt.strftime("%Y-%m-%d %H:%M")  # Gives *string* for export
        return df

    def export_uncompressed_file(self, df, filename, ext, timestamp_info_df, custom_prefix):
        """
        Export variables to file.

        Variables are first saved as csv, regardless of which format was selected.
        If a compressed format was selected, the csv file is first compressed, saved
        in the compressed format and then deleted.

        :param filename: str
        :param suffix: str
        :param ext: str
        :return:
        """
        # First, check if output directory exists
        pkgs.dfun.files.verify_dir(dir=self.outdir)

        if custom_prefix == '':
            pass
        else:
            custom_prefix += '_'

        # Save as uncompressed: .diive.csv (2 header rows) or .csv (e.g. 1 header row)
        filename_ext = f"{custom_prefix}{filename}{ext}"
        filepath = self.outdir / filename_ext
        df.to_csv(filepath, index=False)

        # Save timestamp info
        filename_ext = f"{custom_prefix}{filename}_timestamp_info.csv"
        filepath = self.outdir / filename_ext
        timestamp_info_df.to_csv(filepath, index=False)

        return None

    def compress_file(self, uncompr_filename, uncompr_filepath):
        zipped_file = zf.ZipFile(uncompr_filepath, 'w')
        # note the arcname argument: it enables to directly zip the file w/o including the
        # file-containing folder in the zip file
        zipped_file.write(uncompr_filepath, compress_type=zf.ZIP_DEFLATED,
                          arcname=os.path.basename(uncompr_filename))
        zipped_file.close()
        os.remove(uncompr_filepath)  # Delete the uncompressed file

    def gates(self, df, filename_out_no_ext, timestamp_info_df, filetype, custom_prefix):
        """
        Check if header needs to be flattened, check which filetype.

        :param df: DataFrame
        :param filename_out_no_ext: filename w/o extension
        :return:
        """
        df = self.header_gate(df=df,
                              filetype=filetype)
        self.filetype_gate(df=df,
                           filename_out_no_ext=filename_out_no_ext,
                           timestamp_info_df=timestamp_info_df,
                           filetype=filetype,
                           custom_prefix=custom_prefix)

    def filetype_gate(self, df, filename_out_no_ext, timestamp_info_df, filetype, custom_prefix):
        """ Call export function for selected file type. """

        if '*.diive.csv' in filetype:
            ext = '.diive.csv'
        else:
            ext = '.csv'

        self.export_uncompressed_file(df=df, filename=filename_out_no_ext,
                                      ext=ext, timestamp_info_df=timestamp_info_df,
                                      custom_prefix=custom_prefix)

        # elif 'compressed' in self.filetype:
        #     if '*.amp' in self.filetype:
        #         self.compress_file(df=df, filename_out_no_ext=filename_out_no_ext, ext='.amp')
        #     if '*.zip' in self.filetype:
        #         self.compress_file(df=df, filename_out_no_ext=filename_out_no_ext, ext='.zip')

    def header_gate(self, df, filetype):
        if filetype == '*.diive.csv (2-row header, with variable names and units)':
            # Default
            pass
        elif filetype == '*.csv (1-row header, with variable names and units)':
            df = pkgs.dfun.frames.flatten_multiindex_all_df_cols(df=df)
        elif filetype == '*.csv (1-row header, with variable names)':
            df = df.droplevel(1, axis=1)
        return df
