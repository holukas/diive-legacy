from PyQt5 import QtCore as qtc
from PyQt5 import QtWidgets as qw
from PyQt5.QtGui import QIcon

import gui


class PopupWindow(gui.base.CustomQDialogWindow):
    """ Limit dataset to selected time range. """

    def __init__(self, df, ctx):
        super().__init__()
        self.df = df
        self.ctx = ctx
        self.icon = QIcon(self.ctx.tab_icon_modifications)
        self.docstring = self.__doc__

        # # Tab icon
        # self.TabWidget.setTabIcon(self.tab_ix, QtGui.QIcon(self.ctx.tab_icon_modifications))

        # Dialog window
        self.ref_dte_start, self.ref_dte_end = \
            self.gui()
        self.update_fields()

    def update_fields(self):
        """ Update date range for dateEdit """
        min_data_day_str = self.df.index[0].strftime("%Y-%m-%d")  # str
        max_data_day_str = self.df.index[-1].strftime("%Y-%m-%d")
        min_data_day_qt = qtc.QDate.fromString(min_data_day_str, 'yyyy-MM-dd')  # Qt format
        max_data_day_qt = qtc.QDate.fromString(max_data_day_str, 'yyyy-MM-dd')
        self.ref_dte_start.setDate(min_data_day_qt)
        self.ref_dte_end.setDate(max_data_day_qt)
        self.ref_dte_start.setDate(min_data_day_qt)
        self.ref_dte_end.setDate(max_data_day_qt)

    def gui(self):
        """
        Construct window GUI.
        """
        header_txt = 'Limit Dataset Time Range'
        container_grid = self.setupUi(self, win_title=header_txt, ctx=self.ctx, icon=self.icon)
        gui.elements.add_header_to_grid_top(layout=container_grid, txt=header_txt)
        ref_dte_start = gui.elements.grd_LabelDateEditPair(txt='Time Range Start (YYYY-MM-DD)',
                                                           css_ids=['', ''],
                                                           layout=container_grid,
                                                           row=1, col=0)
        ref_dte_end = gui.elements.grd_LabelDateEditPair(txt='Time Range End (YYYY-MM-DD)',
                                                         css_ids=['', ''],
                                                         layout=container_grid,
                                                         row=2, col=0)
        # Button box: OK and Cancel
        self.button_box = qw.QDialogButtonBox()
        self.button_box.setEnabled(True)
        self.button_box.setStandardButtons(qw.QDialogButtonBox.Cancel | qw.QDialogButtonBox.Ok)
        self.button_box.setObjectName("button_box")
        self.button_box.accepted.connect(self.accept)
        self.button_box.rejected.connect(self.reject)
        container_grid.addWidget(self.button_box, 3, 1, 1, 1)
        container_grid.setRowStretch(4, 1)
        return ref_dte_start, ref_dte_end


class Run():
    """
    Limit DataFrame to specified time window
    """

    def __init__(self, obj):
        self.df = obj.df.copy()
        self.start_date = obj.ref_dte_start.date().toPyDate()
        self.end_date = obj.ref_dte_end.date().toPyDate()

    def apply_limit(self):
        filter = (self.df.index.date >= self.start_date) & (self.df.index.date <= self.end_date)
        self.df = self.df[filter]
        return self.df
