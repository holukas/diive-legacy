"""

SUBSET
------


"""
# matplotlib.use('Qt5Agg')
import pandas as pd
from PyQt5 import QtGui
from PyQt5 import QtWidgets as qw

import gui.add_to_layout
import gui.base
import gui.elements
import gui.make
import logger


class addContent(gui.base.buildTab):
    """Build new tab and populate with contents"""

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id, tab_template='SVV')

        # Tab icon
        self.TabWidget.setTabIcon(self.tab_ix, QtGui.QIcon(app_obj.ctx.tab_icon_modifications))

        # Add settings menu contents
        self.btn_make_new_subset, self.btn_add_all_vars, self.btn_remove_all_vars = \
            self.add_settings_fields()

        # # Add variables required in settings
        # self.populate_settings_fields()

        # Add available variables to variable lists
        gui.base.buildTab.populate_variable_list(obj=self)

        # # Add plots
        # self.populate_plot_area()

    def populate_plot_area(self):
        # This is done in Run due to dynamic amount of axes
        pass

    # def populate_settings_fields(self):
    #     # Add all variables to drop-down lists to allow full flexibility yay
    #     self.drp_class_var.clear()
    #     for ix, colname_tuple in enumerate(self.tab_data_df.columns):
    #         self.drp_class_var.addItem(self.col_list_pretty[ix])
    #
    #     # Set dropdowns to found ix
    #     default_ix = 0
    #     self.drp_class_var.setCurrentIndex(default_ix)
    #
    #     # Update settings text fields
    #     self.lne_lower_class_lim.setText('0')
    #     self.lne_upper_class_lim.setText('99')

    def add_settings_fields(self):
        gui.elements.add_header_subheader_to_grid_top(
            row=0, col=0, rowspan=1, colspan=3,
            layout=self.sett_layout, txt=["Subset",
                                          "Modifications"])
        gui.add_to_layout.add_spacer_item_to_grid(
            row=1, col=0, layout=self.sett_layout)

        # Buttons
        btn_add_all_vars = gui.elements.add_button_to_grid(
            grid_layout=self.sett_layout, txt='Add All To Selected', css_id='',
            row=3, col=0, rowspan=1, colspan=2)

        btn_remove_all_vars = gui.elements.add_button_to_grid(
            grid_layout=self.sett_layout, txt='Remove All From Selected', css_id='',
            row=4, col=0, rowspan=1, colspan=2)

        gui.add_to_layout.add_spacer_item_to_grid(
            row=5, col=0, layout=self.sett_layout)

        btn_make_new_subset = gui.elements.add_button_to_grid(
            grid_layout=self.sett_layout, txt='+ Make New Subset (Keep Only Selected)', css_id='btn_add_as_new_var',
            row=6, col=0, rowspan=1, colspan=2)

        self.sett_layout.setRowStretch(7, 1)

        return btn_make_new_subset, btn_add_all_vars, btn_remove_all_vars


class Run(addContent):
    sub_outdir = "modifications_subset"

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id)
        logger.log(name='>>> Starting Modifications > Subset (mfSUB)', dict={}, highlight=True)  # Log info
        self.sub_outdir = self.project_outdir / self.sub_outdir
        self.selected_vars_lookup_dict = {}
        self.vars_selected_df = pd.DataFrame()
        self.vars_available_df = self.tab_data_df.copy()
        self.btn_make_new_subset.setDisabled(True)

    def add_col(self):
        self.set_col_to_add()
        self.add_col_to_selected()
        self.update_selected_vars_list()
        self.btn_make_new_subset.setEnabled(True)

    def add_all_to_selected(self):
        self.vars_selected_df = self.vars_available_df.copy()
        for ix, col in enumerate(self.vars_selected_df.columns):
            self.selected_vars_lookup_dict[col] = self.col_list_pretty[ix]
        self.update_selected_vars_list()
        self.btn_make_new_subset.setEnabled(True)

    def remove_all_from_selected(self):
        self.vars_selected_df = pd.DataFrame()
        self.selected_vars_lookup_dict = {}
        self.update_selected_vars_list()
        self.check_if_selected_empty()
        self.btn_make_new_subset.setDisabled(True)

    def remove_col(self):
        self.set_col_to_remove()
        self.remove_col_from_selected()
        self.update_selected_vars_list()
        empty = self.check_if_selected_empty()
        if empty:
            self.btn_make_new_subset.setDisabled(True)
        else:
            self.btn_make_new_subset.setEnabled(True)

    def set_col_to_add(self):
        """Get column name of target var from available var list"""
        target_var = self.lst_varlist_available.selectedIndexes()
        target_var_ix = target_var[0].row()
        self.target_col = self.col_dict_tuples[target_var_ix]
        self.target_pretty = self.col_list_pretty[target_var_ix]  # Needs new pretty list (screen names)

    def set_col_to_remove(self):
        """Get column name of target var from selected var list"""
        target_var = self.lst_varlist_selected.selectedIndexes()
        target_var_ix = target_var[0].row()
        self.target_col = list(self.selected_vars_lookup_dict.keys())[target_var_ix]  # Get key by index
        # self.target_col = self.col_dict_tuples[target_var_ix]
        # self.target_pretty = self.col_list_pretty[target_var_ix]  # Needs new pretty list (screen names)

    def add_col_to_selected(self):
        """Add var from available to selected df and lookup dict"""
        self.selected_vars_lookup_dict[self.target_col] = self.target_pretty
        self.vars_selected_df[self.target_col] = self.vars_available_df[self.target_col]

    # def get_settings_from_fields(self):
    #     upper_lim = float(self.lne_upper_class_lim.text())
    #     lower_lim = float(self.lne_lower_class_lim.text())
    #     return upper_lim, lower_lim

    def prepare_export(self):
        export_df = self.vars_selected_df.copy()
        export_df.columns = pd.MultiIndex.from_tuples(export_df.columns)
        return export_df

    def get_selected(self):
        """Return selected variables directly as new main df"""
        export_df = self.prepare_export()
        self.btn_make_new_subset.setDisabled(True)
        # main_df = export_to_main(main_df=main_df,
        #                          export_df=export_df,
        #                          tab_data_df=self.tab_data_df)  # Return results to main
        return export_df

    def remove_col_from_selected(self):
        """Remove from lookup dict and df"""
        del self.selected_vars_lookup_dict[self.target_col]
        self.vars_selected_df.drop(self.target_col, axis=1, inplace=True)

    def update_selected_vars_list(self):
        self.lst_varlist_selected.clear()
        for col in self.vars_selected_df.columns:
            item = qw.QListWidgetItem(self.selected_vars_lookup_dict[col])
            self.lst_varlist_selected.addItem(item)

    def check_if_selected_empty(self):
        _list = self.lst_varlist_selected
        if self.vars_selected_df.empty:
            _list.addItem('-empty-')
            empty = True
        else:
            empty = False
        return empty
