import gui.elements
import gui.plotfuncs
from gui import elements
from gui import plotfuncs


class AddControls():
    """
        Creates the gui control elements and their handles for usage.

    """

    def __init__(self, opt_stack, opt_frame, opt_layout, ref_stack):
        self.opt_stack = opt_stack
        self.opt_frame = opt_frame
        self.opt_layout = opt_layout
        self.ref_stack = ref_stack

        self.add_option()
        self.add_refinements()

    def add_option(self):
        self.opt_btn = elements.add_button_to_grid(grid_layout=self.opt_layout,
                                                   txt='Remove Var', css_id='',
                                                   row=3, col=0, rowspan=1, colspan=1)
        self.opt_stack.addWidget(self.opt_frame)

    def add_refinements(self):
        # Frame & Layout
        ref_frame, ref_layout = elements.add_frame_grid()
        self.ref_stack.addWidget(ref_frame)

        gui.elements.add_header_to_grid_top(layout=ref_layout, txt='MF: Remove Var')

        self.ref_btn_remove_var = elements.add_button_to_grid(grid_layout=ref_layout,
                                                              txt='Remove Var', css_id='',
                                                              row=2, col=0, rowspan=1, colspan=2)

        ref_layout.setRowStretch(3, 1)


class Execute():

    def __init__(self, fig, ax, data_df, focus_df, focus_col, focus_col_orig):
        # kudos: http://benalexkeen.com/resampling-time-series-data-with-pandas/

        self.df = data_df.copy()
        self.focus_df = focus_df.copy()
        self.focus_col = focus_col
        self.focus_col_orig = focus_col_orig
        self.fig = fig  # needed for redraw
        self.ax = ax  # adds to existing axis

        self.ax = gui.plotfuncs.remove_prev_lines(ax=self.ax)

    def remove_col(self):
        # self.rejectval_col = (self.measured_col[0], '[{}]'.format(self.units))
        # self.focus_df[self.rejectval_col] = self.focus_df[self.measured_col] * self.gain

        self.df.drop(self.focus_col_orig, axis=1, inplace=True)

        # self.color = '#e24d42'  # orange red
        # # Marker points
        # self.prev_line = pl_TimeSeries.ExecuteAddToAx(self.fig,
        #                                               x=self.focus_df.index, y=self.focus_df[self.rejectval_col],
        #                                               add_to_ax=self.ax, ms=8, color=self.color, marker='o',
        #                                               linestyle='--', linewidth=5)

        return self.df
