# matplotlib.use('Qt5Agg')

import matplotlib.gridspec as gridspec
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from PyQt5 import QtCore as qtc, QtWidgets as qw
from PyQt5 import QtGui
from pandas.plotting import register_matplotlib_converters

import gui.add_to_layout
import gui.base
import gui.make
from modboxes.plots import heatmap
from pkgs.dfun.frames import export_to_main

pd.plotting.register_matplotlib_converters()  # Needed for time plotting

import gui.elements
import gui.plotfuncs
import logger
from modboxes.plots.styles.LightTheme import *


class addContent(gui.base.buildTab):
    """Build new tab and populate with contents"""

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id, tab_template='SVLP')

        # Tab icon
        self.TabWidget.setTabIcon(self.tab_ix, QtGui.QIcon(app_obj.ctx.tab_icon_modifications))

        # Add settings menu contents
        self.dte_start, self.dte_end, self.btn_preview, self.btn_add_as_new_var, self.drp_apply_to, \
        self.btn_add_timerange, self.drp_inside_outside, self.btn_add_timerange = \
            self.add_settings_fields()

        # Add available variables to variable lists
        gui.base.buildTab.populate_variable_list(obj=self)

        # # Add plots
        # self.axes_dict = self.populate_plot_area()

    def populate_plot_area(self):
        return self.add_axes()

    def add_axes(self):
        pass

    def populate_settings_fields(self):
        pass
        # # Add all variables to drop-down lists to allow full flexibility yay
        # self.drp_xaxis.clear()
        # self.drp_yaxis.clear()
        # for ix, colname_tuple in enumerate(self.tab_data_df.columns):
        #     self.drp_xaxis.addItem(self.col_list_pretty[ix])
        #     self.drp_yaxis.addItem(self.col_list_pretty[ix])

    def add_settings_fields(self):
        # Settings
        gui.elements.add_header_subheader_to_grid_top(layout=self.sett_layout,
                                                      row=0, col=0, rowspan=1, colspan=2,
                                                      txt=["Remove Time Range",
                                                           "Modifications"])
        gui.add_to_layout.add_spacer_item_to_grid(
            row=1, col=0, layout=self.sett_layout)

        # Time range
        gui.elements.add_header_info_pair_in_grid_row(
            row=2, col=0, layout=self.sett_layout,
            txt='Time Range', txt_info_hover='..soon..')
        dte_start = gui.elements.grd_LabelDateEditPair(
            txt='Time Range Start (YYYY-MM-DD)', css_ids=['', ''], layout=self.sett_layout,
            row=3, col=0)
        dte_end = gui.elements.grd_LabelDateEditPair(
            txt='Time Range End (YYYY-MM-DD)', css_ids=['', ''], layout=self.sett_layout,
            row=4, col=0)
        drp_inside_outside = gui.elements.grd_LabelDropdownPair(
            txt='Remove', css_ids=['', ''], layout=self.sett_layout,
            row=5, col=0, orientation='horiz')
        drp_inside_outside.addItems(['Values Inside Time Range',
                                     'Values Outside Time Range'])
        btn_add_timerange = gui.elements.add_button_to_grid(
            grid_layout=self.sett_layout, txt='+ Add Time Range', css_id='btn_teal',
            row=6, col=1, rowspan=1, colspan=1)

        gui.add_to_layout.add_spacer_item_to_grid(
            row=7, col=0, layout=self.sett_layout)

        # Options
        gui.elements.add_header_info_pair_in_grid_row(
            row=8, col=0, layout=self.sett_layout,
            txt='Options', txt_info_hover='..soon..')
        drp_apply_to = gui.elements.grd_LabelDropdownPair(
            txt='Apply To', css_ids=['', ''], layout=self.sett_layout,
            row=9, col=0, orientation='horiz')
        drp_apply_to.addItems(['Selected Variable', 'All Variables'])
        gui.add_to_layout.add_spacer_item_to_grid(
            row=10, col=0, layout=self.sett_layout)

        # Buttons
        btn_preview = gui.elements.add_button_to_grid(
            grid_layout=self.sett_layout, txt='Preview', css_id='',
            row=11, col=0, rowspan=1, colspan=2)
        btn_add_as_new_var = gui.elements.add_button_to_grid(
            grid_layout=self.sett_layout, txt='+ Add As New Variable (if Apply To: Selected Variable)\n'
                                              '+ Modify Dataset (if Apply To: All Variables)',
            css_id='btn_add_as_new_var',
            row=12, col=0, rowspan=1, colspan=2)

        self.sett_layout.setRowStretch(13, 1)
        # drp_display_type = gui_elements.grd_LabelDropdownPair(txt='Display Type (x, y)',
        #                                                       css_ids=['', ''],
        #                                                       layout=self.sett_layout,
        #                                                       row=1, col=0,
        #                                                       orientation='horiz')
        # drp_items = ['Time & Date', 'Year & Day of Year']
        # drp_display_type.addItems(drp_items)

        return dte_start, dte_end, btn_preview, btn_add_as_new_var, drp_apply_to, btn_add_timerange, \
               drp_inside_outside, btn_add_timerange


class Run(addContent):
    color_list = colorwheel_36()  # get some colors
    sub_outdir = "modifications_set_to_missing"

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id)
        logger.log(name='>>> Starting Modifications: Remove Time Range', dict={}, highlight=True)  # Log info
        self.vars_available_df = self.tab_data_df.copy()
        self.sub_outdir = self.project_outdir / self.sub_outdir
        self.stm_df = pd.DataFrame()
        self.timerange_df = pd.DataFrame(columns=['start', 'end', 'inside_outside'])
        self.axes_dict = None
        self.target_col = None
        self.target_isset = False
        self.manage_fields()

    def add_to_rangelist(self):
        """Add time range to list"""
        self.get_settings_from_fields()
        self.add_timerange_to_selected()
        self.update_rangelist()

    def add_timerange_to_selected(self):
        """Collect selected time ranges in df"""
        self.timerange_df.loc[len(self.timerange_df)] = [self.start, self.end, self.inside_outside]

    def update_rangelist(self):
        """Show contents of timerange_df in list"""
        self.lst_rangelist_selected.clear()
        for idx, row in self.timerange_df.iterrows():
            listitem = f"{str(row['start'])} to {str(row['end'])} ({str(row['inside_outside'])})"
            item = qw.QListWidgetItem(listitem)
            self.lst_rangelist_selected.addItem(item)
        self.manage_fields()

    def remove_from_rangelist(self):
        idx = self.lst_rangelist_selected.currentRow()
        self.timerange_df.drop(idx, inplace=True)
        self.timerange_df.reset_index(inplace=True, drop=True)
        self.update_rangelist()

    def set_colnames(self):
        self.target_preview_col = (f"{self.target_col[0]}+mfRTR", self.target_col[1])

    def manage_fields_timerange(self):
        if self.timerange_df.empty:
            self.btn_preview.setDisabled(True)
            self.btn_add_as_new_var.setDisabled(True)
        elif not self.timerange_df.empty and self.target_isset:
            self.btn_preview.setEnabled(True)
            self.btn_add_as_new_var.setEnabled(True)

    def manage_fields(self):
        """Activate and deactivate GUI fields"""
        self.get_settings_from_fields()
        self.manage_fields_timerange()

    def preview(self):
        # self.get_settings_from_fields()

        self.stm_df[self.target_preview_col] = self.stm_df[self.target_col].copy()

        for idx, row in self.timerange_df.iterrows():
            _filter = (self.stm_df.index.date >= row['start']) \
                      & (self.stm_df.index.date <= row['end'])
            if row['inside_outside'] == 'inside':
                self.stm_df.loc[_filter, [self.target_preview_col]] = np.nan
            elif row['inside_outside'] == 'outside':
                self.stm_df.loc[~_filter, [self.target_preview_col]] = np.nan

        self.make_fig()
        self.btn_add_as_new_var.setEnabled(True)

    def prepare_export(self):
        pass

    def export_single_column(self, main_df):
        """
        Remove time range in target variable and insert as new column

        Parameters
        ----------
        main_df

        Returns
        -------
        main_df:    The original input df with one column added. The column added
                    is the target var but with selected time periods set to missing.

        """
        export_df = self.stm_df[[self.target_preview_col]].copy()
        main_df = export_to_main(main_df=main_df,
                                 export_df=export_df,
                                 tab_data_df=self.tab_data_df)  # Return results to main
        self.btn_add_as_new_var.setDisabled(True)
        return main_df

    def export_all_columns(self):
        """
        Remove time range in all variables

        Returns
        -------
        main_df:    The original input df with selected time periods set to missing.
                    Does not add new columns, instead the input columns are kept but
                    with values set to missing.
        """
        export_df = self.tab_data_df.copy()
        for idx, row in self.timerange_df.iterrows():
            filter = (export_df.index.date >= row['start']) \
                     & (export_df.index.date <= row['end'])
            if row['inside_outside'] == 'inside':
                export_df.loc[filter, :] = np.nan
            elif row['inside_outside'] == 'outside':
                export_df.loc[~filter, :] = np.nan
        main_df = export_df.copy()
        return main_df

    def get_selected(self, main_df):
        """Return modified class data back to main data"""
        self.get_settings_from_fields()

        if self.apply_to == 'Selected Variable':
            main_df = self.export_single_column(main_df=main_df)

        elif self.apply_to == 'All Variables':
            main_df = self.export_all_columns()

        self.btn_add_as_new_var.setDisabled(True)

        return main_df

    def update_dynamic_fields(self):
        """ Set line edit default entry and add dropdown options. """
        min_data_day_str = self.stm_df.index[0].strftime("%Y-%m-%d")  # str
        max_data_day_str = self.stm_df.index[-1].strftime("%Y-%m-%d")
        min_data_day_qt = qtc.QDate.fromString(min_data_day_str, 'yyyy-MM-dd')  # Qt format
        max_data_day_qt = qtc.QDate.fromString(max_data_day_str, 'yyyy-MM-dd')
        self.dte_start.setDate(min_data_day_qt)
        self.dte_end.setDate(max_data_day_qt)

    def make_axes_dict(self):
        # Setup grid for multiple axes
        gs = gridspec.GridSpec(1, 2)  # rows, cols
        gs.update(wspace=0.3, hspace=0.3, left=0.03, right=0.97, top=0.97, bottom=0.03)
        ax_before = self.fig.add_subplot(gs[0, 0])
        ax_after = self.fig.add_subplot(gs[0, 1])
        axes_dict = {'ax_before': ax_before, 'ax_after': ax_after}
        return axes_dict

    def update_target(self):
        """Set data for target"""
        self.stm_df = pd.DataFrame()  # Reset
        self.target_col = self.get_selected_var(varlist=self.lst_varlist_available)
        self.set_colnames()
        self.stm_df[self.target_col] = self.tab_data_df[self.target_col].copy()
        self.stm_df[self.target_preview_col] = np.nan
        self.update_dynamic_fields()
        self.make_fig()
        self.target_isset = True
        self.manage_fields()

    def make_fig(self):
        for ax in self.fig.axes:
            self.fig.delaxes(ax)
        self.axes_dict = self.make_axes_dict()
        for col in self.stm_df:
            if self.stm_df[col].empty:
                continue
            if col == self.target_col:
                ax = self.axes_dict['ax_before']
                self.make_heatmap(var_data=self.stm_df[col], ax=ax, fig=self.fig)
            else:
                ax = self.axes_dict['ax_after']
                self.make_heatmap(var_data=self.stm_df[col], ax=ax, fig=self.fig)

    @staticmethod
    def make_heatmap(var_data, ax, fig):
        x, y, z, plot_df = heatmap.Run._prepare(selected_col_data=var_data,
                                                display_type='Time & Date')
        if not var_data.empty:
            # colormap
            if np.nanmin(z) < 0 and np.nanmax(z) > 0:
                cmap_type = 'diverging'
                cmap = plt.get_cmap('RdYlBu_r')  # set colormap, see https://matplotlib.org/users/colormaps.html
            else:
                cmap_type = 'sequential'
                cmap = plt.get_cmap('GnBu')

            z_numvals_unique = len(plot_df['z'].unique())
            if z_numvals_unique > 100:
                vmin = plot_df['z'].quantile(.01)
                vmax = plot_df['z'].quantile(.99)
            else:
                vmin = plot_df['z'].quantile(0)
                vmax = plot_df['z'].quantile(1)

            heatmap.Run.plot_heatmap(x=x, y=y, z=z, ax=ax, cmap=cmap, color_bad='Grey',
                                     info_txt="", vmin=vmin, vmax=vmax, cb_digits_after_comma=1)
            ax.set_facecolor('white')
            ax.set_xticks(['6:00', '12:00', '18:00'])
            ax.set_xticklabels([6, 12, 18])
            gui.plotfuncs.nice_date_ticks(ax=ax, minticks=3, maxticks=7, which='y')
            gui.plotfuncs.default_format(ax=ax, txt_xlabel='Time')
            plt.tight_layout()
            fig.canvas.draw()  # update plot w/ only canvas.draw but NOT with pause() works w/o background flashing

    def get_selected_var(self, varlist):
        """Get column name from dropdown selection"""
        selected_var = varlist.selectedIndexes()
        selected_var_ix = selected_var[0].row()
        selected_col = self.col_dict_tuples[selected_var_ix]
        # selected_pretty = self.col_list_pretty[selected_var_ix]  # Needs new pretty list (screen names)
        return selected_col

    def get_settings_from_fields(self):
        """Get selected settings from fields"""
        self.start = self.dte_start.date().toPyDate()
        self.end = self.dte_end.date().toPyDate()
        self.inside_outside = self.drp_inside_outside.currentText()
        self.apply_to = self.drp_apply_to.currentText()
        if self.inside_outside == 'Values Inside Time Range':
            self.inside_outside = 'inside'
        elif self.inside_outside == 'Values Outside Time Range':
            self.inside_outside = 'outside'
        else:
            self.inside_outside = '-not-specified-'
