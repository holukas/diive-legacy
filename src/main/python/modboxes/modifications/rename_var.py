import pandas as pd
from PyQt5 import QtGui
from PyQt5 import QtWidgets as qw

import gui.base
import gui.elements
import gui.tableview


class addContent(gui.base.buildTab):
    """Build new tab and populate with contents"""

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id, tab_template='ST')

        # Tab icon
        self.TabWidget.setTabIcon(self.tab_ix, QtGui.QIcon(app_obj.ctx.tab_icon_modifications))

        # Add settings menu contents
        self.drp_add_string_to, self.lne_string, self.btn_apply_prefix_suffix, self.btn_run = \
            self.add_settings_fields()

        # # Add variables required in settings
        # self.populate_settings_fields()

        # # Add plots and tableview
        # self.axes_dict = self.populate_plot_area()

    def populate_plot_area(self):
        return self.add_axes()

    def add_axes(self):
        pass

    def populate_settings_fields(self):
        pass

    def add_settings_fields(self):
        gui.elements.add_header_to_grid_top(layout=self.sett_layout, txt='Rename Variables')

        # Bulk rename: prefix / suffix
        gui.elements.add_header_in_grid_row(layout=self.sett_layout, txt='Add Prefix / Suffix', row=1)
        lne_string = gui.elements.add_label_linedit_pair_to_grid(txt='String',
                                                                 css_ids=['', 'cyan'],
                                                                 layout=self.sett_layout,
                                                                 orientation='horiz',
                                                                 row=2, col=0)
        lne_string.setText('_x')

        drp_add_string_to = gui.elements.grd_LabelDropdownPair(txt='Add String To',
                                                               css_ids=['', 'cyan'], layout=self.sett_layout,
                                                               row=3, col=0, orientation='horiz')
        drp_add_string_to.addItem('Variable Name as Suffix')
        drp_add_string_to.addItem('Variable Name as Prefix')
        drp_add_string_to.addItem('Units as Suffix')
        drp_add_string_to.addItem('Units as Prefix')

        # Run button
        btn_add_prefix_suffix = gui.elements.add_button_to_grid(grid_layout=self.sett_layout,
                                                                txt='Add Prefix / Suffix',
                                                                css_id='btn_cat_ControlsRun',
                                                                row=4, col=0, rowspan=1, colspan=2)

        self.sett_layout.addWidget(qw.QLabel(), 5, 0, 1, 1)  # spacer

        # Run button
        btn_run = gui.elements.add_button_to_grid(grid_layout=self.sett_layout,
                                                  txt='Apply New Names', css_id='btn_cat_ControlsRun',
                                                  row=6, col=0, rowspan=1, colspan=2)

        # Stretch
        self.sett_layout.addWidget(qw.QLabel(), 7, 0, 20, 1)  # spacer

        return drp_add_string_to, lne_string, btn_add_prefix_suffix, btn_run


class Run(addContent):
    rename_lookup_dict = {}

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id)
        self.table_df = self.prepare_tableview()
        self.populate_tableview()
        # self.data_df = data_df.copy()
        # self.var = var
        # self.new_varname = new_varname
        # self.new_varunits = new_varunits
        # self.col_list_pretty = col_list_pretty
        # self.col_dict_tuples = col_dict_tuples
        # self.run()

    def add_prefix_suffix(self):
        string, add_string_to = self.get_settings_from_fields()

        if add_string_to == 'Variable Name as Suffix':
            self.table_df['Variable'] = self.table_df['Variable'].apply(lambda x: str(x) + f"{string}")
        elif add_string_to == 'Variable Name as Prefix':
            self.table_df['Variable'] = self.table_df['Variable'].apply(lambda x: f"{string}" + str(x))
        elif add_string_to == 'Units as Suffix':
            self.table_df['Units'] = self.table_df['Units'].apply(lambda x: str(x) + f"{string}")
        elif add_string_to == 'Units as Prefix':
            self.table_df['Units'] = self.table_df['Units'].apply(lambda x: f"{string}" + str(x))

        self.populate_tableview()

    def get_settings_from_fields(self):
        string = self.lne_string.text()
        add_string_to = self.drp_add_string_to.currentText()
        return string, add_string_to

    def prepare_tableview(self):
        # List of column name tuples becomes two columns
        table_df = pd.DataFrame(list(self.tab_data_df.columns),
                                index=self.tab_data_df.columns,
                                columns=['Variable', 'Units'])

        # # Add position index of col
        # table_df['Position'] = np.nan
        # for col in table_df.index:
        #     for key, value in self.col_dict_tuples.items():
        #         if value == col:
        #             table_df.loc[col, 'Position'] = key

        # self.col_dict_tuples.index[col]
        #     listOfKeys = [key for (key, value) in self.col_dict_tuples.items() if value == 43]

        return table_df

    def get_renamed(self):
        self.table_df = self.table_contents._dataframe.copy()  # Contains tuple column names in index
        self.tab_data_df = self.rename_from_table()
        return self.tab_data_df

    def rename_from_table(self):
        self.tab_data_df.columns = [tuple(x) for x in self.tab_data_df.columns]  # Tuples for lookup

        # Generate lookup dict for renaming
        for old_colname in self.table_df.index:
            new_colname = (self.table_df.loc[old_colname, 'Variable'], self.table_df.loc[old_colname, 'Units'])
            self.rename_lookup_dict[old_colname] = new_colname

        # Use lookup dict to rename cols in df
        for old_colname, new_colname in self.rename_lookup_dict.items():
            self.tab_data_df.rename(columns={old_colname: new_colname},
                                    inplace=True)
        self.tab_data_df.columns = pd.MultiIndex.from_tuples(self.tab_data_df.columns)
        return self.tab_data_df

    def populate_tableview(self):
        # Show df in tableview
        self.table_view.reset()
        self.table_contents = gui.tableview.DataFrameModel(df=self.table_df, editable=True)
        self.table_view.setModel(self.table_contents)
        self.table_view.setAlternatingRowColors(True)
        self.table_view.resizeColumnsToContents()
        # self.tableview.setModel(model, qtc.QVariant(qtc.Qt.AlignRight), qtc.Qt.TextAlignmentRole)
        # self.tableview.installEventFilters(self)
        # self.tableview.setSortingEnabled(True)
        # self.tableview.setItemDelegate(FloatDelegate(3))
        return None
