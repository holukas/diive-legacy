import datetime as dt
import os
from pathlib import Path

import matplotlib.dates as mdates
import matplotlib.gridspec as gridspec
import numpy as np
import pandas as pd
from PyQt5 import QtCore as qtc, QtWidgets as qw
from PyQt5 import QtGui

import gui.add_to_layout
import gui.base
import gui.elements
import gui.make
import gui.plotfuncs
import logger
import pkgs
from pkgs.dfun.frames import export_to_main

# pd.set_option('display.max_rows', 50)
pd.set_option('display.max_columns', 20)


# pd.set_option('display.width', 1000)

class addContent(gui.base.buildTab):
    """Build new tab and populate with contents"""

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id, tab_template='SVaVaLP')

        # Tab icon
        self.TabWidget.setTabIcon(self.tab_ix, QtGui.QIcon(app_obj.ctx.tab_icon_modifications))

        # Add settings menu contents
        self.btn_calc, self.btn_add_as_new_var, \
        self.dte_start, self.dte_end, self.btn_add_timerange, self.btn_export_to_file = \
            self.add_settings_fields()

        # # Add variables required in settings
        # self.populate_settings_fields()

        # Add available variables to variable lists
        gui.base.buildTab.populate_multiple_variable_lists(col_list_pretty=self.col_list_pretty,
                                                           df=self.tab_data_df,
                                                           lists=[self.lst_varlist_target,
                                                                  self.lst_varlist_reference])

        # # Add plots
        # self.axes_dict = self.populate_plot_area()

        # # Add special
        # self.lbl_statsbox_num_gaps, self.lbl_statsbox_available = self.add_statsboxes()

    # def populate_plot_area(self):
    #     return self.add_axes()

    def populate_settings_fields(self):
        pass
        # self.lne_timewin_len.setText("14")

    def add_axes(self):
        pass

    def add_settings_fields(self):
        gui.elements.add_header_subheader_to_grid_top(layout=self.sett_layout,
                                                      txt=["Modifications",
                                                           "Span Correction"])
        gui.add_to_layout.add_spacer_item_to_grid(layout=self.sett_layout, row=1, col=0)

        # gui.gui_elements.add_header_in_grid_row(layout=self.sett_layout, txt="Fit Window", row=2)
        # lne_timewin_len = gui_elements.add_label_linedit_pair_to_grid(
        #     txt='Size (days)', css_ids=['', 'cyan'], layout=self.sett_layout,
        #     row=3, col=0, orientation='horiz')
        # gui.gui_elements.add_spacer_item_to_grid(layout=self.sett_layout,
        #                                          row=4, col=0)

        # Time range
        gui.elements.add_header_info_pair_in_grid_row(
            row=2, col=0, layout=self.sett_layout,
            txt='Time Range', txt_info_hover='Define time range for fit. Start and end dates are INCLUSIVE.')
        dte_start = gui.elements.grd_LabelDateTimePickerPair(
            txt='Time Range Start (YYYY-MM-DD hh:mm)', css_ids=['', ''], layout=self.sett_layout,
            row=3, col=0)
        dte_end = gui.elements.grd_LabelDateTimePickerPair(
            txt='Time Range End (YYYY-MM-DD hh:mm)', css_ids=['', ''], layout=self.sett_layout,
            row=4, col=0)
        btn_add_timerange = gui.elements.add_button_to_grid(
            grid_layout=self.sett_layout, txt='+ Add Time Range', css_id='btn_teal',
            row=5, col=1, rowspan=1, colspan=1)

        gui.add_to_layout.add_spacer_item_to_grid(
            row=6, col=0, layout=self.sett_layout)

        btn_calc = gui.elements.add_button_to_grid(
            grid_layout=self.sett_layout, txt='Calculate Ratios', css_id='',
            row=7, col=0, rowspan=1, colspan=2)

        gui.add_to_layout.add_spacer_item_to_grid(layout=self.sett_layout, row=8, col=0)

        btn_export_to_file = gui.elements.add_button_to_grid(grid_layout=self.sett_layout,
                                                             txt='Export Fit Parameters To File',
                                                             css_id='btn_export',
                                                             row=9, col=0, rowspan=1, colspan=2)

        gui.add_to_layout.add_spacer_item_to_grid(layout=self.sett_layout, row=10, col=0)

        btn_add_as_new_var = gui.elements.add_button_to_grid(grid_layout=self.sett_layout,
                                                             txt='+ Add As New Var',
                                                             css_id='btn_add_as_new_var',
                                                             row=11, col=0, rowspan=1, colspan=2)

        self.sett_layout.setRowStretch(12, 2)  # empty row

        return btn_calc, btn_add_as_new_var, dte_start, dte_end, btn_add_timerange, \
               btn_export_to_file


class Run(addContent):
    sub_outdir = "modifications_span_correction"
    short_id = "mfSC"

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id)
        logger.log(name='>>> Starting Modifications: Span Correction', dict={}, highlight=True)  # Log info
        self.sub_outdir = self.project_outdir / self.sub_outdir
        self.class_df = pd.DataFrame()
        self.timerange_df = pd.DataFrame(columns=['start', 'end'])
        self.target_col = None
        self.reference_col = None
        self.target_loaded = False
        self.reference_loaded = False
        self.ready_to_export = False
        self.btn_export_to_file.setDisabled(True)
        self.lst_varlist_reference.setDisabled(True)
        self.btn_add_timerange.setDisabled(True)
        self.btn_calc.setDisabled(True)
        self.btn_add_as_new_var.setDisabled(True)
        self.set_colnames()

    def select_target(self):
        """Select target var from list"""
        self.remove_target()  # Reset target data
        self.set_target_col()
        self.class_df = self.init_class_df()
        self.update_dynamic_fields()
        self.btn_add_timerange.setEnabled(True)
        if self.reference_loaded:  # Add reference data that was already selected
            self.class_df[self.reference_col] = self.tab_data_df[[self.reference_col]].copy()
        self.target_loaded = True
        self.ratio_loaded = False
        self.btn_add_as_new_var.setDisabled(True)
        self.lst_varlist_reference.setEnabled(True)
        self.btn_export_to_file.setDisabled(True)
        self.check_status()
        # self.btn_add_as_new_var.setDisabled(False)
        self.plot_data()

    def select_reference(self):
        """Select reference var from list"""
        self.remove_reference()  # Reset reference; remove previous reference data
        self.set_reference_col()  # Set new reference
        self.class_df[self.reference_col] = self.tab_data_df[[self.reference_col]].copy()
        self.update_dynamic_fields()
        self.btn_add_timerange.setEnabled(True)
        if self.target_loaded:  # Add reference data that was already selected
            self.class_df[self.target_col] = self.tab_data_df[[self.target_col]].copy()
        self.reference_loaded = True
        self.ratio_loaded = False
        self.btn_add_as_new_var.setDisabled(True)
        self.btn_export_to_file.setDisabled(True)
        self.check_status()
        self.plot_data()

    def remove_target(self):
        """Remove previous target data from class_df"""
        if self.target_loaded:
            self.class_df.drop(self.target_col, axis=1, inplace=True)

    def remove_reference(self):
        """Remove previous reference data from class_df"""
        if self.reference_loaded:
            self.class_df.drop(self.reference_col, axis=1, inplace=True)

    def check_status(self):
        if self.target_loaded and self.reference_loaded:
            self.btn_calc.setEnabled(True)

    def set_colnames(self):
        """Set names of columns that are added to DataFrame."""
        # k, d, fitted_values, rsquared, rsquared_adj, results
        self.ratio_ref_over_target_col = ('_ratio_ref/target', '[aux]')
        self.ratio_fit_k_col = ('_ratio_fit_k', '[aux]')
        self.ratio_fit_d_col = ('_ratio_fit_d', '[aux]')
        self.ratio_fit_rsquared_col = ('_ratio_fit_rsquared', '[aux]')
        self.ratio_fit_rsquared_adj_col = ('_ratio_fit_rsquared_adj', '[aux]')
        self.ratio_fit_col = ('_ratio_fit', '[aux]')
        self.fitgroup_col = ('_fitgroup', '[aux]')
        self.target_corrected_col = ('TARGET_CORRECTED', '[aux]')

    def init_aux_cols(self):
        self.class_df[self.ratio_ref_over_target_col] = np.nan
        self.class_df[self.ratio_fit_k_col] = np.nan
        self.class_df[self.ratio_fit_d_col] = np.nan
        self.class_df[self.ratio_fit_rsquared_col] = np.nan
        self.class_df[self.ratio_fit_rsquared_adj_col] = np.nan
        self.class_df[self.ratio_fit_col] = np.nan
        self.class_df[self.target_corrected_col] = np.nan

    def calc(self):
        self.get_settings_from_fields()
        self.init_aux_cols()  # Reset aux cols

        # Calculate ratio across all data
        self.class_df[self.ratio_ref_over_target_col] = \
            self.class_df[self.reference_col] / self.class_df[self.target_col]
        self.ratio_loaded = True

        # Make subset for fitting
        _fit_df = pd.DataFrame()
        _fit_df['_ratio'] = self.class_df[self.ratio_ref_over_target_col].copy()
        _fit_df['_date'] = self.class_df.index.date
        _fit_df['_date'] = pd.to_datetime(_fit_df['_date'])
        _fit_df['_date'] = _fit_df['_date'].map(dt.datetime.toordinal)
        _fit_df['_time'] = (self.class_df.index.hour
                            + (self.class_df.index.minute / 60)
                            + (self.class_df.index.second / 3600)) / 24
        _fit_df['_datetime'] = _fit_df['_date'] + _fit_df['_time']
        _fit_df = _fit_df[['_ratio', '_datetime']]
        _fit_df['_group'] = np.nan

        # Assign fitting groups
        _assign_group = 0
        for idx, row in self.timerange_df.iterrows():
            _assign_group += 1
            filter = (self.class_df.index >= row['start']) \
                     & (self.class_df.index <= row['end'])
            _fit_df.loc[filter, ['_group']] = _assign_group

        # freq = f"{self.timewin_len}D"
        # grouped_freq = _fit_df.groupby(pd.Grouper(level=_fit_df.index.name, freq=freq))

        grouped = _fit_df.groupby(['_group'])

        for group_name, group_df in grouped:
            _group_df = group_df[['_ratio', '_datetime']]
            k, d, fitted_values, rsquared, rsquared_adj, results = \
                pkgs.dfun.regression.linear(df=_group_df)
            self.class_df.loc[_group_df.index, self.ratio_fit_k_col] = k
            self.class_df.loc[_group_df.index, self.ratio_fit_d_col] = d
            self.class_df.loc[_group_df.index, self.ratio_fit_rsquared_col] = rsquared
            self.class_df.loc[_group_df.index, self.ratio_fit_rsquared_adj_col] = rsquared_adj
            self.class_df.loc[_group_df.index, self.fitgroup_col] = group_name
            self.class_df.loc[_group_df.index, self.ratio_fit_col] = fitted_values
            self.class_df.loc[_group_df.index, self.ratio_fit_col] = \
                self.class_df.loc[_group_df.index, self.ratio_fit_col].interpolate()

        # Apply correction to target and save in corrected target column
        self.class_df[self.target_corrected_col] = \
            self.class_df[self.target_col].multiply(self.class_df[self.ratio_fit_col])

        # Fill gaps in corrected target column with the original uncorrected target values
        self.class_df[self.target_corrected_col].fillna(self.class_df[self.target_col], inplace=True)

        self.plot_data()
        self.btn_add_as_new_var.setEnabled(True)
        self.btn_export_to_file.setEnabled(True)

    def make_axes_dict(self):
        for ax in self.fig.axes:
            self.fig.delaxes(ax)
        gs = gridspec.GridSpec(3, 1)  # rows, cols
        gs.update(wspace=0.2, hspace=0.2, left=0.03, right=0.96, top=0.96, bottom=0.03)
        ax_reference_target = self.fig.add_subplot(gs[0, 0])
        ax_ratio_fit = self.fig.add_subplot(gs[1, 0], sharex=ax_reference_target)
        ax_target_corrected = self.fig.add_subplot(gs[2, 0], sharex=ax_reference_target)
        axes_dict = {'ax_reference_target': ax_reference_target,
                     'ax_ratio_fit': ax_ratio_fit,
                     'ax_target_corrected': ax_target_corrected}
        for key, ax in axes_dict.items():
            gui.plotfuncs.default_format(ax=ax, txt_xlabel=False)
        return axes_dict

    def plot_data(self):
        axes_dict = self.make_axes_dict()
        plot_df = self.class_df.copy()

        # axes_dict['ax_reference_target'].text(0.02, 0.96, '{}'.format(col[0]),
        #                                       size=14, color='#FFCA28', backgroundcolor='none',
        #                                       transform=axes_dict['ax_reference_target'].transAxes, alpha=0.6,
        #                                       horizontalalignment='left', verticalalignment='top')

        # Plot target
        if self.target_loaded:
            _stats = plot_df[self.target_col].median()
            axes_dict['ax_reference_target'].plot_date(
                x=plot_df.index, y=plot_df[self.target_col],
                alpha=1, ls='-', marker='o', markeredgecolor='none', ms=4, zorder=98,
                label=f"TARGET: {self.target_col}  median: {_stats}")

        # Plot reference
        if self.reference_loaded:
            _stats = plot_df[self.reference_col].median()
            axes_dict['ax_reference_target'].plot_date(
                x=plot_df.index, y=plot_df[self.reference_col],
                alpha=1, ls='-', marker='o', markeredgecolor='none', ms=4, zorder=98,
                label=f"REFERENCE: {self.reference_col}  median: {_stats}")

        if self.ratio_loaded:
            # Ratio
            axes_dict['ax_ratio_fit'].plot_date(
                x=plot_df.index, y=plot_df[self.ratio_ref_over_target_col],
                alpha=1, ls='-', marker='o', markeredgecolor='none', ms=4, zorder=98,
                label=self.ratio_ref_over_target_col)
            axes_dict['ax_ratio_fit'].plot_date(
                x=plot_df.index, y=plot_df[self.ratio_fit_col],
                alpha=1, ls='-', marker='o', markeredgecolor='none', ms=4, zorder=98,
                label=self.ratio_fit_col)

            # Corrected target
            _stats = plot_df[self.target_corrected_col].median()
            axes_dict['ax_target_corrected'].plot_date(
                x=plot_df.index, y=plot_df[self.target_corrected_col],
                alpha=1, ls='-', marker='o', markeredgecolor='none', ms=4,
                zorder=98,
                label=f"CORRECTED TARGET: {self.target_corrected_col}  median: {_stats}")
            _stats = plot_df[self.reference_col].median()
            axes_dict['ax_target_corrected'].plot_date(
                x=plot_df.index, y=plot_df[self.reference_col],
                alpha=1, ls='-', marker='o', markeredgecolor='none', ms=4, zorder=98,
                label=f"REFERENCE: {self.reference_col}  median: {_stats}")

        for ix, ax in axes_dict.items():
            gui.plotfuncs.default_grid(ax=ax)
            gui.plotfuncs.default_legend(ax=ax)
            locator = mdates.AutoDateLocator(minticks=3, maxticks=30)
            formatter = mdates.ConciseDateFormatter(locator)
            ax.xaxis.set_major_locator(locator)
            ax.xaxis.set_major_formatter(formatter)

        self.fig.canvas.draw()
        self.fig.canvas.flush_events()

    def get_settings_from_fields(self):
        """Get selected settings from fields"""
        # self.timewin_len = int(self.lne_timewin_len.text())

        self.start = self.dte_start.dateTime().toString('yyyy-MM-dd hh:mm')
        self.start = dt.datetime.strptime(self.start, '%Y-%m-%d %H:%M')

        self.end = self.dte_end.dateTime().toString('yyyy-MM-dd hh:mm')
        self.end = dt.datetime.strptime(self.end, '%Y-%m-%d %H:%M')

    def init_class_df(self):
        return self.tab_data_df[[self.target_col]].copy()

    def update_fields(self):
        pass

    def set_target_col(self):
        """Get column name of target var from target var list"""
        target_var = self.lst_varlist_target.selectedIndexes()
        target_var_ix = target_var[0].row()
        self.target_col = self.col_dict_tuples[target_var_ix]

    def set_reference_col(self):
        """Get column name of reference var from reference var list"""
        reference_var = self.lst_varlist_reference.selectedIndexes()
        reference_var_ix = reference_var[0].row()
        self.reference_col = self.col_dict_tuples[reference_var_ix]

    def get_selected(self, main_df):
        """Return modified class data back to main data"""
        export_df = self.prepare_export()
        main_df = export_to_main(main_df=main_df,
                                 export_df=export_df,
                                 tab_data_df=self.tab_data_df)  # Return results to main
        self.btn_add_as_new_var.setDisabled(True)
        return main_df

    def prepare_export(self):
        export_df = self.class_df[[self.target_corrected_col]].copy()  # Corrected target data
        corrected_col = (self.target_col[0] + f"+{self.short_id}", self.target_col[1])
        export_df[corrected_col] = export_df[self.target_corrected_col]
        export_df = export_df[[corrected_col]]
        return export_df

    def export_to_file(self):
        """Export fit results to separate file"""

        # Prepare data for export to file
        out_df = pd.DataFrame(columns=['fit_start', 'fit_end', 'fit_slope_k', 'fit_offset_d'])
        _df = self.class_df.copy()
        _grouped = _df.groupby(_df[self.fitgroup_col])
        for _group, _group_df in _grouped:
            print(_group_df)
            start_dt = _group_df.index[0]
            end_dt = _group_df.index[-1]
            d = _group_df[self.ratio_fit_d_col].loc[start_dt]  # Fit offset
            k = _group_df[self.ratio_fit_k_col].loc[start_dt]  # Fit slope
            out_df.loc[len(out_df)] = [start_dt, end_dt, k, d]

        # Output dir
        outpath = self.project_outdir / self.sub_outdir
        if not Path.is_dir(outpath):
            logger.log(name=f"Creating folder {outpath} ...", dict={}, highlight=False)  # Log info
            os.makedirs(outpath)
        suffix = pkgs.dfun.times.make_timestamp_suffix()
        outfile = outpath / f"fit_results_ratio_{self.reference_col[0]}_over_{self.target_col[0]}_[{suffix}].csv"
        out_df.to_csv(outfile, index=False)
        self.btn_export_to_file.setDisabled(True)

    def remove_from_rangelist(self):
        idx = self.lst_rangelist_selected.currentRow()
        self.timerange_df.drop(idx, inplace=True)
        self.timerange_df.reset_index(inplace=True, drop=True)
        self.update_timerange_list()

    def add_timerange(self):
        """Add time range to list"""
        self.get_settings_from_fields()
        self.add_timerange_to_selected()
        self.update_timerange_list()

    def add_timerange_to_selected(self):
        """Collect selected time ranges in df"""
        self.timerange_df.loc[len(self.timerange_df)] = [self.start, self.end]

    def update_timerange_list(self):
        """Show contents of timerange_df in list"""
        self.lst_rangelist_selected.clear()
        for idx, row in self.timerange_df.iterrows():
            listitem = f"{str(row['start'])} to {str(row['end'])}"
            item = qw.QListWidgetItem(listitem)
            self.lst_rangelist_selected.addItem(item)

    def update_dynamic_fields(self):
        """ Set line edit default entry and add dropdown options. """
        min_data_day_str = self.class_df.index[0].strftime("%Y-%m-%d %H:%M")  # str
        max_data_day_str = self.class_df.index[-1].strftime("%Y-%m-%d %H:%M")
        min_data_day_qt = qtc.QDateTime.fromString(min_data_day_str, 'yyyy-MM-dd hh:mm')  # Qt format
        max_data_day_qt = qtc.QDateTime.fromString(max_data_day_str, 'yyyy-MM-dd hh:mm')
        self.dte_start.setDateTime(min_data_day_qt)
        self.dte_end.setDateTime(max_data_day_qt)
