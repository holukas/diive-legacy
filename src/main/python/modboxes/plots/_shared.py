import gui
import gui.plotfuncs
from modboxes.plots.styles.LightTheme import *
from modboxes.plots.styles.LightTheme import COLOR_MARKER_PREVIEW, SIZE_MARKER_PREVIEW, TYPE_MARKER_PREVIEW, \
    COLOR_MARKER_PREVIEW_EDGE


def add_line_to_existing_ax(fig, add_to_ax, x, y, linestyle, linewidth, legend_txt_suffix=False,
                            alpha=1, color=COLOR_MARKER_PREVIEW, ms=SIZE_MARKER_PREVIEW):
    """ Add plot to existing axis """
    legend_txt = y.name[0]
    if legend_txt_suffix:
        legend_txt += ': ' + legend_txt_suffix

    # fontP = FontProperties()
    # fontP.set_size('x-large')

    line = add_to_ax.plot_date(x=x, y=y, alpha=alpha, ls=linestyle, lw=linewidth,
                               color=color,
                               marker='',
                               markeredgecolor=COLOR_LINE_LIMIT,
                               markeredgewidth=0,
                               ms=ms,
                               zorder=99,
                               label=legend_txt)

    gui.plotfuncs.default_legend(ax=add_to_ax)
    fig.canvas.draw()  # update plot
    return line


def add_to_existing_ax(fig, add_to_ax, x, y, linestyle, linewidth, legend_txt_suffix=False,
                       alpha=1, color=COLOR_MARKER_PREVIEW, ms=SIZE_MARKER_PREVIEW,
                       marker=TYPE_MARKER_PREVIEW):
    """ Add plot to existing axis """
    legend_txt = y.name[0]
    if legend_txt_suffix:
        legend_txt += ': ' + legend_txt_suffix

    # fontP = FontProperties()
    # fontP.set_size('x-large')

    line = add_to_ax.plot_date(x=x, y=y, alpha=alpha, ls=linestyle, lw=linewidth,
                               color=color,
                               marker=marker,
                               markeredgecolor=COLOR_MARKER_PREVIEW_EDGE,
                               markeredgewidth=2,
                               ms=ms,
                               zorder=99,
                               label=legend_txt)
    gui.plotfuncs.default_legend(ax=add_to_ax)
    fig.canvas.draw()  # update plot
    return line
