"""

PERCENTILE PLOTS
----------------

"""
import matplotlib.gridspec as gridspec
import numpy as np
import pandas as pd
from PyQt5 import QtGui as qtg
from PyQt5 import QtWidgets as qtw

import gui
import gui.base
import logger
from .styles import LightTheme as theme


class addContent(gui.base.buildTab):
    """Build new tab and populate with contents"""

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id, tab_template='SVVP')

        # Tab icon
        self.TabWidget.setTabIcon(self.tab_ix, qtg.QIcon(app_obj.ctx.tab_icon_plots))

        # Add settings menu contents
        self.drp_type, self.drp_agg, self.btn_apply = \
            self.add_settings_fields()

        # Add variables required in settings
        self.populate_settings_fields()

        # Add available variables to variable lists
        gui.base.buildTab.populate_variable_list(obj=self)

        # # Add plots
        # self.populate_plot_area()

    def populate_plot_area(self):
        # This is done in Run due to dynamic amount of axes
        pass

    def populate_settings_fields(self):
        pass

    def add_settings_fields(self):
        # Settings
        gui.elements.add_header_to_grid_top(layout=self.sett_layout, txt='Settings')

        drp_type = \
            gui.elements.grd_LabelDropdownPair_dropEnabled(txt='Quantile Type',
                                                           css_ids=['', ''],
                                                           layout=self.sett_layout,
                                                           row=1, col=0,
                                                           orientation='horiz')
        drp_type.addItems(['Percentiles', 'Deciles', 'Quintiles', 'Quartiles'])

        drp_agg = \
            gui.elements.grd_LabelDropdownPair_dropEnabled(txt='Aggregation',
                                                           css_ids=['', ''],
                                                           layout=self.sett_layout,
                                                           row=2, col=0,
                                                           orientation='horiz')
        drp_agg.addItems(['All Data', 'Per Month', 'Per Year'])

        # ref_layout.setRowStretch(3, 1)

        btn_apply = gui.elements.add_button_to_grid(grid_layout=self.sett_layout,
                                                    txt='Apply', css_id='',
                                                    row=3, col=0, rowspan=1, colspan=2)

        self.sett_layout.setRowStretch(4, 1)

        return drp_type, drp_agg, btn_apply


class Run(addContent):
    color_list = theme.colorwheel_36()  # get some colors
    sub_outdir = "plot_quantiles"

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id)
        logger.log(name='>>> Starting Quantile plots', dict={}, highlight=True)  # Log info
        self.selected_vars_lookup_dict = {}
        self.vars_selected_df = pd.DataFrame()
        self.vars_available_df = self.tab_data_df.copy()
        self.sub_outdir = self.project_outdir / self.sub_outdir
        # self.update_btn_status()

    def apply(self):
        self.plot_quantiles()

    def add_var_to_selected(self):
        """ Add feature from available to selected. """
        selected_var = self.lst_varlist_available.selectedIndexes()
        selected_var_ix = selected_var[0].row()
        selected_col = self.col_dict_tuples[selected_var_ix]
        selected_pretty = self.col_list_pretty[selected_var_ix]  # Needs new pretty list (screen names)

        # Add to lookup dict and df
        self.selected_vars_lookup_dict[selected_col] = selected_pretty
        self.vars_selected_df[selected_col] = self.vars_available_df[selected_col]

        self.update_selected_vars_list(list=self.lst_varlist_selected, df=self.vars_selected_df,
                                       lookup_dict=self.selected_vars_lookup_dict)
        self.plot_quantiles()

    def remove_var_from_selected(self):
        selected_var = self.lst_varlist_selected.selectedIndexes()
        selected_var_ix = selected_var[0].row()
        selected_col = list(self.selected_vars_lookup_dict.keys())[selected_var_ix]  # Get key by index
        # selected_pretty = self.selected_features_lookup_dict[selected_col]

        # Remove from lookup dict and df
        del self.selected_vars_lookup_dict[selected_col]
        self.vars_selected_df.drop(selected_col, axis=1, inplace=True)

        self.update_selected_vars_list(list=self.lst_varlist_selected, df=self.vars_selected_df,
                                       lookup_dict=self.selected_vars_lookup_dict)

        empty = self.check_if_selected_empty()

        if empty:
            for ax in self.fig.axes:
                self.fig.delaxes(ax)
                self.fig.canvas.draw()
                self.fig.canvas.flush_events()
        else:
            axes_dict = self.make_axes_dict(fig=self.fig, df=self.vars_selected_df)
            self.plot_quantiles()

    def update_selected_vars_list(self, list, df, lookup_dict):
        list.clear()
        for col in df.columns:
            item = qtw.QListWidgetItem(lookup_dict[col])
            list.addItem(item)

    def update_available_vars_list(self, list, df):
        list.clear()
        list_cols = []
        # Add all variables to drop-down lists to allow full flexibility yay
        for ix, colname_tuple in enumerate(df.columns):
            item = qtw.QListWidgetItem(self.col_list_pretty[ix])
            list.addItem(item)  # add column name to list
            list_cols.append(colname_tuple)
        return list, list_cols

    def check_if_selected_empty(self):
        _list = self.lst_varlist_selected
        if self.vars_selected_df.empty:
            _list.addItem('-empty-')
            empty = True
        else:
            empty = False
        return empty

    def get_type_settings(self):
        type = self.drp_type.currentText()
        if type == 'Percentiles':
            range_index = range(0, 101, 1)
            highlights = [10, 25, 50, 75, 90]
            nbins = 22
        elif type == 'Deciles':
            range_index = range(0, 101, 10)
            highlights = [10, 50, 90]
            nbins = 11
        elif type == 'Quintiles':
            range_index = range(0, 101, 20)
            highlights = []
            nbins = 6
        elif type == 'Quartiles':
            range_index = range(0, 101, 25)
            highlights = [50]
            nbins = 5
        else:
            range_index = highlights = nbins = '-not-defined-'
        return range_index, highlights, nbins

    def plot_quantiles(self):
        """ Quantile plot. """

        agg = self.drp_agg.currentText()
        axes_dict = self.make_axes_dict(fig=self.fig,
                                        df=self.vars_selected_df)

        # Type
        range_index, highlights, nbins = self.get_type_settings()

        current_colnum = -1
        for col, ax in axes_dict.items():
            current_colnum += 1

            # Separate df for plotting
            coldata_df = pd.DataFrame()
            coldata_df[col] = self.vars_selected_df[col].copy().dropna()
            ylim = [coldata_df[col].min(), coldata_df[col].max()]  # Limits for y-axis from all col data
            import calendar
            if agg == 'Per Month':
                uniques = coldata_df.index.month.unique()
                current_monthnum = -1
                for ix, unique in enumerate(uniques):
                    current_monthnum += 1
                    plot_df = coldata_df.loc[(coldata_df.index.month == unique)]
                    self.make_plot(ax=ax, col=col,
                                   range_index=range_index,
                                   plot_df=plot_df,
                                   highlights=highlights,
                                   nbins=nbins,
                                   ylim=ylim,
                                   color=current_monthnum,
                                   label=calendar.month_name[unique],
                                   current_colnum=current_colnum)
            elif agg == 'Per Year':
                uniques = coldata_df.index.year.unique()
                current_yearnum = -1
                for ix, unique in enumerate(uniques):
                    current_yearnum += 1
                    plot_df = coldata_df.loc[(coldata_df.index.year == unique)]
                    self.make_plot(ax=ax, col=col,
                                   range_index=range_index,
                                   plot_df=plot_df,
                                   highlights=highlights,
                                   nbins=nbins,
                                   ylim=ylim,
                                   color=current_yearnum,
                                   label=unique,
                                   current_colnum=current_colnum)
            elif agg == 'All Data':
                plot_df = coldata_df.copy()
                self.make_plot(ax=ax, col=col,
                               range_index=range_index,
                               plot_df=plot_df,
                               highlights=highlights,
                               nbins=nbins,
                               ylim=ylim,
                               color=current_colnum,
                               label='all data',
                               current_colnum=current_colnum)

        self.fig.canvas.draw()  # update plot
        self.fig.canvas.flush_events()

    def make_plot(self, ax, col, range_index, plot_df, highlights, nbins, ylim, color, label, current_colnum):
        # Data for plotting
        quantile_df = pd.DataFrame(index=range_index)
        quantile_df[col] = np.nan
        for p in range_index:
            quantile_df.loc[p, [col]] = plot_df[col].quantile(p / 100)

        # Plot
        ax.plot(quantile_df.index, quantile_df[col],
                color=self.color_list[color], alpha=0.9, ls='-', lw=theme.WIDTH_LINE_DEFAULT,
                marker='o', markeredgecolor='none', ms=theme.SIZE_MARKER_DEFAULT, zorder=99, label=label)

        # Highlight specific percentiles
        args = dict(alpha=1, edgecolors='black', marker='o', s=40, c='None')
        for h in highlights:
            ax.scatter(h, quantile_df.loc[h, [col]], **args)
            # ax.text(h, quantile_df.loc[h][col], quantile_df.loc[h][col],
            #         horizontalalignment='center', verticalalignment='bottom',
            #         size=FONTSIZE_INFO_DEFAULT, color=COLOR_TXT_LEGEND, backgroundcolor='none')

        # Format

        gui.plotfuncs.default_format(ax=ax, txt_xlabel='percentile', txt_ylabel=f'{col[0]} {col[1]}',
                                     txt_ylabel_units='[#]')
        gui.plotfuncs.default_grid(ax=ax)
        ax.locator_params(axis='x', nbins=nbins)
        ax.set_ylim(ylim[0], ylim[1])

        # Legend for first plot
        if current_colnum == 0:
            gui.plotfuncs.default_legend(ax=ax)

    # ax.plot([50, 50], [quantile_df.loc[0][col], quantile_df.loc[50][col]], ls='-')

    # ax2.axhline(y=50, color=COLOR_LINE_ZERO, ls='--', lw=1, alpha=1, zorder=100)
    # ax2.axhline(y=25, color=COLOR_LINE_ZERO, ls='--', lw=1, alpha=1, zorder=100)
    # ax2.axhline(y=75, color=COLOR_LINE_ZERO, ls='--', lw=1, alpha=1, zorder=100)

    # # from matplotlib.ticker import FormatStrFormatter
    # # ax.xaxis.set_major_formatter(FormatStrFormatter('%.2f'))

    def make_axes_dict(self, fig, df):
        """ Create one ax for histogram variable and one for each selected. """
        gs = gridspec.GridSpec(len(df.columns), 1)  # rows, cols
        gs.update(wspace=0.2, hspace=0.25, left=0.05, right=0.95, top=0.95, bottom=0.05)

        # Delete all axes in figure
        for ax in self.fig.axes:
            self.fig.delaxes(ax)

        # Make new axes
        axes_dict = {}
        for ix, col in enumerate(df):
            ax = fig.add_subplot(gs[ix, 0])
            axes_dict[col] = ax

        # Format
        for col, ax in axes_dict.items():
            gui.plotfuncs.default_format(ax=ax, txt_xlabel=False)

        return axes_dict
