"""

MULTIPANEL PLOTS
----------------


"""
import matplotlib.dates as mdates
import matplotlib.gridspec as gridspec
# matplotlib.use('Qt5Agg')
import pandas as pd
from PyQt5 import QtGui
from PyQt5 import QtWidgets as qw

import gui.base
import pkgs.dfun.frames
import gui
import logger
from .styles import LightTheme as theme


# pd.set_option('display.width', 1000)
# pd.set_option('display.max_columns', 15)
# pd.set_option('display.max_rows', 20)


class addContent(gui.base.buildTab):
    """Build new tab and populate with contents"""

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id, tab_template='SVVP')

        # Tab icon
        self.TabWidget.setTabIcon(self.tab_ix, QtGui.QIcon(app_obj.ctx.tab_icon_plots))

        # Add settings menu contents
        self.ref_drp_agg, self.ref_lne_min_vals, self.ref_drp_freq, \
        self.ref_lne_freq_duration, self.btn_apply = \
            self.add_settings_fields()

        # Add variables required in settings
        self.populate_settings_fields()

        # Add available variables to variable lists
        gui.base.buildTab.populate_variable_list(obj=self)

        # # Add plots
        # self.populate_plot_area()

    def populate_plot_area(self):
        # This is done in Run due to dynamic amount of axes
        pass

    def populate_settings_fields(self):
        pass

    def add_settings_fields(self):
        # Settings
        gui.elements.add_header_in_grid_row(layout=self.sett_layout, txt='Settings', row=0)
        self.sett_layout.setRowStretch(1, 0)  # empty row

        gui.elements.add_header_in_grid_row(layout=self.sett_layout, txt='Resampling', row=2,
                                            css_id='lbl_Header3')

        # Frequency
        ref_lne_freq_duration, ref_drp_freq \
            = gui.elements.grd_LabelLineeditDropdownTriplet(txt='Frequency',
                                                            css_ids=['', 'cyan', 'cyan'],
                                                            layout=self.sett_layout,
                                                            orientation='horiz',
                                                            row=3, col=0)
        ref_lne_freq_duration.setText('1')
        ref_drp_freq.addItem('Original')
        # ref_drp_freq.addItem('Minute(s)')
        ref_drp_freq.addItem('Hourly')
        ref_drp_freq.addItem('Daily')
        ref_drp_freq.addItem('Weekly')
        ref_drp_freq.addItem('Monthly')
        ref_drp_freq.addItem('Yearly')

        # Method
        ref_drp_agg = gui.elements.grd_LabelDropdownPair(txt='Aggregation',
                                                         css_ids=['', 'cyan'],
                                                         layout=self.sett_layout,
                                                         orientation='horiz',
                                                         row=4, col=0,
                                                         colspan=2)
        ref_drp_agg.addItem('Original')
        ref_drp_agg.addItem('Mean')
        ref_drp_agg.addItem('Median')
        ref_drp_agg.addItem('SD')
        ref_drp_agg.addItem('Minimum')
        ref_drp_agg.addItem('Maximum')
        ref_drp_agg.addItem('Count')
        ref_drp_agg.addItem('Sum')
        ref_drp_agg.addItem('Variance')

        # Required Minimum Values for e.g. calculating valid mean value
        ref_lne_min_vals = gui.elements.add_label_linedit_pair_to_grid(txt='Minimum Values',
                                                                       css_ids=['', 'cyan'],
                                                                       layout=self.sett_layout,
                                                                       orientation='horiz',
                                                                       row=5, col=0)
        ref_lne_min_vals.setText('0')

        btn_apply = gui.elements.add_button_to_grid(grid_layout=self.sett_layout,
                                                    txt='Apply',
                                                    css_id='btn_cat_ControlsRun',
                                                    row=6, col=0, rowspan=1, colspan=3)

        self.sett_layout.setRowStretch(7, 2)  # empty row

        return ref_drp_agg, ref_lne_min_vals, ref_drp_freq, ref_lne_freq_duration, btn_apply


class Run(addContent):
    """
    Start
    """
    current_color_ix = -1
    current_marker_ix = -1
    plot_color_list = theme.colors_6()
    plot_marker_list = theme.generate_plot_marker_list()
    sub_outdir = "plot_multipanel"

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id)
        logger.log(name='>>> Starting Multipanel plots', dict={}, highlight=True)  # Log info
        self.selected_vars_lookup_dict = {}
        self.vars_selected_df = pd.DataFrame()
        self.vars_available_df = self.tab_data_df.copy()
        self.sub_outdir = self.project_outdir / self.sub_outdir
        # self.update_btn_status()

    def get_settings_from_fields(self):
        self.agg_method = self.ref_drp_agg.currentText()
        self.min_vals = int(self.ref_lne_min_vals.text())
        self.to_freq_duration = int(self.ref_lne_freq_duration.text())
        self.to_freq = self.ref_drp_freq.currentText()

    def adjust_freq(self):
        # Generate frequency string (for pandas)
        self.to_freq = pkgs.dfun.times.generate_freq_str(to_freq=self.to_freq)
        # Add info about duration to freq_str, e.g. '3T' for mean values over 3 minutes
        if self.to_freq != 'Original':
            self.to_freq_str = '{}{}'.format(self.to_freq_duration, self.to_freq)
        # Resampling
        resampled_df = self.resample_if_needed()
        # self.vars_selected_df = resampled_df
        return resampled_df

    def apply(self):
        # Resample only if there are data
        empty = self.check_if_selected_empty()
        if empty:
            return None
        self.get_settings_from_fields()
        resampled_df = self.adjust_freq()
        self.plot_data(df=resampled_df)

    def resample_if_needed(self):
        # Resample data if needed
        if (self.ref_drp_agg == 'Original') | (self.ref_drp_freq == 'Original'):
            # In case any 'Original' freq_str and agg_method are ignored
            resampled_df = self.vars_selected_df.copy()
        else:
            resampled_df, _ = pkgs.dfun.frames.resample_df(df=self.vars_selected_df,
                                                           freq_str=self.to_freq_str,
                                                           agg_method=self.agg_method,
                                                           min_vals=self.min_vals,
                                                           out_timestamp_convention='Middle of Record',
                                                           to_freq_duration=self.to_freq_duration,
                                                           to_freq=self.to_freq)
        return resampled_df

    def add_var_to_selected(self):
        """ Add feature from available to selected. """
        selected_var = self.lst_varlist_available.selectedIndexes()
        selected_var_ix = selected_var[0].row()
        selected_col = self.col_dict_tuples[selected_var_ix]
        selected_pretty = self.col_list_pretty[selected_var_ix]  # Needs new pretty list (screen names)

        # Add to lookup dict and df
        self.selected_vars_lookup_dict[selected_col] = selected_pretty
        self.vars_selected_df[selected_col] = self.vars_available_df[selected_col]

        self.update_selected_vars_list(list=self.lst_varlist_selected, df=self.vars_selected_df,
                                       lookup_dict=self.selected_vars_lookup_dict)

        self.plot_data(df=self.vars_selected_df)

    def make_axes_dict(self, fig, df):

        gs = gridspec.GridSpec(len(df.columns), 1)  # rows, cols
        gs.update(wspace=0.2, hspace=0.2, left=0.03, right=0.97, top=0.97, bottom=0.03)

        # Delete all axes in figure
        for ax in self.fig.axes:
            self.fig.delaxes(ax)

        # Make new axes
        axes_dict = {}
        first_ax = -9999
        for ix, col in enumerate(df):  # todo df.columns?
            if ix == 0:
                ax = fig.add_subplot(gs[ix, 0])
                first_ax = ax
            else:
                ax = fig.add_subplot(gs[ix, 0], sharex=first_ax)
            axes_dict[col] = ax

        # Format
        for col, ax in axes_dict.items():
            gui.plotfuncs.default_format(ax=ax, txt_xlabel=False)

        return axes_dict

    def plot_data(self, df):
        axes_dict = self.make_axes_dict(fig=self.fig, df=self.vars_selected_df)

        color_list = theme.colorwheel_36()
        current_color = -1
        for col, ax in axes_dict.items():
            current_color += 1
            ax.plot_date(x=df.index, y=df[col],
                         color=color_list[current_color], alpha=1, ls='-',
                         marker='o', markeredgecolor='none', ms=4, zorder=98, label='XXX')
            ax.text(0.01, 0.97, f"{col[0]}",
                    size=theme.FONTSIZE_HEADER_AXIS, color=theme.FONTCOLOR_HEADER_AXIS,
                    backgroundcolor='none', transform=ax.transAxes, alpha=1,
                    horizontalalignment='left', verticalalignment='top', zorder=99)

            gui.plotfuncs.default_grid(ax=ax)

            locator = mdates.AutoDateLocator(minticks=3, maxticks=30)
            formatter = mdates.ConciseDateFormatter(locator)
            ax.xaxis.set_major_locator(locator)
            ax.xaxis.set_major_formatter(formatter)

            self.fig.canvas.draw()
            self.fig.canvas.flush_events()

    def remove_var_from_selected(self):

        selected_var = self.lst_varlist_selected.selectedIndexes()
        selected_var_ix = selected_var[0].row()
        selected_col = list(self.selected_vars_lookup_dict.keys())[selected_var_ix]  # Get key by index
        # selected_pretty = self.selected_features_lookup_dict[selected_col]

        # Remove from lookup dict and df
        del self.selected_vars_lookup_dict[selected_col]
        self.vars_selected_df.drop(selected_col, axis=1, inplace=True)

        self.update_selected_vars_list(list=self.lst_varlist_selected, df=self.vars_selected_df,
                                       lookup_dict=self.selected_vars_lookup_dict)

        empty = self.check_if_selected_empty()

        if empty:
            for ax in self.fig.axes:
                self.fig.delaxes(ax)
                self.fig.canvas.draw()
                self.fig.canvas.flush_events()
        else:
            self.plot_data(df=self.vars_selected_df)

    def update_selected_vars_list(self, list, df, lookup_dict):
        list.clear()
        for col in df.columns:
            item = qw.QListWidgetItem(lookup_dict[col])
            list.addItem(item)

    def check_if_selected_empty(self):
        _list = self.lst_varlist_selected
        if self.vars_selected_df.empty:
            _list.addItem('-empty-')
            empty = True
        else:
            empty = False
        return empty

    def update_available_vars_list(self, list, df):
        list.clear()
        list_cols = []
        # Add all variables to drop-down lists to allow full flexibility yay
        for ix, colname_tuple in enumerate(df.columns):
            item = qw.QListWidgetItem(self.col_list_pretty[ix])
            list.addItem(item)  # add column name to list
            list_cols.append(colname_tuple)
        return list, list_cols

    def set_marker_style(self):
        self.current_color_ix, self.current_marker_ix = \
            gui.plotfuncs.set_marker_color(current_color_ix=self.current_color_ix,
                                           plot_color_list=self.plot_color_list,
                                           current_marker_ix=self.current_marker_ix,
                                           plot_marker_list=self.plot_marker_list)
