"""

DIEL CYCLES
-----------

"""
import calendar
import datetime as dt

import pandas as pd
import plotly.graph_objects as go
from PyQt5 import QtGui
from PyQt5 import QtWidgets as qw
# matplotlib.use('Qt5Agg')
from plotly.subplots import make_subplots

import gui.base
import gui.elements
import gui.plotfuncs
import gui.plotfuncs
import logger
from gui import elements, tabs
from modboxes.plots.styles.LightTheme import *


class addContent(gui.base.buildTab):
    """Build new tab and populate with contents"""

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id, tab_template='SVonVF')

        # Tab icon
        self.TabWidget.setTabIcon(self.tab_ix, QtGui.QIcon(app_obj.ctx.tab_icon_plots))

        # Add settings menu contents
        self.drp_agg, self.drp_display, self.btn_show_in_plot = \
            self.add_settings_fields()

        # Add available variables to variable lists
        gui.base.buildTab.populate_variable_list(obj=self)

        # # Add plots
        # self.axes_dict = self.populate_plot_area()

    def populate_plot_area(self):
        # This is done in Run due to dynamic amount of axes
        pass

    def populate_settings_fields(self):
        pass

    def add_settings_fields(self):
        # Settings
        gui.elements.add_header_to_grid_top(layout=self.sett_layout, txt='Settings')

        # Time scale
        drp_agg = \
            elements.grd_LabelDropdownPair_dropEnabled(txt='Aggregation',
                                                       css_ids=['', ''],
                                                       layout=self.sett_layout,
                                                       row=1, col=0,
                                                       orientation='horiz')
        drp_agg.addItems(['Per Month'])

        # Display
        drp_display = \
            elements.grd_LabelDropdownPair_dropEnabled(txt='Display',
                                                       css_ids=['', ''],
                                                       layout=self.sett_layout,
                                                       row=2, col=0,
                                                       orientation='horiz')
        drp_display.addItems(['All In One Plot', 'Next To Each Other'])

        btn_show_in_plot = elements.add_button_to_grid(grid_layout=self.sett_layout,
                                                       txt='Show in Plot', css_id='',
                                                       row=3, col=0, rowspan=1, colspan=2)

        self.sett_layout.setRowStretch(4, 1)

        return drp_agg, drp_display, btn_show_in_plot


class Run(addContent):
    color_list = colorwheel_36()  # get some colors
    sub_outdir = "plot_diel_cycles"

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id)
        logger.log(name='>>> Starting Diel Cycle plots', dict={}, highlight=True)  # Log info
        self.selected_vars_lookup_dict = {}
        self.vars_selected_df = pd.DataFrame()
        self.vars_available_df = self.tab_data_df.copy()
        self.sub_outdir = self.project_outdir / self.sub_outdir
        self.webview = self.add_webview(to_layout=self.lyt_plot_area)

    def add_var_to_selected(self):
        """ Add feature from available to selected. """
        selected_var = self.lst_varlist_available.selectedIndexes()
        selected_var_ix = selected_var[0].row()
        selected_col = self.col_dict_tuples[selected_var_ix]
        selected_pretty = self.col_list_pretty[selected_var_ix]  # Needs new pretty list (screen names)

        # Add to lookup dict and df
        self.selected_vars_lookup_dict[selected_col] = selected_pretty
        self.vars_selected_df[selected_col] = self.vars_available_df[selected_col]

        self.update_selected_vars_list(list=self.lst_varlist_selected, df=self.vars_selected_df,
                                       lookup_dict=self.selected_vars_lookup_dict)

        self.plot_diel_cycles()

    def remove_var_from_selected(self):
        selected_var = self.lst_varlist_selected.selectedIndexes()
        selected_var_ix = selected_var[0].row()
        selected_col = list(self.selected_vars_lookup_dict.keys())[selected_var_ix]  # Get key by index
        # selected_pretty = self.selected_features_lookup_dict[selected_col]

        # Remove from lookup dict and df
        del self.selected_vars_lookup_dict[selected_col]
        self.vars_selected_df.drop(selected_col, axis=1, inplace=True)

        self.update_selected_vars_list(list=self.lst_varlist_selected, df=self.vars_selected_df,
                                       lookup_dict=self.selected_vars_lookup_dict)

        empty = self.check_if_selected_empty()

        if empty:
            pass
        else:
            self.plot_diel_cycles()

    def check_if_selected_empty(self):
        _list = self.lst_varlist_selected
        if self.vars_selected_df.empty:
            _list.addItem('-empty-')
            empty = True
        else:
            empty = False
        return empty

    def get_settings_from_fields(self):
        self.agg = self.drp_agg.currentText()

    def add_artificial_date(self, df, month):
        """Add artificial date to time"""
        _fake_date_col = ('_fake_date', '[aux]', '[aux]')
        _fake_datetime_col = ('_fake_datetime', '[aux]', '[aux]')
        _time_col = ('_time', '[aux]', '[aux]')

        # True time in separate column
        df[_time_col] = df.index
        df[_time_col] = df[_time_col].astype(str)
        df[_time_col] = pd.to_timedelta(df[_time_col])

        # Create fake date with month as day, makes plotting on time axis easy
        df[_fake_date_col] = dt.datetime(1900, 1, month)

        # Combine fake date and true time to datetime
        df[_fake_datetime_col] = pd.to_datetime(df[_fake_date_col] + df[_time_col])

        # Remove unneeded cols
        df.drop([_time_col, _fake_date_col], axis=1, inplace=True)

        # Set index to created datetime
        df.set_index(_fake_datetime_col, inplace=True, drop=True)
        return df

    def unique_vars(self, df):
        unique_vars = list(zip(df.columns.get_level_values(0),
                               df.columns.get_level_values(1)))
        return set(unique_vars)

    def aggregate(self):
        """Calculate diel cycles for each month and collect in new df"""

        df = self.vars_selected_df.copy()
        allmonths_df = pd.DataFrame()

        grouped = df.groupby(df.index.month)
        for month, month_df in grouped:
            month_str = calendar.month_abbr[month]
            time_df = month_df.groupby(month_df.index.time).agg(['count', 'mean', 'std', 'median', self.q95, self.q05])
            time_df = self.add_artificial_date(df=time_df, month=month)
            vars = self.unique_vars(df=time_df)

            for var in vars:
                time_df[(var[0], var[1], 'mean+std')] = time_df[var]['mean'] + time_df[var]['std']
                time_df[(var[0], var[1], 'mean-std')] = time_df[var]['mean'] - time_df[var]['std']

            allmonths_df = allmonths_df.append(time_df)
        return allmonths_df

    def plot_diel_cycles(self):
        """
        Diel cycle plots

        kudos:
        https://stackoverflow.com/questions/64307402/plotly-how-to-plot-a-range-with-a-line-in-the-center-using-a-datetime-index

        """

        # Get settings
        self.get_settings_from_fields()
        # num_vars = len(self.vars_selected_df.columns)

        # Aggregate, diel cycles for each month
        allmonths_df = self.aggregate()
        selected_vars = self.unique_vars(df=allmonths_df)
        num_vars = len(selected_vars)

        fig = make_subplots(rows=num_vars, cols=1,
                            shared_xaxes=True,
                            vertical_spacing=0.02)

        for ix, col in enumerate(selected_vars):
            subplot_row = ix + 1
            cur_var = f"{col[0]}"
            cur_units = f"{col[1]}"
            # series = self.vars_selected_df[col]
            # grouped = series.groupby(series.index.month)
            subset_var_df = allmonths_df[col].copy()

            # _filter = subset_var_df.index.day == 6
            # subset_var_df = subset_var_df[_filter]

            grouped = subset_var_df.groupby(subset_var_df.index.day)
            for month_as_day, month_as_day_df in grouped:
                month_str = calendar.month_abbr[month_as_day]
                x = month_as_day_df.index
                y1_upper = [y for y in month_as_day_df['mean+std']]
                y1_lower = [y for y in month_as_day_df['mean-std']]
                y1_lower = y1_lower[::-1]

                fig.add_trace(go.Scatter(x=x, y=month_as_day_df['mean'],
                                         name=f"{month_str} {cur_var} mean",
                                         line=dict(width=2)),
                              row=subplot_row, col=1)

                fig.add_trace(go.Scatter(x=x, y=month_as_day_df['mean+std'],
                                         name=f"{month_str} {cur_var} mean+std",
                                         line=dict(width=2)),
                              row=subplot_row, col=1)

                fig.add_trace(go.Scatter(x=x.append(x[::-1]), y=y1_upper + y1_lower,
                                         fill='tozerox', fillcolor='rgba(0,100,80,0.05)',
                                         line=dict(width=0), showlegend=True,
                                         name=f"{month_str} {cur_var} std", hoverinfo='skip',
                                         ),
                              row=subplot_row, col=1)

                # fig.update_xaxes(type='date', tickformat='%H:%M', row=subplot_row, col=1)
                fig.update_yaxes(title_text=f"{cur_var} {cur_units}", row=subplot_row, col=1)

        args = dict(showline=True, showgrid=True, linecolor='black',
                    linewidth=1, ticks='inside', mirror=True)
        fig.update_xaxes(args)
        fig.update_yaxes(args, zeroline=True, zerolinewidth=1, zerolinecolor='black')
        fig.update_layout(title='Diel Cycles', plot_bgcolor='white', autosize=True)
        self.webview.setHtml(fig.to_html(include_plotlyjs='cdn'))

    def q95(self, x):
        return x.quantile(0.95)

    def q05(self, x):
        return x.quantile(0.05)

    def resample(self, series):
        resampled_series_df = series \
            .groupby(series.index.month) \
            .groupby(series.index.time) \
            .agg(['mean', 'median', self.q95, self.q05])
        return resampled_series_df

    def loop_uniques(self, uniques, color_list, cumulative_for, x_units, df, col, ax):
        ymin = False
        ymax = False
        for ix, unique in enumerate(uniques):

            if cumulative_for == 'Per Month':
                subset_unique_df = df.loc[(df.index.month == unique)]
            elif cumulative_for == 'Per Year':
                subset_unique_df = df.loc[(df.index.year == unique)]
            else:
                subset_unique_df = df.loc[(df.index.year == unique)]

            subset_unique_df = subset_unique_df.cumsum()
            y = subset_unique_df[col]
            ymin = y.min() if y.min() < ymin else ymin
            ymax = y.max() if y.max() > ymax else ymax

            if cumulative_for == 'Per Month':
                x = y.index.date
            elif cumulative_for == 'Per Year':
                total_hours_day_fraction = (y.index.hour + (y.index.minute / 60) + (y.index.second / 3600)) / 24
                x = y.index.dayofyear + total_hours_day_fraction
            else:
                x = y.index.dayofyear

            # x = y.index + pd.DateOffset(month=1)  # set month to 1

            legend_txt = f"{col[0]}, {unique}, cumulative = {y[-1]:.3f}"

            # if self.drp_show_in_same_plot.currentText() == 'Yes':
            #     self.plot(x=x, y=y, dates=False, legend_txt=legend_txt, ax=ax,
            #               line_color=color_list[ix], x_units=x_units, col=col, ylim=[ymin, ymax])
            # else:
            self.plot(x=x, y=y, dates=False, legend_txt=legend_txt, ax=ax,
                      line_color=color_list[ix], x_units=x_units, col=col, ylim=[ymin, ymax])

    def update_selected_vars_list(self, list, df, lookup_dict):
        list.clear()
        for col in df.columns:
            item = qw.QListWidgetItem(lookup_dict[col])
            list.addItem(item)

    def update_available_vars_list(self, list, df):
        list.clear()
        list_cols = []
        # Add all variables to drop-down lists to allow full flexibility yay
        for ix, colname_tuple in enumerate(df.columns):
            item = qw.QListWidgetItem(self.col_list_pretty[ix])
            list.addItem(item)  # add column name to list
            list_cols.append(colname_tuple)
        return list, list_cols
