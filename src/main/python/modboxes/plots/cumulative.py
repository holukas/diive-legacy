"""

CUMULATIVE PLOTS
----------------


By default, creates cumulative plots per year for the selected variable.
Cumulative plots per months or continuously (all data cumulative) can be
created on demand.

"""
import matplotlib.gridspec as gridspec
import pandas as pd
from PyQt5 import QtGui
from PyQt5 import QtWidgets as qw

import gui
import gui.base
import logger
from .styles import LightTheme as theme


class addContent(gui.base.buildTab):
    """Build new tab and populate with contents"""

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id, tab_template='SVonVP')

        # Tab icon
        self.TabWidget.setTabIcon(self.tab_ix, QtGui.QIcon(app_obj.ctx.tab_icon_plots))

        # Add settings menu contents
        self.drp_cumulative_method, self.btn_show_in_plot, self.drp_show_in_same_plot = \
            self.add_settings_fields()

        # Add available variables to variable lists
        gui.base.buildTab.populate_variable_list(obj=self)

        # # Add plots
        # self.axes_dict = self.populate_plot_area()

    def populate_plot_area(self):
        # This is done in Run due to dynamic amount of axes
        pass

    def populate_settings_fields(self):
        pass

    def add_settings_fields(self):
        # Settings
        gui.elements.add_header_to_grid_top(layout=self.sett_layout, txt='Settings')

        # Time scale
        drp_cumulative_method = \
            gui.elements.grd_LabelDropdownPair_dropEnabled(txt='Cumulative Method',
                                                           css_ids=['', ''],
                                                           layout=self.sett_layout,
                                                           row=1, col=0,
                                                           orientation='horiz')
        drp_cumulative_method.addItems(['Per Year', 'Continuous', 'Per Month'])

        drp_show_in_same_plot = \
            gui.elements.grd_LabelDropdownPair_dropEnabled(txt='Show In Same Plot',
                                                           css_ids=['', ''],
                                                           layout=self.sett_layout,
                                                           row=2, col=0,
                                                           orientation='horiz')
        drp_show_in_same_plot.addItems(['Yes', 'No'])

        btn_show_in_plot = gui.elements.add_button_to_grid(grid_layout=self.sett_layout,
                                                           txt='Show in Plot', css_id='',
                                                           row=3, col=0, rowspan=1, colspan=2)

        self.sett_layout.setRowStretch(4, 1)

        return drp_cumulative_method, btn_show_in_plot, drp_show_in_same_plot


class Run(addContent):
    color_list = theme.colorwheel_36()  # get some colors
    sub_outdir = "plot_cumulative"

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id)
        logger.log(name='>>> Starting Cumulative plots', dict={}, highlight=True)  # Log info
        self.selected_vars_lookup_dict = {}
        self.vars_selected_df = pd.DataFrame()
        self.vars_available_df = self.tab_data_df.copy()
        self.sub_outdir = self.project_outdir / self.sub_outdir

    def add_var_to_selected(self):
        """ Add feature from available to selected. """
        selected_var = self.lst_varlist_available.selectedIndexes()
        selected_var_ix = selected_var[0].row()
        selected_col = self.col_dict_tuples[selected_var_ix]
        selected_pretty = self.col_list_pretty[selected_var_ix]  # Needs new pretty list (screen names)

        # Add to lookup dict and df
        self.selected_vars_lookup_dict[selected_col] = selected_pretty
        self.vars_selected_df[selected_col] = self.vars_available_df[selected_col]

        self.update_selected_vars_list(list=self.lst_varlist_selected, df=self.vars_selected_df,
                                       lookup_dict=self.selected_vars_lookup_dict)

        self.plot_cumulative_line()

    def remove_var_from_selected(self):

        selected_var = self.lst_varlist_selected.selectedIndexes()
        selected_var_ix = selected_var[0].row()
        selected_col = list(self.selected_vars_lookup_dict.keys())[selected_var_ix]  # Get key by index
        # selected_pretty = self.selected_features_lookup_dict[selected_col]

        # Remove from lookup dict and df
        del self.selected_vars_lookup_dict[selected_col]
        self.vars_selected_df.drop(selected_col, axis=1, inplace=True)

        self.update_selected_vars_list(list=self.lst_varlist_selected, df=self.vars_selected_df,
                                       lookup_dict=self.selected_vars_lookup_dict)

        empty = self.check_if_selected_empty()

        if empty:
            for ax in self.fig.axes:
                self.fig.delaxes(ax)
                self.fig.canvas.draw()
                self.fig.canvas.flush_events()
        else:
            self.plot_cumulative_line()

    def update_selected_vars_list(self, list, df, lookup_dict):
        list.clear()
        for col in df.columns:
            item = qw.QListWidgetItem(lookup_dict[col])
            list.addItem(item)

    def update_available_vars_list(self, list, df):
        list.clear()
        list_cols = []
        # Add all variables to drop-down lists to allow full flexibility yay
        for ix, colname_tuple in enumerate(df.columns):
            item = qw.QListWidgetItem(self.col_list_pretty[ix])
            list.addItem(item)  # add column name to list
            list_cols.append(colname_tuple)
        return list, list_cols

    def check_if_selected_empty(self):
        _list = self.lst_varlist_selected
        if self.vars_selected_df.empty:
            _list.addItem('-empty-')
            empty = True
        else:
            empty = False
        return empty

    def plot_cumulative_line(self):
        """ Cumulative plot. """

        # Get settings
        cumulative_for = self.drp_cumulative_method.currentText()

        axes_dict = self.make_axes_dict(fig=self.fig, df=self.vars_selected_df)
        current_color = -1

        for col, ax in axes_dict.items():

            if cumulative_for == 'Continuous':
                current_color += 1
                y = self.vars_selected_df[col].cumsum()
                x = y.index
                legend_txt = f"{col[0]} all available data, cumulative = {y[-1]:.3f}"
                self.plot(x=x, y=y, dates=True, legend_txt=legend_txt, ax=ax,
                          line_color=self.color_list[current_color], x_units='datetime',
                          col=col, ylim=[y.min(), y.max()])

            elif cumulative_for != 'Continuous':

                if cumulative_for == 'Per Month':
                    uniques = self.vars_selected_df.index.month.unique()  # unique months
                    self.loop_uniques(uniques=uniques, color_list=self.color_list,
                                      cumulative_for=cumulative_for, x_units='datetime',
                                      df=self.vars_selected_df, col=col, ax=ax)

                elif cumulative_for == 'Per Year':
                    uniques = self.vars_selected_df.index.year.unique()  # unique years
                    self.loop_uniques(uniques=uniques, color_list=self.color_list,
                                      cumulative_for=cumulative_for, x_units='doy',
                                      df=self.vars_selected_df, col=col, ax=ax)

                else:
                    uniques = self.vars_selected_df.index.year.unique()  # default: unique years
                    self.loop_uniques(uniques=uniques, color_list=self.color_list,
                                      cumulative_for=cumulative_for, x_units='doy',
                                      df=self.vars_selected_df, col=col, ax=ax)

    def loop_uniques(self, uniques, color_list, cumulative_for, x_units, df, col, ax):
        ymin = False
        ymax = False
        for ix, unique in enumerate(uniques):

            if cumulative_for == 'Per Month':
                subset_unique_df = df.loc[(df.index.month == unique)]
            elif cumulative_for == 'Per Year':
                subset_unique_df = df.loc[(df.index.year == unique)]
            else:
                subset_unique_df = df.loc[(df.index.year == unique)]

            subset_unique_df = subset_unique_df.cumsum()
            y = subset_unique_df[col]
            ymin = y.min() if y.min() < ymin else ymin
            ymax = y.max() if y.max() > ymax else ymax

            if cumulative_for == 'Per Month':
                x = y.index.date
            elif cumulative_for == 'Per Year':
                total_hours_day_fraction = (y.index.hour + (y.index.minute / 60) + (y.index.second / 3600)) / 24
                x = y.index.dayofyear + total_hours_day_fraction
            else:
                x = y.index.dayofyear

            # x = y.index + pd.DateOffset(month=1)  # set month to 1

            legend_txt = f"{col[0]}, {unique}, cumulative = {y[-1]:.3f}"

            # if self.drp_show_in_same_plot.currentText() == 'Yes':
            #     self.plot(x=x, y=y, dates=False, legend_txt=legend_txt, ax=ax,
            #               line_color=color_list[ix], x_units=x_units, col=col, ylim=[ymin, ymax])
            # else:
            self.plot(x=x, y=y, dates=False, legend_txt=legend_txt, ax=ax,
                      line_color=color_list[ix], x_units=x_units, col=col, ylim=[ymin, ymax])

    def plot(self, x, y, dates, legend_txt, line_color, x_units, col, ax, ylim):

        xlabel = 'Date'
        if x_units == 'datetime':
            ax.plot_date(x=x, y=y,
                         color=line_color, alpha=0.9, ls='-', lw=theme.WIDTH_LINE_DEFAULT,
                         marker='', markeredgecolor='none', ms=0, zorder=99, label=legend_txt)
        elif x_units == 'doy':
            ax.plot(x, y,
                    color=line_color, alpha=0.9, ls='-', lw=theme.WIDTH_LINE_DEFAULT,
                    marker='', markeredgecolor='none', ms=0, zorder=99, label=legend_txt)
            xlabel = 'Day of year'

        ax.text(0.01, 0.97, f"{col[0]}",
                size=theme.FONTSIZE_HEADER_AXIS_LARGE, color=theme.FONTCOLOR_HEADER_AXIS,
                backgroundcolor='none', transform=ax.transAxes, alpha=1,
                horizontalalignment='left', verticalalignment='top', zorder=99)

        gui.plotfuncs.default_format(ax=ax, txt_xlabel=xlabel, txt_ylabel=col[0], txt_ylabel_units=col[1],
                                     label_color=theme.COLOR_LINE_CUMULATIVE, fontsize=theme.FONTSIZE_LABELS_AXIS)

        gui.plotfuncs.default_grid(ax=ax)

        gui.plotfuncs.default_legend(ax=ax, loc='upper right', bbox_to_anchor=(1, 1), labelspacing=0.2)

        ax.set_ylim(ylim[0], ylim[1])

        if (ylim[0] < 0) & (ylim[1] > 0):
            # ax.axhline(y=0, color=COLOR_LINE_ZERO, ls='-', lw=1, alpha=1, zorder=100)  # light green 500
            ax.axhline(y=0, color=theme.COLOR_LINE_ZERO, ls='-', lw=theme.WIDTH_LINE_ZERO, alpha=1,
                       zorder=100)  # light green 500

        if dates:
            gui.plotfuncs.nice_date_ticks(ax=ax, minticks=3, maxticks=30, which='x')

        self.fig.canvas.draw()  # update plot
        self.fig.canvas.flush_events()

    def make_axes_dict(self, fig, df):

        # Delete all axes in figure
        for ax in self.fig.axes:
            self.fig.delaxes(ax)

        if self.drp_show_in_same_plot.currentText() == 'Yes':
            gs = gridspec.GridSpec(1, 1)  # rows, cols
        else:
            gs = gridspec.GridSpec(len(df.columns), 1)  # rows, cols
        gs.update(wspace=0.2, hspace=0.25, left=0.06, right=0.97, top=0.97, bottom=0.03)

        # Make new axes
        axes_dict = {}
        first_ax = -9999
        for ix, col in enumerate(df):  # todo df.columns?
            if ix == 0:
                ax = fig.add_subplot(gs[ix, 0])
                first_ax = ax
            else:
                if self.drp_show_in_same_plot.currentText() == 'Yes':
                    ax = first_ax
                else:
                    ax = fig.add_subplot(gs[ix, 0], sharex=first_ax)
            axes_dict[col] = ax

        # Format
        for col, ax in axes_dict.items():
            gui.plotfuncs.default_format(ax=ax, txt_xlabel=False)

        return axes_dict
