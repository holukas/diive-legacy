import matplotlib.colors as col
import matplotlib.gridspec as gridspec
# matplotlib.use('Qt5Agg')
import numpy as np
import pandas as pd
from PyQt5 import QtGui
from PyQt5 import QtWidgets as qw

import gui
import gui.base
from .styles import LightTheme as theme


class addContent(gui.base.buildTab):
    """Build new tab and populate with contents"""

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id, tab_template='SP')

        # Tab icon
        self.TabWidget.setTabIcon(self.tab_ix, QtGui.QIcon(app_obj.ctx.tab_icon_plots))

        # Add settings menu contents
        self.drp_x, self.drp_y, self.drp_z, self.drp_z_agg, self.lne_min_vals_per_bin, self.lne_number_of_bins, \
        self.drp_show_z_vals, self.btn_show_in_plot = \
            self.add_settings_fields()

        # Add variables required in settings
        self.populate_settings_fields()

        # Add plots
        self.axes_dict = self.populate_plot_area()

    def populate_plot_area(self):
        return self.add_axes()

    def populate_settings_fields(self):
        # PLOTS SCATTER: update the refinement, all vars are needed in the drop-down menu
        self.drp_x.clear()
        self.drp_y.clear()
        self.drp_z.clear()

        for ix, col in enumerate(self.col_list_pretty):
            self.drp_x.addItem(self.col_list_pretty[ix])
            self.drp_y.addItem(self.col_list_pretty[ix])
            self.drp_z.addItem(self.col_list_pretty[ix])

    def add_axes(self):
        gs = gridspec.GridSpec(1, 25)  # rows, cols
        gs.update(wspace=0.2, hspace=0.2, left=0.03, right=0.96, top=0.96, bottom=0.03)
        ax_main = self.fig.add_subplot(gs[0, 0:24])
        ax_cbar = self.fig.add_subplot(gs[0, 24])
        axes_dict = {'ax_main': ax_main, 'ax_cbar': ax_cbar}
        for key, ax in axes_dict.items():
            gui.plotfuncs.default_format(ax=ax, txt_xlabel=False)
        return axes_dict

    def add_settings_fields(self):
        # Select data
        gui.elements.add_header_in_grid_row(layout=self.sett_layout, txt='Select Data', row=0)

        drp_x = gui.elements.grd_LabelDropdownPair_dropEnabled(txt='X',
                                                               css_ids=['', ''],
                                                               layout=self.sett_layout,
                                                               row=1, col=0,
                                                               orientation='horiz')

        drp_y = gui.elements.grd_LabelDropdownPair(txt='Y',
                                                   css_ids=['', ''],
                                                   layout=self.sett_layout,
                                                   row=2, col=0,
                                                   orientation='horiz')

        drp_z = gui.elements.grd_LabelDropdownPair(txt='Z',
                                                   css_ids=['', ''],
                                                   layout=self.sett_layout,
                                                   row=3, col=0,
                                                   orientation='horiz')

        self.sett_layout.addWidget(qw.QLabel(), 4, 0, 1, 1)  # spacer

        # Aggregation
        gui.elements.add_header_in_grid_row(layout=self.sett_layout, txt='Aggregation', row=5)

        drp_z_agg = gui.elements.grd_LabelDropdownPair(txt='Z Aggregation',
                                                       css_ids=['', ''],
                                                       layout=self.sett_layout,
                                                       row=6, col=0,
                                                       orientation='horiz')
        drp_z_agg.addItems(['median', 'mean', 'min', 'max', 'std', 'count', 'sum'])

        lne_min_vals_per_bin = \
            gui.elements.add_label_linedit_pair_to_grid(txt='Min Values Per Bin >',
                                                        css_ids=['', 'cyan'], layout=self.sett_layout,
                                                        row=7, col=0, orientation='horiz')
        lne_min_vals_per_bin.setText('0')

        lne_number_of_bins = \
            gui.elements.add_label_linedit_pair_to_grid(txt='Number Of Bins',
                                                        css_ids=['', 'cyan'], layout=self.sett_layout,
                                                        row=8, col=0, orientation='horiz')
        lne_number_of_bins.setText('20')

        self.sett_layout.addWidget(qw.QLabel(), 9, 0, 1, 1)  # spacer

        # Display
        gui.elements.add_header_in_grid_row(layout=self.sett_layout, txt='Display', row=10)

        drp_show_z_vals = gui.elements.grd_LabelDropdownPair(txt='Show z Values As Text',
                                                             css_ids=['', ''],
                                                             layout=self.sett_layout,
                                                             row=11, col=0,
                                                             orientation='horiz')
        drp_show_z_vals.addItems(['Yes', 'No'])

        self.sett_layout.addWidget(qw.QLabel(), 12, 0, 1, 1)  # spacer

        btn_show_in_plot = gui.elements.add_button_to_grid(grid_layout=self.sett_layout,
                                                           txt='Show in Plot', css_id='',
                                                           row=13, col=0, rowspan=1, colspan=2)

        self.sett_layout.setRowStretch(14, 1)

        return drp_x, drp_y, drp_z, drp_z_agg, lne_min_vals_per_bin, lne_number_of_bins, \
               drp_show_z_vals, btn_show_in_plot


class Run(addContent):
    sub_outdir = "plot_hexbins"

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id)
        self.sub_outdir = self.project_outdir / self.sub_outdir
        # self.update_btn_status()

    def make_hexbins_df(self):
        # Get data
        x_ix = self.drp_x.currentIndex()
        y_ix = self.drp_y.currentIndex()
        z_ix = self.drp_z.currentIndex()
        x_col = self.col_dict_tuples[x_ix]
        y_col = self.col_dict_tuples[y_ix]
        z_col = self.col_dict_tuples[z_ix]

        # Drop duplicate column names from df
        # Necessary in case e.g. x is the same as y
        plot_list = [x_col, y_col, z_col]
        plot_list_no_duplicates = []
        for ix, x in enumerate(plot_list):
            if plot_list[ix] in plot_list_no_duplicates:
                pass
            else:
                plot_list_no_duplicates.append(plot_list[ix])
        hexbins_df = self.tab_data_df[plot_list_no_duplicates]
        # # Elegant solution to check for duplicates in column index, but slow
        # # https://stackoverflow.com/questions/54373153/removing-duplicate-columns-from-a-pandas-dataframe
        # plot_df = plot_df.T.drop_duplicates().T
        hexbins_df = hexbins_df.dropna()
        return hexbins_df, x_col, y_col, z_col

    def agg_method(self):
        reduce_C_function = {
            'mean': np.mean,
            'median': np.median,
            'sum': np.sum,
            'std': np.std,
            'min': np.min,
            'max': np.max,
            'count': np.count_nonzero
        }
        z_agg = self.drp_z_agg.currentText()
        func = reduce_C_function[z_agg]
        return func

    def plot_hexbins(self):
        # Get selected data
        hexbins_df, x_col, y_col, z_col = self.make_hexbins_df()

        # Get settings
        show_z_vals = self.drp_show_z_vals.currentText()
        min_vals_per_bin = int(self.lne_min_vals_per_bin.text())
        number_of_bins = int(self.lne_number_of_bins.text())

        ax = self.axes_dict['ax_main']
        ax_cbar = self.axes_dict['ax_cbar']
        ax.clear()
        ax_cbar.clear()

        try:
            cmap = self.create_cmap()

            # vmin = vmin, vmax = vmax, extent=extent_norm,
            ax_handle = ax.hexbin(hexbins_df[x_col], hexbins_df[y_col],
                                  C=hexbins_df[z_col],
                                  gridsize=number_of_bins,
                                  reduce_C_function=self.agg_method(),
                                  mincnt=min_vals_per_bin,
                                  cmap=cmap,
                                  edgecolors='none',
                                  linewidths=0.1,
                                  zorder=0)

            xy_bin_coords, z_values = self.get_bin_vals(ax_handle=ax_handle)

            if show_z_vals == 'Yes':
                self.put_txt_in_bins(ax=ax, xy_bin_coords=xy_bin_coords, z_values=z_values)

            bins_df = pd.DataFrame.from_records(xy_bin_coords)
            bins_df.rename(columns={0: 'x', 1: 'y'}, inplace=True)
            bins_df['hexbin_value'] = pd.DataFrame(z_values)

            # ax.set_xlim(bins_df['x'].min(), bins_df['x'].max())
            # ax.set_ylim(bins_df['y'].min(), bins_df['y'].max())

            cbar1 = self.fig.colorbar(ax_handle, cmap=cmap, cax=ax_cbar)  # Add colorbar to axis
            cbar1.outline.set_visible(False)

            gui.plotfuncs.default_format(ax=ax, txt_xlabel=x_col[0], txt_ylabel=y_col[0], txt_ylabel_units=y_col[1],
                                         label_color='#eab839', fontsize=theme.FONTSIZE_LABELS_AXIS)

            # self.format_legend(ax=ax)

            self.fig.canvas.draw()
            self.fig.canvas.flush_events()

        except:
            pass

    def create_cmap(self):
        # cpool = ['#5559a3', '#7c9ac0', '#cdcccb', '#f8df9d', '#ee825f']  # MIXES
        # cmap = col.ListedColormap(cpool, 'indexed')
        # # cm.register_cmap('cmap_abs')

        color1 = '#5559a3'  # blue
        color2 = '#5559a3'  # blue
        color3 = '#668cbd'
        color4 = '#CCCCCC'  # neutral grey
        color5 = '#fbe099'
        color6 = '#ef835f'
        color7 = '#b42f4c'
        cmap = col.LinearSegmentedColormap.from_list('better_coolwarm',
                                                     [color1, color2, color3, color4, color5, color6, color7])
        return cmap

    def get_bin_vals(self, ax_handle):
        # gets values for each hexbin
        # http://stackoverflow.com/questions/12951065/get-bins-coordinates-with-hexbin-in-matplotlib
        xy_bin_coords = ax_handle.get_offsets()  # Contains x and y coordinates
        z_values = ax_handle.get_array()
        return xy_bin_coords, z_values

    def put_txt_in_bins(self, ax, xy_bin_coords, z_values):
        for x in range(xy_bin_coords.shape[0]):  # todo plot dots in shapes to mark deposition?
            binx, biny = xy_bin_coords[x][0], xy_bin_coords[x][1]
            ax.text(binx, biny, '{:.1f}'.format(z_values[x]),
                    size=theme.FONTSIZE_INFO_DEFAULT, color='k', backgroundcolor='none', alpha=1,
                    horizontalalignment='center', verticalalignment='center')
        return None
