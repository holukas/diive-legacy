# matplotlib.use('Qt5Agg')
import copy

import matplotlib.gridspec as gridspec
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from pandas.plotting import register_matplotlib_converters

import gui.add_to_layout
import gui.base
import gui.make

pd.plotting.register_matplotlib_converters()  # Needed for time plotting

import logger
from PyQt5 import QtGui
import gui
from .styles import LightTheme as theme


class addContent(gui.base.buildTab):
    """Build new tab and populate with contents"""

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id, tab_template='SVP')

        # Tab icon
        self.TabWidget.setTabIcon(self.tab_ix, QtGui.QIcon(app_obj.ctx.tab_icon_plots))

        # Add settings menu contents
        self.drp_display_type, self.drp_cmap, self.drp_nan_color, self.drp_digits_after_comma, \
        self.btn_save_triplet_png, self.btn_save_borderless = \
            self.add_settings_fields()

        # Add available variables to variable lists
        gui.base.buildTab.populate_variable_list(obj=self)

        # Add plots
        self.axes_dict = self.populate_plot_area()

    def populate_plot_area(self):
        return self.add_axes()

    def add_axes(self):
        # Setup grid for multiple axes
        gs = gridspec.GridSpec(1, 3)  # rows, cols
        gs.update(wspace=0.3, hspace=0.3, left=0.03, right=0.97, top=0.97, bottom=0.03)

        # 3 axes w/ different percentile limits
        ax_100 = self.fig.add_subplot(gs[0, 0])
        ax_05_95 = self.fig.add_subplot(gs[0, 1])
        ax_25_75 = self.fig.add_subplot(gs[0, 2])
        axes_dict = {'ax_100': ax_100, 'ax_05_95': ax_05_95, 'ax_25_75': ax_25_75}
        return axes_dict

    def populate_settings_fields(self):
        pass
        # # Add all variables to drop-down lists to allow full flexibility yay
        # self.drp_xaxis.clear()
        # self.drp_yaxis.clear()
        # for ix, colname_tuple in enumerate(self.tab_data_df.columns):
        #     self.drp_xaxis.addItem(self.col_list_pretty[ix])
        #     self.drp_yaxis.addItem(self.col_list_pretty[ix])

    def add_settings_fields(self):
        gui.elements.add_header_subheader_to_grid_top(
            row=0, col=0, rowspan=1, colspan=3,
            txt=["Heatmap", "plots"], layout=self.sett_layout, )
        gui.add_to_layout.add_spacer_item_to_grid(layout=self.sett_layout, row=1, col=0)

        # Settings
        gui.elements.add_header_in_grid_row(
            row=2, layout=self.sett_layout, txt='Settings', )

        drp_display_type = gui.elements.grd_LabelDropdownPair(
            row=3, col=0, txt='Display Type (x, y)',
            css_ids=['', ''], layout=self.sett_layout, orientation='horiz')
        drp_items = ['Time & Date', 'Year & Day of Year']
        drp_display_type.addItems(drp_items)

        drp_cmap = gui.elements.grd_LabelDropdownPair(
            row=4, col=0, txt='Colormap',
            css_ids=['', ''], layout=self.sett_layout, orientation='horiz')
        drp_items = ['RdYlBu_r', 'RdYlBu', 'Spectral_r', 'Spectral',
                     'coolwarm_r', 'coolwarm', 'bwr_r', 'bwr', 'jet_r', 'jet', 'Greys',
                     'GnBu', 'GnBu_r']
        drp_cmap.addItems(drp_items)

        drp_nan_color = gui.elements.grd_LabelDropdownPair(
            row=5, col=0, txt='Missing Values Color',
            css_ids=['', ''], layout=self.sett_layout, orientation='horiz')
        drp_nan_color.addItems(['Grey', 'Red', 'Blue', 'White', 'Black'])

        drp_digits_after_comma = gui.elements.grd_LabelDropdownPair(
            row=6, col=0, txt='Digits After Comma',
            css_ids=['', ''], layout=self.sett_layout, orientation='horiz')
        drp_digits_after_comma.addItems(['0', '1', '2', '3', '4', '5', '6'])

        btn_save_triplet_png = gui.elements.add_button_to_grid(
            row=7, col=0, grid_layout=self.sett_layout,
            txt='Save Plot Triplet as PNG', css_id='', rowspan=1, colspan=2)

        self.sett_layout.setRowStretch(8, 1)

        gui.elements.add_header_in_grid_row(
            row=9, layout=self.sett_layout, txt='Special')

        btn_save_borderless = gui.elements.add_button_to_grid(
            row=10, col=0, grid_layout=self.sett_layout,
            txt='Save Borderless Plot (B/W and Texture)', css_id='', rowspan=1, colspan=2)

        return drp_display_type, drp_cmap, drp_nan_color, drp_digits_after_comma, \
               btn_save_triplet_png, btn_save_borderless


class Run(addContent):
    color_list = theme.colorwheel_36()  # get some colors
    sub_outdir = "plot_heatmap"

    def __init__(self, app_obj, title, tab_id, version_info):
        super().__init__(app_obj, title, tab_id)
        logger.log(name='>>> Starting Heatmap plots', dict={}, highlight=True)  # Log info
        self.selected_vars_lookup_dict = {}
        self.vars_available_df = self.tab_data_df.copy()
        self.sub_outdir = self.project_outdir / self.sub_outdir
        self.version_info = version_info
        # self.update_btn_status()

        self.btn_save_triplet_png.setDisabled(True)
        self.btn_save_borderless.setDisabled(True)

    def get_settings_from_fields(self):
        # Selected var
        selected_var = self.lst_varlist_available.selectedIndexes()
        selected_var_ix = selected_var[0].row()
        self.selected_col = self.col_dict_tuples[selected_var_ix]
        self.display_type = self.drp_display_type.currentText()

    def make_plot_df(self):
        pass

    def make_triplet(self):
        """Plot three heatmaps with same data but different color scaling"""
        self.get_settings_from_fields()
        selected_col_data = self.tab_data_df[self.selected_col].copy()
        x, y, z, plot_df = Run._prepare(selected_col_data, display_type=self.drp_display_type.currentText())
        for key, value in self.axes_dict.items():
            if key == 'ax_100':
                ax = value
                vmin = plot_df['z'].quantile(0)
                vmax = plot_df['z'].quantile(1)
                info_txt = 'All Values'
            elif key == 'ax_05_95':
                ax = value
                vmin = plot_df['z'].quantile(0.05)
                vmax = plot_df['z'].quantile(0.95)
                info_txt = 'Values Pooled Outside 5-95th Percentile Range'
            else:
                ax = value
                vmin = plot_df['z'].quantile(0.25)
                vmax = plot_df['z'].quantile(0.75)
                info_txt = 'Values Pooled Outside 25-75th Percentile Range'
            Run.plot_heatmap(x=x, y=y, z=z, ax=ax, cmap=self.drp_cmap.currentText(),
                             color_bad=self.drp_nan_color.currentText(),
                             info_txt=info_txt, vmin=vmin, vmax=vmax,
                             cb_digits_after_comma=self.drp_digits_after_comma.currentText())
        self.fig.canvas.draw()  # update plot
        self.fig.canvas.flush_events()

        # Activate export buttons
        self.btn_save_triplet_png.setEnabled(True)
        self.btn_save_borderless.setEnabled(True)

    @staticmethod
    def plot_heatmap(ax, cmap, color_bad, info_txt, vmin, vmax, cb_digits_after_comma, x, y, z):
        cmap, z = Run._set_cmap(cmap=cmap, color_bad=color_bad, z=z)
        Run._remove_cbar(ax=ax)
        p = ax.pcolormesh(x, y, z, linewidths=1, cmap=cmap,
                          vmin=vmin, vmax=vmax, shading='flat', zorder=99)
        ax.set_title(info_txt, color='black', size=theme.FONTSIZE_HEADER_AXIS)
        cb = plt.colorbar(p, ax=ax, format=f"%.{int(cb_digits_after_comma)}f")
        cb.ax.tick_params(labelsize=theme.FONTSIZE_LABELS_AXIS)
        # cbytick_obj = plt.getp(cb.axes_dict, 'yticklabels')  # Set y tick label color
        # plt.setp(cbytick_obj, color='black', fontsize=FONTSIZE_HEADER_AXIS)
        # ax.set_xticks(['3:00', '6:00', '9:00', '12:00', '15:00', '18:00', '21:00'])
        # ax.set_xticklabels([3, 6, 9, 12, 15, 18, 21])
        gui.plotfuncs.nice_date_ticks(ax=ax, minticks=4, maxticks=8, which='y')
        gui.plotfuncs.default_format(ax=ax, txt_xlabel='Time', txt_ylabel='Date')

    @staticmethod
    def _remove_cbar(ax):
        """
        Reset ax and colorbar

        Removing previous colorbars after plot update is tricky, b/c ax.clear()
        does not remove the colorbar, but only the plot itself, even though the
        colorbar is part of ax. However, the colorbar needs to be removed before
        ax.clear(), otherwise the colorbar cannot be accessed anymore.
        """
        try:
            cb = ax.collections[-1].colorbar  # Get the last (and only) colorbar in ax
            cb.remove()
        except:
            pass
        ax.clear()

    @staticmethod
    def _prepare(selected_col_data, display_type):
        """Transform data for plotting"""
        if not selected_col_data.empty:
            plot_df = pd.DataFrame(index=selected_col_data.index, columns=['z'],
                                   data=selected_col_data.values)
            xaxis_vals, yaxis_vals = Run._set_xy_axes_type(df=plot_df, display_type=display_type)
            plot_df['y_vals'] = yaxis_vals
            plot_df['x_vals'] = xaxis_vals

            if display_type == 'Year & Day of Year':
                plot_df = plot_df.resample('1D').mean()
                plot_df['y_vals'] = plot_df.index.dayofyear
                plot_df['x_vals'] = plot_df.index.year

            if len(plot_df) > 0:
                plot_df.reset_index(drop=True, inplace=True)

                # put needed data in new df_pivot, then pivot to bring it in needed shape
                plot_df_pivot = plot_df.pivot(index='y_vals', columns='x_vals', values='z')  ## new in pandas 23.4
                x = plot_df_pivot.columns.values
                y = plot_df_pivot.index.values
                z = plot_df_pivot.values

                if display_type == 'Time & Date':
                    x = np.append(x, x[-1])
                    y = np.append(y, y[-1])
                if display_type == 'Year & Day of Year':
                    x = np.append(x, 'end')
                    # y = np.append(y, y[-1])

                # if self.display_type == 'Year & Day of Year':
                #     # x = [str(xx) for xx in x]
                #     # x needs to be extended by 1 value, otherwise the plot cuts off the last year
                #     x = np.append(x, 'end')
            else:
                x = y = z = -9999
                plot_df = -9999

        else:
            x = y = z = -9999
            plot_df = -9999

        return x, y, z, plot_df

    @staticmethod
    def _set_xy_axes_type(df, display_type):
        if display_type == 'Time & Date':
            xaxis_vals = df.index.time
            yaxis_vals = df.index.date
            from pandas.plotting import register_matplotlib_converters
            pd.plotting.register_matplotlib_converters()  # Needed for time plotting
        elif display_type == 'Year & Day of Year':
            xaxis_vals = df.index.year
            yaxis_vals = df.index.date
        else:
            xaxis_vals = df.index.time  # Fallback
            yaxis_vals = df.index.date
        return xaxis_vals, yaxis_vals

    @staticmethod
    def _set_cmap(cmap, color_bad, z):
        # Colormap
        cmap = copy.copy(plt.get_cmap(cmap))  # Needed for now, https://github.com/matplotlib/matplotlib/issues/17634
        cmap.set_bad(color=color_bad, alpha=1.)  # Set missing data to specific color
        z = np.ma.masked_invalid(z)  # Mask NaN as missing
        return cmap, z

    def save_heatmap_triplet(self):
        """Save plot triplet."""
        # First, check if output directory exists
        pkgs.dfun.files.verify_dir(dir=self.sub_outdir)

        # DIIVE info
        info = self.version_info
        # info = self.ctx.build_settings  # deprecated
        diive_str = r'$\bf{}$'.format(info['app_name'])
        version_str = r'$\bf{}$'.format(info['version'])
        diive_version_str = f"generated using {diive_str} {version_str}"
        self.add_fig_text(txt=diive_version_str, position='top right')

        # Filename
        filename_out = f"heatmap_{self.selected_col[0]}_{self.selected_col[1]}.png"
        filepath_out = self.sub_outdir / filename_out
        exported_str = f"exported to {filepath_out}"
        self.add_fig_text(txt=exported_str, position='bottom right')

        # Save
        self.fig.savefig(filepath_out, format='png', bbox_inches='tight', facecolor='w',
                         transparent=True, dpi=150)

    def save_borderless_heatmap(self):
        """Save borderless plots (b/w heightmap and texture) for Blender render."""
        # https://www.youtube.com/watch?v=BXDSfrzR0zI

        # First, check if output directory exists
        pkgs.dfun.files.verify_dir(dir=self.sub_outdir)

        self.get_settings_from_fields()
        selected_col_data = self.tab_data_df[self.selected_col].copy()
        x, y, z, plot_df = Run._prepare(selected_col_data, display_type=self.drp_display_type.currentText())

        # Smooth z with rolling mean
        # _plot_df = self.plot_df.copy()
        plot_df['z_rolling_median'] = plot_df['z'].copy()
        vmin = plot_df['z_rolling_median'].quantile(0.01)
        vmax = plot_df['z_rolling_median'].quantile(0.99)
        # _plot_df['z_rolling_median'] = self.plot_df['z'].rolling(window=3, center=True, min_periods=2).mean()
        # _plot_df['z_rolling_median'] = _plot_df['z_rolling_median'].rolling(window=12, center=True, min_periods=3).mean()
        plots_list = ['normal', 'normal_r',
                      'heightmap_bw', 'heightmap_bw_r',
                      'texture_color', 'texture_color_r']  # _r is reverse cmap
        for plot in plots_list:
            if plot == 'normal':
                cmap = 'jet'
                str = 'NORMAL'
            elif plot == 'normal_r':
                cmap = 'jet_r'
                str = 'NORMAL_REVERSE'
            elif plot == 'heightmap_bw':
                cmap = 'Greys'
                str = 'HEIGHTMAP'
            elif plot == 'heightmap_bw_r':
                cmap = 'Greys_r'
                str = 'HEIGHTMAP_REVERSE'
            elif plot == 'texture_color':
                cmap = 'jet'
                str = 'TEXTURE'
            elif plot == 'texture_color_r':
                cmap = 'jet_r'
                str = 'TEXTURE_REVERSE'
            else:
                cmap = 'jet'
                str = '-NOT-SPECIFIED-FALLBACK-'

            # Figure without borders for rendering, b/w used as heightmap, colored used as texture
            fig_render_bw = plt.figure(figsize=(2, 24), frameon=False)  # frameon False = borderless
            ax_render_bw = fig_render_bw.add_axes((0, 0, 1, 1))  # left, bottom, width, height
            p = ax_render_bw.pcolormesh(x, y, z, linewidths=0, cmap=cmap, antialiased=False,
                                        vmin=vmin, vmax=vmax, shading='flat', zorder=99)

            if 'normal' not in plot:
                # For heightmap and texture, hide all ticks, labels etc, only plot is needed
                gui.plotfuncs.make_patch_spines_invisible(ax=ax_render_bw)
                gui.plotfuncs.hide_ticks_and_ticklabels(ax=ax_render_bw)
                gui.plotfuncs.hide_xaxis_yaxis(ax=ax_render_bw)

            if 'normal' in plot:
                # For normal plot, output also colorbar
                cb = plt.colorbar(p, ax=ax_render_bw, format=f"%.{int(self.drp_digits_after_comma.currentText())}f")
                cb.ax.tick_params(labelsize=theme.FONTSIZE_LABELS_AXIS * 2)
                # cbytick_obj = plt.getp(cb.axes_dict, 'yticklabels')  # Set y tick label color
                # plt.setp(cbytick_obj, color='#999c9f', fontsize=12)

            # Save to file
            filename_out = f"_{str}_heatmap_{self.selected_col[0]}_{self.selected_col[1]}.png"
            filepath_out = self.sub_outdir / filename_out
            fig_render_bw.savefig(filepath_out, format='png', bbox_inches='tight', facecolor='w',
                                  transparent=True, dpi=300)

    def add_fig_text(self, txt, position):

        ha = 'right'
        va = 'top'
        x = y = 1
        backgroundcolor = 'none'

        if position == 'top right':
            x = y = 1
            size = 9
        elif position == 'bottom right':
            x = 1
            y = 0
            va = 'bottom'
            size = 7
        elif position == 'top left':
            x = 0
            y = 1
            va = 'top'
            ha = 'left'
            size = 20
        else:
            size = 99

        self.fig.text(x, y, txt,
                      horizontalalignment=ha, verticalalignment=va,
                      transform=self.fig.transFigure, size=size, color=theme.COLOR_TXT_LEGEND,
                      backgroundcolor=backgroundcolor)
