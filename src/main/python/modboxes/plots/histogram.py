"""

HISTOGRAM
---------

"""
import matplotlib.gridspec as gridspec
# matplotlib.use('Qt5Agg')
import pandas as pd
from PyQt5 import QtGui
from PyQt5 import QtWidgets as qw

import gui.base
import pkgs.dfun.frames
import gui
import logger
from .styles import LightTheme as theme


class addContent(gui.base.buildTab):
    """Build new tab and populate with contents"""

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id, tab_template='SVVP')

        # Tab icon
        self.TabWidget.setTabIcon(self.tab_ix, QtGui.QIcon(app_obj.ctx.tab_icon_plots))

        # Add settings menu contents
        self.btn_apply, self.lne_num_bins, self.drp_rng_data, self.drp_digits_after_comma = \
            self.add_settings_fields()

        # Add variables required in settings
        self.populate_settings_fields()

        # Add available variables to variable lists
        gui.base.buildTab.populate_variable_list(obj=self)

        # # Add plots
        # self.populate_plot_area()

    def populate_plot_area(self):
        # This is done in Run due to dynamic amount of axes
        pass

    def populate_settings_fields(self):
        pass

    def add_settings_fields(self):
        # Settings
        gui.elements.add_header_to_grid_top(layout=self.sett_layout, txt='Settings')

        lne_num_bins = gui.elements.add_label_linedit_pair_to_grid(layout=self.sett_layout,
                                                                   txt='Number Of Bins',
                                                                   css_ids=['', 'cyan'],
                                                                   orientation='horiz',
                                                                   row=1, col=0)
        lne_num_bins.setText('20')

        drp_rng_data = \
            gui.elements.grd_LabelDropdownPair_dropEnabled(txt='Data Range',
                                                           css_ids=['', ''],
                                                           layout=self.sett_layout,
                                                           row=2, col=0,
                                                           orientation='horiz')
        drp_rng_data.addItem('Full Range')
        drp_rng_data.addItem('Percentile 1-99')
        drp_rng_data.addItem('Percentile 5-95')
        drp_rng_data.addItem('Percentile 25-75')

        self.sett_layout.setRowStretch(6, 1)

        # Display
        gui.elements.add_header_in_grid_row(layout=self.sett_layout, txt='Display', row=3)
        drp_digits_after_comma = gui.elements.grd_LabelDropdownPair(txt='x: Digits After Comma',
                                                                    css_ids=['', ''],
                                                                    layout=self.sett_layout,
                                                                    row=4, col=0,
                                                                    orientation='horiz')
        drp_digits_after_comma.addItems(['0', '1', '2', '3', '4', '5'])

        btn_apply = gui.elements.add_button_to_grid(grid_layout=self.sett_layout,
                                                    txt='Apply', css_id='',
                                                    row=5, col=0, rowspan=1, colspan=2)

        self.sett_layout.setRowStretch(6, 1)

        return btn_apply, lne_num_bins, drp_rng_data, drp_digits_after_comma


class Run(addContent):
    color_list = theme.colorwheel_36()  # get some colors
    sub_outdir = "plot_histogram"

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id)
        logger.log(name='>>> Starting Cumulative plots', dict={}, highlight=True)  # Log info
        self.selected_vars_lookup_dict = {}
        self.vars_selected_df = pd.DataFrame()
        self.vars_available_df = self.tab_data_df.copy()
        self.sub_outdir = self.project_outdir / self.sub_outdir
        # self.update_btn_status()

    def apply(self):
        self.plot_cumulative_line()

    def add_var_to_selected(self):
        """ Add feature from available to selected. """
        selected_var = self.lst_varlist_available.selectedIndexes()
        selected_var_ix = selected_var[0].row()
        selected_col = self.col_dict_tuples[selected_var_ix]
        selected_pretty = self.col_list_pretty[selected_var_ix]  # Needs new pretty list (screen names)

        # Add to lookup dict and df
        self.selected_vars_lookup_dict[selected_col] = selected_pretty
        self.vars_selected_df[selected_col] = self.vars_available_df[selected_col]

        self.update_selected_vars_list(list=self.lst_varlist_selected, df=self.vars_selected_df,
                                       lookup_dict=self.selected_vars_lookup_dict)

        self.plot_cumulative_line()

    def remove_var_from_selected(self):

        selected_var = self.lst_varlist_selected.selectedIndexes()
        selected_var_ix = selected_var[0].row()
        selected_col = list(self.selected_vars_lookup_dict.keys())[selected_var_ix]  # Get key by index
        # selected_pretty = self.selected_features_lookup_dict[selected_col]

        # Remove from lookup dict and df
        del self.selected_vars_lookup_dict[selected_col]
        self.vars_selected_df.drop(selected_col, axis=1, inplace=True)

        self.update_selected_vars_list(list=self.lst_varlist_selected, df=self.vars_selected_df,
                                       lookup_dict=self.selected_vars_lookup_dict)

        empty = self.check_if_selected_empty()

        if empty:
            for ax in self.fig.axes:
                self.fig.delaxes(ax)
                self.fig.canvas.draw()
                self.fig.canvas.flush_events()
        else:
            axes_dict = self.make_axes_dict(fig=self.fig, df=self.vars_selected_df)
            self.plot_cumulative_line()

    def update_selected_vars_list(self, list, df, lookup_dict):
        list.clear()
        for col in df.columns:
            item = qw.QListWidgetItem(lookup_dict[col])
            list.addItem(item)

    def update_available_vars_list(self, list, df):
        list.clear()
        list_cols = []
        # Add all variables to drop-down lists to allow full flexibility yay
        for ix, colname_tuple in enumerate(df.columns):
            item = qw.QListWidgetItem(self.col_list_pretty[ix])
            list.addItem(item)  # add column name to list
            list_cols.append(colname_tuple)
        return list, list_cols

    def check_if_selected_empty(self):
        _list = self.lst_varlist_selected
        if self.vars_selected_df.empty:
            _list.addItem('-empty-')
            empty = True
        else:
            empty = False
        return empty

    def plot_cumulative_line(self):
        """ Cumulative plot. """

        axes_dict = self.make_axes_dict(fig=self.fig,
                                        df=self.vars_selected_df)

        num_bins = int(self.lne_num_bins.text())
        rng_data = self.drp_rng_data.currentText()
        digits_after_comma = int(self.drp_digits_after_comma.currentText())

        current_colnum = -1
        for col, ax in axes_dict.items():
            current_colnum += 1

            # Separate df for plotting
            plot_df = pd.DataFrame()
            plot_df[col] = self.vars_selected_df[col].copy().dropna()

            # Data range
            if rng_data == 'Full Range':
                pass
            elif rng_data == 'Percentile 1-99':
                plot_df = pkgs.dfun.frames.limit_data_range_percentiles(df=plot_df, col=col, perc_limits=[0.01, 0.99])
            elif rng_data == 'Percentile 5-95':
                plot_df = pkgs.dfun.frames.limit_data_range_percentiles(df=plot_df, col=col, perc_limits=[0.05, 0.95])
            elif rng_data == 'Percentile 25-75':
                plot_df = pkgs.dfun.frames.limit_data_range_percentiles(df=plot_df, col=col, perc_limits=[0.25, 0.75])

            # Assign groups
            plot_df['group'] = pd.cut(plot_df[col],
                                      bins=num_bins,
                                      retbins=False,
                                      duplicates='drop',
                                      labels=False)
            # plot_df['groupas'], labels = pd.qcut(plot_df.iloc[:, 0], q=20, retbins=True, duplicates='drop')  # Series

            # Data
            plot_df_agg = plot_df.groupby('group').agg(['count', 'min', 'max'])
            label_bin_start = plot_df_agg[col]['min'].to_list()
            if digits_after_comma > 0:
                label_bin_start = [round(x, digits_after_comma) for x in
                                   label_bin_start]  # Labels for x-ticks show start of bins
            else:
                label_bin_start = [int(x) for x in label_bin_start]

            # Histogram bars
            bar_positions = plot_df_agg.index + 0.5  # Shift by half position
            ax.bar(x=bar_positions, height=plot_df_agg[col]['count'], width=0.9,
                   label='counts', color=self.color_list[current_colnum], zorder=98)
            ax.set_xticks(plot_df_agg.index)  # Position of ticks
            ax.set_xticklabels(label_bin_start)  # Labels of ticks, shown in plot
            if len(label_bin_start) > 30:
                ax.tick_params(rotation=45)
            else:
                ax.tick_params(rotation=0)
            gui.plotfuncs.default_format(ax=ax, txt_xlabel=f'bin {col[1]}', txt_ylabel=f'{col[0]} counts',
                                         txt_ylabel_units='[#]')
            gui.plotfuncs.default_grid(ax=ax)

            # Cumulative counts line
            plot_df_agg_cum = plot_df_agg.cumsum()
            count_perc = plot_df_agg_cum[col]['count'].multiply(1 / plot_df_agg_cum[col]['count'].max())
            count_perc = count_perc.multiply(100)
            linemarker_positions = count_perc.index + 1  # Shift by one full position
            ax2 = gui.plotfuncs.make_secondary_yaxis(ax=ax)
            ax2.plot(linemarker_positions, count_perc,
                     color='black', alpha=0.9, ls='-', lw=theme.WIDTH_LINE_DEFAULT,
                     marker='o', markeredgecolor='none', ms=theme.SIZE_MARKER_DEFAULT, zorder=99,
                     label='cumulative counts')
            gui.plotfuncs.default_format(ax=ax2, txt_ylabel='cumulative counts', txt_ylabel_units='%')
            ax2.set_yticks([25, 50, 75, 100])

            # ax2.axhline(y=50, color=COLOR_LINE_ZERO, ls='--', lw=1, alpha=1, zorder=100)
            # ax2.axhline(y=25, color=COLOR_LINE_ZERO, ls='--', lw=1, alpha=1, zorder=100)
            # ax2.axhline(y=75, color=COLOR_LINE_ZERO, ls='--', lw=1, alpha=1, zorder=100)

            # Legend for first plot
            if current_colnum == 0:
                gui.plotfuncs.default_legend(ax=ax)

            # # ax.locator_params(axis='x', nbins=4)
            # # from matplotlib.ticker import FormatStrFormatter
            # # ax.xaxis.set_major_formatter(FormatStrFormatter('%.2f'))

        self.fig.canvas.draw()  # update plot
        self.fig.canvas.flush_events()

    def make_axes_dict(self, fig, df):
        """ Create one ax for histogram variable and one for each selected. """
        gs = gridspec.GridSpec(len(df.columns), 1)  # rows, cols
        gs.update(wspace=0.2, hspace=0.25, left=0.05, right=0.95, top=0.95, bottom=0.05)

        # Delete all axes in figure
        for ax in self.fig.axes:
            self.fig.delaxes(ax)

        # Make new axes
        axes_dict = {}
        for ix, col in enumerate(df):
            ax = fig.add_subplot(gs[ix, 0])
            axes_dict[col] = ax

        # Format
        for col, ax in axes_dict.items():
            gui.plotfuncs.default_format(ax=ax, txt_xlabel=False)

        return axes_dict
