"""

WINDROSE PLOTS
--------------
#PLOWINRO

"""
import fnmatch

import matplotlib.gridspec as gridspec
import numpy as np
import pandas as pd
from PyQt5 import QtGui
from PyQt5 import QtWidgets as qw

import gui
import gui.base
import logger
from utils.vargroups import *
from .styles import LightTheme as theme


# pd.set_option('display.width', 1000)
# pd.set_option('display.max_columns', 15)
# pd.set_option('display.max_rows', 20)


class addContent(gui.base.buildTab):
    """Build new tab and populate with contents"""

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id, tab_template='SVVP')

        # Tab icon
        self.TabWidget.setTabIcon(self.tab_ix, QtGui.QIcon(app_obj.ctx.tab_icon_plots))

        # Add settings menu contents
        self.btn_export, self.ref_drp_agg, self.ref_drp_wind_var, self.btn_apply, \
        self.infobox, self.ref_drp_sectors = \
            self.add_settings_fields()

        # Add variables required in settings
        self.populate_settings_fields()

        # Add available variables to variable lists
        gui.base.buildTab.populate_variable_list(obj=self)

        # # Add plots
        # self.axes_dict = self.populate_plot_area()

    def populate_plot_area(self):
        # Not needed due to dynamic number of axes
        pass

    def populate_settings_fields(self):
        self.ref_drp_wind_var.clear()
        default_winddir_ix = 0
        default_winddir_ix_found = False

        # Add all variables to drop-down lists to allow full flexibility yay
        for ix, colname_tuple in enumerate(self.tab_data_df.columns):
            self.ref_drp_wind_var.addItem(self.col_list_pretty[ix])
            # Check if column appears in the global vars
            if any(fnmatch.fnmatch(colname_tuple[0], vid) for vid in WIND_DIR):
                default_winddir_ix = ix if not default_winddir_ix_found else default_winddir_ix
                default_winddir_ix_found = True

        # Set dropdowns to found ix
        self.ref_drp_wind_var.setCurrentIndex(default_winddir_ix)

    def add_axes(self):
        gs = gridspec.GridSpec(1, 1)  # rows, cols
        # gs.update(wspace=0.2, hspace=0.2, left=0.03, right=0.97, top=0.97, bottom=0.03)
        ax = self.fig.add_subplot(gs[0, 0])
        axes_dict = {'ax': ax}
        for key, ax in axes_dict.items():
            gui.plotfuncs.default_format(ax=ax, txt_xlabel=False)
        return axes_dict

    def add_settings_fields(self):
        gui.elements.add_header_in_grid_row(layout=self.sett_layout, txt='Settings', row=0)
        self.sett_layout.setRowStretch(1, 0)  # empty row

        # Wind variable
        ref_drp_wind_var = gui.elements.grd_LabelDropdownPair(txt='Wind Direction Variable (°)',
                                                              css_ids=['', 'cyan'],
                                                              layout=self.sett_layout,
                                                              orientation='horiz',
                                                              row=2, col=0,
                                                              colspan=2)

        # Method
        ref_drp_agg = gui.elements.grd_LabelDropdownPair(txt='Aggregation',
                                                         css_ids=['', 'cyan'],
                                                         layout=self.sett_layout,
                                                         orientation='horiz',
                                                         row=3, col=0,
                                                         colspan=2)
        ref_drp_agg.addItem('Mean')
        ref_drp_agg.addItem('Median')
        ref_drp_agg.addItem('SD')
        ref_drp_agg.addItem('Minimum')
        ref_drp_agg.addItem('Maximum')
        ref_drp_agg.addItem('Count')
        ref_drp_agg.addItem('Sum')
        ref_drp_agg.addItem('Variance')

        # Method
        ref_drp_sectors = gui.elements.grd_LabelDropdownPair(txt='Wind Sectors',
                                                             css_ids=['', 'cyan'],
                                                             layout=self.sett_layout,
                                                             orientation='horiz',
                                                             row=4, col=0,
                                                             colspan=2)
        ref_drp_sectors.addItem('9 (40° per Sector)')
        ref_drp_sectors.addItem('12 (30° per Sector)')
        ref_drp_sectors.addItem('18 (20° per Sector)')
        ref_drp_sectors.addItem('36 (10° per Sector')

        btn_apply = gui.elements.add_button_to_grid(grid_layout=self.sett_layout,
                                                    txt='Apply',
                                                    css_id='btn_cat_ControlsRun',
                                                    row=5, col=0, rowspan=1, colspan=3)

        btn_export = gui.elements.add_button_to_grid(grid_layout=self.sett_layout,
                                                     txt='Export',
                                                     css_id='btn_cat_ControlsRun',
                                                     row=6, col=0, rowspan=1, colspan=3)

        self.sett_layout.setRowStretch(7, 2)  # empty row

        infobox = gui.elements.add_infobox_to_grid(txt='', grid_layout=self.sett_layout, css_id='',
                                                   row=8, col=0, rowspan=1, colspan=2)
        self.sett_layout.setRowStretch(8, 1)  # empty row

        return btn_export, ref_drp_agg, ref_drp_wind_var, btn_apply, infobox, ref_drp_sectors


class Run(addContent):
    """
    Start
    """
    current_color_ix = -1
    current_marker_ix = -1
    plot_color_list = theme.colors_6()
    plot_marker_list = theme.generate_plot_marker_list()
    wind_sector_col = ('wind_sector', '[deg]')
    width_col = ('width', '[aux]')
    sub_outdir = "plot_wind_sectors"

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id)
        logger.log(name='>>> Starting Windrose plots', dict={}, highlight=True)  # Log info
        self.selected_vars_lookup_dict = {}
        self.vars_selected_df = pd.DataFrame()
        self.vars_available_df = self.tab_data_df.copy()
        self.sub_outdir = self.project_outdir / self.sub_outdir
        # self.update_btn_status()
        self.section_groups_df = pd.DataFrame()

    def apply(self):
        self.axes_dict = self.make_axes_dict(fig=self.fig, df=self.vars_selected_df)
        self.plot_windrose(df=self.vars_selected_df, axes_dict=self.axes_dict)

    def remove_var_from_selected(self):

        selected_var = self.lst_varlist_selected.selectedIndexes()
        selected_var_ix = selected_var[0].row()
        selected_col = list(self.selected_vars_lookup_dict.keys())[selected_var_ix]  # Get key by index

        # Remove from lookup dict and df
        del self.selected_vars_lookup_dict[selected_col]
        self.vars_selected_df.drop(selected_col, axis=1, inplace=True)

        self.update_selected_vars_list(list=self.lst_varlist_selected, df=self.vars_selected_df,
                                       lookup_dict=self.selected_vars_lookup_dict)

        empty = self.check_if_selected_empty()

        if empty:
            for ax in self.fig.axes:
                self.fig.delaxes(ax)
                self.fig.canvas.draw()
                self.fig.canvas.flush_events()
        else:
            self.axes_dict = self.make_axes_dict(fig=self.fig, df=self.vars_selected_df)
            self.plot_windrose(df=self.vars_selected_df, axes_dict=self.axes_dict)

    def add_var_to_selected(self):
        """ Add variable from available to selected. """
        selected_var = self.lst_varlist_available.selectedIndexes()
        selected_var_ix = selected_var[0].row()
        selected_col = self.col_dict_tuples[selected_var_ix]
        selected_pretty = self.col_list_pretty[selected_var_ix]  # Needs new pretty list (screen names)

        # Add to lookup dict and df
        self.selected_vars_lookup_dict[selected_col] = selected_pretty
        self.vars_selected_df[selected_col] = self.vars_available_df[selected_col]

        self.update_selected_vars_list(list=self.lst_varlist_selected, df=self.vars_selected_df,
                                       lookup_dict=self.selected_vars_lookup_dict)

        self.axes_dict = self.make_axes_dict(fig=self.fig, df=self.vars_selected_df)
        self.plot_windrose(df=self.vars_selected_df, axes_dict=self.axes_dict)

    def make_axes_dict(self, fig, df):
        """ Assign data columns to axes. """

        # num_plots = len(df.columns)

        gs = gridspec.GridSpec(3, 3)  # rows, cols
        gs.update(wspace=0.4, hspace=0.4, left=0.1, right=0.9, top=0.9, bottom=0.1)

        # Delete all axes in figure
        for ax in self.fig.axes:
            self.fig.delaxes(ax)

        # Make new axes
        axes_dict = {}
        first_ax = -9999
        gs_row = 0  # Needs to start at zero
        gs_col = -1
        ix_counter = -1

        for ix, col in enumerate(df.columns):

            # Location ox ax in gridspec
            ix_counter += 1
            gs_col += 1
            if ix_counter > 2:  # 3 plots per fig row
                ix_counter = 0
                gs_col = 0  # Reset to first column
                gs_row += 1  # Start next row

            if ix == 0:
                ax = fig.add_subplot(gs[gs_row, gs_col], projection='polar')
                first_ax = ax
            else:
                ax = fig.add_subplot(gs[gs_row, gs_col], projection='polar', sharex=first_ax)
            axes_dict[col] = ax  # Assign column data to ax

        self.fig.canvas.draw()
        self.fig.canvas.flush_events()

        # # Format
        # for col, ax in axes_dict.items():
        #     gui.plotfuncs.default_format(ax=ax)

        return axes_dict

    def export(self):
        """ Export wind sector plots and their data. """

        pkgs.dfun.files.verify_dir(dir=self.sub_outdir)
        current_timestamp = pkgs.dfun.times.make_timestamp_suffix()

        filepath_out_fig = self.sub_outdir / f'plot_wind_sectors_{current_timestamp}.png'
        self.fig.savefig(filepath_out_fig, format='png', bbox_inches='tight', facecolor='w',
                         transparent=True, dpi=150)

        export_df = self.section_groups_df.copy()
        export_df.columns = pd.MultiIndex.from_tuples(export_df.columns)
        filepath_out_data = self.sub_outdir / f'data_wind_sectors_{current_timestamp}.csv'
        export_df.to_csv(filepath_out_data, index=False)
        linebreak = '\n'

        self.infobox.setText('')
        self.infobox.setText(f'Plot was exported to:{linebreak}'
                             f'{filepath_out_fig}{linebreak}'
                             f'{linebreak}{linebreak}'
                             f'Data was exported to:{linebreak}'
                             f'{filepath_out_data}{linebreak}')

    def plot_windrose(self, df, axes_dict):

        """
        plots windrose plots from DataFrame
        Requires conversion from degrees to radians
        """

        color_list = theme.colorwheel_36()
        color_ix = -1
        # winddir_col = ('wind_dir', '[deg_from_north]')

        # Get settings
        winddir_pretty = self.ref_drp_wind_var.currentText()
        winddir_ix = self.col_list_pretty.index(winddir_pretty)
        winddir_col = self.col_dict_tuples[winddir_ix]
        agg = self.ref_drp_agg.currentText()

        sectors = int(self.ref_drp_sectors.currentText().split(' ')[0])  # Get first number from dropdown selection
        section_width = 360 / sectors

        self.fig.suptitle(agg + 's', fontsize=16, fontweight='bold')

        for col, ax in axes_dict.items():
            color_ix += 1

            # Wind and scalar data
            plot_df = df[[col]]
            plot_df[winddir_col] = self.tab_data_df[winddir_col].copy()  # Add wind dir for plotting
            plot_df = plot_df.dropna()

            # Divide wind directions in different sections, spanning 10 degrees
            # section_width = 40
            plot_df[self.wind_sector_col] = plot_df[winddir_col].copy().apply(
                lambda x: int(section_width * round(float(x) / section_width)))

            # Calculate grouped stats, store in Series
            aggS = -9999
            self.section_groups_df = plot_df.groupby(self.wind_sector_col)
            if agg == 'Mean':
                aggS = self.section_groups_df.mean()[col]
                agg_col_suffix = '_mean'
            elif agg == 'Median':
                aggS = self.section_groups_df.median_col()[col]
                agg_col_suffix = '_median'
            elif agg == 'SD':
                aggS = self.section_groups_df.std_col()[col]
                agg_col_suffix = '_SD'
            elif agg == 'Minimum':
                aggS = self.section_groups_df.min()[col]
                agg_col_suffix = '_min'
            elif agg == 'Maximum':
                aggS = self.section_groups_df.max()[col]
                agg_col_suffix = '_max'
            elif agg == 'Count':
                aggS = self.section_groups_df.count()[col]
                agg_col_suffix = '_count'
            elif agg == 'Variance':
                aggS = self.section_groups_df.var_col()[col]
                agg_col_suffix = '_variance'
            else:
                agg_col_suffix = '_-9999'

            # Define col names for df
            agg_col = (col[0] + agg_col_suffix, col[1])

            # Build df from Series for plotting
            self.section_groups_df = plot_df.groupby(self.wind_sector_col).count()
            self.section_groups_df[agg_col] = aggS
            self.section_groups_df[self.width_col] = section_width - 2
            self.section_groups_df.reset_index(inplace=True)

            # Conversion and preparation
            sections = self.section_groups_df[self.wind_sector_col].values
            theta = np.radians(sections)  # position in degrees, must be converted to radians for plotting
            width = self.section_groups_df[(self.width_col)].values
            width = np.radians(width)  # width of section, e.g. position 90 w/ width 10 spans from 85-95 in the plot

            # Setup plot
            ax.set_theta_zero_location("N")
            ax.set_theta_direction(-1)
            ax.set_theta_offset(np.radians([90]))  # Set the offset for the location of 0 in radians.

            # title
            ax.bar(theta, self.section_groups_df[agg_col], width=width, bottom=0.0, color=color_list[color_ix])
            ax.set_title(col[0], fontsize=12, fontweight='bold', y=1.15)

        self.fig.canvas.draw()
        self.fig.canvas.flush_events()

    def update_selected_vars_list(self, list, df, lookup_dict):
        list.clear()
        for col in df.columns:
            item = qw.QListWidgetItem(lookup_dict[col])
            list.addItem(item)

    def check_if_selected_empty(self):
        _list = self.lst_varlist_selected
        if self.vars_selected_df.empty:
            _list.addItem('-empty-')
            empty = True
        else:
            empty = False
        return empty

    def update_available_vars_list(self, list, df):
        list.clear()
        list_cols = []
        # Add all variables to drop-down lists to allow full flexibility yay
        for ix, colname_tuple in enumerate(df.columns):
            item = qw.QListWidgetItem(self.col_list_pretty[ix])
            list.addItem(item)  # add column name to list
            list_cols.append(colname_tuple)
        return list, list_cols
