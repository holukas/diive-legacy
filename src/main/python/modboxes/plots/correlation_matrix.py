"""

DYNAMIC CORRELATION MATRIX
--------------------------

Selecting variables from a list automatically generates a correlation matrix
based in the selected variables. With each new variable selected, the matrix
is immediately updated. Similarly, the matrix is updated when a previously
selected variable is removed from the selection.


"""
import matplotlib.gridspec as gridspec
# matplotlib.use('Qt5Agg')
import numpy as np
import pandas as pd
import seaborn as sns
from PyQt5 import QtGui
from PyQt5 import QtWidgets as qw

import gui
import gui.base
import logger
import pkgs
from .styles import LightTheme as theme


# pd.set_option('display.width', 1000)
# pd.set_option('display.max_columns', 15)
# pd.set_option('display.max_rows', 20)


class addContent(gui.base.buildTab):
    """Build new tab and populate with contents"""

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id, tab_template='SVVP')

        # Tab icon
        self.TabWidget.setTabIcon(self.tab_ix, QtGui.QIcon(app_obj.ctx.tab_icon_plots))

        # Add settings menu contents
        self.lne_correlation_threshold, self.infobox, self.btn_apply, self.btn_export = \
            self.add_settings_fields()

        # Add available variables to variable lists
        gui.base.buildTab.populate_variable_list(obj=self)

        # Add plots and tableview
        self.axes_dict = self.populate_plot_area()

    def populate_plot_area(self):
        return self.add_axes()

    def populate_settings_fields(self):
        pass

    def add_axes(self):
        gs = gridspec.GridSpec(1, 1)  # rows, cols
        gs.update(wspace=0.2, hspace=0.2, left=0.03, right=0.97, top=0.97, bottom=0.03)

        # plots
        ax_main = self.fig.add_subplot(gs[0, 0])
        axes_dict = {'ax_main': ax_main}
        for key, ax in axes_dict.items():
            gui.plotfuncs.default_format(ax=ax, txt_xlabel=False)
        return axes_dict

    def add_settings_fields(self):
        # Settings
        gui.elements.add_header_in_grid_row(layout=self.sett_layout, txt='Settings', row=0)
        self.sett_layout.setRowStretch(1, 0)  # empty row

        # Threshold, defines which values are shown
        lne_correlation_threshold = gui.elements.add_label_linedit_pair_to_grid(txt='Correlation Threshold >=',
                                                                                css_ids=['', 'cyan'],
                                                                                layout=self.sett_layout,
                                                                                orientation='horiz',
                                                                                row=2, col=0)
        lne_correlation_threshold.setText('0')
        btn_apply = gui.elements.add_button_to_grid(grid_layout=self.sett_layout,
                                                    txt='Apply',
                                                    css_id='btn_cat_ControlsRun',
                                                    row=3, col=0, rowspan=1, colspan=3)
        self.sett_layout.setRowStretch(4, 0)  # empty row

        btn_export = gui.elements.add_button_to_grid(grid_layout=self.sett_layout,
                                                     txt='Export',
                                                     css_id='btn_cat_ControlsRun',
                                                     row=5, col=0, rowspan=1, colspan=3)

        self.sett_layout.setRowStretch(6, 2)  # empty row

        infobox = gui.elements.add_infobox_to_grid(txt='', grid_layout=self.sett_layout, css_id='',
                                                   row=7, col=0, rowspan=1, colspan=2)
        self.sett_layout.setRowStretch(8, 1)  # empty row

        return lne_correlation_threshold, infobox, btn_apply, btn_export


class Run(addContent):
    """
    Start
    """
    current_color_ix = -1
    current_marker_ix = -1
    plot_color_list = theme.colors_6()
    plot_marker_list = theme.generate_plot_marker_list()
    corr_df = pd.DataFrame()
    sub_outdir = "plot_correlation matrix"

    def __init__(self, app_obj, title, tab_id):
        super().__init__(app_obj, title, tab_id)
        logger.log(name='>>> Starting Correlation Matrix Plotting', dict={}, highlight=True)  # Log info
        self.selected_vars_lookup_dict = {}
        self.vars_selected_df = pd.DataFrame()
        self.vars_available_df = self.tab_data_df.copy()
        # self.update_btn_status()
        self.orig_data_df = self.tab_data_df.copy()  # In case of reset
        self.sub_outdir = self.project_outdir / self.sub_outdir

    def apply(self):
        self.get_settings_from_fields()
        self.ax = self.make_ax(fig=self.fig)
        corr_df = self.vars_selected_df.corr()
        corr_df = self.filter_by_threshold(df=corr_df, threshold=float(self.lne_correlation_threshold.text()))
        self.plot_correlation_matrix(df=corr_df)

    def get_settings_from_fields(self):
        self.correlation_threshold = float(self.lne_correlation_threshold.text())

    def filter_by_threshold(self, df, threshold):
        filter_threshold = df.abs() >= threshold
        df = df[filter_threshold]
        return df

    def remove_var_from_selected(self):
        selected_var = self.lst_varlist_selected.selectedIndexes()
        selected_var_ix = selected_var[0].row()
        selected_col = list(self.selected_vars_lookup_dict.keys())[selected_var_ix]  # Get key by index

        # Remove from lookup dict and df
        del self.selected_vars_lookup_dict[selected_col]
        self.vars_selected_df.drop(selected_col, axis=1, inplace=True)

        self.update_selected_vars_list(list=self.lst_varlist_selected, df=self.vars_selected_df,
                                       lookup_dict=self.selected_vars_lookup_dict)

        empty = self.check_if_selected_empty()

        if empty:
            for ax in self.fig.axes:
                self.fig.delaxes(ax)
                self.fig.canvas.draw()
                self.fig.canvas.flush_events()
        else:
            self.ax = self.make_ax(fig=self.fig)
            corr_df = self.vars_selected_df.corr()
            self.plot_correlation_matrix(df=corr_df)

    def add_var_to_selected(self):
        """ Add variable from available to selected. """
        selected_var = self.lst_varlist_available.selectedIndexes()
        selected_var_ix = selected_var[0].row()
        selected_col = self.col_dict_tuples[selected_var_ix]
        selected_pretty = self.col_list_pretty[selected_var_ix]  # Needs new pretty list (screen names)

        # Add to lookup dict and df
        self.selected_vars_lookup_dict[selected_col] = selected_pretty
        self.vars_selected_df[selected_col] = self.vars_available_df[selected_col]

        self.update_selected_vars_list(list=self.lst_varlist_selected, df=self.vars_selected_df,
                                       lookup_dict=self.selected_vars_lookup_dict)

        self.ax = self.make_ax(fig=self.fig)
        corr_df = self.vars_selected_df.corr()
        self.plot_correlation_matrix(df=corr_df)

    def make_ax(self, fig):

        gs = gridspec.GridSpec(1, 1)  # rows, cols
        # gs.update(wspace=0.2, hspace=0.2, left=0.1, right=0.9, top=0.9, bottom=0.1)

        # Delete all axes in figure
        for ax in self.fig.axes:
            self.fig.delaxes(ax)

        ax = fig.add_subplot(gs[0, 0])
        gui.plotfuncs.default_format(ax=ax, txt_xlabel=False)  # Format

        return ax

    def export(self):
        """ Export correlation plot and its data. """
        pkgs.dfun.files.verify_dir(dir=self.sub_outdir)
        current_timestamp = pkgs.dfun.times.make_timestamp_suffix()

        filepath_out_fig = self.sub_outdir / f'plot_correlation_matrix_{current_timestamp}.png'
        self.fig.savefig(filepath_out_fig, format='png', bbox_inches='tight', facecolor='w',
                         transparent=True, dpi=150)

        filepath_out_data = self.sub_outdir / f'data_correlation_matrix_{current_timestamp}.csv'
        self.corr_df.to_csv(filepath_out_data)
        linebreak = '\n'

        self.infobox.setText('')
        self.infobox.setText(f'Plot was exported to:{linebreak}'
                             f'{filepath_out_fig}{linebreak}'
                             f'{linebreak}{linebreak}'
                             f'Data was exported to:{linebreak}'
                             f'{filepath_out_data}{linebreak}')

    def plot_correlation_matrix(self, df):
        corr_df = df.copy()
        self.corr_df = self.filter_by_threshold(df=corr_df, threshold=float(self.lne_correlation_threshold.text()))

        # Generate a mask for the upper triangle
        mask = np.triu(np.ones_like(self.corr_df, dtype=np.bool))

        # Generate a custom diverging colormap
        cmap = sns.diverging_palette(220, 10, n=11, as_cmap=True)

        # Column labels
        f = ['\n'.join(col).strip() for col in self.corr_df.columns.values]
        xticklabels = yticklabels = f

        # Draw the heatmap with the mask and correct aspect ratio
        sns.heatmap(self.corr_df, ax=self.ax, mask=mask, cmap=cmap, center=0,
                    square=False, linewidths=3, cbar_kws={"shrink": .5, "label": "pearson correlation coefficient"},
                    annot=True, annot_kws={"fontsize": theme.FONTSIZE_INFO_DEFAULT}, fmt='.2', vmin=-1, vmax=1,
                    xticklabels=xticklabels, yticklabels=yticklabels)

        self.fig.canvas.draw()
        self.fig.canvas.flush_events()

    def update_selected_vars_list(self, list, df, lookup_dict):
        list.clear()
        for col in df.columns:
            item = qw.QListWidgetItem(lookup_dict[col])
            list.addItem(item)

    def check_if_selected_empty(self):
        _list = self.lst_varlist_selected
        if self.vars_selected_df.empty:
            _list.addItem('-empty-')
            empty = True
        else:
            empty = False
        return empty

    def update_available_vars_list(self, list, df):
        list.clear()
        list_cols = []
        # Add all variables to drop-down lists to allow full flexibility yay
        for ix, colname_tuple in enumerate(df.columns):
            item = qw.QListWidgetItem(self.col_list_pretty[ix])
            list.addItem(item)  # add column name to list
            list_cols.append(colname_tuple)
        return list, list_cols

    def set_marker_style(self):
        self.current_color_ix, self.current_marker_ix = \
            gui.plotfuncs.set_marker_color(current_color_ix=self.current_color_ix,
                                           plot_color_list=self.plot_color_list,
                                           current_marker_ix=self.current_marker_ix,
                                           plot_marker_list=self.plot_marker_list)
