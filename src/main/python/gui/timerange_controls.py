import gui.combine
import gui.make
from gui import elements


class TimeRangeControls():

    def __init__(self, layout):
        # Dropdowns for time range

        # Start
        _label = gui.make.qlabel(txt='START', css_id='lbl_info2')
        layout.addWidget(_label, stretch=0)

        # Start year
        _label, self.drp_year_start = gui.combine.label_combobox(
            txt='Year', max_visible_items=12, css_ids=['lbl_info', 'timerange'])
        layout.addWidget(_label)
        layout.addWidget(self.drp_year_start)

        # Start month
        _label, self.drp_month_start = gui.combine.label_combobox(
            txt='Month', max_visible_items=12, css_ids=['lbl_info', 'timerange'])
        layout.addWidget(_label)
        layout.addWidget(self.drp_month_start)

        # Start day
        _label, self.drp_day_start = gui.combine.label_combobox(
            txt='Day', max_visible_items=31, css_ids=['lbl_info', 'timerange'])
        layout.addWidget(_label)
        layout.addWidget(self.drp_day_start)

        # Spacer
        _spacer = gui.make.qlabel(txt=' | ', css_id='lbl_info')
        layout.addWidget(_spacer, stretch=0)

        # End
        _label = gui.make.qlabel(txt='END', css_id='lbl_info2')
        layout.addWidget(_label, stretch=0)

        # End year
        _label, self.drp_year_end = gui.combine.label_combobox(
            txt='Year', max_visible_items=12, css_ids=['lbl_info', 'timerange'])
        layout.addWidget(_label)
        layout.addWidget(self.drp_year_end)

        # End month
        _label, self.drp_month_end = gui.combine.label_combobox(
            txt='Month', max_visible_items=12, css_ids=['lbl_info', 'timerange'])
        layout.addWidget(_label)
        layout.addWidget(self.drp_month_end)

        # End day
        _label, self.drp_day_end = gui.combine.label_combobox(
            txt='Day', max_visible_items=31, css_ids=['lbl_info', 'timerange'])
        layout.addWidget(_label)
        layout.addWidget(self.drp_day_end)

        # Buttons
        self.btn_apply = elements.add_button_to_layout(txt='Apply', layout=layout, css_id='btn_timerange')
        self.btn_reset = elements.add_button_to_layout(txt='Reset', layout=layout, css_id='btn_timerange')
