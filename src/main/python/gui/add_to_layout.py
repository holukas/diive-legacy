from PyQt5 import QtWidgets as qtw


def add_to_grid(layout, widget, row: int, col: int, rowspan: int = 1, colspan: int = 1):
    if isinstance(widget, qtw.QFrame) \
            | isinstance(widget, qtw.QLineEdit) \
            | isinstance(widget, qtw.QLabel) \
            | isinstance(widget, qtw.QPushButton) \
            | isinstance(widget, qtw.QComboBox):
        layout.addWidget(widget, row, col, rowspan, colspan)
    elif isinstance(widget, qtw.QSpacerItem):
        layout.addItem(widget, row, col, rowspan, colspan)


def add_spacer_item_to_grid(layout, row, col, width=10, height=10):
    spacer_item = qtw.QSpacerItem(width, height)
    layout.addItem(spacer_item, row, col, 1, 1)  # empty row
