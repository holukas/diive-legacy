import matplotlib.pyplot as plt
from PyQt5 import QtWidgets as qtw
from matplotlib.backends.backend_qt5 import NavigationToolbar2QT as NavigationToolbar
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas

import gui
import gui.combine


def set_target(qlist:qtw.QListWidget, col_dict_tuples:dict):
    var = qlist.selectedIndexes()
    target_var_ix = var[0].row()
    col = col_dict_tuples[target_var_ix]
    return col


# def populate_variable_list(qlist, dfcols, col_list_pretty):
#     """Add variables to variable list"""
#     qlist.clear()
#     list_cols = []
#     for ix, colname_tuple in enumerate(dfcols):
#         item = qtw.QListWidgetItem(col_list_pretty[ix])
#         qlist.addItem(item)  # add column name to list
#         list_cols.append(colname_tuple)


def _tabcol_plotarea(tabwidget):
    """Assemble plot area for tab column"""
    frame = gui.make.qframe()
    layout = gui.make.qlayout_vert()
    frame.setLayout(layout)
    fig = plt.Figure(facecolor='white')
    canvas = FigureCanvas(fig)

    layout.addWidget(canvas, stretch=4)
    toolbar = NavigationToolbar(canvas, parent=tabwidget)
    layout.addWidget(toolbar, stretch=0)
    return frame, fig, layout


def _tabcol_settings():
    """Assemble settings menu for tab column"""
    frame, grid = gui.combine.frame_grid()
    stacked = gui.make.qstacked()
    stacked.addWidget(frame)
    return stacked, grid


def _tabcol_varlist(header_txt):
    """Assemble variable list for tab column"""
    frame = gui.make.qframe(css_id='frm_VariableList', max_width=600)
    layout = gui.make.qlayout_vert()
    frame.setLayout(layout)
    header = gui.make.qlabel(txt=header_txt, css_id='lbl_Header2')
    layout.addWidget(header, stretch=0)
    varlist = gui.make.qlist()
    lineedit = gui.make.qlineedit(css_id='search_box')
    layout.addWidget(lineedit)
    layout.addWidget(varlist)
    return frame, lineedit, varlist

class TemplateBase:

    def __init__(self, app_obj, title, tab_id):
        self.TabWidget = app_obj.TabWidget  # TabWidget in the main window
        self.col_list_pretty = app_obj.col_list_pretty
        self.col_dict_tuples = app_obj.col_dict_tuples
        self.tab_data_df = app_obj.data_df.copy()  # Tab instance of data from main window / variables tab
        self.project_outdir = app_obj.project_outdir
        self.ctx = app_obj.ctx
        self.title = title
        self.tab_id = tab_id

    def add_tab_to_main_window(self, splitter):
        lyt_tab_vert, self.tab_ix = self.TabWidget.add_new_tab(title=f"{self.title} {self.tab_id}")
        self.TabWidget.setCurrentIndex(self.tab_ix)  # Select newly created tab
        lyt_tab_vert.addWidget(splitter, stretch=1)  ## add Splitter to tab layout


class TemplateSVP(TemplateBase):
    """
    Build tab based on the generic BaseTemplate

    Build SVP tab with 3 columns:
        #1 settings (S)
        #2 variable list (V)  /available variables
        #3 plot area (P)
    """

    def __init__(self, app_obj, tab_title, tab_id):
        super().__init__(app_obj, tab_title, tab_id)
        self.splitter = gui.make.qsplitter()
        self._addcol_settings()
        self._addcol_varlist()
        self._addcol_plotarea()
        self.splitter.setStretchFactor(0, 2)
        self.splitter.setStretchFactor(1, 1)
        self.splitter.setStretchFactor(2, 3)
        self.add_tab_to_main_window(splitter=self.splitter)

    def _addcol_settings(self):
        self.sett_menu, self.sett_layout = _tabcol_settings()
        self.splitter.addWidget(self.sett_menu)  # Settings menu (left)

    def _addcol_varlist(self):
        self.frm_varlist_available, self.lne_filter_varlist, self.lst_varlist_available =\
            _tabcol_varlist(header_txt='Available Variables')
        self.splitter.addWidget(self.frm_varlist_available)  # List of vars

    def _addcol_plotarea(self):
        self.frm_plot_area, self.fig, self.lyt_plot_area = _tabcol_plotarea(tabwidget=self.TabWidget)
        self.splitter.addWidget(self.frm_plot_area)


class QCustomTabWidget(qtw.QTabWidget):
    """TabWidget for main window, tabs will be added to this widget"""

    def __init__(self, parent=None):
        super(QCustomTabWidget, self).__init__(parent)
        self.setTabsClosable(True)
        # self.tabBar().setTabButton(0, qw.QTabBar.RightSide, 0)

    def add_new_tab(self, title):
        newtab_frame = qtw.QFrame()
        newtab_layout = qtw.QVBoxLayout()
        newtab_frame.setLayout(newtab_layout)
        tab_ix = self.addTab(newtab_frame, title)
        return newtab_layout, tab_ix

    def closeTab(self, currentIndex):
        if currentIndex > 0:
            currentQWidget = self.widget(currentIndex)
            currentQWidget.deleteLater()
            self.removeTab(currentIndex)
            self.setCurrentIndex(currentIndex - 1)

    def get(self):
        pass


def find_free_tab_id(TabWidget, tab_ids):
    """
    Check if a tab with a name that contains all elements in tab_ids already exists
    and then append ID number to tab_name.
    """

    num_tabs = TabWidget.count()  ## number of tabs

    # Check if the plot tab already exists in the tabbar
    tabs_existing = []  # collects names of already exising tabs
    for tab_ix in range(num_tabs):
        if all(tab_id in TabWidget.tabText(tab_ix) for tab_id in tab_ids):
            tabs_existing.append(TabWidget.tabText(tab_ix))

    # Check which ids (int) are already in use
    ids_existing = []
    for tab_existing in tabs_existing:
        splits = tab_existing.split(' ')
        ids_existing.append(int(splits[-1]))

    # Check if list is empty, i.e. not ids exist yet
    if not ids_existing:
        new_tab_id = 1
    else:
        new_tab_id = max(ids_existing) + 1

    # # Alternatively: search for the next available (not used) id
    # start_id = 1
    # while start_id in ids_existing:
    #     start_id += 1
    # new_tab_id = start_id

    # num_tabs = len(tabs_existing)
    # new_tab_id = num_tabs + 1
    return new_tab_id
