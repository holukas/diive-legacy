import pandas as pd
from PyQt5 import QtCore as qtc

from gui.base import buildTab


def show_df(df, tableview, how_many_rows=None):
    # Show df in tableview
    if how_many_rows:
        model = DataFrameModel(df=df.head(how_many_rows))
    else:
        model = DataFrameModel(df=df)
    tableview.setModel(model)
    tableview.setAlternatingRowColors(True)
    tableview.resizeColumnsToContents()
    # self.table_view.setSizeAdjustPolicy(qw.QAbstractScrollArea.AdjustToContents)
    # self.table_view.setSizeAdjustPolicy(qw.QAbstractScrollArea.AdjustToContents)
    # self.tableview.setModel(model, qtc.QVariant(qtc.Qt.AlignRight), qtc.Qt.TextAlignmentRole)
    # self.tableview.installEventFilters(self)
    # self.tableview.setSortingEnabled(True)
    # self.tableview.setItemDelegate(FloatDelegate(3))
    return None


class DataFrameModel(qtc.QAbstractTableModel):
    """
     https://www.learnpyqt.com/courses/model-views/qtableview-modelviews-numpy-pandas/
     https://stackoverflow.com/questions/44603119/how-to-display-a-pandas-data-frm_CategoryOptions-with-pyqt5
     https://stackoverflow.com/questions/28186118/how-to-make-qtableview-to-enter-the-editing-mode-only-on-double-click
     https://stackoverflow.com/questions/26965185/how-to-programmatically-change-update-data-in-python-pyqt4-tableview
     """

    DtypeRole = qtc.Qt.UserRole + 1000
    ValueRole = qtc.Qt.UserRole + 1001

    def __init__(self, df=pd.DataFrame(), editable=False, parent=None):
        super(DataFrameModel, self).__init__(parent)
        self._dataframe = df.copy()
        self.editable = editable

    def setDataFrame(self, dataframe):
        self.beginResetModel()
        self._dataframe = dataframe.copy()
        self.endResetModel()

    def dataFrame(self):
        return self._dataframe

    dataFrame = qtc.pyqtProperty(pd.DataFrame, fget=dataFrame, fset=setDataFrame)

    @qtc.pyqtSlot(int, qtc.Qt.Orientation, result=str)
    def headerData(self, section: int, orientation: qtc.Qt.Orientation, role: int = qtc.Qt.DisplayRole):
        if role == qtc.Qt.DisplayRole:
            if orientation == qtc.Qt.Horizontal:
                return str(self._dataframe.columns[section])

            if orientation == qtc.Qt.Vertical:
                return str(self._dataframe.index[section])

        return qtc.QVariant()

    def rowCount(self, parent=qtc.QModelIndex()):
        if parent.isValid():
            return 0
        return len(self._dataframe.index)

    def columnCount(self, parent=qtc.QModelIndex()):
        if parent.isValid():
            return 0
        return self._dataframe.columns.size

    def data(self, index, role=qtc.Qt.DisplayRole):
        if not index.isValid() or not (0 <= index.row() < self.rowCount() \
                                       and 0 <= index.column() < self.columnCount()):
            return qtc.QVariant()
        row = self._dataframe.index[index.row()]
        col = self._dataframe.columns[index.column()]
        dt = self._dataframe[col].dtype

        try:
            val = self._dataframe.loc[row][col]
        except:
            print("M")

        if role == qtc.Qt.DisplayRole:
            return str(val)
        elif role == DataFrameModel.ValueRole:
            return val
        if role == DataFrameModel.DtypeRole:
            return dt
        return qtc.QVariant()

    def setData(self, index, value, role=qtc.Qt.EditRole):
        """Update value to edited value"""
        if self.editable:
            if index.isValid():
                row = index.row()
                col = index.column()
                self._dataframe.iloc[row][col] = str(value)
                self.dataChanged.emit(index, index, (qtc.Qt.DisplayRole,))
                # self.setDataFrame(dataframe=self._dataframe)
                # print(f"changed {value}")
                return True
        return False

    def roleNames(self):
        roles = {
            qtc.Qt.DisplayRole: b'display',
            DataFrameModel.DtypeRole: b'dtype',
            DataFrameModel.ValueRole: b'value'
        }
        return roles

    def flags(self, index):
        if self.editable:
            return qtc.Qt.ItemIsEnabled | qtc.Qt.ItemIsSelectable | qtc.Qt.ItemIsEditable
        else:
            return qtc.Qt.ItemIsEnabled | qtc.Qt.ItemIsSelectable


def add_to_grid(layout, row, col, rowspan, colspan):
    frm, table_view = buildTab.make_tableview_in_frame()
    layout.addWidget(frm, row, col, rowspan, colspan)
    return table_view
