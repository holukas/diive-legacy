from gui import elements


class TopMenu:
    """ Create dropdown menu at the top. """

    def __init__(self, main_win, ctx):
        self.ctx = ctx
        self.mainMenu = main_win.menuBar()

        # File
        self.btn_fileImportSettings, self.btn_fileAddFile, self.btn_fileLoadNewFile, self.btn_fileOutputDir = \
            self.file()

        # Plots
        self.btn_plotsCorrMatrix, self.btn_plotsHexbins, self.btn_plotsMultiPanel, self.btn_plotsScatter, \
        self.btn_plotsWindSectors, self.btn_plotsCumulative, self.btn_plotsHeatmap, \
        self.btn_plotsHistogram, self.btn_plotsQuantiles, self.btn_plotsDielCycles = \
            self.plots()

        # Outlier detection
        self.btn_outlierDetDouble_Diff, self.btn_outlierDetIQR, self.btn_outlierRunning, \
        self.btn_outlierDetAbsLim, self.btn_outlierTrim, \
        self.btn_outlierDetHampelFilter = \
            self.outlier_detection()

        # Analyses
        self.btn_analysesAggregator, self.btn_analysesClassFinder, self.btn_analysesGapFinder, \
        self.btn_analysesFeatureSelection = \
            self.analyses()

        # Gap-filling
        self.btn_gapfillingRandomForest, self.btn_gapfillingLookupTable, self.btn_gapfillingSimpleRunning = \
            self.gap_filling()

        # Modifications
        self.btn_modificationsSpanCorrection, self.btn_modificationsLimitDatasetTimeRange, \
        self.btn_modificationsRenameVar, self.btn_modificationsRemoveTimeRange, \
        self.btn_modificationsSubset = \
            self.modifications()

        # Create variable
        self.btn_createVariableDefineSeasons, self.btn_createVariableAddNewEvent, \
        self.btn_createVariableTimeSince, self.btn_createVariableCombineColumns, \
        self.btn_createVariableLagFeatures, self.btn_createVariableApplyGain, \
        self.btn_createVariableBinning = \
            self.create_variable()

        # Eddy covariance quality control
        self.btn_EddyCovarianceQualityControl = \
            self.eddy_covariance()

        # Export
        self.btn_exportExportDataset = \
            self.export()

    def eddy_covariance(self):
        menu = self.mainMenu.addMenu('Eddy Covariance')
        btn_EddyCovarianceQualityControl = elements.menu_action(txt='Flux Quality Control', menu=menu,
                                                                icon=self.ctx.icon_open_in_new_tab)
        return btn_EddyCovarianceQualityControl

    def file(self):
        """ File menu """
        menu = self.mainMenu.addMenu('&File')
        btn_fileLoadNewFile = elements.menu_action(txt='Load New File ...', menu=menu,
                                                   icon=False)
        btn_fileAddFile = elements.menu_action(txt='Add File ...', menu=menu,
                                               icon=self.ctx.icon_btn_menu_add)
        btn_fileOutputDir = elements.menu_action(txt='Output Directory ...', menu=menu,
                                                 icon=self.ctx.icon_btn_menu_settings)
        btn_fileImportSettings = elements.menu_action(txt='File Import Settings ...', menu=menu,
                                                      icon=self.ctx.icon_btn_menu_settings)
        return btn_fileImportSettings, btn_fileAddFile, btn_fileLoadNewFile, btn_fileOutputDir

    def plots(self):
        """ plots menu """
        menu = self.mainMenu.addMenu('Plots')
        btn_plotsCorrMatrix = elements.menu_action(txt='Correlation Matrix', menu=menu,
                                                   icon=self.ctx.icon_open_in_new_tab)
        btn_plotsCumulative = elements.menu_action(txt='Cumulative', menu=menu,
                                                   icon=self.ctx.icon_open_in_new_tab)
        btn_plotsDielCycles = elements.menu_action(txt='Diel Cycles', menu=menu,
                                                   icon=self.ctx.icon_open_in_new_tab)
        btn_plotsHeatmap = elements.menu_action(txt='Heatmap', menu=menu,
                                                icon=self.ctx.icon_open_in_new_tab)
        btn_plotsHexbins = elements.menu_action(txt='Hexbins', menu=menu,
                                                icon=self.ctx.icon_open_in_new_tab)
        btn_plotsHistogram = elements.menu_action(txt='Histogram', menu=menu,
                                                  icon=self.ctx.icon_open_in_new_tab)
        btn_plotsMultiPanel = elements.menu_action(txt='Multipanel', menu=menu,
                                                   icon=self.ctx.icon_open_in_new_tab)
        btn_plotsQuantiles = elements.menu_action(txt='Quantiles', menu=menu,
                                                  icon=self.ctx.icon_open_in_new_tab)
        btn_plotsScatter = elements.menu_action(txt='Scatter', menu=menu,
                                                icon=self.ctx.icon_open_in_new_tab)
        # btn_plotsUstarThresholdsFXN = gui_elements.menu_action(txt='u* Thresholds (FXN)', menu=menu,
        #                                                        icon=self.ctx.icon_open_in_new_tab)
        btn_plotsWindSectors = elements.menu_action(txt='Wind Sectors', menu=menu,
                                                    icon=self.ctx.icon_open_in_new_tab)
        return btn_plotsCorrMatrix, btn_plotsHexbins, btn_plotsMultiPanel, btn_plotsScatter, \
               btn_plotsWindSectors, btn_plotsCumulative, btn_plotsHeatmap, \
               btn_plotsHistogram, btn_plotsQuantiles, btn_plotsDielCycles
        # return btn_plotsCorrMatrix, btn_plotsHexbins, btn_plotsMultiPanel, btn_plotsScatter, \
        #        btn_plotsUstarThresholdsFXN, btn_plotsWindSectors, btn_plotsCumulative, btn_plotsHeatmap, \
        #        btn_plotsHistogram, btn_plotsQuantiles

    def analyses(self):
        """ Analyses menu """
        menu = self.mainMenu.addMenu('Analyses')
        btn_analysesAggregator = elements.menu_action(txt='Aggregator', menu=menu,
                                                      icon=self.ctx.icon_open_in_new_tab)
        btn_analysesClassFinder = elements.menu_action(txt='Class Finder', menu=menu,
                                                       icon=self.ctx.icon_open_in_new_tab)
        btn_analysesFeatureSelection = elements.menu_action(txt='Feature Selection', menu=menu,
                                                            icon=self.ctx.icon_open_in_new_tab)
        btn_analysesGapFinder = elements.menu_action(txt='Gap Finder', menu=menu,
                                                     icon=self.ctx.icon_open_in_new_tab)
        return btn_analysesAggregator, btn_analysesClassFinder, btn_analysesGapFinder, btn_analysesFeatureSelection

    def gap_filling(self):
        menu = self.mainMenu.addMenu('Gap-filling')
        btn_gapfillingLookupTable = elements.menu_action(txt='Lookup Table',
                                                         menu=menu, icon=self.ctx.icon_open_in_new_tab)
        btn_gapfillingRandomForest = elements.menu_action(txt='Random Forest',
                                                          menu=menu, icon=self.ctx.icon_open_in_new_tab)
        btn_gapfillingSimpleRunning = elements.menu_action(txt='Simple Running',
                                                           menu=menu, icon=self.ctx.icon_open_in_new_tab)
        return btn_gapfillingRandomForest, btn_gapfillingLookupTable, btn_gapfillingSimpleRunning

    def modifications(self):
        menu = self.mainMenu.addMenu('Modifications')
        btn_modificationsLimitDatasetTimeRange = elements.menu_action(txt='Limit Dataset Time Range',
                                                                      menu=menu, icon=self.ctx.icon_btn_new_window)
        btn_modificationsRemoveTimeRange = elements.menu_action(txt='Remove Time Range',
                                                                menu=menu, icon=self.ctx.icon_open_in_new_tab)
        btn_modificationsRenameVar = elements.menu_action(txt='Rename Variables',
                                                          menu=menu, icon=self.ctx.icon_open_in_new_tab)
        btn_modificationsSpanCorrection = elements.menu_action(txt='Span Correction',
                                                               menu=menu,
                                                               icon=self.ctx.icon_open_in_new_tab)
        btn_modificationsSubset = elements.menu_action(txt='Subset',
                                                       menu=menu, icon=self.ctx.icon_open_in_new_tab)
        return btn_modificationsSpanCorrection, btn_modificationsLimitDatasetTimeRange, \
               btn_modificationsRenameVar, \
               btn_modificationsRemoveTimeRange, btn_modificationsSubset

    def create_variable(self):
        menu = self.mainMenu.addMenu('Create Variable')
        btn_modificationsAddNewEvent = elements.menu_action(txt='Add New Event',
                                                            menu=menu, icon=self.ctx.icon_btn_new_window)
        btn_createVariableApplyGain = elements.menu_action(txt='Apply Gain',
                                                           menu=menu,
                                                           icon=self.ctx.icon_open_in_new_tab)
        btn_createVariableBinning = elements.menu_action(txt='Binning',
                                                         menu=menu, icon=self.ctx.icon_open_in_new_tab)
        btn_createVariableCombineColumns = elements.menu_action(txt='Combine Columns',
                                                                menu=menu,
                                                                icon=self.ctx.icon_open_in_new_tab)
        btn_createVariableDefineSeasons = elements.menu_action(txt='Define Seasons',
                                                               menu=menu,
                                                               icon=self.ctx.icon_open_in_new_tab)
        btn_createVariableLagFeatures = elements.menu_action(txt='Lag Features',
                                                             menu=menu,
                                                             icon=self.ctx.icon_open_in_new_tab)
        btn_createVariableTimeSince = elements.menu_action(txt='Time Since',
                                                           menu=menu,
                                                           icon=self.ctx.icon_open_in_new_tab)
        return btn_createVariableDefineSeasons, btn_modificationsAddNewEvent, btn_createVariableTimeSince, \
               btn_createVariableCombineColumns, btn_createVariableLagFeatures, \
               btn_createVariableApplyGain, btn_createVariableBinning

    def export(self):
        menu = self.mainMenu.addMenu('Export')
        btn_exportExportDataset = elements.menu_action(txt='Export Dataset',
                                                       menu=menu,
                                                       icon=self.ctx.icon_btn_new_window)
        return btn_exportExportDataset

    def outlier_detection(self):
        menu = self.mainMenu.addMenu('Outlier Detection')
        btn_outlierDetAbsLim = elements.menu_action(txt='Absolute Limits',
                                                    menu=menu,
                                                    icon=self.ctx.icon_open_in_new_tab)
        btn_outlierDetDouble_Diff = elements.menu_action(txt='Double-differenced Time Series',
                                                         menu=menu,
                                                         icon=self.ctx.icon_open_in_new_tab)
        btn_outlierDetHampelFilter = elements.menu_action(txt='Hampel Filter',
                                                          menu=menu,
                                                          icon=self.ctx.icon_open_in_new_tab)
        btn_outlierDetIQR = elements.menu_action(txt='Interquartile Range',
                                                 menu=menu,
                                                 icon=self.ctx.icon_open_in_new_tab)
        btn_outlierRunning = elements.menu_action(txt='Running',
                                                  menu=menu,
                                                  icon=self.ctx.icon_open_in_new_tab)
        btn_outlierTrim = elements.menu_action(txt='Trim',
                                               menu=menu,
                                               icon=self.ctx.icon_open_in_new_tab)

        return btn_outlierDetDouble_Diff, btn_outlierDetIQR, btn_outlierRunning, btn_outlierDetAbsLim, \
               btn_outlierTrim, btn_outlierDetHampelFilter

# def get(self):
#     return self.mainMenu
