"""
Convenience functions for the gui

adding buttons, lists, CSS classes, ...
"""
from PyQt5 import QtCore as qtc
from PyQt5 import QtWidgets as qtw
from PyQt5.QtGui import QDoubleValidator
from PyQt5.QtGui import QIcon


def menu_action(txt, menu, icon):
    """
    Add button to top menu.

    Actions can be added to menuBar, but buttons cannot be added. Therefore, first
    add action, and then set the button as default widget for that action. Action
    needs to be returned so it can be added to the menuBar, button needs to be
    returned so it can be connected to a method. The button can be styled using CSS,
    while the action cannot be styled (as far as I know, I read it somewhere).

    :param txt: str that is shown on the button
    :param menu: menuBar()
    :return: action and button objects
    """
    action = qtw.QWidgetAction(menu)
    button = qtw.QPushButton(txt)
    if icon:
        button.setIcon(QIcon(icon))
    # button.setToolTip('Look, a tooltip!')
    button.setProperty('labelClass', 'top_menu_dropdown_item')
    action.setDefaultWidget(button)
    menu.addAction(action)

    # button = qw.QPushButton(text=txt)
    # from PyQt5.QtGui import QIcon
    # button.setIcon(QIcon(icon))

    # grid.addWidget(button, row, col, rowspan, colspan)

    return button


def add_frame_grid():
    frame = qtw.QFrame()
    frame.setContentsMargins(0, 0, 0, 0)
    frame.setProperty('labelClass', '')
    grid = qtw.QGridLayout()
    grid.setSpacing(5)
    frame.setLayout(grid)
    return frame, grid


def add_button_to_grid(grid_layout, txt, css_id, row, col, rowspan, colspan):
    button = qtw.QPushButton(text=txt)
    button.setProperty('labelClass', css_id)
    # button.setToolTip('Look, a tooltip!')
    grid_layout.addWidget(button, row, col, rowspan, colspan)
    return button


def add_simplebutton_to_grid(grid_layout, txt, row, col, rowspan, colspan):
    button = qtw.QPushButton(text=txt)
    button.setProperty('labelClass', 'btn_red')
    # button.setToolTip('Look, a tooltip!')
    grid_layout.addWidget(button, row, col, rowspan, colspan)
    return button


def add_simplebutton_to_layout(layout, txt, style):
    if style == 'red':
        style = 'btn_red'
    elif style == 'teal':
        style = 'btn_teal'
    button = qtw.QPushButton(text=txt)
    button.setProperty('labelClass', style)
    # button.setToolTip('Look, a tooltip!')
    layout.addWidget(button)
    return button


def add_sublayout_to_grid(grid_layout, row, col, rowspan, colspan):
    frame = qtw.QFrame()
    layout_horiz = qtw.QHBoxLayout()
    frame.setLayout(layout_horiz)
    grid_layout.addWidget(frame, row, col, rowspan, colspan)
    return layout_horiz


def add_iconbutton_to_grid(grid_layout, txt, css_id, row, col, rowspan, colspan, icon):
    button = qtw.QPushButton(text=txt)
    button.setProperty('labelClass', css_id)

    button.setIcon(QIcon(icon))
    # button.setToolTip('Look, a tooltip!')
    grid_layout.addWidget(button, row, col, rowspan, colspan)
    return button


def grd_Label(lyt, txt, css_id, row, col, rowspan, colspan):
    lbl = qtw.QLabel(text=txt)
    lbl.setProperty('labelClass', css_id)
    lyt.addWidget(lbl, row, col, rowspan, colspan)
    return lbl


def add_button_to_layout(layout, txt, css_id):
    button = qtw.QPushButton(text=txt)
    button.setProperty('labelClass', css_id)
    layout.addWidget(button)
    return button


def add_label_to_layout(txt, css_id, layout):
    lbl = qtw.QLabel(text=txt)
    lbl.setProperty('labelClass', css_id)
    layout.addWidget(lbl, stretch=0)
    return lbl


def add_infobox_to_layout(txt, css_id, layout):
    infobox = qtw.QTextEdit()
    # infobox.setAcceptRichText(True)
    infobox.setReadOnly(True)
    infobox.setProperty('labelClass', css_id)
    infobox.setText(txt)
    # infobox.setHtml(txt)
    layout.addWidget(infobox, stretch=0)
    return infobox


def add_infobox_to_grid(txt, css_id, grid_layout, row, col, rowspan, colspan):
    infobox = qtw.QTextEdit()
    # infobox.setAcceptRichText(True)
    infobox.setReadOnly(True)
    infobox.setProperty('labelClass', css_id)
    infobox.setText(txt)
    # infobox.setHtml(txt)
    grid_layout.addWidget(infobox, row, col, rowspan, colspan)
    return infobox


def add_lineedit_to_layout(css_id, layout):
    lne_val = qtw.QLineEdit()
    lne_val.setProperty('labelClass', css_id)
    layout.addWidget(lne_val)
    return lne_val


def add_label_pair_to_grid_layout(txt, css_ids, layout, row, col, orientation):
    # Adds two labels to a grid lyt_CategoryOptions: one for the label text, one for the value.
    # Returns the id for the value label so it can later be updated with real values.
    lbl_txt = qtw.QLabel(text=txt)
    lbl_val = qtw.QLabel(text='-')
    lbl_txt.setProperty('labelClass', css_ids[0])
    lbl_val.setProperty('labelClass', css_ids[1])
    # lbl_val.setMaximumSize(100, 10)

    if orientation == 'horiz':
        lbl_txt.setAlignment(qtc.Qt.AlignRight | qtc.Qt.AlignVCenter)
        lbl_val.setAlignment(qtc.Qt.AlignLeft | qtc.Qt.AlignVCenter)
        layout.addWidget(lbl_txt, row, col, 1, 1)
        layout.addWidget(lbl_val, row, col + 1, 1, 1)

    elif orientation == 'vert':
        lbl_txt.setAlignment(qtc.Qt.AlignLeft | qtc.Qt.AlignVCenter)
        lbl_val.setAlignment(qtc.Qt.AlignLeft | qtc.Qt.AlignVCenter)
        layout.addWidget(lbl_txt, row, col, 1, 1)
        layout.addWidget(lbl_val, row + 1, col, 1, 1)

    return lbl_val


def add_textedit_to_grid(layout, row, col, rowspan, colspan):
    frm_textbox = qtw.QFrame()
    lyt_textbox = qtw.QVBoxLayout()
    frm_textbox.setLayout(lyt_textbox)
    txe_textbox = qtw.QTextEdit()
    # txe_textbox.setText("/"*1000)
    lyt_textbox.addWidget(txe_textbox)
    layout.addWidget(frm_textbox, row, col, rowspan, colspan)
    return txe_textbox


def add_label_triple_linedit_to_grid(txt, css_ids, layout, row, col, orientation):
    # Adds two labels to a grid lyt_CategoryOptions: one for the label text, one for the value.
    # Returns the id for the value label so it can later be updated with real values.

    lbl_txt = qtw.QLabel(text=txt)
    lbl_txt.setProperty('labelClass', css_ids[0])
    lbl_txt.setAlignment(qtc.Qt.AlignRight | qtc.Qt.AlignVCenter)
    layout.addWidget(lbl_txt, row, col, 1, 1)

    sub_frame = qtw.QFrame()
    sub_lyt = qtw.QHBoxLayout()
    sub_frame.setLayout(sub_lyt)

    lne_val_1 = qtw.QLineEdit()
    lne_val_2 = qtw.QLineEdit()
    lne_val_3 = qtw.QLineEdit()

    onlyDouble = QDoubleValidator()  # Allow only doubles as input (floats, integers, no text)
    lne_val_1.setValidator(onlyDouble)
    lne_val_2.setValidator(onlyDouble)
    lne_val_3.setValidator(onlyDouble)

    lne_val_1.setProperty('labelClass', css_ids[1])
    lne_val_2.setProperty('labelClass', css_ids[1])
    lne_val_3.setProperty('labelClass', css_ids[1])

    lne_val_1.setAlignment(qtc.Qt.AlignLeft | qtc.Qt.AlignVCenter)
    lne_val_2.setAlignment(qtc.Qt.AlignLeft | qtc.Qt.AlignVCenter)
    lne_val_3.setAlignment(qtc.Qt.AlignLeft | qtc.Qt.AlignVCenter)

    # sub_lyt.addWidget(lbl_txt)
    sub_lyt.addWidget(lne_val_1)
    sub_lyt.addWidget(lne_val_2)
    sub_lyt.addWidget(lne_val_3)

    layout.addWidget(sub_frame, row, col + 1, 1, 1)

    return lne_val_1, lne_val_2, lne_val_3


def add_label_dropdown_info_triplet_to_grid(txt, css_ids, layout, row, col, orientation, txt_info_hover, colspan=1):
    lbl_txt = qtw.QLabel(text=txt)
    drp_options = qtw.QComboBox()
    drp_options.setMaxVisibleItems(30)
    lbl_txt.setProperty('labelClass', css_ids[0])
    drp_options.setProperty('labelClass', css_ids[1])

    lbl_info = qtw.QLabel('🛈')
    lbl_info.setProperty('labelClass', 'lbl_info_hover')
    lbl_info.setToolTip(txt_info_hover)

    if orientation == 'horiz':
        lbl_txt.setAlignment(qtc.Qt.AlignRight | qtc.Qt.AlignVCenter)
        layout.addWidget(lbl_txt, row, col, 1, 1)
        layout.addWidget(drp_options, row, col + 1, 1, colspan)
        layout.addWidget(lbl_info, row, col + 2, 1, colspan)

    # elif orientation == 'vert':
    #     lbl_txt.setAlignment(qtc.Qt.AlignLeft | qtc.Qt.AlignVCenter)
    #     drp_opt_default_epqc.setAlignment(qtc.Qt.AlignLeft | qtc.Qt.AlignVCenter)
    #     lyt.addWidget(lbl_txt, row, col, 1, 1)
    #     lyt.addWidget(drp_opt_default_epqc, row + 1, col, 1, 1)

    return drp_options


def add_label_linedit_pair_to_grid(txt, css_ids, layout, row, col, orientation):
    # Adds two labels to a grid lyt_CategoryOptions: one for the label text, one for the value.
    # Returns the id for the value label so it can later be updated with real values.
    lbl_txt = qtw.QLabel(text=txt)
    lne_val = qtw.QLineEdit()
    lbl_txt.setProperty('labelClass', css_ids[0])
    lne_val.setProperty('labelClass', css_ids[1])

    if orientation == 'horiz':
        lbl_txt.setAlignment(qtc.Qt.AlignRight | qtc.Qt.AlignVCenter)
        lne_val.setAlignment(qtc.Qt.AlignLeft | qtc.Qt.AlignVCenter)
        layout.addWidget(lbl_txt, row, col, 1, 1)
        layout.addWidget(lne_val, row, col + 1, 1, 1)
        layout.setColumnStretch(0, 0)
        layout.setColumnStretch(1, 1)

    elif orientation == 'vert':
        lbl_txt.setAlignment(qtc.Qt.AlignCenter | qtc.Qt.AlignVCenter)
        lne_val.setAlignment(qtc.Qt.AlignCenter | qtc.Qt.AlignVCenter)
        layout.addWidget(lbl_txt, row, col, 1, 1)
        layout.addWidget(lne_val, row + 1, col, 1, 1)
        layout.setColumnStretch(0, 1)
        layout.setColumnStretch(1, 1)

    return lne_val


def add_label_linedit_info_triplet_to_grid(txt, css_ids, layout, row, col, orientation, txt_info_hover):
    # Adds two labels to a grid lyt_CategoryOptions: one for the label text, one for the value.
    # Returns the id for the value label so it can later be updated with real values.
    lbl_txt = qtw.QLabel(text=txt)
    lne_val = qtw.QLineEdit()
    lbl_txt.setProperty('labelClass', css_ids[0])
    lne_val.setProperty('labelClass', css_ids[1])

    lbl_info = qtw.QLabel('🛈')
    lbl_info.setProperty('labelClass', 'lbl_info_hover')
    lbl_info.setToolTip(txt_info_hover)
    # pixmap = QPixmap(infopic)
    # pixmap = pixmap.scaledToWidth(10)
    # lbl_image.setPixmap(pixmap)

    if orientation == 'horiz':
        lbl_txt.setAlignment(qtc.Qt.AlignRight | qtc.Qt.AlignVCenter)
        lne_val.setAlignment(qtc.Qt.AlignLeft | qtc.Qt.AlignVCenter)
        layout.addWidget(lbl_txt, row, col, 1, 1)
        layout.addWidget(lne_val, row, col + 1, 1, 1)
        layout.addWidget(lbl_info, row, col + 2, 1, 1)

    # elif orientation == 'vert':
    #     lbl_txt.setAlignment(qtc.Qt.AlignLeft | qtc.Qt.AlignVCenter)
    #     lne_val.setAlignment(qtc.Qt.AlignLeft | qtc.Qt.AlignVCenter)
    #     layout.addWidget(lbl_txt, row, col, 1, 1)
    #     layout.addWidget(lne_val, row + 1, col, 1, 1)

    layout.setColumnStretch(0, 0)
    layout.setColumnStretch(1, 1)

    return lne_val


def grd_LabelDateEditPair(layout, row, col, css_ids, txt):
    lbl_txt = qtw.QLabel(text=txt)
    dte_date = qtw.QDateEdit()
    # dte_date = qtw.QDateEdit(calendarPopup=True)
    lbl_txt.setProperty('labelClass', css_ids[0])
    dte_date.setProperty('labelClass', css_ids[1])
    dte_date.setAlignment(qtc.Qt.AlignCenter | qtc.Qt.AlignVCenter)

    lbl_txt.setAlignment(qtc.Qt.AlignRight | qtc.Qt.AlignVCenter)
    layout.addWidget(lbl_txt, row, col, 1, 1)
    layout.addWidget(dte_date, row, col + 1, 1, 1)

    # date_edit.setCalendarPopup(True)
    # date_edit.calendarWidget().installEventFilter()
    return dte_date


def grd_LabelDateTimePickerPair(layout, row, col, css_ids, txt):
    lbl_txt = qtw.QLabel(text=txt)
    lbl_txt.setProperty('labelClass', css_ids[0])
    lbl_txt.setAlignment(qtc.Qt.AlignRight | qtc.Qt.AlignVCenter)
    dtp_datetimepicker = qtw.QDateTimeEdit()
    dtp_datetimepicker.setProperty('labelClass', css_ids[1])
    dtp_datetimepicker.setAlignment(qtc.Qt.AlignCenter | qtc.Qt.AlignVCenter)
    layout.addWidget(lbl_txt, row, col, 1, 1)
    layout.addWidget(dtp_datetimepicker, row, col + 1, 1, 1)
    return dtp_datetimepicker


def grd_LabelLineeditDropdownTriplet(txt, css_ids, layout, row, col, orientation):
    # Adds two labels to a grid lyt_CategoryOptions: one for the label text, one for the value.
    # Returns the id for the value label so it can later be updated with real values.
    lbl_txt = qtw.QLabel(text=txt)
    lne_duration = qtw.QLineEdit()
    drp_freq = qtw.QComboBox()

    lbl_txt.setProperty('labelClass', css_ids[0])
    lne_duration.setProperty('labelClass', css_ids[1])
    drp_freq.setProperty('labelClass', css_ids[2])

    if orientation == 'horiz':
        lbl_txt.setAlignment(qtc.Qt.AlignRight | qtc.Qt.AlignVCenter)
        layout.addWidget(lbl_txt, row, col, 1, 1)
        layout.addWidget(lne_duration, row, col + 1, 1, 1)
        layout.addWidget(drp_freq, row, col + 2, 1, 1)

    return lne_duration, drp_freq


def grd_add_dropdown(layout, row, col, rowspan=1, colspan=1, css_id='cyan'):
    drp_options = qtw.QComboBox()
    drp_options.setMaxVisibleItems(30)
    drp_options.setProperty('labelClass', css_id)
    layout.addWidget(drp_options, row, col, rowspan, colspan)
    return drp_options


def add_subgrid_drp_lne_to_grid(add_to_layout, row, col, lbl_flag_a, lbl_drp):
    frame = qtw.QFrame()
    grid = qtw.QGridLayout()
    frame.setLayout(grid)

    drp = grd_LabelDropdownPair(txt=lbl_drp, css_ids=['', ''], layout=grid,
                                row=0, col=0, orientation='vert')
    lne_flag_a_lim = add_label_linedit_pair_to_grid(txt=lbl_flag_a, css_ids=['', 'cyan'], layout=grid,
                                                    row=0, col=1, orientation='vert')

    add_to_layout.addWidget(frame, row, col)
    return drp, lne_flag_a_lim


def add_one_subgrid_lne_to_grid(add_to_layout, row, col, lbl_flag_a):
    frame = qtw.QFrame()
    grid = qtw.QGridLayout()
    frame.setLayout(grid)

    lne_flag_a_lim = add_label_linedit_pair_to_grid(txt=lbl_flag_a, css_ids=['', 'cyan'], layout=grid,
                                                    row=0, col=0, orientation='vert')

    add_to_layout.addWidget(frame, row, col)
    return lne_flag_a_lim


def add_two_subgrid_lnes_to_grid(add_to_layout, row, col, lbl_flag_a, lbl_flag_b):
    frame = qtw.QFrame()
    grid = qtw.QGridLayout()
    frame.setLayout(grid)

    lne_flag_a_lim = add_label_linedit_pair_to_grid(txt=lbl_flag_a, css_ids=['', 'cyan'], layout=grid,
                                                    row=0, col=0, orientation='vert')
    lne_flag_b_lim = add_label_linedit_pair_to_grid(txt=lbl_flag_b, css_ids=['', 'cyan'], layout=grid,
                                                    row=0, col=1, orientation='vert')

    add_to_layout.addWidget(frame, row, col)
    return lne_flag_a_lim, lne_flag_b_lim


def grd_LabelDropdownPair(txt, css_ids, layout, row, col, orientation, colspan=1):
    # Adds two labels to a grid lyt_CategoryOptions: one for the label text, one for the value.
    # Returns the id for the value label so it can later be updated with real values.
    lbl_txt = qtw.QLabel(text=txt)
    drp_options = qtw.QComboBox()
    drp_options.setMaxVisibleItems(30)

    lbl_txt.setProperty('labelClass', css_ids[0])
    drp_options.setProperty('labelClass', css_ids[1])

    if orientation == 'horiz':
        lbl_txt.setAlignment(qtc.Qt.AlignRight | qtc.Qt.AlignVCenter)
        # drp_options.setAlignment(qtc.Qt.AlignLeft | qtc.Qt.AlignVCenter)

        # drp_opt_default_epqc.setAlignment(qtc.Qt.AlignLeft | qtc.Qt.AlignVCenter)
        layout.addWidget(lbl_txt, row, col, 1, 1)
        layout.addWidget(drp_options, row, col + 1, 1, colspan)

    elif orientation == 'vert':
        lbl_txt.setAlignment(qtc.Qt.AlignLeft | qtc.Qt.AlignVCenter)
        layout.addWidget(lbl_txt, row, col, 1, 1)
        layout.addWidget(drp_options, row + 1, col, 1, colspan)
        # drp_opt_default_epqc.setAlignment(qtc.Qt.AlignLeft | qtc.Qt.AlignVCenter)
        # lyt.addWidget(lbl_txt, row, col, 1, 1)
        # lyt.addWidget(drp_opt_default_epqc, row + 1, col, 1, 1)

    return drp_options


def grd_LabelDropdownPair_dropEnabled(txt, css_ids, layout, row, col, orientation):
    """
        * Accepts drops
        * Adds two labels to a grid lyt_CategoryOptions: one for the label text, one for the value.
        * Returns the id for the value label so it can later be updated with real values.

    """
    lbl_txt = qtw.QLabel(text=txt)
    drp_options = ComboBox_dropEnabled()
    # drp_options = qw.QComboBox()
    # drp_options.setAcceptDrops(True)

    lbl_txt.setProperty('labelClass', css_ids[0])
    drp_options.setProperty('labelClass', css_ids[1])

    if orientation == 'horiz':
        lbl_txt.setAlignment(qtc.Qt.AlignRight | qtc.Qt.AlignVCenter)
        layout.addWidget(lbl_txt, row, col, 1, 1)
        layout.addWidget(drp_options, row, col + 1, 1, 1)

    return drp_options


class ComboBox_dropEnabled(qtw.QComboBox):
    # todo testing drag and drop from variable list to here
    # todo https://stackoverflow.com/questions/22774168/how-to-drag-and-drop-from-listwidget-onto-combobox
    def __init__(self):
        self.model_mime_type = 'application/x-qabstractitemmodeldatalist'  ## format from variable list
        super(ComboBox_dropEnabled, self).__init__()
        self.setAcceptDrops(True)
        self.setMaxVisibleItems(30)

    def dragEnterEvent(self, event):
        if event.mimeData().hasFormat(self.model_mime_type) or event.mimeData().hasFormat('text/plain'):
            event.accept()
        else:
            event.ignore()
        # print("formats: ", event.mimeData().formats())
        # if event.mimeData().hasFormat("application/x-qabstractitemmodeldatalist"):  ## this format is from var list
        #     event.acceptProposedAction()

    def dropEvent(self, event):
        if event.mimeData().hasUrls():
            event.setDropAction(qtc.Qt.CopyAction)
            event.accept()
            links = []
            for url in event.mimeData().urls():
                links.append(str(url.toLocalFile()))
            # self.emit(qtc.SIGNAL("dropped"), links)
        else:
            super(ComboBox_dropEnabled, self).dropEvent(event)
            # self.emit(qtc.SIGNAL("dropped"))


def grd_LabelDropdownPairRowSpan(txt, css_ids, lyt, row, col, orientation, rowspan):
    # Adds two labels to a grid lyt_CategoryOptions: one for the label text, one for the value.
    # Returns the id for the value label so it can later be updated with real values.
    lbl_txt = qtw.QLabel(text=txt)
    drp_options = qtw.QComboBox()

    lbl_txt.setProperty('labelClass', css_ids[0])
    drp_options.setProperty('labelClass', css_ids[1])

    if orientation == 'horiz':
        lbl_txt.setAlignment(qtc.Qt.AlignRight | qtc.Qt.AlignVCenter)
        # drp_options.setAlignment(qtc.Qt.AlignLeft | qtc.Qt.AlignVCenter)

        # drp_opt_default_epqc.setAlignment(qtc.Qt.AlignLeft | qtc.Qt.AlignVCenter)
        lyt.addWidget(lbl_txt, row, col, 1, 1)
        lyt.addWidget(drp_options, row, col + 1, rowspan, 1)

    # elif orientation == 'vert':
    #     lbl_txt.setAlignment(qtc.Qt.AlignLeft | qtc.Qt.AlignVCenter)
    #     drp_opt_default_epqc.setAlignment(qtc.Qt.AlignLeft | qtc.Qt.AlignVCenter)
    #     lyt.addWidget(lbl_txt, row, col, 1, 1)
    #     lyt.addWidget(drp_opt_default_epqc, row + 1, col, 1, 1)

    return drp_options


def add_list_to_layout(layout, add_empty_string=True):
    # list = ListDragEnabled()
    list = qtw.QListWidget()
    list.setDragEnabled(True)
    list.setObjectName("list")
    if add_empty_string:
        list.addItem("-empty-")
    layout.addWidget(list)
    return list


class ListDragEnabled(qtw.QListWidget):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.setDragEnabled(True)
        # self.setSelectionMode(self.ExtendedSelection)  ## for multiple selections
        # self.setAcceptDrops(True)


def update_content_info(df, element):
    info_str = ''
    for c in df.columns:
        info_str += c[0] + ' / '
    element.setText('{}'.format(info_str))


def btn_txt_live_update(btn, txt, perc):
    # btn.setStyleSheet("background-color: qlineargradient("
    #                   "x1: 0, y1: 0, x2: {p}, y2: {p}, stop: 0  # 55545f, stop: {p} #3d3c44)".format(p=perc))
    # btn.setStyleSheet("QPushButton { border: }")
    if perc == -9999:
        btn.setText("{t}".format(t=txt))
    else:

        btn.setText("[{p}%] {t}".format(t=txt, p=int(perc * 100)))

    qtw.QApplication.processEvents()
    return None


def add_header_to_grid_top(layout, txt):
    _header = grd_Label(lyt=layout,
                        txt=txt,
                        css_id='lbl_Header2',
                        row=0, col=0, rowspan=1, colspan=1)

    return None


def add_header_subheader_to_grid_top(layout, txt, row=0, col=0, rowspan=1, colspan=1):
    frame = qtw.QFrame()
    sub_layout = qtw.QVBoxLayout()
    frame.setLayout(sub_layout)
    frame.setContentsMargins(0, 0, 0, 0)
    sub_layout.setContentsMargins(0, 0, 0, 0)
    sub_layout.setSpacing(0)  # Important for zero margins

    lbl = qtw.QLabel(text=txt[0])
    lbl.setProperty('labelClass', 'lbl_tab_settings_header1')
    sub_layout.addWidget(lbl)
    lbl = qtw.QLabel(text=txt[1])
    lbl.setProperty('labelClass', 'lbl_tab_settings_subheader1')
    sub_layout.addWidget(lbl)

    layout.addWidget(frame, row, col, rowspan, colspan)


def add_header_in_grid_row(layout, txt, row, col=0, css_id='lbl_Header2', rowspan=1, colspan=1):
    _header = grd_Label(lyt=layout,
                        txt=txt,
                        css_id=css_id,
                        row=row, col=col, rowspan=rowspan, colspan=colspan)
    return None


def add_checkbox_to_grid(layout, row, col, rowspan=1, colspan=1):
    checkbox = qtw.QCheckBox()
    # checkbox.setAlignment(qtc.Qt.AlignRight | qtc.Qt.AlignVCenter)
    layout.addWidget(checkbox, row, col, rowspan, colspan)
    return checkbox


def add_label_checkbox_pair_to_grid(txt, css_id, layout, row, col, rowspan=1, colspan=1):
    # Checkbox label
    lbl = qtw.QLabel(text=txt)
    lbl.setProperty('labelClass', css_id)
    lbl.setAlignment(qtc.Qt.AlignLeft | qtc.Qt.AlignBaseline)

    # Checkbox
    checkbox = qtw.QCheckBox()
    # checkbox.setAlignment(qtc.Qt.AlignRight | qtc.Qt.AlignVCenter)

    layout.addWidget(lbl, row, col, rowspan, colspan)
    layout.addWidget(checkbox, row, col + 1, rowspan, colspan)
    return checkbox


def add_info_hover_to_grid(layout, txt_info_hover, row, col, rowspan=1, colspan=1):
    lbl_info = qtw.QLabel('🛈')
    lbl_info.setProperty('labelClass', 'lbl_info_hover')
    lbl_info.setToolTip(txt_info_hover)
    lbl_info.setAlignment(qtc.Qt.AlignRight | qtc.Qt.AlignBaseline)
    layout.addWidget(lbl_info, row, col, rowspan, colspan)


def add_header_info_pair_in_grid_row(layout, txt, row, col=0, colspan=1, css_id='lbl_Header2', txt_info_hover=""):
    # Header label
    lbl = qtw.QLabel(text=txt)
    lbl.setProperty('labelClass', css_id)
    lbl.setAlignment(qtc.Qt.AlignLeft | qtc.Qt.AlignBaseline)

    # Info label
    lbl_info = qtw.QLabel('🛈')
    lbl_info.setProperty('labelClass', 'lbl_info_hover')
    lbl_info.setToolTip(txt_info_hover)
    lbl_info.setAlignment(qtc.Qt.AlignRight | qtc.Qt.AlignBaseline)

    # Collect widgets in layout
    h_layout = qtw.QHBoxLayout()
    h_layout.setContentsMargins(0, 0, 0, 0)
    h_layout.setSpacing(0)
    h_layout.addWidget(lbl)
    h_layout.addWidget(lbl_info)

    # Put layout into frame
    frame = qtw.QFrame()
    frame.setLayout(h_layout)

    layout.addWidget(frame, row, col, 1, colspan)
    return None


def add_layout_dropdown_pair(label_txt, layout, max_visible_items, css_ids):
    # Dropdowns for time range
    lbl_txt = qtw.QLabel(text=label_txt)
    lbl_txt.setAlignment(qtc.Qt.AlignRight | qtc.Qt.AlignVCenter)
    drp_options = qtw.QComboBox()
    drp_options.setMaxVisibleItems(max_visible_items)
    lbl_txt.setProperty('labelClass', css_ids[0])
    drp_options.setProperty('labelClass', css_ids[1])
    layout.addWidget(lbl_txt)
    layout.addWidget(drp_options)
    return drp_options


