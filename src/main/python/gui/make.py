from PyQt5 import QtGui as qtg
from PyQt5 import QtWidgets as qtw


def qframe(css_id: str = None, min_width: int = 100, max_width: int = None):
    frame = qtw.QFrame()
    frame.setContentsMargins(0, 0, 0, 0)
    frame.setMinimumWidth(min_width)
    if css_id:
        frame.setProperty('labelClass', css_id)
    if max_width:
        frame.setMaximumWidth(max_width)
    return frame


def qlayout_vert(spacing: int = 0):
    layout = qtw.QVBoxLayout()
    layout.setContentsMargins(0, 0, 0, 0)
    layout.setSpacing(spacing)
    return layout


def qlayout_horiz(spacing: int = 0):
    layout = qtw.QHBoxLayout()
    layout.setContentsMargins(0, 0, 0, 0)
    layout.setSpacing(spacing)
    return layout


def qlayout_grid(spacing: int = 0):
    grid = qtw.QGridLayout()
    grid.setContentsMargins(0, 0, 0, 0)
    grid.setSpacing(spacing)
    return grid


def qlineedit(css_id: str = None, validator: str = None):
    """Create QLineEdit widget with specific properties"""
    lineedit = qtw.QLineEdit()
    if css_id:
        lineedit.setProperty('labelClass', css_id)
    _validator = None
    if validator == 'int':
        _validator = qtg.QIntValidator()
    elif validator == 'float':
        _validator = qtg.QDoubleValidator()
    if _validator:
        lineedit.setValidator(_validator)
    return lineedit


def qlist(add_empty_string=True):
    list = qtw.QListWidget()
    list.setDragEnabled(False)
    list.setObjectName("list")
    list.setWordWrap(False)
    list.setAlternatingRowColors(True)
    if add_empty_string:
        list.addItem("-empty-")
    return list


def qlabel(txt: str, css_id: str = None, max_width: int = None):
    label = qtw.QLabel(txt)
    label.setWordWrap(True)
    if css_id:
        label.setProperty('labelClass', css_id)
    if max_width:
        label.setMaximumWidth(max_width)
    return label


def qwidget(parent=None, css_id: str = None):
    widget = qtw.QWidget(parent)
    # widget.setObjectName("centralwidget")
    widget.setContentsMargins(0, 0, 0, 0)
    if css_id:
        widget.setAccessibleName(css_id)
    return widget


def qsplitter(stretch_left: int = 1, stretch_right: int = 1):
    splitter = qtw.QSplitter()
    splitter.setStretchFactor(stretch_left, stretch_right)
    return splitter


def qcombobox(max_visible_items, css_id):
    combobox = qtw.QComboBox()
    combobox.setMaxVisibleItems(max_visible_items)
    combobox.setProperty('labelClass', css_id)
    return combobox


def qstacked():
    qstacked = qtw.QStackedWidget()
    qstacked.setProperty('labelClass', '')
    qstacked.setContentsMargins(0, 0, 0, 0)
    return qstacked


def qspaceritem(width=10, height=10):
    spaceritem = qtw.QSpacerItem(width, height)
    return spaceritem


def qbutton(txt: str, css_id: str = None, tooltip: str = None):
    button = qtw.QPushButton(text=txt)
    if css_id:
        button.setProperty('labelClass', css_id)
    if tooltip:
        button.setToolTip(tooltip)
    return button
