import fnmatch

from PyQt5 import QtCore as qtc
from PyQt5 import QtGui as qtg
from PyQt5 import QtWidgets as qtw

import utils
from pkgs.dfun.frames import sort_multiindex_columns_names


def update_settings_fields(drp:qtw.QComboBox, dfcols, col_list_pretty, vargroup):
    """Add all variables to dropdown menu and pre-select entry if possible"""
    drp.clear()
    default_ix_found = False
    default_ix = 0
    for ix, colname_tuple in enumerate(dfcols):
        drp.addItem(col_list_pretty[ix])
        # Check if column appears in the global vars
        if any(fnmatch.fnmatch(colname_tuple[0], vid) for vid in vargroup):
            default_ix = ix if not default_ix_found else default_ix
            default_ix_found = True
    # Set dropdowns to found ix
    drp.setCurrentIndex(default_ix)


def update_varlist(df, ctx, lst_varlist):
    """Update main variable list"""

    df = sort_multiindex_columns_names(df=df, priority_vars=utils.vargroups.PRIORITY_VARS)

    col_dict_tuples = {}  # Full column names (tuples)
    col_list_pretty = []  # Pretty column names (string) for output in gui, used for referencing via index
    lst_varlist.clear()  # List in gui

    # Loop through data columns and assign index number
    for ix, colname_tuple in enumerate(df.columns):
        # Main list in left sidebar

        # Name of var shown in list
        # col_list_pretty = '{}: {}'.format(ix, colname_tuple[0])  # pretty string
        var_pretty = '{}: {}  {}'.format(ix, colname_tuple[0], colname_tuple[1])  # pretty string
        q = qtw.QListWidgetItem(var_pretty)

        # Set icon depending on type
        if str(colname_tuple[0]).startswith('.') \
                | ('#' in colname_tuple[0]) \
                | ('++' in colname_tuple[0]):
            q.setIcon(qtg.QIcon(ctx.icon_list_newvar))
        elif any(fnmatch.fnmatch(colname_tuple[0], flux_id) for flux_id in utils.vargroups.FLUXES_VARS):
            q.setIcon(qtg.QIcon(ctx.icon_flux))
        else:
            try:
                q.setIcon(qtg.QIcon(ctx.icon_var_default))
            except:
                print("--error--")

        lst_varlist.addItem(q)  # add column name to list

        # for each element in list, stores index in list and full column name (tuple)
        list_ix = ix
        col_dict_tuples[list_ix] = colname_tuple
        col_list_pretty.append(var_pretty)

    return df, lst_varlist, col_dict_tuples, col_list_pretty

    # # TODO alternative way of showing list items, html tags are possible:
    # widgitItem = qtw.QListWidgetItem()
    # widget = qtw.QWidget()
    # widgetText = qtw.QLabel(f'{ix}: <b>{colname_tuple[0]}</b>  {colname_tuple[1]}')
    # widgetLayout = qtw.QHBoxLayout()
    # widgetLayout.addWidget(widgetText)
    # widgetLayout.setSizeConstraint(qtw.QLayout.SetFixedSize)
    # widget.setLayout(widgetLayout)
    # self.lst_Variables.addItem(widgitItem)  # add column name to list
    # widgitItem.setSizeHint(widget.sizeHint())
    # self.lst_Variables.setItemWidget(widgitItem, widget)
    # # mylist.addItem(widgitItem)
    # # widgitItem.setSizeHint(widget.sizeHint())
    # # mylist.setItemWidget(widgitItem, widget)


def add_custom_item(text, tags: list, freq: str, filename, listwidget, tooltip):
    if 'OTHER' in tags:
        css = 'customListItemTagBlueGrey'
    elif 'DIIVE' in tags:
        css = 'customListItemTagGreen'
    elif 'EDDYPRO' in tags:
        css = 'customListItemTagAmber'
    elif 'REDDYPROC' in tags:
        css = 'customListItemTagCyan'
    elif 'TOA5' in tags:
        css = 'customListItemTagRed'
    elif 'FLUXNET' in tags:
        css = 'customListItemTagYellow'
    else:
        css = 'customListItemTagBlueGrey'

    item = qtw.QListWidgetItem()

    widget = qtw.QWidget()
    widget.setProperty('labelClass', 'customListItem')
    layout = qtw.QHBoxLayout()
    widget.setLayout(layout)

    for tag in tags:
        taglabel = qtw.QLabel(f'{tag}')
        taglabel.setProperty('labelClass', css)
        layout.addWidget(taglabel)

        freqlabel = qtw.QLabel(f'{freq}')
        freqlabel.setProperty('labelClass', css)
        layout.addWidget(freqlabel)

    label = qtw.QLabel(f'{text}')
    label.setProperty('labelClass', 'customListItemText')
    layout.addWidget(label)

    layout.setSizeConstraint(qtw.QLayout.SetFixedSize)
    listwidget.addItem(item)
    item.setData(qtc.Qt.UserRole, filename)
    item.setSizeHint(widget.sizeHint())
    item.setToolTip(tooltip)
    listwidget.setItemWidget(item, widget)

    return listwidget


def filter_var_list(lne_filter_varlist, lst_varlist, col_dict_tuples):
    # self.lst_Variables.row.clear()
    filter_txt = lne_filter_varlist.text()

    for row in range(lst_varlist.count()):
        lst_varlist.setRowHidden(row, False)

    if filter_txt != '':
        for row in range(lst_varlist.count()):
            # row_txt = str(self.lst_Variables.item(row).text())
            row_tuple = col_dict_tuples[row]
            search_term = '*{}*'.format(filter_txt)
            # search_terms = filter_txt.split(' ')

            # Search for term in variable names and units
            import fnmatch
            if fnmatch.filter(row_tuple, search_term):
                lst_varlist.setRowHidden(row, False)
            else:
                lst_varlist.setRowHidden(row, True)


def get_col_from_drp_pretty(col_list_pretty, col_dict_tuples, drp):
    """Get column name of selected variable"""
    selected_pretty = drp.currentText()
    selected_pretty_ix = col_list_pretty.index(selected_pretty)
    selected_col = col_dict_tuples[selected_pretty_ix]
    return selected_col
