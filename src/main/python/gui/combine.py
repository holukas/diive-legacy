from PyQt5 import QtCore as qtc

import gui.make as make


def frame_grid():
    frame = make.qframe()
    grid = make.qlayout_grid()
    frame.setLayout(grid)
    return frame, grid


def label_combobox(txt, max_visible_items, css_ids):
    label = make.qlabel(txt=txt, css_id=css_ids[0])

    # todo refactor
    label.setAlignment(qtc.Qt.AlignRight | qtc.Qt.AlignVCenter)

    dropdown = make.qcombobox(max_visible_items=max_visible_items, css_id=css_ids[1])
    return label, dropdown


def frame_header_subheader(header_txt: str = None, header_css_id: str = 'lbl_tab_settings_header1',
                           subheader_txt: str = None, subheader_css_id: str = 'lbl_tab_settings_subheader1'):
    frame = make.qframe()
    layout = make.qlayout_vert()
    frame.setLayout(layout)
    header = make.qlabel(txt=header_txt, css_id=header_css_id)
    layout.addWidget(header)
    subheader = make.qlabel(txt=subheader_txt, css_id=subheader_css_id)
    layout.addWidget(subheader)
    return frame


def header(header_txt: str, css_id: str = 'lbl_Header2'):
    header = make.qlabel(txt=header_txt, css_id=css_id)
    return header


def label_lineedit_horiz(txt: str, css_ids: list, **kwargs):
    label = make.qlabel(txt=txt, css_id=css_ids[0])
    label.setAlignment(qtc.Qt.AlignRight | qtc.Qt.AlignVCenter)
    lineedit = make.qlineedit(css_id=css_ids[1], **kwargs)
    lineedit.setAlignment(qtc.Qt.AlignLeft | qtc.Qt.AlignVCenter)
    return label, lineedit
